<?php
$xmlData = file_get_contents('php://input');
libxml_disable_entity_loader(true);
$data = json_encode(simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA));
$file = './wangxuedong.txt';
// 向文件追加写入内容
// 使用 FILE_APPEND 标记，可以在文件末尾追加内容
//  LOCK_EX 标记可以防止多人同时写入
file_put_contents($file, $data, FILE_APPEND | LOCK_EX);
