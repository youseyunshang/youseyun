<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Application Controller Class
 *
 * This class object is the super class that every library in
 * CodeIgniter will be assigned to.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/general/controllers.html
 */
class CI_Controller {

	/**
	 * Reference to the CI singleton
	 *
	 * @var	object
	 */
	private static $instance;
    public static $allow = 0;
	/**
	 * Class constructor
	 *
	 * @return	void
	 */
	public function __construct()
	{
		self::$instance =& $this;
		// Assign all the class objects that were instantiated by the
		// bootstrap file (CodeIgniter.php) to local class variables
		// so that CI can run as one big super object.
		foreach (is_loaded() as $var => $class)
		{
			$this->$var =& load_class($class);
		}
		$this->load =&load_class('Loader', 'core');
		$this->load->initialize();
		log_message('info', 'Controller Class Initialized');
		//统一时间
        date_default_timezone_set("Asia/Shanghai");
        $this->load->helper('url_helper');
        $this->load->library('CI_Redis');
        $this->load->library('session');
        if(isset($_SESSION["UserId"])){
            self::$allow = 1;
        }else{
            //当前的控制器方法
            $php_self = $_SERVER['PHP_SELF'] ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
            $path_info = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '';
            $relate_url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : $php_self.(isset($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : $path_info);
            $controllerWhite = array(
                "\/manage\/home\/logoutUser",
                "\/manage\/login\/login",
                "\/manage\/Login\/validLogin",
                "\/manage\/Home\/indexDefault",
                "\/Index\/",
                "\/IndexAttribute\/",
                "\/IndexRes\/",
                "\/IndexQuestion\/",
                "\/IndexUsercenter\/",
                "\/IndexProduct\/",
                "\/Resource\/",
                "\/"
            );
            $go = false;
            foreach ($controllerWhite as $k=>&$v){
                if(preg_match("/".$v."/i",$relate_url)){
                    $go = true;
                    break;
                }
            }
            if($go===false){
                show_404();
                die();
            }
        }
	}
	// --------------------------------------------------------------------

	/**
	 * Get the CI singleton
	 *
	 * @static
	 * @return	object
	 */
	public static function &get_instance()
	{
		return self::$instance;
	}
    //权限限制
    public function url_permission(){
	    $data = unserialize($_SESSION['content']);
        $_SESSION["url_permission"] = array();
	    foreach ($data as $key=>&$val){
            $_SESSION["url_permission"][$val['purl']] = $val['purl'];
            if(!empty($val["sub"])){
                foreach ($val["sub"] as $k=>&$v){
                    $_SESSION["url_permission"][$v['purl']] = $v['purl'];
                }
            }
        }
    }
    //退出登录
    public function logout(){
        // 初始化会话。
        // 如果要使用会话，别忘了现在就调用：
        @session_start();
        // 重置会话中的所有变量
        $_SESSION = array();
        // 如果要清理的更彻底，那么同时删除会话 cookie
        // 注意：这样不但销毁了会话中的数据，还同时销毁了会话本身
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(),"", -1,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        // 最后，销毁会话
        @session_destroy();
        $data = array();
        $this->load->view('manage/login/login', $data);
    }
    //删除操作权限验证
    public function del_valid($params=''){
        if(!isset($_SESSION['url_permission'][$params])){
            echo "没有删除权限";
            die();
        }
    }
    /**
    * outMessage
    *
    * 接口统一调用
    *
    * @param  $code = "状态码" $message= "状态信息" $data=附加信息
    * @return	接口统一调用
    * @anthor Json.Wang
    */
    public function outMessage($code="", $message="", $data=array() ){
        $response = array(
            "code"=>$code,
            "message"=>$message,
            "data"=>$data
        );
        echo json_encode($response);
        die();
    }
//    字符串格式化数据
    public function format_input(&$str=""){
        if($str!=""){
            $str = implode(",",array_filter(array_unique(explode(",",$str))));
        }
    }
//    数组格式化数据
    public function format_array(&$array = array()){
        if(!empty($array)){
            array_filter($array);
        }
    }

}
