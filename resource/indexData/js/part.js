//问答模块
var questionPart = {
    "noPicture":function(obj){
        var html='<div class="pannel-index">';
        html+='<a href="'+WINDOWS_INDEX_REQUEST+'/indexQuestion/questionInfo/'+obj.unqid+'">';
        html+='<div class="pannel-index-que-title"><p>'+obj.title+'</p></div>';
        if(obj.content){
            let queContent = obj.content.replace(/<\/?.+?>/g, "");
            if(queContent.length>50){
                queContent = queContent.substring(0,50)+"。。。";
            }
            html+='<div class="pannel-index-que-content"><p>'+queContent+'</p></div>';
        }else{
            html+='<div class="pannel-index-que-content"><p>暂无回答！</p></div>';
        }
        html+=questionPart.attrhtml(obj.attrId);
        html+=questionPart.questionUpdown(obj);
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        return html;
    },
    "onePicture":function(obj,photo){
        var html='<div class="pannel-index">';
        html+='<a href="'+WINDOWS_INDEX_REQUEST+'/indexQuestion/questionInfo/'+obj.unqid+'">';
        html+='<div class="pannel-index-que-title"><p>'+obj.title+'</p></div>';
        if(obj.createUserInfos){
            let userInfo = eval('('+obj.createUserInfos+')');
            html+='<div class="pannel-index-que-answer">';
            html+='<div class="pannel-index-que-answer-left">';
            html+='<div style="width: 100%;overflow: hidden;box-sizing: border-box;">';
            html+='<div style="width: 20%;height: 50px;float: left;display: flex;flex-direction: row;justify-content: left; align-items: center;">';
            html+='<img src="'+WINDOWS_URL_DEFINE+userInfo.userLogo+'" style="width: 80%;margin-top: 5px;border-radius: 80%;">';
            html+='</div>';
            html+='<div style="width: 60%;float: left;margin-top:5px;"><p style="margin: 0px;height: 25px;line-height: 28px;font-size: 0.3rem;">'+userInfo.realname+'</p><p style="font-size: 12px;margin: 0px;color: #999;">'+userInfo.comName+'</p></div>';
            html+='</div>';
            let queContent = obj.content.replace(/<\/?.+?>/g, "");
            if(queContent.length>50){
                queContent = queContent.substring(0,50)+"。。。";
            }
            html+='<div class="content"><p style="margin:0px;font-size: 16px;color: #666;">'+queContent+'</p></div>';
            html+='</div>';
            html+='<div class="pannel-index-que-answer-right"><img src="'+photo.coverLogoOne+'" style="width: 100%;height: 90px;margin-top: 20px;border-radius: 5px;"></div>';
            html+='</div>';
        }else{
            html+='<div class="pannel-index-que-answer">';
            html+='<p style="margin:0px;font-size: 12px;color: #666;">'+obj.content+'</p>';
            html+='</div>';
        }
        html+=questionPart.attrhtml(obj.attrId);
        html+=questionPart.questionUpdown(obj);
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        return html;
    },
    "threePicture":function(obj,photo){
        var html='<div class="pannel-index">';
        html+='<a href="'+WINDOWS_INDEX_REQUEST+'/indexQuestion/questionInfo/'+obj.unqid+'">';
        html+='<div class="pannel-index-que-title"><p>'+obj.title+'</p></div>';
        html+='<div class="pannel-index-que-content" style="overflow: hidden;">';
        html+='<img src="'+photo.coverLogoOne+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='<img src="'+photo.coverLogoTwo+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;margin:0px 2%;">';
        html+='<img src="'+photo.coverLogoThree+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='</div>';
        let queContent = obj.content.replace(/<\/?.+?>/g, "");
        if(queContent.length>50){
            queContent = queContent.substring(0,50)+"。。。";
        }
        html+='<div class="pannel-index-que-content"><p>'+queContent+'</p></div>';
        html+=questionPart.attrhtml(obj.attrId);
        html+=questionPart.questionUpdown(obj);
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        return html;
    },
    "attrhtml":function(obj){
        var html='';
        if(obj){
            let attrhtmlObj = eval('('+obj+')');
            if(attrhtmlObj.length>0){
                html+='<div class="pannel-index-que-attr">';
                $.each(attrhtmlObj,function(k,v){
                    if(k>3){
                        return false;
                    }
                    html+='<span >'+v.title+'</span>';
                });
                html+='</div>';
            }
        }
        return html;
    },
    "questionUpdown":function(obj){
        var html='';
        html+='<div class="pannel-index-que-updown">';
        if(obj.save&&obj.save>0){
            html+='<a href="javascript:;" qid="'+obj.unqid+'"><span class="foot-icon foot-icon-save"></span><span class="foot-icon-num" style="color: #0053fb;">'+(obj.savenum||0)+'</span></a>';
        }else{
            html+='<a href="javascript:;" onclick="questionPart.clickSave(this);" qid="'+obj.unqid+'"><span class="foot-icon foot-icon-nosave"></span><span class="foot-icon-num">'+(obj.savenum||0)+'</span></a>';
        }
        html+='<a href="'+WINDOWS_INDEX_REQUEST+'/indexQuestion/addAnswer/'+obj.unqid+'"><span class="foot-icon" style="background-position: 220px -90px;"></span><span class="foot-icon-num">'+obj.answermun+'</span></a>';
        html+='<a href="javascript:;" onclick="questionPart.clickLight(this);" qid="'+obj.unqid+'"><span class="foot-icon" style="background-position: 292px -230px;"></span><span class="foot-icon-num" light="'+obj.light+'">'+(obj.light>99?'99<sup>+</sup>':obj.light)+'</span></a>';
        html+='</div>';
        return html;
    },
    'clickLight':function(that){
        var lightNum = Number($(that).find('.foot-icon-num').attr('light'));
        $.ajax({
            url: WINDOWS_INDEX_REQUEST+"/IndexQuestion/light",
            type : "POST",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data : {"unqId":$(that).attr("qid"),"light":lightNum},
            dataType : "json",
            success: function (response) {
                if(response.code=="200"){
                    $(that).find('.foot-icon').attr("style","background-position: 320px -230px;");
                    $(that).find('.foot-icon-num').attr("style","color: #0053fb;");
                    $(that).find('.foot-icon-num').attr("light",++lightNum);
                    if(lightNum<99){
                        $(that).find('.foot-icon-num').html(++lightNum);
                    }else{
                        $(that).find('.foot-icon-num').html('99<sup>+</sup>');
                    }
                }else{
                    alert(response.message);
                }
            }
        });
    },
    'clickSave':function(that){
        if(!$(that).find('.foot-icon').hasClass('foot-icon-save')){
            var num = Number($(that).find('.foot-icon-num').html());
            console.log(num);
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/IndexQuestion/collection",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : {"unqId":$(that).attr("qid"),"num":num},
                dataType : "json",
                success: function (response) {
                    if(response.code=="200"){
                        $(that).find('.foot-icon').removeClass("foot-icon-nosave");
                        $(that).find('.foot-icon').addClass("foot-icon-save");
                        $(that).find('.foot-icon-num').attr("style","color: #0053fb;");
                        $(that).find('.foot-icon-num').html( ++num );
                    }else{
                        alert(response.message);
                    }
                }
            });
        }
    }
}
//资源模块
var resPart = {
    'resNoPicture':function (obj) {
        var signLog = obj.signLog!=''?eval('('+obj.signLog+')'):[],attrLog = obj.attrLog!=''?eval('('+obj.attrLog+')'):[],html='';
        html+='<div class="pannel-index flex-block">';
        html+='<a href="'+WINDOWS_INDEX_REQUEST+'/indexRes/resInfos/'+obj.Id+'" style="text-decoration: none;color: #000;">';
        if(obj.title){
            //html+='<div class="pannel-index-res-title"><p>'+obj.title.substring(0,20)+'</p></div>';
            html+='<div class="pannel-index-res-title"><p>'+obj.title+'</p></div>';
        }
        if(obj.notice){
            html+='<div class="pannel-index-res-content">';
            if(obj.notice.length>50){
                html+='<p>'+obj.notice.substring(0,50)+'。。。</p>';
            }else{
                html+='<p>'+obj.notice.substring(0,50)+'</p>';
            }
            html+='</div>';
        }
        html+='<div class="pannel-index-res-attr">';
        html+='<span >资讯</span>';
        if(signLog){
            $.each(signLog,function(sk,sv){
                if(sk>2){
                    return false;
                }
                html+='<span >'+sv.attrname+'</span>';
            });
        }
        if(attrLog){
            $.each(attrLog,function(sk,sv){
                if(sk>1){
                    return false;
                }
                html+='<span>'+sv.attrname+'</span>';
            });
        }
        html+='</div>';
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        $(".pannel-content").append(html);
    },
    'resBigPicture':function (obj) {
        var photo = eval('('+obj.photo+')'),
            signLog = obj.signLog!=''?eval('('+obj.signLog+')'):[],attrLog = obj.attrLog!=''?eval('('+obj.attrLog+')'):[],html='';
        html+='<div class="pannel-index flex-block">';
        html+='<a href="'+WINDOWS_INDEX_REQUEST+'/indexRes/resInfos/'+obj.Id+'" style="text-decoration: none;color: #000;">';
        if(obj.title){
            html+='<div class="pannel-index-res-title"><p>'+obj.title+'</p></div>';
        }
        html+='<div class="pannel-index-res-img">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoOne+'" style="">';
        html+='</div>';
        if(obj.notice&&obj.notice!=''){
            html+='<div class="pannel-index-res-content">';
            if(obj.notice.length>50){
                html+='<p>'+obj.notice.substring(0,50)+'。。。</p>';
            }else{
                html+='<p>'+obj.notice.substring(0,50)+'</p>';
            }
            html+='</div>';
        }
        html+='<div class="pannel-index-res-attr">';
        html+='<span >资讯</span>';
        if(signLog){
            $.each(signLog,function(sk,sv){
                if(sk>=2){
                    return false;
                }
                html+='<span >'+sv.attrname+'</span>';
            });
        }
        if(attrLog){
            $.each(attrLog,function(sk,sv){
                if(sk>1){
                    return false;
                }
                html+='<span>'+sv.attrname+'</span>';
            });
        }
        html+='</div>';
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        $(".pannel-content").append(html);
    },
    'resSmallPicture':function (obj) {

    },
    'resThreePicture':function (obj) {
        var photo = eval('('+obj.photo+')'),
            signLog = obj.signLog!=''?eval('('+obj.signLog+')'):[],attrLog = obj.attrLog!=''?eval('('+obj.attrLog+')'):[],html='';
        html+='<div class="pannel-index flex-block">';
        html+='<a href="'+WINDOWS_INDEX_REQUEST+'/indexRes/resInfos/'+obj.Id+'" style="text-decoration: none;color: #000;">';
        if(obj.title){
            html+='<div class="pannel-index-res-title"><p>'+obj.title+'</p></div>';
        }
        html+='<div class="pannel-index-res-content" style="overflow: hidden;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoOne+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoTwo+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;margin:0px 2%;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoThree+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='</div>';
        html+='<div class="pannel-index-res-attr">';
        html+='<span>资讯</span>';
        if(signLog){
            $.each(signLog,function(sk,sv){
                if(sk>1){
                    return false;
                }
                html+='<span>'+sv.attrname+'</span>';
            });
        }
        if(attrLog){
            $.each(attrLog,function(sk,sv){
                if(sk>1){
                    return false;
                }
                html+='<span>'+sv.attrname+'</span>';
            });
        }
        html+='</div>';
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        $(".pannel-content").append(html);
    },
}
//广告模块
var advertPart = {
    'advertBigPicture':function(obj){
        var photo = eval('('+obj.photo+')'),html='';
        html+='<div class="pannel-index">';
        html+='<a href="#" style="text-decoration: none;color: #000;">';
        if(obj.notice){
            html+='<div class="pannel-index-res-title"><p>'+obj.notice.substring(0,20)+'</p></div>';
        }
        html+='<div class="pannel-index-res-content" style="overflow: hidden;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoOne+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoTwo+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;margin:0px 2%;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoThree+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='</div>';
        html+='<div class="pannel-index-que-attr">';
        html+='<span style="background: #fff;border: 1px solid #1868f5;">广告</span>';
        html+='</div>';
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        $(".pannel-content").append(html);
    },
    'advertSmallPicture':function(obj){
    },
    'advertThreePicture':function(obj){
        var photo = eval('('+obj.photo+')'),html='';
        //三图广告
        html+='<div class="pannel-index">';
        html+='<a href="#" style="text-decoration: none;color: #000;">';
        if(obj.notice){
            html+='<div class="pannel-index-res-title"><p>'+obj.notice.substring(0,20)+'</p></div>';
        }
        html+='<div class="pannel-index-res-content" style="overflow: hidden;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoOne+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoTwo+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;margin:0px 2%;">';
        html+='<img src="'+WINDOWS_URL_DEFINE+'/resource/uploads/'+photo.coverLogoThree+'" style="display: block;width:32%;border-radius:5px;height:80px;float: left;">';
        html+='</div>';
        html+='<div class="pannel-index-que-attr">';
        html+='<span style="background: #fff;border: 1px solid #1868f5;">广告</span>';
        html+='</div>';
        html+='<div class="pannel-index-underline">';
        html+='</div>';
        html+='</a>';
        html+='</div>';
        $(".pannel-content").append(html);
    }
}