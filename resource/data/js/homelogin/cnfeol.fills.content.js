/*!
*a&d fills v1.1
*2017-06-16 13:51:20
*/

var AdvHandler = {
    imgSiteUrl: "https://img.cnfeol.com/",
    init: function () {
        var scripts = jQuery("[name=cnfeol_fills_custom]");
        var list = new Array();
        var callbacks = new Array();
        var pars = new Array();
        for (var i = 0, len = scripts.length; i < len; i++) {
            var keys = scripts[i].getAttribute("data-loca");
            var initMethod = scripts[i].getAttribute("data-init");
            list.push(keys);
            callbacks.push(initMethod);
            pars.push(scripts[i].parentNode);
        }
        AdvHandler.cnfeolFillsMult(list.toString(), pars, callbacks);
    },
    cnfeolFillsMult: function (parameters, parNode, parCallbacks) {
        var url = AdvHandler.imgSiteUrl + "api/actions/getcode";
        var ieurl = url + "?code=" + parameters + "&v=" + Date.parse(new Date());
        var iejson;
        var datas = { code: parameters };
        if ('XDomainRequest' in window && window.XDomainRequest !== null) {
            var xdr = new XDomainRequest();
            xdr.open('get', ieurl);
            xdr.onprogress = function () { };
            xdr.ontimeout = function () { };
            xdr.onerror = function () { };
            xdr.onload = function () {
                iejson = jQuery.parseJSON(xdr.responseText);
                if (iejson == null || typeof (iejson) == 'undefined') {
                    iejson = jQuery.parseJSON(data.firstChild.textContent);
                }
                var d = iejson;
                if (d != null && d.length > 0) {
                    for (var i = 0; i < d.length; i++) {
                        if (d[i] == null || d[i] == "") continue;
                        var html = parNode[i].innerHTML;
                        parNode[i].innerHTML = html + d[i];
                        eval(parCallbacks[i]);
                    }
                }
                AdvHandler.clickBak();
            };
            xdr.send();
        }
        else if (navigator.userAgent.indexOf('MSIE') != -1 &&
                 parseInt(navigator.userAgent.match(/MSIE ([\d.]+)/)[1], 10) < 8) {
            return false;
        }
        else {
            jQuery.ajax({
                type: "get",
                url: url,
                data: datas, async: true, //dataType:"text",
                success: function (d) {
                    if (d != null && d.length > 0) {
                        for (var i = 0; i < d.length; i++) {
                            if (d[i] == null || d[i] == "") continue;
                            var html = parNode[i].innerHTML;
                            parNode[i].innerHTML = html + d[i];
                            eval(parCallbacks[i]);
                        }
                    }
                    AdvHandler.clickBak();
                }
            });
        }
    },
    clickBak: function () {
        jQuery("a[data-click=remote]").on("click", function () {
            var click = jQuery(this).attr("data-click");
            if (click == null) return;
            var url = jQuery(this).attr("data-url");
            if (!url) return false;
            var series = jQuery(this).attr("data-series");
            jQuery(this).attr("href", AdvHandler.imgSiteUrl + "api/actions/clickbak/?series=" + series + "&redict=" + url);
        });
    }
};
window.onload = jQuery(AdvHandler.init());
