/*!
*a&d fills v1.1
*2015-10-13 10:47:20
*/
var imgSiteUrl = "https://img.cnfeol.com/";
loadExtentFile(imgSiteUrl + "content/remote/cnfeol_base.all.css?v=20161", "css");
window.onload = jQuery(function () {
    var scripts = jQuery("[name=cnfeol_fills]");
    var list = new Array();
    var pars = new Array();
    for (var i = 0, len = scripts.length; i < len; i++) {
        var keys = scripts[i].getAttribute("data-loca");
        list.push(keys);
        pars.push(scripts[i].parentNode);
    }
    cnfeolFillsMult(list.toString(), pars);
});
function cnfeolFillsMult(parameters, parNode) {
    var url = imgSiteUrl + "api/actions/getcode";
    var ieurl = url + "?code=" + parameters + "&v=" + Date.parse(new Date());
    var iejson;
    var datas = { code: parameters };
    if ('XDomainRequest' in window && window.XDomainRequest !== null) {
        var xdr = new XDomainRequest();
        xdr.open('get', ieurl);
        xdr.onprogress = function () { };
        xdr.ontimeout = function () { };
        xdr.onerror = function () { };
        xdr.onload = function () {
            iejson = jQuery.parseJSON(xdr.responseText);
            if (iejson == null || typeof (iejson) == 'undefined') {
                iejson = jQuery.parseJSON(data.firstChild.textContent);
            }
            var d = iejson;
            if (d != null && d.length > 0) {
                for (var i = 0; i < d.length; i++) {
                    if (d[i] == null || d[i] == "") continue;
                    var html = parNode[i].innerHTML;
                    parNode[i].innerHTML = html + d[i];
                }
                cnfeol_plays();
                showTheFirst();
                showCouplet();
                enslide();
                clickBak();
            }
        };
        xdr.send();
    }
    else if (navigator.userAgent.indexOf('MSIE') != -1 &&
             parseInt(navigator.userAgent.match(/MSIE ([\d.]+)/)[1], 10) < 8) {
        return false;
    }
    else {
        jQuery.ajax({
            type: "get",
            url: url,
            data: datas, async: true, //dataType:"text",
            success: function (d) {
                if (d != null && d.length > 0) {
                    for (var i = 0; i < d.length; i++) {
                        if (d[i] == null || d[i] == "") continue;
                        var html = parNode[i].innerHTML;
                        parNode[i].innerHTML = html + d[i];
                    }
                    cnfeol_plays();
                    showTheFirst();
                    showCouplet();
                    enslide();
                    clickBak();
                }
            }
        });
    }
}
function successcall(d, parNode) {
    if (d != null && d.length > 0) {
        for (var i = 0; i < d.length; i++) {
            if (d[i] == null || d[i] == "") continue;
            var html = parNode[i].innerHTML;
            parNode[i].innerHTML = html + d[i];
        }
        cnfeol_plays();
        showTheFirst();
        showCouplet();
        enslide();
        clickBak();
    }
}
function crossDomainAjax(url, successCallback) {
    if ('XDomainRequest' in window && window.XDomainRequest !== null) {
        var xdr = new XDomainRequest(); // Use Microsoft XDR
        xdr.open('get', url);
        xdr.onload = function () {
            var dom = new ActiveXObject('Microsoft.XMLDOM'),
                JSON = jQuery.parseJSON(xdr.responseText);
            dom.async = false;
            if (JSON == null || typeof (JSON) == 'undefined') {
                JSON = $.parseJSON(data.firstChild.textContent);
            }
            successCallback(JSON); // internal function
        };
        xdr.onerror = function () {
            _result = false;
        };
        xdr.send();
    }
    else if (navigator.userAgent.indexOf('MSIE') != -1 &&
             parseInt(navigator.userAgent.match(/MSIE ([\d.]+)/)[1], 10) < 8) {
        return false;
    }
    else {
        $.ajax({
            url: url,
            cache: false,
            dataType: 'json',
            type: 'GET',
            async: false, // must be set to false
            success: function (data, success) {
                successCallback(data);
            }
        });
    }
}
function clickBak() {
    jQuery("a[data-click=remote]").on("click", function () {
        var click = jQuery(this).attr("data-click");
        if (click == null) return;
        var url = jQuery(this).attr("data-url");
        if (!url) return false;
        var series = jQuery(this).attr("data-series");
        jQuery(this).attr("href", imgSiteUrl + "api/actions/clickbak/?series=" + series + "&redict=" + url);
    });
}
function loadExtentFile(filePath, fileType) {
    if (fileType == "js") {
        var oJs = document.createElement('script');
        oJs.setAttribute("type", "text/javascript");
        oJs.setAttribute("src", filePath);
        document.getElementsByTagName("head")[0].appendChild(oJs);
    } else if (fileType == "css") {
        var oCss = document.createElement("link");
        oCss.setAttribute("rel", "stylesheet");
        oCss.setAttribute("type", "text/css");
        oCss.setAttribute("href", filePath);
        document.getElementsByTagName("head")[0].appendChild(oCss);
    }
}
function pres(para) {
    var pre = jQuery(para).prev("ul");
    var lis = jQuery(pre).children("li");
    var cur = jQuery(pre).children("li[cur=1]");
    if (cur.length == 0) cur = lis[0];
    var len = lis.length - 1;
    var curindex = jQuery(cur).index();
    var index = jQuery(cur).index() - 1;
    if (curindex == 0) index = len;
    jQuery(cur).attr("cur", "0").hide();
    jQuery(lis).eq(index).attr("cur", "1").fadeIn(Math.random() * 1000);
}
function nexts(para) {
    var pre = jQuery(para).prevAll("ul");
    var lis = jQuery(pre).children("li");
    var cur = jQuery(pre).children("li[cur=1]");
    if (cur.length == 0) cur = lis[0];
    var len = lis.length - 1;
    var curindex = jQuery(cur).index();
    var index = jQuery(cur).index() + 1;
    if (curindex == len) index = 0;
    jQuery(cur).attr("cur", "0").hide();
    jQuery(lis).eq(index).attr("cur", "1").fadeIn(Math.random() * 1000);
}
function showTheFirst() {
    jQuery(".cnfelo_click_slide ul").each(function () {
        var lis = jQuery(this).children("li");
        lis.eq(0).attr("cur", "1").show();
        if (lis.length == 1) {
            jQuery(this).nextAll("a").hide();
        }
    });
    setInterval("autoSlides()", 6000);
}
function autoSlides() {
    jQuery(".cnfelo_click_slide ul").each(function () {
        var child = jQuery(this).children("li");
        var curindex = jQuery(this).children("li[cur=1]").index();
        var len = child.length - 1;
        if (len == 0) return;
        var index = curindex + 1;
        if (curindex == len) index = 0;
        jQuery(child).eq(curindex).attr("cur", "0").hide();
        jQuery(child).eq(index).attr("cur", "1").fadeIn(Math.random() * 1000);
    });
}
function cnfeol_plays() {
    jQuery(".cnfeol_plays").each(function () {
        var item = jQuery(this);
        //item.slideToggle(500);
        autoClosePlays(item);
    });
}
var t;
var spread = true;
function autoClosePlays(item) {
    t = setTimeout(function () {
        jQuery(item).addClass("cnfeol_plays_min");
        jQuery(".cnfeol_plays_span").addClass("cnfeol_plays_span_min");
        spreadPlays();
        return false;
    }, 6000);
}
function spreadPlays() {
    jQuery(".cnfeol_plays_min").on("click", function () {
        jQuery(this).find("span").removeClass("cnfeol_plays_span_min");
        jQuery(this).removeClass("cnfeol_plays_min").addClass("cnfeol_plays");
    });
}
function clickClosePlays() {
    clearTimeout(t);
    var item = jQuery(".cnfeol_plays");
    setTimeout(function () {
        item.removeClass("cnfeol_plays").addClass("cnfeol_plays_min");
        jQuery(".cnfeol_plays_span").addClass("cnfeol_plays_span_min");
        spreadPlays();
        return false;
    }, 0);
}
function showCouplet() {
    jQuery(".cnfeol_floats").each(function () {
        var ind = jQuery(this).index(jQuery(".cnfeol_floats"));
        var posi = ind % 2 == 0 ? true : false;
        posi == true ? jQuery(this).addClass("cnfeol_floats_left") : jQuery(this).addClass("cnfeol_floats_right");
    });
    closeCouplet();
}
function closeCouplet() {
    jQuery(".cnfeol_floats").delegate("p", "click", function () {
        jQuery(".cnfeol_floats").remove();
    });
}
//enslide
//enslide
var slidet = [];
var slideout = [];

function enslide() {
    jQuery(".cnfelo_click_slide").each(function () {
        var count = jQuery(this).find("li").length;
        jQuery(this).find("li").eq(0).addClass("curlion");
        if (count <= 1) return true;
        for (var l = 0; l < count; l++) {
            var s;
            if (l == 0)
                s = "<a style='width:6px;height:6px;margin-right:5px;' class='slidepa curon' onclick='curclick(this)'>" + (l + 1) + "</a>";
            else
                s = "<a style='width:6px;height:6px;margin-right:5px;' class='slidepa' onclick='curclick(this)'>" + (l + 1) + "</a>";
            jQuery(s).appendTo(jQuery(this).find("p"));
        }
        slidestart(count, $(".cnfelo_click_slide").index($(this)), $(this));
    });
}
function curclick(d) {
    var item = parseInt(d.text) - 1;
    var ddd = jQuery(d).closest(".cnfelo_click_slide");
    var lis = jQuery(ddd).find("li");
    var dd = $(".cnfelo_click_slide").index($(ddd));
    clearInterval(slidet[dd]);
    clearTimeout(slideout[dd]);
    cnfeloclickslide(lis.length, item, $(ddd));
    //slideout[dd] = setTimeout(function () { slidestart(lis.length, item, $(ddd)) }, 10000);
    cnfeol_plays();
};
function slidestart(c, i, w) {
    slidet[i] = setInterval(function () { cnfeloclickslide(c, null, w) }, 6000);
}
function cnfeloclickslide(c, i, w) {
    if (i == null)
        i = $(w).find("li.curlion").index() + 1; //jQuery(".cnfelo_click_slide li.curlion").index() + 1;
    if (i >= c) i = 0;
    var lis = $(w).find("li");
    lis.removeClass("curlion");
    lis.eq(i).addClass("curlion");
    lis.attr("style","display: none;");
    lis.eq(i).attr("style","display: list-item;");
    lis.attr("cur",0);
    lis.eq(i).attr("cur",1)
    var ps = $(w).find("p").find("a");
    ps.removeClass("curon");
    ps.eq(i).addClass("curon");
}
function GetRandomNum(Min, Max) {
    var Range = Max - Min;
    var Rand = Math.random();
    return (Min + Math.round(Rand * Range));
}