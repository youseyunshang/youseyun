﻿//登录会员弹出层发布站内信
//id为收件人ID
//status分为
//入驻会员之间站内信 = 1,
//入驻会员发给管理员站内信 = 2,
//管理员发给入驻会员站内信 = 3,
//管理员之间站内信 = 4
function showMailBox(id, status, type, title,entityId) {
    $.post("/Member/MailBox/GetName", { SendeeId: id, type: status }, function (data) {
        if (data != null && data != "") {
            var html = '<div><input type="hidden" name="SendeeId" id="mailSendeeId" value="' + id + '" /><table  style="width:476px;height:205px;"><tr><td width="65px" style="text-align:right"><span style="color: #d70c19;vertical-align: middle;">*</span><span style="color: #282828">收件人:</span></td><td><input type="text" readonly="readonly" style="width:333px;height: 25px;border-radius: 5px;border: 1px solid rgb(198,198,198);padding: 5px;" name="" id="mailName" placeholder="" value="' + data + '" /></td></tr><tr><td style="text-align:right"><span style="color: #d70c19;vertical-align: middle;">*</span><span style="color: #282828">标题:</span></td><td><input style="width:333px;height: 25px;border-radius: 5px;border: 1px solid rgb(198,198,198);padding: 5px;" type="text" name="Title" id="mailTitle" placeholder="请输入标题" maxlength="100" value="" /></td></tr><tr><td style="text-align:right"><span style="color: #d70c19;vertical-align: middle;">*</span><span style="color: #282828">内容:</span></td><td><textarea rows="5" style="width:335px;border-radius: 5px;border: 1px solid rgb(198,198,198);padding: 5px;" name="Content" id="mailContent" placeholder="请输入内容"></textarea></td></tr></table></div>';
            layer.open({
                status: 1,
                title: title,
                area: ['475px', '350px'], //宽高
                content: html,
                btnAlign: 'c',
                btn: ['发送'] //按钮
                , yes: function () {
                    var sendeeId = $("#mailSendeeId").val();
                    var title = $("#mailTitle").val();
                    var content = $("#mailContent").val();
                    if (title == "") {
                        layer.tips("请输入标题", '#mailTitle', {
                            tips: [1, '#666666'] //还可配置颜色
                        });
                    } else if (content == "") {
                        layer.tips("请填写正文内容", '#mailContent', {
                            tips: [1, '#666666'] //还可配置颜色
                        });
                    } else {
                        $.post("/Member/MailBox/SaveMail", { SendeeId: sendeeId, Title: title, Content: content, Status: status, MailType: type, entityID: entityId, isReply: false }, function (data) {
                            if (data.success) {
                                layer.closeAll();
                                layer.msg('发送成功', { icon: 1 });
                            } else {
                                if (data.data == 1) {
                                    layer.tips(data.message, '#mailName', {
                                        tips: [1, '#666666'] //还可配置颜色
                                    });
                                } else if (data.data == 2) {
                                    layer.tips(data.message, '#mailTitle', {
                                        tips: [1, '#666666'] //还可配置颜色
                                    });
                                } else if (data.data == 3) {
                                    layer.tips(data.message, '#mailContent', {
                                        tips: [1, '#666666'] //还可配置颜色
                                    });
                                } else {
                                    layer.tips(data.message, '#mailName', {
                                        tips: [1, '#666666'] //还可配置颜色
                                    });
                                }

                            }
                        });
                    }
                }
            });
        } else {
            layer.alert("请选择收件人！", {
                skin: 'layui-layer-molv' //样式类名
 , closeBtn: 0
            });
        }
    });
}

//游客发送站内信
function showVisitorMailBox(id, status, type, title) {
    var html = '<div><input type="hidden" name="SendeeId" id="mailSendeeId" value="' + id + '" /><table  style="width:476px;height:205px;"><tr><td width="70px" style="text-align:right"><span>联系电话:</span></td><td><input type="tel" name="" id="mailTel" style="width:373px" maxlength="11" placeholder="请填写您的电话，方便卖家及时联系到您" value="" /></td></tr><tr><td style="text-align:right"><span>标题:</span></td><td><input style="width:373px" type="text" name="Title" id="mailTitle" placeholder="请输入标题" maxlength="100" value="" /></td></tr><tr><td style="text-align:right"><span>内容:</span></td><td><textarea rows="5" style="width:375px" name="Content" id="mailContent" placeholder="请输入内容"></textarea></td></tr></table></div>';
    layer.open({
        status: 1,
        title: title,
        skin: 'layui-layer-rim', //加上边框
        area: ['500px', '350px'], //宽高
        content: html,
        btn: ['发送'] //按钮
        , yes: function () {
            var sendeeId = $("#mailSendeeId").val();
            var title = $("#mailTitle").val();
            var content = $("#mailContent").val();
            var tel = $("#mailTel").val();
            $.post("/Default/SaveMail", { SendeeId: sendeeId, Title: title, Content: content, Status: status, Telephone: tel, MailType: type }, function (data) {
                if (data.success) {
                    layer.closeAll();
                    layer.msg('发送成功，请等待会员与你联系', { icon: 1 });
                } else {
                    if (data.data == 1) {
                        layer.tips(data.message, '#mailTel', {
                            tips: [1, '#666666'] //还可配置颜色
                        });
                    } else if (data.data == 2) {
                        layer.tips(data.message, '#mailTitle', {
                            tips: [1, '#666666'] //还可配置颜色
                        });
                    } else if (data.data == 3) {
                        layer.tips(data.message, '#mailContent', {
                            tips: [1, '#666666'] //还可配置颜色
                        });
                    } else {
                        layer.tips(data.message, '#mailTel', {
                            tips: [1, '#666666'] //还可配置颜色
                        });
                    }
                }
            });
        }
    });
}
//id为收件人ID
//type为收件人站内信类型
//title弹出层标题
//status分为
//入驻会员之间站内信 = 1,
//入驻会员发给管理员站内信 = 2,
//管理员发给入驻会员站内信 = 3,
//管理员之间站内信 = 4
function checkFun(id, status, type, title, entityId) {
    if (title != null && title != "") {

    } else {
        title = "发送站内信";
    }
    $.post("/Default/CheckIsLogin", { code: entityId }, function (data) {
        if (data.success) {//已登录
            //showMailBox(id, status, type, title, entityId);
            if (type == 6) {//询价
                window.location.href = "/member/mailbox/inquiryindex?code="+entityId;
            } else if (type == 4) {//报价
                window.location.href = "/member/mailbox/quoteindex?code=" + entityId;
            }
        } else {//未登录
            
            if (data.data == 1) {
                layer.alert("平台禁止自己给自己发送消息", {
                    skin: 'layui-layer-molv' //样式类名
                      , closeBtn: 0
                });
            } else {
                layer.confirm('要进行该操作，请先登录！', {
                    btn: ['立即登录', '取消'] //按钮
                }, function () {
                    window.location.href = "/Member/Logon/Index";
                }, function () {

                });
            }
            //showVisitorMailBox(id, status, type, title);
        }
    });
}

window.setTimeout(function () {
    $.post("/Default/CheckIsLogin", {}, function (data) {
        if (data.success) {//已登录
            $.post("/Member/MailBox/GetNewMail", {}, function (data) {
                    var jsonData = $.parseJSON(data);
                    if (data != null && jsonData != null && jsonData.length > 0) {
                        var html = '<div style="margin:5px;"><ul style="list-style:none">';
                        html = html + '<li style="margin: 10px 5px;">您有<span style="color:red">' + jsonData.length + '</span>条站内信未读，<a href="/Member/MailBox/Index">点击阅读</a></li></ul></div>';
                        layer.open({
                            title: '站内信提醒'
                            , content: html
                            , offset: 'rb'
                            , time: 6000
                            , shade: 0
                            , area: ['270px', '180px']
                            ,yes: function(index, layero){
                                layer.close(index); //如果设定了yes回调，需进行手工关闭
                                window.location.href = "/Member/MailBox/Index";
                        }
                        });
                    }
                });
        }
    });
},15000);
//向前翻页
function downPage(count) {
    var index = $("#nowPage").text() - 2;
    if (index < 0) {
        index = 0
    }
    $("#mail_" + index).siblings().css("display", "none");
    $("#endLi").css("display", "block");
    $("#mail_" + index).css("display", "block");
    if (parseInt(index) + 1 <= 0) {
        $("#nowPage").text(1);
    } else {
        $("#nowPage").text(parseInt(index) + 1);
    }
}
//向后翻页
function upPage(count) {
    var index = $("#nowPage").text();
    if (index > count - 1) {
        index = count - 1;
    }
    $("#mail_" + index).siblings().css("display", "none");
    $("#endLi").css("display", "block");
    $("#mail_" + index).css("display", "block");
    var temp = parseInt(index) + 1;
    if (temp > count) {
        $("#nowPage").text(count);
    } else {
        $("#nowPage").text(parseInt(index) + 1);
    }
}

// 跳转路径
function link(link) {
    document.location.href = link;
    event.stopPropagation();
}


function funSeach() {
    var txt = $("#searchlookup").val();
    var type = 0;//$("#selType").val();
    if (txt != null && txt.trim().length > 0) {
        location.href = "/search?kw=" + txt;
    } else {
        location.href = "/search";
    }
}

// 非主页列表
$('.nav_all').mouseenter(function () {
    $(this).children('.nav_box').css({ display: 'block' })
});

//回到顶部函数
function goTop() {
    $('html,body').animate({ 'scrollTop': 0 }, 300);
}