var browser = {
    versions: function () {
        var u = navigator.userAgent, app = navigator.appVersion;
        return {   //移动终端浏览器版本信息
            trident: u.indexOf('Trident') > -1, //IE内核
            presto: u.indexOf('Presto') > -1, //opera内核
            webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
            gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') == -1, //火狐内核
            mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
            ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
            android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android终端或uc浏览器
            iPhone: u.indexOf('iPhone') > -1, //是否为iPhone或者QQHD浏览器
            iPad: u.indexOf('iPad') > -1, //是否iPad
            webApp: u.indexOf('Safari') == -1 //是否web应该程序，没有头部与底部
        };
    }(),
    language: (navigator.browserLanguage || navigator.language).toLowerCase()
}
if (browser.versions.mobile) {//判断是否是移动设备打开。browser代码在下面
    var ua = navigator.userAgent.toLowerCase();//获取判断用的对象
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
        $(function(){
            var url = location.href.split('#').toString();//url不能写死
            $.ajax({
                type : "post",
                url : WINDOWS_INDEX_REQUEST+"/index/ajaxSignShare",
                dataType : "json",
                async : false,
                data:{url:url},
                success : function(data) {
                    wx.config({
                        debug: false,////生产环境需要关闭debug模式
                        appId: data.appid,//appId通过微信服务号后台查看
                        timestamp: data.timestamp,//生成签名的时间戳
                        nonceStr: data.nonceStr,//生成签名的随机字符串
                        signature: data.signature,//签名
                        jsApiList: [//需要调用的JS接口列表
                            'checkJsApi',//判断当前客户端版本是否支持指定JS接口
                            'onMenuShareTimeline',//分享给好友
                            'onMenuShareAppMessage'//分享到朋友圈
                        ]
                    });
                },
                error: function(xhr, status, error) {
                    alert(status);
                    alert(xhr.responseText);
                }
            });
        });
        wx.ready(function () {
            var link = window.location.href+"/?&";
            wx.onMenuShareAppMessage({
                title: '有色云商_'+$("title").html(),
                link: link,
                imgUrl: WINDOWS_URL_DEFINE+'/resource/indexData/indexDefault/shareIcon.png',// 自定义图标
                desc: '金属行业的今日头条，关注您想要的行业资讯与交易信息！', // 分享描述
                type: 'link', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () { // 用户确认分享后执行的回调函数

                },
                cancel: function () {// 用户取消分享后执行的回调函数

                }
            });
            wx.error(function (res) {
                alert(res.errMsg);
            });
        });
    }
    if (ua.match(/WeiBo/i) == "weibo") {
    }
    if (ua.match(/QQ/i) == "qq") {
    }
    if (browser.versions.ios) {
    }
    if(browser.versions.android){
    }
} else {
}