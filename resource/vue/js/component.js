document.write('<script src="'+WINDOWS_URL_DEFINE+'/resource/vue/js/pca-code.js" type="text/javascript" charset="utf-8"></script>');
//  带logo的头部样式
Vue.component('headers',{
    data() {
        return {
            showMore: false
        }
    },
    template:   `
        <div class="header">
            <div class="logo" @click="indexDefault"></div>
            <div class="btns">
                <div class="btn searchs"></div>
                <div class="btn more" @click="showMore = !showMore"></div>
            </div>
            <div class="more-list" v-if="showMore" @click="showMore = false">
                <p @click="question">发布问题</p>
                <p @click="supple">发布供应</p>
                <p @click="purchase">发布求购</p>
            </div>
        </div>
        `,
    methods:{
        indexDefault(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/index/indexDefault';
        },
        question(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexQuestion/addQuestion';
        },
        supple(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductFirst';
        },
        purchase(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/getProductFirst';
        }
    }
})

//  带关闭的头部样式
Vue.component('tops',{
    props: {
        title: {
            type: String,
            default: '温馨提示'
        },
    },
    template:   `
        <div class="tops" @click="back">
            <div class="topBack"></div>
            <div class="title">{{title}}</div>
        </div>
        `,
    methods:{
        back() {
            window.history.go(-1)
        }
    }
})

// select组件 --主体部分
Vue.component('form-select',{
    props:{
        title: {
            type: String
        },
        placeholder: {
            type: String
        },
        checked: {
            type: Boolean,
            default: false
        },
        red: {
            type: Boolean,
            default: false
        },
        error: {
            type: Boolean
        }
    },
    template: `
        <div class="form-select" :class="{error: error}">
            <div class="form-title"><span :class="red ? 'red' : ''">{{title}}</span></div><div :class="{'form-content': true, 'form-checked': checked}">{{placeholder}}</div><div class="form-img"></div>
        </div>
    `,
    methods:{
        // searchVal(e){
        //     var that = this;
        //     that.$emit('sendSearch', e)
        // },
    }
})
// select组件 --主体部分
Vue.component('form-input',{
    props:{
        id: {
            type: String
        },
        title: {
            type: String
        },
        checked: {
            type: Boolean
        },
        placeholder: {
            type: String
        },
        red: {
            type: Boolean,
            default: false
        },
        value: {
            type: [String, Number]
        },
        max: {
            type: [Number, String],
            default: 666
        },
        error: {
            type: Boolean
        },
        disabled: {
            type: Boolean,
            default: false
        },
        type: {
            type: String,
            default: 'text'
        },
        unit: {
            type: [String, Boolean],
            default: false
        }
    },
    template: `
        <div class="form-input" :class="{error: error}">
            <div class="title">
			<span :class="red ? 'red' : ''">{{title}}</span>
			</div>
			<input :type="type" :id="id" :disabled="disabled"  :maxlength="max" class="content" :placeholder="placeholder" :value="value" @input="(e) => $emit('input', e.target.value.trim())"/>
            <span class="unit" v-if="unit">{{unit}}</span>
        </div>
    `,
    methods:{
        // searchVal(e){
        //     var that = this;
        //     that.$emit('sendSearch', e)
        // },
    }
})

// 下来列表主要部分组件
Vue.component('select-list',{
    props:{
        title: {
            type: String
        },
        list: {
            type: [Object, Array]
        }
    },
    data() {
        return {
            id: -1
        }
    },
    template: `
        <div class="select-list">
            <div class="title">{{title}}</div>
            <div class="list">
                <div v-for="i in list" :class="{choose: i.id === id}" @click="check(i)">{{i.name}}</div>
            </div>
            <div class="close" @click="close">取消</div>
        </div>
    `,
    methods:{
        check(i) {
            this.id = i.id;
            this.$emit('change', i);
            if(i.id == 0) {
                this.$emit('close',true);
            }
        },
        close() {
            this.$emit('close',false);
        },
        go(e) {
            window.location.href = e 
        }
    }
})
// 地区选择组件
Vue.component('check-area',{
    data() {
        return {
            provinceActive: true,
            cityActive: false,
            Province: false,
            provinceList: address,
            cityList: [],
            showP: true,
            showC: false,
            form: {
                provinceCode: '',
                cityCode: '',
                provinceName: '',
                cityName: '',
            }
        }
    },
    template: `
        <div class="check-area">
            <div class="title">地区选择</div>
            <div class="list1">
                <div class="left">
                    <div v-for="i in provinceList" :class="{choose: form.provinceCode == i.code}" @click="provinceSelected(i)">{{i.name}}</div>
                </div>
                <div class="right">
                    <div v-for="e in cityList" :class="{choose: form.cityCode == e.code}" @click="citySelected(e)">{{e.name}}</div>
                </div>
            </div>
            <div class="close" @click="close">取消</div>
        </div>
    `,
    created() {
    },
    methods:{
        close() {
            this.$emit('close',false);
        },
        provinceSelected(i) {
            this.provinceActive = true;
            this.cityActive = false;
            if(this.form.cityName) {
                this.Province = true;
            }
            this.showP = true;
            this.showC = false;
            if(i) {
                if(this.form.provinceCode !== i.code) {
                    this.form.cityName = '';
                }
                this.form.provinceCode = i.code;
                this.provinceActive = false;
                this.cityActive = true;
                this.Province = true;
                this.showP = false;
                this.showC = true;
                this.cityList = i.children;
                this.form.provinceName = i.name;
            }
        },
        citySelected(i) {
            this.provinceActive = false;
            this.cityActive = true;
            this.Province = true;
            this.showP = false;
            this.showC = true;
            if(i) {
                this.form.cityCode = i.code;
                this.provinceActive = false;
                this.cityActive = false;
                this.Province = true;
                this.showP = false;
                this.showC = false;
                this.form.cityName = i.name;
                this.$emit('change', this.form)
            }
        }
    }
})
// 单选组件
Vue.component('form-radio',{
    props:{
        id: {
            type: String
        },
        title: {
            type: String
        },
        checked: {
            type: [String, Number],
            default: "1"
        },
        red: {
            type: Boolean,
            default: false
        },
        list: {
            type: [Object, Array]
        }
    },
    mounted() {
        // console.log(this.checked)
    },
    data() {
        return {
            datas: this.checked
        }
    },
    template: `
        <div class="form-radio">
            <div class="title"><span :class="red ? 'red' : ''">{{title}}</span></div>
            <div class="content">
                <div class="radio" v-for="item in list">
                    <i><div :class="checked === item.id ? 'check' : ''"></div></i>
                    <label :class="checked === item.id ? '' : 'uncheck'">{{item.name}}</label>
                    <input @click="change()" type="radio" name="radio" :value="item.id" v-model="datas">
                </div>
            </div>
        </div>
    `,
    watch: {
        datas() {
            // this.checked = this.datas;
            this.$emit('change', this.datas);
            return ;
        }
    },
    methods: {
        change() {
            //this.$emit('change', this.checked);
        }
    }
})
//底部导航
Vue.component('footers',{
    props: {
        page: {
            type: String,
            default: 'home'
        }
    },
    template:   `
        <div class="footer">
            <div :class="{active: page === 'home'}" @click="taps('home')">
                <span class="home"></span>
                <p>首页</p>
            </div>
            <div :class="{active: page === 'shop'}" @click="taps('shop')">
                <span class="shop"></span>
                <p>商城</p>
            </div>
            <div :class="{active: page === 'car'}" @click="taps('car')">
                <span class="car"></span>
                <p>购物车</p>
            </div>
            <div :class="{active: page === 'my'}" @click="taps('my')">
                <span class="my"></span>
                <p>我的</p>
            </div>
        </div>
        `,
    methods:{
        taps(e) {
            this.page = e;
            console.log(this.page);
            switch (this.page) {
                case "home":
                    window.location.href=WINDOWS_INDEX_REQUEST+'/index/indexdefault';
                break;
                case "shop":
                    window.location.href=WINDOWS_INDEX_REQUEST+'/indexProduct/index';
                    break;
                case "car":
                    window.location.href=WINDOWS_INDEX_REQUEST+'/indexProduct/shopCar';
                    break;
                case "my":
                    window.location.href=WINDOWS_INDEX_REQUEST+'/indexUsercenter/index';
                    break;

            }
            // this.$emit('change', this.checked);
        }
    }
})
