// document.write('<script src="'+WINDOWS_URL_DEFINE+'/resource/vue/js/jquery-3.4.1.min.js" type="text/javascript" charset="utf-8"></script>');
document.write('<script src="'+WINDOWS_URL_DEFINE+'/resource/vue/js/vue.js" type="text/javascript" charset="utf-8"></script>');
document.write('<script src="'+WINDOWS_URL_DEFINE+'/resource/vue/js/component.js" type="text/javascript" charset="utf-8"></script>');

// 初始化 字号赋值
onReady(
    function(){
        document.documentElement.style.fontSize = document.documentElement.clientWidth / 7.5 + 'px'
    }
)
// 监听窗口变化 重新设置基础字号
window.onresize = function(){
    onReady(
        function(){
            document.documentElement.style.fontSize = document.documentElement.clientWidth / 7.5 + 'px'
        }
    )
}

// html 加载完设置字号
function onReady(fn){
    var readyState = document.readyState;
    if(readyState === 'interactive' || readyState === 'complete') {
        fn()
    }else{
        window.addEventListener("DOMContentLoaded",fn);
    }
}
