var app = new Vue({
    el: '#app',
    data: {
        showId: '',
        taps: 'supple',
        purchaseList: purchase,
        suppleList: supple
    },
    mounted(){

    },
    methods: {
        resClick(e) {
            this.taps = e;
        },
        update(e,type){
            console.log(e);
            var data = {
                "type":type,
                "Id":e.Id
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/updateCreateProduct',data)
                .then(function (response) {
                    if(response.data.code=="200"){
                        if(type=="supple"){
                            window.location.href = WINDOWS_INDEX_REQUEST+"/IndexProduct/createProductFirst";
                        }else{
                            window.location.href = WINDOWS_INDEX_REQUEST+"/IndexProduct/getProductFirst";
                        }
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        },
        del(e,type){

        },
        suppleSelected(e){
            $(".operate").hide();
            if($("div[suppleId='"+e+"']").attr("sonId")=="0"){
                $("div[suppleId='"+e+"']").attr("sonId","1");
                $("div[suppleId='"+e+"']").show();
            }else{
                $("div[suppleId='"+e+"']").attr("sonId","0");
                $("div[suppleId='"+e+"']").hide();
            }

        },
        purchaseSelected(e){
            $(".operate").hide();
            if($("div[purchaseId='"+e+"']").attr("sonId")=="0"){
                $("div[purchaseId='"+e+"']").attr("sonId","1");
                $("div[purchaseId='"+e+"']").show();
            }else{
                $("div[purchaseId='"+e+"']").attr("sonId","0");
                $("div[purchaseId='"+e+"']").hide();
            }
        }
    }
})

