var app = new Vue({
    el: '#app',
    data: {
        content: ''
    },
    methods: {
        submit(){
            var data = {
                'title':$('#title').html(),
                'content':$('#content').getValue(),
                'unqid':$('#unqid').html()
            }
            //判断数据！
            if(data.content.length<100){
                alert("内容过少，请多填写在发布！");
                return false;
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexQuestion/addAnswer',data)
                .then(function (response) {
                    if(response.data>0){
                        window.location.href=WINDOWS_INDEX_REQUEST+'/IndexQuestion/questionLists';
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        question(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexQuestion/questionLists';
        }
    }
})

