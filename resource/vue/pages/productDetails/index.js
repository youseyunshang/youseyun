var app = new Vue({
    el: '#app',
    data: {
        show: false,
        content: '',
        bannerList: swiperPhone||[],
        productData: data||[],
        recommend: forYouData||[],
        manager: shopManager||[]
    },
    mounted() {
        this.initSwiper()
    },
    methods: {
        initSwiper() {
            var mySwiper = new Swiper ('.swiper-container', {
                loop: true,
                pagination: {
                    el: '.swiper-pagination',
                    type: 'fraction'
                },
            });
            $("#forPhone").attr("href","tel:"+this.manager.tel);
        },
        offer(){
            window.location.href = WINDOWS_INDEX_REQUEST+'/indexProduct/offer/'+this.productData.Id;
        },
        insertShopCar(){
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/shopCar',this.productData)
                .then(function (response) {
                    if(response.data.code=="200"){
                        alert(response.data.message);
                        window.location.href=WINDOWS_INDEX_REQUEST+'/indexProduct/shopCar';
                    }else if(response.data.data.valid=="noLogin"){
                        window.location.href=WINDOWS_INDEX_REQUEST+'/index/login';
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        }
    }
})

