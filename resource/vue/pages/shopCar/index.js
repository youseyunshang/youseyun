var app = new Vue({
    el: '#app',
    data: {
        'edit':false,
        'shopCarList':shopCarData.length>0?shopCarData:false,
        'allMoney':0
    },
    mounted(){
        this.init();
    },
    methods: {
        init(){
            var formoney = 0;
            $.each(shopCarData,function(k,v){
                formoney+=v.minOrderNum*v.unitPrice;
            });
            this.allMoney = formoney;
        },
        goShop(){
            window.location.href = WINDOWS_INDEX_REQUEST+"/IndexProduct/productList?type=supple";
        },
        priceReduce(Id){
            var count = Number($("span[pid='"+Id+"']").parent().find(".count").html());
            $("span[pid='"+Id+"']").parent().find(".count").html(--count);
            if($("span[pid='"+Id+"']").hasClass("active")){
                //去掉勾选
                var formoney = this.allMoney,
                    onePrice = Number($("span[pid='"+Id+"']").parent().find(".onePrice").html());
                formoney -= onePrice;
                this.allMoney = formoney;
            }
        },
        priceAdd(Id){
            var count = Number($("span[pid='"+Id+"']").parent().find(".count").html());
            $("span[pid='"+Id+"']").parent().find(".count").html(++count);
            if($("span[pid='"+Id+"']").hasClass("active")){
                //去掉勾选
                var formoney = this.allMoney,
                    onePrice = Number($("span[pid='"+Id+"']").parent().find(".onePrice").html());
                formoney += onePrice;
                this.allMoney = formoney;
            }
        },
        checkbox(Id){
            var formoney = this.allMoney,
                onePrice = Number($("span[pid='"+Id+"']").parent().find(".onePrice").html()),
                count = Number($("span[pid='"+Id+"']").parent().find(".count").html());
            if($("span[pid='"+Id+"']").hasClass("active")){
                //去掉勾选
                $("span[pid='"+Id+"']").removeClass("active");
                $("span[attrId='parent']").removeClass("active");
                formoney = formoney - onePrice*count;
            }else{
                //添加勾选
                $("span[pid='"+Id+"']").addClass("active");
                $("span[attrId='parent']").addClass("active");
                $.each($("span[attrId='son']"),function(k,v){
                    if(!$(v).hasClass("active")){
                        //去掉勾选
                        $("span[attrId='parent']").removeClass("active");
                        return false;
                    }
                });
                formoney = formoney + onePrice*count;
            }
            this.allMoney = formoney;
        },
        editItem(){
            if(!this.edit){
                this.edit = true;
                $("span[attrId='son']").removeClass("active");
            }else{
                this.edit = false;
                $("span[attrId='son']").addClass("active");
                $("span[attrId='parent']").addClass("active");
            }
        },
        Settlement(){
            var data = {
                "formoney": this.allMoney,
                "list": []
            };
            $.each($("span[attrId='son']"),function(k,v){
                if($(v).hasClass("active")){
                    //去掉勾选
                    data.list.push({"Id":$(v).attr("pid"),"onePrice":$(v).parent().find(".onePrice").html(),"count":$(v).parent().find(".count").html()})
                }
            });
            window.location.href = WINDOWS_URL_DEFINE+"/indexProduct/shopCarPay/"+this.allMoney;
            // axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/shopCarPay',data)
            //     .then((response)=> {
            //         if(response.data.code=="200"){
            //             // console.log(response.data);
            //             this.jsApiParameters = response.data.jsApiParameters;
            //             this.showPayParams = true;
            //             // alert("删除成功！");
            //             // window.location.href = WINDOWS_URL_DEFINE+"/indexProduct/shopCar";
            //         }
            //     })
            //     .catch(function (error) {
            //         this.publish = true;
            //         console.log(error);
            //     });
        },
        deleteShopCar(){
            var data = [];
            $.each($("span[attrId='son']"),function(k,v){
                if($(v).hasClass("active")){
                    //去掉勾选
                    data.push({"Id":$(v).attr("pid")})
                }
            });
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/delShopCarData',data)
                .then((response)=> {
                    if(response.data.code=="200"){
                        alert("删除成功！");
                        window.location.href = WINDOWS_URL_DEFINE+"/indexProduct/shopCar";
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });

        },
        hrefProductInfo(Id){
            window.location.href = WINDOWS_INDEX_REQUEST+"/IndexProduct/productInfo/"+Id;
        }
    }
})

