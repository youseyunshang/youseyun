var app = new Vue({
    el: '#app',
    data: {
        productName:preData.productName||'',
        productNotice:preData.productNotice||'',
        other:preData.other||'',
        num:preData.num||'',
        price:preData.price||'',
        types: '',
        Id:'',
        showTypes: false,
        typesList: [
            {
                id: 1,
                name: '常规采购（1个月）'
            },
            {
                id: 2,
                name: '紧急采购（3天）'
            }
        ],
        showGranularity:false,
        granularityTypeId:preData.granularityTypeId||"",
        granularityType:preData.granularityType||"",
        granularityList:granularityList||[],
		//牌号
        showbrandName:false,
        brandNameTypeId:preData.brandNameTypeId||"",
		brandNameType:preData.brandNameType||"",
        brandNameList:brandNameList||[],
        sendType: preData.sendType||1,
        sendTypeList: [
            {
                id: 1,
                name: '送到买家指定地点'
            },
            {
                id: 2,
                name: '上门自提'
            }
        ],
        require: preData.require||1,
        requireList: [
            {
                id: 1,
                name: '无发票'
            },
            {
                id: 2,
                name: '增值税发票'
            },
            {
                id: 3,
                name: '普通发票'
            }
        ],
        photo: '',
        showArea: false,
        form: {
            lawyer_province: '',
            lawyer_province_code: '',
            lawyer_city: '',
            lawyer_city_code: '',
            time: preData.endTime||''
        },
        area: preData.area||''
    },
    mounted(){
        this.init();
        this.Dates();
    },
    methods: {
        init(){
        },
        next(){
            var data = {
                productName:this.productName,
                productNotice:this.productNotice,
                other:this.other,
                num:this.num,
                price:this.price,
                area:this.area,
                sendType:this.sendType,
                require:this.require,
                brandNameType:this.brandNameType,
                brandNameTypeId:this.brandNameTypeId,
                granularityType:this.granularityType,
                granularityTypeId:this.granularityTypeId,
                endTime:$("#times").html()
            }
            if(data.productName==""){
                alert("产品名称不能为空！");
                return false;
            }
            if(data.num==""){
                alert("产品数量不能为空！");
                return false;
            }
            if(data.price==""){
                alert("单价不能为空！");
                return false;
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/getProductSecond',data)
                .then(function (response) {
                    if(response.data.code=="200"&&response.data.data){
                        window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/getProductFinish';
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        },
        pre(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/getProductFirst';
        },
        checkArea(e) {
            this.area = e.provinceName +"-"+ e.cityName;
            this.showArea = false;
            this.form.lawyer_province = e.provinceName;
            this.form.lawyer_province_code = e.provinceCode;
            this.form.lawyer_city = e.cityName;
            this.form.lawyer_city_code = e.cityCode;
        },
        async handleChange({ target: { files: [file] } }) {
            if (!file) {
                return;
            }
            //处理图片
        },
        Dates(e){
            var that = this;
            new moTimeCJJ({
                domId : "times",
                //minTime : "2010-10-22 23:22:11",
                // maxTime : "2030-10-22 23:22:11",
                value: that.form.time , // 2018-02-02 02:34:59
                type : "YMDHM" // ["YMDHMS","YMD","HMS"]
            })
        }
    }
})

