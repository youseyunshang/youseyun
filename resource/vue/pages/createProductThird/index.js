var app = new Vue({
    el: '#app',
    data: {
        types: '',
        showTypes: false,
        typesList: [
            {
                id: 1,
                name: '铁'
            },
            {
                id: 2,
                name: '铜'
            }
        ],
        sendType: 1,
        sendTypeList: [
            {
                id: 1,
                name: '送到买家指定地点'
            },
            {
                id: 2,
                name: '上门自提'
            }
        ],
        require: 1,
        requireList: [
            {
                id: 1,
                name: '无发票'
            },
            {
                id: 2,
                name: '增值税发票'
            },
            {
                id: 3,
                name: '普通发票'
            }
        ],
        qualityInspectionSheet:preData.qualityInspectionSheet||'',
        warehouseReceipt: preData.warehouseReceipt||'',
        datas: preData.hasqualitySheet||'1',
        reason:preData.reason||''
    },
    mounted(){
        this.init();
    },
    methods: {
        init(){
        },
        next(){
        var data = {
           "hasqualitySheet":this.datas,
           "qualityInspectionSheet":this.qualityInspectionSheet,
           "warehouseReceipt":this.warehouseReceipt,
            "reason":this.reason
        }
        if(data.hasqualitySheet=="1"){
            if(data.qualityInspectionSheet==""){
                alert("请上传自检单！");
                return false;
            }
        }else{
            if(data.reason==""){
                alert("请说明理由！");
                return false;
            }
        }
        // if(data.warehouseReceipt==""){
        //     alert("请上传入库单！");
        //     return false;
        // }
        axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductThird',data)
            .then(function (response) {
                if(response.data.code=="200"&&response.data.data){
                    window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductFinish';
                }
            })
            .catch(function (error) {
                this.publish = true;
                console.log(error);
            });
    },
    pre(){
        window.location.href = WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductSecond';
    },
    handleChange(e) {
        var $target = e.target || e.srcElement;
        var file = $target.files[0];
        var reader = new FileReader();
        reader.onload = (data) => {
            let res = data.target || data.srcElement;
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/preUpload',{"data":res.result})
                .then((response)=> {
                    if(response.data.code=="200"&&response.data.data){
                        if($target.id=='qualityInspectionSheet'){
                            this.qualityInspectionSheet = response.data.data.url;
                        }else{
                            this.warehouseReceipt = response.data.data.url;
                        }
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        };
        reader.readAsDataURL(file);
    },
    }
})

