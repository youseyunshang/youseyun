var app = new Vue({
    el: '#app',
    data: {
        vip: false,
        status:false,
        showJoin:true
    },
    mounted(){
        this.init();
        this.showJoin = userinfo?false:true;
    },
    created(){

    },
    methods: {
        goInfo() {
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/personalInfo'
        },
        collection() {
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/myCollection'
        },
        qanda() {
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/myQAndA'
        },
        hobby(){
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/myHobby'
        },
		order(){
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/myOrder'
        },
        indexDefault(){
            window.location.href = WINDOWS_INDEX_REQUEST+'/index/indexdefault'
        },
        companyValid(){
            if(this.status){
                window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/companyValid'
            }else{
                window.location.href = WINDOWS_INDEX_REQUEST+'/index/login'
            }
        },
        sandp(){
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/myProduct'
        },
        async init(){
            await axios.post(WINDOWS_INDEX_REQUEST+'/IndexUsercenter/ajaxPersonalInfo',{})
                .then( (response) => {
                    if(response.data&&response.data.grade>1){
                            this.vip = true;
                            this.status = true;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        accountSet(){
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/accountSet'
        }
    }
})

