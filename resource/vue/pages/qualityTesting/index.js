var app = new Vue({
    el: '#app',
    data: {
        comName:'',
        goodsName:'',
        goodsNum:'',
        detailLocation:'',
        person:'',
        tel:'',
        notice:'',
        qualityCom:companyData||[],
        types: '',
        showTypes: false,
        typesList: [
            {
                id: 1,
                name: '铁'
            },
            {
                id: 2,
                name: '铜'
            }
        ],
        sendType: 1,
        sendTypeList: [
            {
                id: 1,
                name: '送到买家指定地点'
            },
            {
                id: 2,
                name: '上门自提'
            }
        ],
        require: 1,
        requireList: [
            {
                id: 1,
                name: '无发票'
            },
            {
                id: 2,
                name: '增值税发票'
            },
            {
                id: 3,
                name: '普通发票'
            }
        ],
        photo: '',
        area: '',
        showArea: false,
        form: {
            lawyer_province: '',
            lawyer_province_code: '',
            lawyer_city: '',
            lawyer_city_code: '',
            time: '选择时间'
        },
        showMore: false
    },
    mounted(){
        this.Dates();
    },
    methods: {
        checkArea(e) {
            this.area = e.provinceName + e.cityName;
            this.showArea = false;
            this.form.lawyer_province = e.provinceName;
            this.form.lawyer_province_code = e.provinceCode;
            this.form.lawyer_city = e.cityName;
            this.form.lawyer_city_code = e.cityCode;
        },
        async handleChange({ target: { files: [file] } }){
            if (!file) {
                return;
            }
            //处理图片
        },
        Dates(e){
            var that = this;
            new moTimeCJJ({
               domId : "times", 
               //minTime : "2010-10-22 23:22:11",
              // maxTime : "2030-10-22 23:22:11",
               value: that.form.time , // 2018-02-02 02:34:59
               type : "YMDHM" // ["YMDHMS","YMD","HMS"]
            })
        },
        submit(){
            var data ={
                comName:this.comName,
                goodsName:this.goodsName,
                goodsNum:this.goodsNum,
                detailLocation:this.detailLocation,
                person:this.person,
                tel:this.tel,
                notice:this.notice,
                qualityComId:this.qualityCom.Id||0
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/qualityTesting',data)
                .then(function (response) {
                    if(response.data.code=="200"){
                        alert(response.data.message);
                        window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/index';
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        }
    }
})

