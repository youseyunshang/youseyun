var myCollectionObj = {
    "resData":null,
    "questionData":null,
    "init":function(){
        //图片报错
        document.addEventListener("error", function (e) {
            var elem = e.target;
            if (elem.tagName.toLowerCase() == 'img') {
                elem.src = WINDOWS_URL_DEFINE+"/resource/indexData/indexDefault/error.png";
            }
        }, true);
    }
}
myCollectionObj.init();
var app = new Vue({
    el: '#app',
    data: {
        aid: '',
        showId: '',
        informationList: [
            // {
            //     id: 0,
            //     title: '盛屯矿业:在印尼投建年产3.4万吨镍金属项目盛屯矿业高开？',
            //     source: '上海金属网',
            //     time: '1天前',
            //     type: 'oneBig',
            //     imgUrl: '../../images/img2.png',
            //     desc: '友山镍业拟在印度尼西亚纬达贝工业园投建年产3.4万吨镍金属量高冰镍项目,项目建设总投资为4.07亿美元...',
            //     label: ['资讯', '钛合金']
            // },
            // {
            //     id: 1,
            //     title: '盛屯矿业:在印尼投建年产3.4万吨镍金属项目盛屯矿业高开？',
            //     source: '上海金属网',
            //     time: '1天前',
            //     type: 'oneSmall',
            //     imgUrl: '../../images/img3.png',
            //     desc: '友山镍业拟在印度尼西亚纬达贝工业园投建年产3.4万吨镍金属量高冰镍项目,项目建设总投资为4.07亿美元...',
            //     label: ['资讯', '钛合金']
            // },
            // {
            //     id: 2,
            //     title: '盛屯矿业:在印尼投建年产3.4万吨镍金属项目盛屯矿业高开？',
            //     source: '上海金属网',
            //     time: '1天前',
            //     type: 'threeSmall',
            //     imgUrl: ['../../images/img4.png', '../../images/img5.png', '../../images/img6.png'],
            //     desc: '友山镍业拟在印度尼西亚纬达贝工业园投建年产3.4万吨镍金属量高冰镍项目,项目建设总投资为4.07亿美元...',
            //     label: ['资讯', '钛合金']
            // },
            // {
            //     id: 3,
            //     title: '盛屯矿业:在印尼投建年产3.4万吨镍金属项目盛屯矿业高开？',
            //     source: '上海金属网',
            //     time: '1天前',
            //     type: 'none',
            //     imgUrl: '',
            //     desc: '友山镍业拟在印度尼西亚纬达贝工业园投建年产3.4万吨镍金属量高冰镍项目,项目建设总投资为4.07亿美元...',
            //     label: ['日评', '宏观']
            // }
        ],
        taps: 'information',
        quotationList: [
            {
                id: 10,
                time: '8月13日',
                type: '锰系',
                children: [
                    {
                        qid: '',
                        title: 'SMM1#电解铜',
                        price: '46500-46590',
                        average: '46555',
                        time: '8-13',
                        float: '-5',
                        type: 'down'
                    },
                    {
                        title: 'SMM1#电解铜',
                        price: '46500-46590',
                        average: '46555',
                        time: '8-13',
                        float: '+5',
                        type: 'up'
                    },
                    {
                        title: 'SMM1#电解铜',
                        price: '46500-46590',
                        average: '46555',
                        time: '8-13',
                        float: '-5',
                        type: 'down'
                    }
                ]
            }
        ],
        showId1: '',
        QAndAList: [
            // {
            //     title: '盛屯矿业:在印尼投建年产3.4万吨镍金属项目盛屯矿业高开？',
            //     desc: '友山镍业拟在印度尼西亚纬达贝工业园投建年产3.4万吨镍金属量高冰镍项目,项目建设总投资为4.07亿美元...',
            //     num1: '1',
            //     num2: '2',
            //     num3: '3'
            // }
        ],
        showId2: ''
    },
    mounted(){
        let _this = this;
        $.ajax({
            url: WINDOWS_INDEX_REQUEST+"/IndexUsercenter/loadingData",
            type : "POST",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data : {},
            dataType : "text",
            success: function (response) {
                if(response.length>1){
                    var data = eval('('+response+')');
                    myCollectionObj.resData = data.myRes;
                    myCollectionObj.questionData = data.myquestion;
                    _this.resClick("information");
                }
            }
        });
    },
    created(){
    },
    methods: {
        resClick(tap){
            this.taps = tap;
            switch (this.taps) {
                case "information":
                    this.informationList = [];
                    for (var i=0;i<myCollectionObj.resData.length;i++){
                        var v = myCollectionObj.resData[i];
                        //金属属性
                        // let attrLog = eval("("+v.attrLog+")");
                        let signLog = v.signLog?eval("("+v.signLog+")"):"",signData=[],j=0;
                        for (j=0;j<signLog.length;j++){
                            if(j<4){
                                signData.push(signLog[j]['attrname']);
                            }
                        }
                        v.photoImg = eval("("+v.photo+")");
                        switch (v.photoType) {
                            case '0':
                                v.photoType = 'none';
                                v.imgUrl = '';
                                break;
                            case '1':
                                v.photoType = 'oneBig';
                                v.imgUrl = [];
                                v.imgUrl[0]=WINDOWS_URL_DEFINE+"/resource/uploads/"+v.photoImg['coverLogoOne'];
                                break;
                            case '2':
                                v.photoType = 'oneSmall';
                                v.imgUrl = [];
                                v.imgUrl[0]=WINDOWS_URL_DEFINE+"/resource/uploads/"+v.photoImg['coverLogoOne'];
                                break;
                            case '3':
                                v.photoType = 'threeSmall';
                                v.imgUrl = [];
                                v.imgUrl[0]=WINDOWS_URL_DEFINE+"/resource/uploads/"+v.photoImg['coverLogoOne'];
                                v.imgUrl[1]=WINDOWS_URL_DEFINE+"/resource/uploads/"+v.photoImg['coverLogoTwo'];
                                v.imgUrl[2]=WINDOWS_URL_DEFINE+"/resource/uploads/"+v.photoImg['coverLogoThree'];
                                break;
                        }
                        this.informationList.push({
                            id: v.Id,
                            title: v.title,
                            source: v.origin,
                            time: '',
                            type: v.photoType,
                            imgUrl: v.imgUrl,
                            desc: v.notice,
                            label: signData
                        });
                    }
                    break;
                case "quotation":
                    break;
                case "QAndA":
                    this.QAndAList = [];
                    for (var i=0;i<myCollectionObj.questionData.length;i++){
                        var v = myCollectionObj.questionData[i];
                        this.QAndAList.push({
                            qid: v.unqid,
                            title: v.title,
                            desc: v.content,
                            num1: '1',
                            num2: v.answermun,
                            num3: v.light
                        });
                    }
                    break;
            }
        },
        resInfos(that){
            if(that){
                window.location.href = WINDOWS_INDEX_REQUEST+'/indexRes/resInfos/'+that;
            }
        },
        questionInfos(that){
            if(that){
                window.location.href = WINDOWS_INDEX_REQUEST+'/indexQuestion/questionInfo/'+that;
            }
        }

    }
})

