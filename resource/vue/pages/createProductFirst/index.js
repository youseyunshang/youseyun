var app = new Vue({
    el: '#app',
    data: {
        //一级属性
        shopFirst: false,
        shopFirstName:'',
        shopFirstId:'',
        //二级属性
        shopSecondShow:false,
        shopSecond: false,
        shopSecondName:'',
        shopSecondId:'',
        //三级属性
        shopThirdShow:false,
        shopThird: false,
        shopThirdName:'',
        shopThirdId:'',
        //商品形态
        shopStatus: false,
        shopStatusName:'',
        shopStatusId:'',
        //企业类型
        comType: false,
        comTypeName: '',
        comTypeId: '',
        //一级列表
        shopFirstList:shopFirst,
        //二级列表
        shopSecondList:[],
        //三级列表
        shopThirdList:[],
        //形态列表
        shopStatusList:shopStatus,
        //企业类型
        comTypeList: [
            {
                id: 1,
                name: '生产商'
            },
            {
                id: 2,
                name: '贸易商'
            },
            {
                id: 3,
                name: '设备服务'
            },
            {
                id: 4,
                name: '钢厂'
            },
            {
                id: 5,
                name: '物流'
            }
        ],
    },
    mounted(){
        this.init();
    },
    methods: {
        init() {
            if(preData.attr){
                this.shopFirstName=preData.attr[0]['shopFirstName'];
                this.shopFirstId=preData.attr[0]['shopFirstId'];
                if(preData.attr[1]['shopSecondName']!=""){
                    this.shopSecondShow = true;
                }
                this.shopSecondName=preData.attr[1]['shopSecondName'];
                this.shopSecondId=preData.attr[1]['shopSecondId'];
                if(preData.attr[2]['shopThirdName']!=""){
                    this.shopThirdShow = true;
                }
                this.shopThirdName=preData.attr[2]['shopThirdName'];
                this.shopThirdId=preData.attr[2]['shopThirdId'];
                this.shopStatusName=preData.shopStatus[0]['shopStatusName'];
                this.shopStatusId=preData.shopStatus[0]['shopStatusId'];
                this.comTypeName=preData.comType[0]['comTypeName'];
                this.comTypeId=preData.comType[0]['comTypeId'];
            }
        },
        shopChange(id,type){
            if(id&&shopSon){
                var list = new Array();
                $.each(shopSon,function(k,v){
                    if(id==v.pid){
                        list.push(v);
                    }
                });
                if(list){
                    switch(type){
                        case 2:
                            this.shopSecondShow = true;
                            this.shopSecondList = list;

                            this.shopThirdShow = false;
                            this.shopThirdList = [];
                            break;
                        case 3:
                            this.shopThirdShow = true;
                            this.shopThirdList = list;
                            break;
                        case 4:
                            break;
                    }
                }
            }
        },
        next(){
            if(this.shopFirstName==''){
                alert("请选择产品系！");
                return false;
            }
            if(this.shopStatusName==''){
                alert("请选择产品形态！");
                return false;
            }
            if(this.comTypeName==''){
                alert("请选择企业类型！");
                return false;
            }
            var data = {
                'attr':[{
                    shopFirstName:this.shopFirstName,
                    shopFirstId:this.shopFirstId
                },{
                    shopSecondName:this.shopSecondName,
                    shopSecondId:this.shopSecondId
                },{
                    shopThirdName:this.shopThirdName,
                    shopThirdId:this.shopThirdId
                }],
                'shopStatus':[{
                    shopStatusName:this.shopStatusName,
                    shopStatusId:this.shopStatusId
                }],
                'comType':[{
                    comTypeName: this.comTypeName,
                    comTypeId: this.comTypeId
                }]
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductFirst',data)
                .then(function (response) {
                    if(response.data>0){
                        window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductSecond';
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        }
    }
})

