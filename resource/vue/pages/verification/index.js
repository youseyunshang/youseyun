var app = new Vue({
    el: '#app',
    data: {
        showNotice: true,
        IMG_URL: '',
        showType: false,
        showType1: false,
        typeList: [
            {
                id: 1,
                name: '生产商'
            },
            {
                id: 2,
                name: '贸易商'
            },
            {
                id: 3,
                name: '设备服务'
            },
            {
                id: 4,
                name: '钢厂'
            },
            {
                id: 5,
                name: '物流'
            },
            {
                id: 6,
                name: '工贸一体'
            },
            {
                id: 7,
                name: '其他'
            }
        ],
        originList: [
            {
                id: 1,
                name: '铬',
                children: [
                    {
                        id: 11,
                        name: '铬铁'
                    },
                    {
                        id: 12,
                        name: '铬矿'
                    },
                    {
                        id: 13,
                        name: '氮化铬铁'
                    },
                    {
                        id: 14,
                        name: '金属铬'
                    }
                ]
            },
            {
                id: 2,
                name: '钒',
                children: [
                    {
                        id: 21,
                        name: '钒铁'
                    },
                    {
                        id: 22,
                        name: '钒矿'
                    },
                    {
                        id: 23,
                        name: '氮化钒铁'
                    },
                    {
                        id: 24,
                        name: '金属钒'
                    }
                ]
            },
            {
                id: 3,
                name: '硼',
                children: [
                    {
                        id: 31,
                        name: '硼铁'
                    },
                    {
                        id: 32,
                        name: '硼矿'
                    },
                    {
                        id: 33,
                        name: '氮化硼铁'
                    },
                    {
                        id: 34,
                        name: '金属硼'
                    }
                ]
            },
            {
                id: 4,
                name: '镍',
                children: [
                    {
                        id: 41,
                        name: '镍铁'
                    },
                    {
                        id: 42,
                        name: '镍矿'
                    },
                    {
                        id: 43,
                        name: '氮化镍铁'
                    },
                    {
                        id: 44,
                        name: '金属镍'
                    }
                ]
            },
            {
                id: 5,
                name: '稀土',
                children: [
                    {
                        id: 51,
                        name: '稀土铁'
                    },
                    {
                        id: 52,
                        name: '稀土矿'
                    },
                    {
                        id: 53,
                        name: '氮化稀土铁'
                    },
                    {
                        id: 54,
                        name: '金属稀土'
                    }
                ]
            },
            {
                id: 6,
                name: '啥也不是',
                children: [
                    {
                        id: 61,
                        name: '啥也不是铁'
                    },
                    {
                        id: 62,
                        name: '啥也不是矿'
                    },
                    {
                        id: 63,
                        name: '氮化啥也不是铁'
                    },
                    {
                        id: 64,
                        name: '金属啥也不是'
                    }
                ]
            },
            {
                id: 7,
                name: '其他',
                children: [
                    {
                        id: 71,
                        name: '其他铁'
                    },
                    {
                        id: 72,
                        name: '其他矿'
                    },
                    {
                        id: 73,
                        name: '氮化其他铁'
                    },
                    {
                        id: 74,
                        name: '金属其他'
                    }
                ]
            }
        ],
        chooseId: '',
        chooseId1: '',
        chooseId2: '',
        childrenList: [],
        userInfo:{
            comLicense:'',
            comName:'',
            comContent: '',
            comPerson:'',
            comTel:'',
            comType:'',
            comLocation:''
        }
    },
    mounted(){
        this.init();
    },
    methods: {
        show1() {
            this.showType = true;
            $('#app').attr('style', 'overflow:hidden;position:fixed;height:100%;');
        },
        show2() {
            this.showType1 = true;
            $('#app').attr('style', 'overflow:hidden;position:fixed;height:100%;');
        },
        async init(){
            await axios.post(WINDOWS_INDEX_REQUEST+'/IndexUsercenter/ajaxPersonalInfo',{})
                .then( (response) => {
                    if(response.data){
                        this.userInfo.comName = response.data.comName;
                        this.userInfo.comContent = response.data.comContent;
                        this.userInfo.comPerson = response.data.comPerson;
                        this.userInfo.comTel = response.data.comTel;
                        this.userInfo.comLicense = response.data.comLicense?WINDOWS_URL_DEFINE+response.data.comLicense:"";
                        this.userInfo.comLocation = response.data.comLocation;
                        this.chooseId = response.data.comType;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        handleChange(e) {
            let $target = e.target || e.srcElement;
            let file = $target.files[0];
            var reader = new FileReader();
            reader.onload = (data) => {
                let res = data.target || data.srcElement;
                this.userInfo.comLicense = res.result;
            };
            reader.readAsDataURL(file);
        },
        chooseType(id) {
            this.chooseId = id;
        },
        chooseType1(e) {
            this.chooseId1 = e.id;
            this.childrenList = e.children;
        },
        chooseType2(id) {
            this.chooseId2 = id;
        },
        closeShow() {
            this.showType = false;
            this.showType1 = false;
            $('#app').removeAttr("style");
        },
        uploadPicture(){
            this.$el.querySelector('.hiddenInput').click();
        },
        submit(){
            this.userInfo.comType = this.chooseId;
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexUsercenter/updateComInfo',this.userInfo)
                .then(function (response) {
                    alert("修改成功！");
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
})

