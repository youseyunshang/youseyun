var app = new Vue({
    el: '#app',
    data: {
        firstAttr: [],
        typeList: [],
        weekList: weekData,
        statusList: shopStatus,
        page: 1,
        list: '',
        showMore: true,
        loading: false,
        checkedId: 0,
        menu: false,
        itemType: '',
        brandName: '',
        granularity: '',
        attrId: '',
        styleId: '',
        brandList: [],
        granularityList: []
    },
    mounted(){
        this.initSwiper();
        this.styleId = where.styleId;
        this.attrId = where.attrId;
        this.itemType = where.type;
        this.getData();
        this.firstAttr = [{id: 0, name: '全部类别'},...shopFirst];
        this.typeList = [{checkId: 0, list: this.firstAttr}];
    },
    methods: {
        submit() {
            this.checkedId = this.typeList[0].checkId;
            this.showMenu();
            this.page = 1;
            this.getData();
        },
        resetId() {
            this.attrId = '';
            this.typeList = [{checkId: 0, list: this.firstAttr}];
            this.brandName = '';
            this.styleId = '';
            this.granularity = '';
            this.brandList = [];
            this.granularityList = [];
        },
        attrChange(i) {
            this.resetId();
            this.attrId = i.id;
            this.checkId = i.id;
            this.page = 1;
            this.getData();
        },
        firstChange(index, i) {
            this.typeList[index].checkId = i.id;
            this.attrId = i.id;
            let newArr = []
            shopSon.map(e => {
                if(e.pid === i.id) {
                    newArr.push(e)
                }
            });
            this.typeList = this.typeList.slice(0, index + 1);
            this.typeList.splice(index + 1, 0, {checkId: 0, list: newArr});
            this.brandList = brandName[i.id] || [];
            this.granularityList = granularity[i.id] || [];
        },
        showMenu() {
            this.menu = !this.menu;
            if(this.menu) {
                var height = window.innerHeight / 100;
                $('#app').attr("style", "overflow:hidden;height:"+height+"rem");
            }else {
                $('#app').removeAttr("style");
            }
        },
        goDetails(type,id) {
            if(type === 'supple') {
                window.location.href = WINDOWS_INDEX_REQUEST+"/IndexProduct/productInfo/"+id;
            }else if(type === 'purchase') {

            }
        },
        getMore() {
            this.loading = true;
            this.page++;
            this.getData();
        },
        getData() {
            var data = {
                'page': this.page,
                'where': {
                    "itemType": this.itemType,
                    "brandName": this.brandName,
                    "granularity": this.granularity,
                    "attrId": this.attrId,
                    "styleId": this.styleId
                }
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/allShopData',data)
                .then((response) => {
                    this.loading = false
                    if(response.data.code === '200') {
                        if(this.page === 1) {
                            this.list = response.data.data
                        }else {
                            this.list.push(...response.data.data)
                        }
                    }else if(response.data.code === '404') {
                        this.showMore = false
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        initSwiper() {
            var mySwiper1 = new Swiper ('.swiper1', {
                loop: true
            })
            var mySwiper2 = new Swiper ('.swiper2', {
                direction : 'vertical',
                slidesPerView : 1,
                slidesPerGroup : 1,
                preventClicks : false,//默认true
                preventClicksPropagation : false,
                slideToClickedSlide: false,
                autoplay: {
                    delay: 3000,
                },
                loop:true
            })
        }
    }
})

