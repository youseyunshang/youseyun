var app = new Vue({
    el: '#app',
    data: {
        billHolder:'',
        tel:'',
        email:'',
        showBillHolder: true,
        check2: false
    },
    mounted(){
    },
    methods: {
        finish(){
            if(!this.check2){
                alert("请同意服务协议!");
                return false;
            }
            var data = {
                'billHolder':$("#billHolder").val(),
                'tel':$("#tel").val(),
                'email':$("#email").val(),
                'showBillHolder':this.showBillHolder
            }
            if(data.billHolder==""){
                alert("挂单人不能为空！");
                return false;
            }
            if(data.tel==""){
                alert("手机号不能为空！");
                return false;
            }
            if(!(/^1[3456789]\d{9}$/.test(data.tel))){
                alert("手机号码有误，请重填");
                return false;
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductFinish',data)
                .then(function (response) {
                    if(response.data.code=="200"&&response.data.data){
                        alert("发布成功，等待后台审核！");
                        window.location.href=WINDOWS_INDEX_REQUEST+'/indexProduct/index';
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        },
    }
})

