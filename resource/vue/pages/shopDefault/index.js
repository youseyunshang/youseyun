var app = new Vue({
    el: '#app',
    data: {
        todayList: todayPublish,
        weekList: weekData,
        newList: newSupple,
        page: 1,
        list: '',
        showMore: true,
        loading: false,
        select:true
    },
    mounted(){
        this.initSwiper();
        this.getData()
    },
    methods: {
        goDetails(type,id) {
            if(type === 'supple') {
                window.location.href = WINDOWS_INDEX_REQUEST+"/IndexProduct/productInfo/"+id;
            }else if(type === 'purchase') {
                window.location.href = WINDOWS_INDEX_REQUEST+"/IndexProduct/purchaseInfo/"+id;
            }
        },
        getMore() {
            this.loading = true;
            this.page++;
            this.getData();
        },
        getData() {
            var data = {
                'page': this.page
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/allShopData',data)
                .then((response) => {
                    this.loading = false
                    if(response.data.code === '200') {
                        if(this.page === 1) {
                            this.list = response.data.data
                        }else {
                            this.list.push(...response.data.data)
                        }
                    }else if(response.data.code === '404') {
                        this.showMore = false
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        initSwiper() {
            var mySwiper1 = new Swiper ('.swiper1', {
                direction : 'vertical',
                slidesPerView : 2,
                slidesPerGroup : 2,
                preventClicks : false,//默认true
                preventClicksPropagation : false,
                slideToClickedSlide: false,
                autoplay: {
                    delay: 3000,
                },
                loop:true
            })
            var mySwiper2 = new Swiper ('.swiper2', {
                direction : 'vertical',
                slidesPerView : 1,
                slidesPerGroup : 1,
                preventClicks : false,//默认true
                preventClicksPropagation : false,
                slideToClickedSlide: false,
                autoplay: {
                    delay: 3000,
                },
                loop:true
            })
        },
        publishInfo(type){
            var type = type||'supple';
            switch (type) {
                //采购
                case "purchase":
                    window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/getProductFirst';
                    break;
                //供应
                case "supple":
                    window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductFirst';
                    break;
                default:
                    window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductFirst';
                    break;
            }
        },
        productList(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/productList';
        },
        linkSupple(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/productList?type=supple';
        },
        linkPurchase(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/productList?type=purchase';
        },
        company(type){
            var type = type ||1;
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/companyList?type='+type;
        }
    }
})

