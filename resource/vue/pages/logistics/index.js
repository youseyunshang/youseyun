var app = new Vue({
    el: '#app',
    data: {
        showArea: false,
        form: {
            lawyer_province: '',
            lawyer_province_code: '',
            lawyer_city: '',
            lawyer_city_code: '',
            time: '选择时间',
            companyName: '',
            goodsName: '',
            loadAreaShow: false,
            loadArea: '',
            loadAreaDetails: '',
            goodsPack: '',
            goodsWeight: '',
            loadContactName: '',
            loadContactTel: '',
            unloadAreaShow: false,
            unloadArea: '',
            unloadAreaDetails: '',
            unloadContactName: '',
            unloadContactTel: '',
            remarks: ''
        },
        showMore: false,
        logisticsCom:companyData||[]
    },
    mounted(){
        this.Dates();
    },
    methods: {
        loadAreaChange(e) {
            this.form.loadArea = e.provinceName +"-"+ e.cityName;
            this.form.loadAreaShow = false;
            this.form.lawyer_province = e.provinceName;
            this.form.lawyer_province_code = e.provinceCode;
            this.form.lawyer_city = e.cityName;
            this.form.lawyer_city_code = e.cityCode;
        },
        unloadAreaChange(e){
            this.form.unloadArea = e.provinceName +"-"+ e.cityName;
            this.form.unloadAreaShow = false;
            this.form.lawyer_province = e.provinceName;
            this.form.lawyer_province_code = e.provinceCode;
            this.form.lawyer_city = e.cityName;
            this.form.lawyer_city_code = e.cityCode;
        },
        Dates(e){
            var that = this;
            new moTimeCJJ({
               domId : "times", 
               //minTime : "2010-10-22 23:22:11",
              // maxTime : "2030-10-22 23:22:11",
               value: that.form.time , // 2018-02-02 02:34:59
               type : "YMDHM" // ["YMDHMS","YMD","HMS"]
            })
        },
        submit() {
            var data = {
                "companyName":this.form.companyName,
                "times":this.form.times,
                "goodsName":this.form.goodsName,
                "goodsPack":this.form.goodsPack,
                "goodsWeight":this.form.goodsWeight,
                "loadArea":this.form.loadArea,
                "loadAreaDetails":this.form.loadAreaDetails,
                "loadContactName":this.form.loadContactName,
                "loadContactTel":this.form.loadContactTel,
                "unloadArea":this.form.unloadArea,
                "unloadAreaDetails":this.form.unloadAreaDetails,
                "unloadContactName":this.form.unloadContactName,
                "unloadContactTel":this.form.unloadContactTel,
                "remarks":this.form.remarks,
                "logisticsComId":this.logisticsCom.Id||0
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/logistics',data)
            .then(function (response) {
                if(response.data.code=="200"){
                    alert(response.data.message);
                    window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/index';
                }
            })
            .catch(function (error) {
                this.publish = true;
                console.log(error);
            });
        }
    }
})

