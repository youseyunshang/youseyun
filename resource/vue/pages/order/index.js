var app = new Vue({
    el: '#app',
    data: {
        taps: 1,
        list: [
            {
                id: 0,
                title: '全部'
            },
            {
                id: 1,
                title: '待审核'
            },
            {
                id: 2,
                title: '待付款'
            },
            {
                id: 3,
                title: '待发货'
            },
            {
                id: 4,
                title: '待收货'
            },
            {
                id: 5,
                title: '交易成功'
            },
            {
                id: 6,
                title: '交易取消'
            },
            {
                id: 7,
                title: '售后处理'
            }
        ],
        checkedId: 0,
        productList: [
            {
                id: 0,
                type: 0,
                status: 1,
                order: '01234567896',
                total: 98,
                list: [
                    {
                        img: '../../images/img.png',
                        title: '低碳铬铁',
                        total: 1,
                        unit: 41,
                        price: 41,
                        name: '天津中威进出口有限公司'
                    },
                    {
                        img: '../../images/img.png',
                        title: '低碳铬铁',
                        total: 1,
                        unit: 41,
                        price: 41,
                        name: '天津中威进出口有限公司'
                    }
                ]                
            }
        ],
        showId: '',
        type: ['买入订单', '售出订单']
    },
    mounted(){
    },
    methods: {
        
    }
})

