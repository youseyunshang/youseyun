var app = new Vue({
    el: '#app',
    data: {
        showArea: false,
        form: {
            time: '选择时间',
        },
        showType: false,
        showSend: false,
        showPay: false,
        type: '',
        send: '',
        pay: '',
        typeList: typeList||[],
        sendList: sendList||[],
        payList: payList||[],
        purchaseId:purchaseId,
        unitPrice:'',
        billHolder:'',
        tel:''
    },
    mounted(){
        this.Dates();
    },
    methods: {
        Dates(e){
            var that = this;
            new moTimeCJJ({
               domId : "times", 
               //minTime : "2010-10-22 23:22:11",
              // maxTime : "2030-10-22 23:22:11",
               value: that.form.time , // 2018-02-02 02:34:59
               type : "YMDHM" // ["YMDHMS","YMD","HMS"]
            })
        },
        offer(){
            var data = {
                "purchaseId":this.purchaseId,
                "taxCategory":this.type,
                "unitPrice":this.unitPrice,
                "distributionMode":this.send,
                "payment":this.pay,
                "endTime":$("#times").html(),
                "billHolder":this.billHolder,
                "tel":this.tel
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/offer',data)
                .then((response)=> {
                    if(response.data.code=="200"&&response.data.data){
                        alert("报价成功！");
                        window.location.href = WINDOWS_URL_DEFINE+"/indexProduct/index";
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });
        }
    }
})

