var app = new Vue({
    el: '#app',
    data: {
        QList: [
        ],
        AList: [
        ],
        taps: true
    },
    mounted(){
        this.init();
    },
    methods: {
        async init(){
            await axios.post(WINDOWS_INDEX_REQUEST+'/IndexUsercenter/myQAndAData',{})
                .then( (response) => {
                    var i=0,j=0;
                    this.QList = [];
                    for(i=0;i<response.data.qresult.length;i++){
                        this.QList.push({
                            title: response.data.qresult[i]['title'],
                            num: response.data.qresult[i]['answermun']
                        });
                    }
                    this.AList = [];
                    for(i=0;i<response.data.answer.length;i++){
                        this.AList.push({
                            title: response.data.answer[i]['title'],
                            desc: response.data.answer[i]['content'],
                            time: '1天前'
                        });
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
})

