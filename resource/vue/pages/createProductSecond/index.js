var app = new Vue({
    el: '#app',
    data: {
        productName:preData.title||'',
        productNotice:preData.content||'',
        num:preData.num||'',
        lastNum:preData.minOrderNum||'',
        lastPrice:preData.unitPrice||'',
        //truePer:'',
        types: '',
        showTypes: false,
        typesList: [
            {
                id: 1,
                name: '铁'
            },
            {
                id: 2,
                name: '铜'
            }
        ],
		//粒度
		granularityType:preData.granularityType||"",
		granularityTypeId:preData.granularityTypeId||"",
        showGranularity:false,
        granularityList:granularityList||[],
        sendType: preData.distributionMode||1,
        sendTypeList: [
            {
                id: 1,
                name: '送到买家指定地点'
            },
            {
                id: 2,
                name: '上门自提'
            }
        ],
        require: preData.invoice||1,
        requireList: [
            {
                id: 1,
                name: '无发票'
            },
            {
                id: 2,
                name: '增值税发票'
            },
            {
                id: 3,
                name: '普通发票'
            }
        ],
        photo: preData.productPhoto||'',
		//货物生产地
		ProductOrigin: preData.ProductOrigin||'',
        showProductOrigin: false,
        formProductOrigin: {
            lawyer_province: '',
            lawyer_province_code: '',
            lawyer_city: '',
            lawyer_city_code: '',
        },
		//货物所在地
		ProductLoation: preData.ProductLoation||'',
        showProductLoation: false,
        formProductLoation: {
            lawyer_province: '',
            lawyer_province_code: '',
            lawyer_city: '',
            lawyer_city_code: ''
        },
        form:{
            time: preData.endTime||''
        },
		//牌号
		brandNameType:preData.brandNameType||"",
		brandNameTypeId:preData.brandNameTypeId||"",
        showbrandName:false, 
        brandNameList:brandNameList||[],
        otherParams:preData.otherParams||''
    },
    mounted(){
        this.init();
    },
    methods: {
        init(){
        },
        next(){
            var data = {
                'title':$("#productName").val(),
                'content':$("#productNotice").val(),
                'num':$("#num").val(),
                'minOrderNum':$("#lastNum").val(),
                'unitPrice':$("#lastPrice").val(),
                //'granularity':$("#truePer").val(),
                'productPhoto':$(".uploadImages").find("img").attr("src"),
                'distributionMode':this.sendType,
                'invoice':this.require,
                'ProductOrigin':this.ProductOrigin,
                'ProductLoation':this.ProductLoation,
                'timeTypes':this.timeTypes,
                'timeId':this.timeId,
				'granularityType':this.granularityType,
                'granularityTypeId':this.granularityTypeId,
                'brandNameType':this.brandNameType,
                'brandNameTypeId':this.brandNameTypeId,
                'otherParams':this.otherParams,
                'endTime':$("#dateSelectorOne").val()
            }
            if(data.title==""){
               alert("产品名称不能为空！");
               return false;
            }
            if(data.num==""){
                alert("产品数量不能为空！");
                return false;
            }
            if(data.minOrderNum==""){
                alert("最小起订量不能为空！");
                return false;
            }
            if(data.unitPrice==""){
                alert("单价不能为空！");
                return false;
            }
            if(data.granularity==""){
                alert("含量不能为空！");
                return false;
            }
            if(Number(data.minOrderNum)>Number(data.num)){
                alert("最小供应量不能高于总供应量！");
                return false;
            }
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductSecond',data)
            .then(function (response) {
                if(response.data.code=="200"&&response.data.data){
                    window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductThird';
                }
            })
            .catch(function (error) {
                this.publish = true;
                console.log(error);
            });
        },
        pre(){
            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexProduct/createProductFirst';
        },
        ProductLoations(e) {
            this.ProductLoation = e.provinceName +"-"+ e.cityName;
            this.showProductLoation = false;
            this.formProductLoation.lawyer_province = e.provinceName;
            this.formProductLoation.lawyer_province_code = e.provinceCode;
            this.formProductLoation.lawyer_city = e.cityName;
            this.formProductLoation.lawyer_city_code = e.cityCode;
        },
        ProductOrigins(e) {
            this.ProductOrigin = e.provinceName +"-"+ e.cityName;
            this.showProductOrigin = false;
            this.formProductOrigin.lawyer_province = e.provinceName;
            this.formProductOrigin.lawyer_province_code = e.provinceCode;
            this.formProductOrigin.lawyer_city = e.cityName;
            this.formProductOrigin.lawyer_city_code = e.cityCode;
        },
        handleChange(e) {
            let $target = e.target || e.srcElement;
            let file = $target.files[0];
            var reader = new FileReader();
            reader.onload = (data) => {
                let res = data.target || data.srcElement;
                axios.post(WINDOWS_INDEX_REQUEST+'/IndexProduct/preUpload',{"data":res.result})
                .then((response)=> {
                    if(response.data.code=="200"&&response.data.data){
                        this.photo = response.data.data.url;
                    }
                })
                .catch(function (error) {
                    this.publish = true;
                    console.log(error);
                });

            };
            reader.readAsDataURL(file);
        }
    }
})

