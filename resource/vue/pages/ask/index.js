var app = new Vue({
    el: '#app',
    data: {
        showMore: false,
        form: {
            desc: '',
            title: ''
        },
        showNotice: true,
        labelList: [
        ],
        chooseList: [
            {
                id: 0,
                title: '#有色金属',
                pid:0
            },
            {
                id: 1,
                title: '#无色金属',
                pid:1
            }
        ],
        showMask: false,
        isshow: false,
        searchSignAttr:'',
        attrResponse:[],
        publish:true
    },
    computed: {
        isSubmitBtnDisabled() {
            const { form } = this;
            
            return !(
                form.title
            );
        }
    },
    mounted(){
        this.makeExpandingArea(titles);
        this.init();
    },
    created(){
        // this.searchSign();
    },
    methods: {
        init(){
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexQuestion/addQuestionData',{})
                .then((response)=>{
                    if(response.data){
                        this.attrResponse = response.data;
                        this.searchSign();
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        submit() {
            if(this.publish){
                this.publish = false;
                var data = {
                    'title':this.form.title,
                    'content':$('#content').getValue(),
                    'attrId':this.labelList
                }
                axios.post(WINDOWS_INDEX_REQUEST+'/IndexQuestion/addQuestion',data)
                    .then(function (response) {
                        if(response.data>0){
                            window.location.href=WINDOWS_INDEX_REQUEST+'/IndexQuestion/questionLists';
                        }else{
                            this.publish = true;
                        }
                    })
                    .catch(function (error) {
                        this.publish = true;
                        console.log(error);
                    });
            }else{
                alert("正在发布请稍等！");
            }

        },
        makeExpandingArea(el) {
            var setStyle = function(el) {
                el.style.height = 'auto';
                el.style.height = el.scrollHeight + 'px';
            }
            var delayedResize = function(el) {
                window.setTimeout(function() {
                        setStyle(el)
                    },
                    0);
            }
            if (el.addEventListener) {
                el.addEventListener('input', function() {
                    setStyle(el)
                }, false);
                setStyle(el)
            } else if (el.attachEvent) {
                el.attachEvent('onpropertychange', function() {
                    setStyle(el)
                });
                setStyle(el)
            }
            if (window.VBArray && window.addEventListener) { //IE9
                el.attachEvent("onkeydown", function() {
                    var key = window.event.keyCode;
                    if (key == 8 || key == 46) delayedResize(el);

                });
                el.attachEvent("oncut", function() {
                    delayedResize(el);
                }); //处理粘贴
            }
        },
        textAreaChange(){
            //字符
            if(this.form.title.charAt(this.form.title.length-1)=='?'||this.form.title.charAt(this.form.title.length-1)=='？'){
                this.form.title = this.form.title.substring(0, 50);
                if(this.form.title.charAt(this.form.title.length-1)!='?'&&this.form.title.charAt(this.form.title.length-1)!='？'){
                    this.form.title = this.form.title+"?";
                }
            }else{
                this.form.title = this.form.title.substring(0, 50)+"?";
            }

            this.labelList = [];
            if(this.attrResponse){
                var attrLength=this.attrResponse.length;
                for (var i=0;i<attrLength;i++){

                    if(this.form.title.indexOf(this.attrResponse[i].attrname) != -1 ){
                        //匹配上
                        this.labelList.push({id:this.attrResponse[i].Id ,title: this.attrResponse[i].attrname});
                    }
                    if(this.labelList.length>1){
                        break;
                    }
                }
            }
        },
        textAreafocus(){
            this.form.title = this.form.title.replace("?","");
            this.form.title = this.form.title.replace("？","");
        },
        textAreablur(){
            if(this.form.title.charAt(this.form.title.length-1)!='?'&&this.form.title.charAt(this.form.title.length-1)!='？'){
                this.form.title = this.form.title.substring(0, 50)+"?";
            }
        },
        searchSign(){
            this.chooseList = [];
            if(this.attrResponse){
                var attrLength=this.attrResponse.length;
                for (var i=0;i<attrLength;i++){
                    if(this.searchSignAttr.indexOf(this.attrResponse[i].attrname) != -1 ){
                        //匹配上
                        this.chooseList.push({id:this.attrResponse[i].Id ,title: this.attrResponse[i].attrname,pid:i});
                    }
                    if(this.chooseList.length>5){
                        break;
                    }
                }
            }
        },
        addSign(pid){
            if(this.labelList.length>4){
                alert("最多选择5个标签！");
                return false;
            }
            if(this.attrResponse){
                var pid = pid || 0;
                if(this.attrResponse[pid]){
                    for (var i=0;i<this.labelList.length;i++){
                        if(this.labelList[i].id==this.attrResponse[pid].Id){
                            return false;
                        }
                    }
                    this.labelList.push({id:this.attrResponse[pid].Id ,title: this.attrResponse[pid].attrname});
                }
            }

        },
        changeSearchSign(){
            this.searchSign();
        }
    }
})

