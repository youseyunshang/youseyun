var app = new Vue({
    el: '#app',
    data: {
        showNotice: true,
        userInfo:{
            avatar:'',
            realname:'',
            sex: '1',
            tel:'',
            grade:'未认证'
        }
    },
    mounted(){
        this.init();
    },
    methods: {
        uploadPicture(){
            $("#replaceImg").click();
            // this.$el.querySelector('.hiddenInput').click();
        },
        handleFile: function (e){
            let $target = e.target || e.srcElement;
            let file = $target.files[0];
            var reader = new FileReader();
            reader.onload = (data) => {
                let res = data.target || data.srcElement;
                this.userInfo.avatar = res.result;
                console.log(this.userInfo.avatar);
            };
            reader.readAsDataURL(file);
        },
        async init(){
            await axios.post(WINDOWS_INDEX_REQUEST+'/IndexUsercenter/ajaxPersonalInfo',{})
                .then( (response) => {
                    if(response.data){
                        this.userInfo.realname = response.data.realname;
                        this.userInfo.tel = response.data.tel;
                        this.userInfo.sex = response.data.gender;
                        this.userInfo.grade = response.data.grade>0?"已认证":"未认证";
                        this.userInfo.avatar = response.data.userLogo?WINDOWS_URL_DEFINE+response.data.userLogo:WINDOWS_URL_DEFINE+"/resource/vue/images/uploadImage.png";
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        submit(){
            this.userInfo.avatar = $("#userInfoImg").attr("src");
            axios.post(WINDOWS_INDEX_REQUEST+'/IndexUsercenter/updateUserInfo',this.userInfo)
                .then(function (response) {
                    alert("修改成功！");
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        sexs(e) {
            this.sex = e;
        },
        companyValid(){
            window.location.href = WINDOWS_INDEX_REQUEST+'/IndexUsercenter/companyValid'
        }

    }
});

