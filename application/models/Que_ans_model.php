<?php
class Que_ans_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取问题信息
    public function get_question()
    {
        $query = $this->db->select("*")->from('cbc_question')->get();
        return $query->result_array();
    }
    //关联查询cbc_user/cbc_attr/cbc_question
    public function get_question_select(){
        $query = $this->db->select("cbc_question.Id,cbc_question.unqid,cbc_question.title,cbc_question.content,cbc_question.createTime,cbc_question.answermun,cbc_user.username,cbc_question.attrId")->from('cbc_question')->join("cbc_user","cbc_question.createUserId = cbc_user.Id","left")->where("cbc_question.status","1")->get();
        return $query->result_array();
    }
    /**
     * del_question
     *
     * 删除问题
     * @static
     * @return	0 失败 1成功
     */
    public function del_question($params=0){
        if($params>0){
            $this->db->trans_start();
            //删除问题的答案
            $this->db->where('qid',$_POST['unqid'])->delete("cbc_answer");
            //删除用户收藏的问题表
            $this->db->where('qId',$params)->delete("cbc_user_collection_que");
            //删除问题属性表
            $this->db->where('qId',$params)->delete("cbc_que_attr");
            //删除问题
            $this->db->where('Id',$params)->delete("cbc_question");
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //获取答案数据
    public function get_answer($params){
        $query = $this->db->select("cbc_answer.Id as aId,cbc_answer.content,cbc_answer.createTime,cbc_user.username")->from('cbc_answer')->join("cbc_user","cbc_answer.createUserId = cbc_user.Id","left")->where("cbc_answer.qid",$params)->where("cbc_answer.status","1")->get();
        return $query->result_array();
    }
    //删除答案
    public function del_answer($params=0){
        return $this->db->where('Id', $params)->update('cbc_answer', array("status"=>0));
//        return $this->db->where('Id',$params)->delete("cbc_answer");
    }
}