<?php
class Supple_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance = &$this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    /**
     * get_order_where
     * @notice 获取问题信息
     * @return	void
     * @anthor Json.Wang
     */
    public function get_order_where($params = array())
    {
        if(!empty($params)){
            $this->db->select("*")->from('cbc_supple');
            foreach ($params as $key=>$val){
                $this->db->where($key,$val);
            }
            $this->db->order_by("Id", "DESC");
            $query = $this->db->get();
            return $query->result_array();
        }else{
            $query = $this->db->select("*")->from('cbc_supple')->order_by("Id", "DESC")->get();
            return $query->result_array();
        }
    }
    //删除问题
//    public function del_order($params=0){
//        return $this->db->where('Id',$params)->delete("cbc_supple");
//    }
    /**
     * supple_status
     * @notice 更新供应状态
     * @return	void
     * @anthor Json.Wang
     */
    public function supple_status($params=array()){
        if(!empty($params)){
            $arr = array(
                "status"=>$params['status']
            );
            return $this->db->where('Id',$params['Id'])->update('cbc_supple', $arr);
        }else{
            return 0;
        }
    }
    /**
     * insert_Supple
     * @notice 新建供应信息
     * @return	void
     * @anthor Json.Wang
     */
    public function insert_Supple($params=array()){
        if(!empty($params)){
//            $this->db->trans_start();
            $this->db->insert('cbc_supple', $params);
            return $this->db->insert_id();
//            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    /**
     * update_Supple
     * @notice 更新供应信息
     * @return	void
     * @anthor Json.Wang
     */
    public function update_Supple($params = array(),$unqidCode=""){
        if(!empty($unqidCode)){
            $this->db->trans_start();
            $this->db->where('unqidCode',$unqidCode)->update('cbc_supple', $params);
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    /**
     * shop_car_where
     * @notice 查看购物车内是否有商品
     * @return	void
     * @anthor Json.Wang
     */
    public function shop_car_where($params = array())
    {
        if(!empty($params)){
            $this->db->select("*")->from('cbc_shop_car');
            foreach ($params as $key=>$val){
                $this->db->where($key,$val);
            }
            $this->db->order_by("Id", "DESC");
            $query = $this->db->get();
            return $query->result_array();
        }else{
            $query = $this->db->select("*")->from('cbc_shop_car')->order_by("Id", "DESC")->get();
            return $query->result_array();
        }
    }
    /**
     * insert_shop_car
     * @notice 加入购物车
     * @return	void
     * @anthor Json.Wang
     */
    public function insert_shop_car($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
              $this->db->insert('cbc_shop_car', $params);
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    /**
     * del_shop_car_data
     * @notice 删除购物车
     * @return	void
     * @anthor Json.Wang
     */
    public function del_shop_car_data($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            foreach ($params as $key=>$val){
                $this->db->query('DELETE FROM cbc_shop_car WHERE cbc_shop_car.Id = '.$val['Id']);
//                $this->db->where('Id','8')->delete("cbc_shop_car");
            }
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }

}