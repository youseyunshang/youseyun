<?php
class Index_attr_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
        $this->load->driver('cache');
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //所有属性
    public function get_all_attr(){
        $query = $this->db->select("*")->from('cbc_attr')->get();
        return $query->result_array();
    }
    //信息金属属性
    public function get_info_attr(){
        $query = $this->db->select("*")->from('cbc_attr')->where("cbc_attr.type","0")->get();
        return $query->result_array();
    }
    //喜好标签属性
    public function get_sign_attr(){
        $query = $this->db->select("*")->from('cbc_attr')->where("cbc_attr.type","1")->get();
        return $query->result_array();
    }
    //商城金属属性
    public function get_shop_attr(){
        $query = $this->db->select("*")->from('cbc_attr')->where("cbc_attr.type","2")->get();
        return $query->result_array();
    }
    //金属形态属性
    public function get_status_attr(){
        $query = $this->db->select("*")->from('cbc_attr')->where("cbc_attr.type","3")->get();
        return $query->result_array();
    }
    //信息一级金属属性
    public function get_info_attr_first(){
        if($result = $this->cache->redis->get('infoAttrFitst')){
            return json_decode($result,true);
        }else{
            $query = $this->db->select("*")->from('cbc_attr')->where("cbc_attr.type","0")->where("cbc_attr.pid","0")->get();
            $result = $query->result_array();
            $this->cache->redis->save('infoAttrFitst',json_encode($result),86400);
            return $result;
        }

    }
    //商城一级金属属性
    public function get_shop_attr_first(){
        if($result = $this->cache->redis->get('shopAttrFitst')){
            return json_decode($result,true);
        }else{
            $query = $this->db->select("*")->from('cbc_attr')->where("cbc_attr.type","2")->where("cbc_attr.pid","0")->get();
            $result = $query->result_array();
            $this->cache->redis->save('shopAttrFitst',json_encode($result),86400);
            return $result;
        }
    }
}