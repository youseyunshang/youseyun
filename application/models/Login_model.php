<?php
class Login_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取管理员信息
    public function get_userinfos($params = FALSE)
    {
        if ($params === FALSE)
        {
            $query = $this->db->select("cbc_admin.Id AS UserId,cbc_admin.username,cbc_admin.tel,cbc_admin.realname,cbc_admin.gender,cbc_admin.workYear,cbc_admin.logo,cbc_admin.email,cbc_admin.wechart,cbc_admin.attrId,cbc_role.Id as roleId,cbc_role.rolename")->from('cbc_admin')->join("cbc_role","cbc_admin.roleId=cbc_role.Id","left")->get();
            return $query->result_array();
        }
        $query = $this->db->select("cbc_admin.Id AS UserId,cbc_admin.username,cbc_admin.tel,cbc_admin.pwd,cbc_admin.realname,cbc_admin.gender,cbc_admin.workYear,cbc_admin.logo,cbc_admin.email,cbc_admin.wechart,cbc_admin.attrId,cbc_admin.logo,cbc_admin.wechartpicture,cbc_role.Id as roleId,cbc_role.rolename,cbc_role.content")->from('cbc_admin')->join("cbc_role","cbc_admin.roleId=cbc_role.Id","left")->where("cbc_admin.username",$params)->get();
        return $query->row_array();
    }
    //插入管理员信息
    public function inset_userinfos($params = array())
    {
        if(empty($params)){
            return false;
        }else{
            $data = array(
                'username' => $params['username'],
                'tel' =>$params['tel'],
                'realname' => $params['realname'],
                'pwd' => md5(md5($params['pwd'])),
                'gender' =>$params['gender'],
                'workYear' =>isset($params['workYear'])?$params['workYear']:'',
                'email' => isset($params['email'])?$params['email']:'',
                'roleId' =>$params['roleId'],
                'wechart' => isset($params['wechart'])?$params['wechart']:'',
                'attrId' => isset($params['saleman'])?$params['saleman']:'',
                'logo' => isset($params['logoUrl'])?$params['logoUrl']:'',
                'wechartpicture' =>isset($params['wechartLogoUrl'])?$params['wechartLogoUrl']:'',
                'createTime' => time()
            );
            return $this->db->insert('cbc_admin', $data);
        }
    }
    //更新管理员信息
    public function update_userinfos($params = array()){
        if(empty($params)){
            return false;
        }else{
            $data = array(
                'username' => $params['username'],
                'tel' =>$params['tel'],
                'realname' => $params['realname'],
                'pwd' => md5(md5($params['pwd'])),
                'gender' =>$params['gender'],
                'workYear' =>isset($params['workYear'])?$params['workYear']:'',
                'email' => isset($params['email'])?$params['email']:'',
                'roleId' =>$params['roleId'],
                'wechart' => isset($params['wechart'])?$params['wechart']:'',
                'attrId' => isset($params['saleman'])?$params['saleman']:'',
                'logo' => isset($params['logoUrl'])?$params['logoUrl']:'',
                'wechartpicture' =>isset($params['wechartLogoUrl'])?$params['wechartLogoUrl']:'',
                'createTime' => time()
            );
            if($params['pwd']=="123456"){
                unset($data['pwd']);
            }

            return $this->db->where('Id', $params['UserId'])->update('cbc_admin', $data);
        }
    }
    //删除管理员信息
    public function del_userinfos($params=0){
        return $this->db->where('Id',$params)->delete("cbc_admin");
    }
    //获取角色信息
    //修改角色
    public function get_role($params = FALSE){
        if ($params === FALSE)
        {
            $query = $this->db->get('cbc_role');
            return $query->result_array();
        }
        $query = $this->db->get_where('cbc_role', array('Id' => $params));
        return $query->row_array();
    }
    //商城业务员信息
    public function get_manager($data=array()){
        $this->db->select("*")->from('cbc_admin');
        if(!empty($data)){
            foreach ($data as $key=>$val){
                $this->db->where($key,$val);
            }
        }
        $this->db->order_by('Id desc');
        $query = $this->db->get();
        return $query->result_array();
    }
}