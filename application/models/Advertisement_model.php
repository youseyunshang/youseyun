<?php
class Advertisement_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取关键词标签
    public function get_sign_attr(){
        $query = $this->db->where('type',1)->get('cbc_attr');
        return $query->result_array();
    }
    //插入广告
    public function insert_advertisement($params=array()){
        $this->db->trans_start();
        $this->db->insert('cbc_advertisement', $params);
        return $this->db->trans_complete();
    }
    //更新资源数据
    public function update_advertisement($params = array()){
        $this->db->trans_start();
        $this->db->where('Id',$_POST['Id'])->update('cbc_advertisement', $params);
        return $this->db->trans_complete();
    }
    //关联查询cbc_res/cbc_admin
    public function get_advertisement($where = false){
        if($where === false){
            $query = $this->db->select("*")->from('cbc_advertisement')->get();
            return $query->result_array();
        }else{
            $query = $this->db->get_where('cbc_advertisement', array('Id' => $where['Id']));
            return $query->row_array();
        }
    }
    //删除信息
    public function del_advertisement($params=0){
        $this->db->trans_start();
        $this->db->where('Id',$params)->delete("cbc_advertisement");
        return $this->db->trans_complete();
    }
}