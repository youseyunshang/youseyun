<?php
class User_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取会员信息
    public function get_userinfos($params = FALSE)
    {
        if ($params === FALSE)
        {
            $query = $this->db->select("*")->from('cbc_user')->order_by("Id","DESC")->get();
            return $query->result_array();
        }
        $query = $this->db->select("*")->from('cbc_user')->where("cbc_user.Id",$params)->get();
        return $query->row_array();
    }
    //插入会员信息
    public function inset_userinfos($params = array())
    {
        if(empty($params)){
            return false;
        }else{
            $data = array(
                'username' => $params['username'],
                'tel' =>$params['tel'],
                'realname' => $params['realname'],
                'pwd' => md5(md5($params['pwd'])),
                'gender' =>$params['gender'],
                'grade' =>1,
                'email' => isset($params['email'])?$params['email']:'',
                'department' => isset($params['department'])?$params['department']:'',
                'provid' => isset($params['provid'])?$params['provid']:'',
                'cityid' => isset($params['cityid'])?$params['cityid']:'',
                'idCard' => isset($params['idCard'])?$params['idCard']:'',
                'status' => 1,
                'createTime' => time(),
                'userLogo' => isset($params['userLogo'])?$params['userLogo']:'',
                'comName' => isset($params['comName'])?$params['comName']:'',
                'comType' => isset($params['comType'])?$params['comType']:'',
                'comContent' => isset($params['comContent'])?$params['comContent']:'',
                'comOrigin' => isset($params['comOrigin'])?$params['comOrigin']:'',
                'comPerson' => isset($params['comPerson'])?$params['comPerson']:'',
                'comTel' => isset($params['comTel'])?$params['comTel']:'',
                'comLicense' => isset($params['comLicense'])?$params['comLicense']:'',
                'comLogo' => isset($params['comLogo'])?$params['comLogo']:''
            );
            return $this->db->insert('cbc_user', $data);
        }
    }
    //更新会员信息
    public function update_userinfos($params = array()){
        if(empty($params)){
            return false;
        }else{
            $data = array(
                'username' => $params['username'],
                'tel' =>$params['tel'],
                'realname' => $params['realname'],
                'pwd' => md5(md5($params['pwd'])),
                'gender' =>$params['gender'],
                'grade' =>1,
                'email' => isset($params['email'])?$params['email']:'',
                'department' => isset($params['department'])?$params['department']:'',
                'idCard' => isset($params['idCard'])?$params['idCard']:'',
                'status' => 1,
                'createTime' => time(),
                'userLogo' => isset($params['userLogo'])?$params['userLogo']:'',
                'comName' => isset($params['comName'])?$params['comName']:'',
                'comType' => isset($params['comType'])?$params['comType']:'',
                'comContent' => isset($params['comContent'])?$params['comContent']:'',
                'comOrigin' => isset($params['comOrigin'])?$params['comOrigin']:'',
                'comPerson' => isset($params['comPerson'])?$params['comPerson']:'',
                'comTel' => isset($params['comTel'])?$params['comTel']:'',
                'comLicense' => isset($params['comLicense'])?$params['comLicense']:'',
                'comLogo' => isset($params['comLogo'])?$params['comLogo']:''
            );
            if($params['pwd']=="123456"){
                unset($data['pwd']);
            }
            return $this->db->where('Id', $params['UserId'])->update('cbc_user', $data);
        }
    }
    //批量导入用户
    public function insert_all_user($data){
        $this->db->insert_batch('cbc_user', $data);
    }
    //删除会员信息
    public function del_userinfos($params=0){
        return $this->db->where('Id',$params)->delete("cbc_user");
    }
    //会员审核通过
    public function user_status($data = array()){
        if($data['status']>0){
            $arr = array(
                "grade"=>"2",
                "createTime"=>time()
            );
        }else{
            $arr = array(
                "grade"=>"1",
                "createTime"=>time()
            );
        }
        return $this->db->where('Id', $data['Id'])->update('cbc_user', $arr);
    }
}