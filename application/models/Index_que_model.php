<?php
class Index_que_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    /**
     * 添加问题
     *
     * @static
     * @return	object
     */
    public function add_question($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->insert('cbc_question', $params);
            $qId = $this->db->insert_id();
            $data  = array();
            if(!empty($post['attrId'])){
                unset($val);
                foreach ($post['attrId'] as $key=>&$val){
                    $data[] = array("qId"=>$qId,"attrId"=>$val['id']);
                }
                $this->db->insert_batch('cbc_que_attr', $data);
            }
            //暂时屏蔽问题缓存
//            CI_Redis::get_instance()->redis->delete('questionInfos');
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //删除问题--假删除
    public function del_question( $params = 0 ){
        if($params>0){
            return $this->db->where('Id', $params)->update('cbc_question', array("status"=>0));
        }else{
            return 0;
        }
    }
    //添加答案
    public function add_answer($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->insert('cbc_answer', $params);
            $aId = $this->db->insert_id();
            $query = $this->db->get_where('cbc_question', array('unqid' => $params['qid']));
            $data = $query->row_array();
            $amswerId = $data['topAnswerId']>0?$data['topAnswerId']:$aId;
            $this->db->where('unqid', $params['qid'])->update('cbc_question', array("answermun"=>$data['answermun']+1,"topAnswerId"=>$amswerId));
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //获取问题信息
    public function get_question($params = array())
    {
        if(!empty($params)){
            $this->db->select("*")->from('cbc_question');
            foreach ($params as $key=>$val){
                $this->db->where($key,$val);
            }
            $this->db->where('cbc_question.status','1');
            $this->db->order_by("cbc_question.createTime","DESC");
            $query = $this->db->get();
            return $query->result_array();
        }else{
            $query = $this->db->select("*")->from('cbc_question')->join("cbc_answer","cbc_question.topAnswerId = cbc_answer.Id","left")->where('cbc_question.status','1')->order_by("cbc_question.createTime","DESC")->get();
            return $query->result_array();
        }
    }
    //获取问题的答案数据
    public function get_answers($params = array()){

            $this->db->select("*")->from('cbc_answer');
            if(!empty($params)){
                foreach ($params as $key=>$val){
                    $this->db->where($key,$val);
                }
            }
            $this->db->order_by("up","DESC");
            $query = $this->db->get();
            return $query->result_array();
    }
    //获得答案的问题
    public function get_answers_question($params = array()){
        if(!empty($params)){
            $this->db->select("cbc_question.Id as qId,cbc_question.title,cbc_answer.content as content")->from('cbc_answer')->join("cbc_question","cbc_question.unqid = cbc_answer.qId");
            foreach ($params as $key=>$val){
                $this->db->where($key,$val);
            }
            $query = $this->db->get();
            return $query->result_array();
        }else{
            return 0;
        }
    }
    //搜藏问题
    public function collection_question($params){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->insert('cbc_user_collection_que', $params);
//            self::update_question_aboutnum(,);
            //更新当前用户收藏的问题
            //$result = self::get_collection_question(array("userId"=>$params['userId']));
            //更新当前
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //获得当前用户的收藏问题有哪些
    public function get_collection_question($params=array()){
            $this->db->select("*")->from('cbc_user_collection_que');
            if(!empty($params)){
                foreach ($params as $key=>$val){
                    $this->db->where($key,$val);
                }
            }
            $query = $this->db->get();
            return $query->result_array();
//            $result = $query->result_array();
//            $data = array();
//            foreach ($result as $key=>&$val){
//                $data[$val['qId']] = $val;
//            }
//            return CI_Redis::get_instance()->redis->set('collection_question_'.$params['userId'],json_encode($data));
    }
    //更新问题中数量的显示
    public function update_question_aboutnum($params=array(),$qid=false){
        if(!empty($params)&&$qid!==false){
            return $this->db->where('unqid', $qid)->update('cbc_question',$params);
        }else{
            return 0;
        }
    }
    //点亮
    public function light_question( $params = array() ){
        if(!empty($params)){
            return $this->db->where('unqid', $params['qId'])->update('cbc_question', array("light"=>$params['light']));
        }else{
            return 0;
        }
    }
    //点赞
    public function up_answer($params = array()){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->insert('cbc_user_answer_up', $params);
            $query = $this->db->get_where('cbc_answer', array('Id' => $params['aId']));
            $data = $query->row_array();
            $up = $data['up']+1;
            $this->db->where('Id', $params['aId'])->update('cbc_answer', array("up"=>$up));
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //获得当前用户的点赞的答案有哪些
    public function get_up_answer($params=array()){
        $this->db->select("*")->from('cbc_user_answer_up');
        if(!empty($params)){
            foreach ($params as $key=>$val){
                $this->db->where($key,$val);
            }
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    //删除点赞所有数据
    public function del_all_updown_answer(){
        return $this->db->truncate('cbc_user_answer_up');
    }
    //批量插入点赞数据
    public function up_answer_branch($data=array()){
        if(!empty($data)){
            return $this->db->insert_batch('cbc_user_answer_up', $data);
        }else{
            return 0;
        }
    }
    //更新答案数据
    public function update_answer($aId=0,$array=array()){
        if($aId>0){
            $this->db->trans_start();
            $this->db->where('Id', $aId)->update('cbc_answer', $array);
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
}