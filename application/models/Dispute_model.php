<?php
class Dispute_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取权限信息
//    public function get_permissionInfo(){
//        $query = $this->db->get('cbc_permission');
//        return $query->result_array();
//    }
    //获取角色信息
    //修改角色
    public function get_dispute($params = FALSE){
        if ($params === FALSE)
        {
            $query = $this->db->select('cbc_dispute_order.Id,
            cbc_dispute_order.ordernumber,
	cbc_dispute_order.disputeCom,
	cbc_dispute_order.content,
	cbc_dispute_order.saleman,
	cbc_dispute_order.saleTel,
	cbc_dispute_order.createTime,
	cbc_dispute_order.status,
	cbc_user.username')->from('cbc_dispute_order')->join('cbc_user','cbc_dispute_order.createId = cbc_user.Id','left')->get();
            return $query->result_array();
        }
        $query = $this->db->get_where('cbc_dispute_order', array('Id' => $params));
        return $query->row_array();
    }
    //添加角色
//    public function add_role($params=array()){
//        $data = array(
//            'rolename' => $params['rolename'],
//            'content' =>$params['content'],
//            'createTime' => time()
//        );
//        return $this->db->insert('cbc_role', $data);
//    }
//    //删除角色
//    public function del_role($params = 0){
//        return $this->db->where('Id',$params)->delete("cbc_role");
//    }
//    //跟新角色
//    public function update_role($data = array()){
//        return $this->db->where('Id', $data['Id'])->update('cbc_role', $data);
//    }
}