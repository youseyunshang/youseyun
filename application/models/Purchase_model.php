<?php
class Purchase_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取采购数据
    public function get_order($params=0)
    {
        if($params<1){
            $query = $this->db->select("*")->from('cbc_purchase')->get();
            return $query->result_array();
        }else{
            $query = $this->db->select("*")->from('cbc_purchase')->where('Id',$params)->get();
            return $query->result_array();
        }

    }
	//获取采购数据
    public function get_order_where($params = array())
    {
        $this->db->select("*")->from('cbc_purchase');
        if(!empty($params)) {
            foreach ($params as $key => $val) {
                $this->db->where($key, $val);
            }
        }
        $this->db->order_by("Id", "DESC");
        $query = $this->db->get();
        return $query->result_array();
    }
    //获取会员信息
    public function get_userinfos($params = FALSE)
    {
        if ($params === FALSE)
        {
            $query = $this->db->select("*")->from('cbc_user')->get();
            return $query->result_array();
        }
        $query = $this->db->select("*")->from('cbc_user')->where("cbc_user.Id",$params)->get();
        return $query->row_array();
    }
    //关联查询cbc_user/cbc_attr/cbc_question
    public function get_order_select($params=0){
        if($params<1){
            $query = $this->db->select("cbc_purchase.Id,
	cbc_purchase.billHolder,
	cbc_purchase.billHolderStatus,
	cbc_purchase.brandName,
	cbc_purchase.companyType,
	cbc_purchase.title,
	cbc_purchase.content,
	cbc_purchase.createTime,
	cbc_purchase.distributionMode,
	cbc_purchase.email,
	cbc_purchase.startTime,
	cbc_purchase.endTime,
	cbc_purchase.granularity,
	cbc_purchase.invoice,
  cbc_purchase.location,
  cbc_purchase.num, 
	cbc_purchase.placeOrigin,
	cbc_purchase.residualNum,
	cbc_purchase.suppleCompanyType,
	cbc_purchase.tel,
	cbc_purchase.unitPrice,
	cbc_purchase.warehouseReceipt,
	cbc_purchase.status,
	cbc_purchase.unqidCode,
  cbc_user.username,
  cbc_attr.attrname")->from('cbc_purchase')->join("cbc_user","cbc_purchase.userId = cbc_user.Id","left")->join("cbc_attr","cbc_purchase.attrId = cbc_attr.Id","left")->order_by("Id", "DESC")->get();
            return $query->result_array();
        }else{
            $query = $this->db->select("cbc_purchase.Id,
	cbc_purchase.billHolder,
	cbc_purchase.billHolderStatus,
	cbc_purchase.brandName,
	cbc_purchase.companyType,
	cbc_purchase.title,
	cbc_purchase.content,
	cbc_purchase.createTime,
	cbc_purchase.distributionMode,
	cbc_purchase.email,
	cbc_purchase.startTime,
	cbc_purchase.endTime,
	cbc_purchase.granularity,
	cbc_purchase.invoice,
  cbc_purchase.location,
  cbc_purchase.num, 
	cbc_purchase.placeOrigin,
	cbc_purchase.residualNum,
	cbc_purchase.suppleCompanyType,
	cbc_purchase.tel,
	cbc_purchase.unitPrice,
	cbc_purchase.warehouseReceipt,
	cbc_purchase.status,
	cbc_purchase.unqidCode,
  cbc_user.username,
  cbc_attr.attrname")->from('cbc_purchase')->join("cbc_user","cbc_purchase.userId = cbc_user.Id","left")->join("cbc_attr","cbc_purchase.attrId = cbc_attr.Id","left")->where('cbc_purchase.Id',$params)->get();
            return $query->result_array();
        }

    }
    /**
     * purchase_status
     * @notice 更新采购状态
     * @return	array
     * @anthor	Json.Wang
     */
    public function purchase_status($params=array()){
        if(!empty($params)){
            $arr = array(
                "status"=>$params['status'],
                "startTime"=>time()
            );
            return $this->db->where('Id',$params['Id'])->update('cbc_purchase', $arr);
        }else{
            return 0;
        }
    }
    //获取报价人信息
    public function get_all_purchaseorder($params=0){
        $query = $this->db->select("*")->from('cbc_purchase_order')->where('cbc_purchase_order.purchaseId',$params)->get();
        return $query->result_array();
    }
    //新建采购信息
    public function insertPurchase($params=array()){
        if(!empty($params)){
//            $this->db->trans_start();
            $this->db->insert('cbc_purchase', $params);
            return $this->db->insert_id();
//            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //更新供应信息
    public function update_Purchase($params = array(),$unqidCode=""){
        if(!empty($unqidCode)){
            $this->db->trans_start();
            $this->db->where('unqidCode',$unqidCode)->update('cbc_purchase', $params);
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    public function insertPurchaseOrder($params=array()){
        if(!empty($params)){
//            $this->db->trans_start();
            $this->db->insert('cbc_purchase_order', $params);
            return $this->db->insert_id();
//            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    public function update_purchaseorder($params = array(),$Id=""){
        if(!empty($Id)){
            $this->db->trans_start();
            $this->db->where('Id',$Id)->update('cbc_purchase_order', $params);
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }

}