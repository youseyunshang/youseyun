<?php
class Index_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取今日的报价数据
    public function get_reportform_log($params=0)
    {
        if ($params > 0) {
            $query = $this->db->get_where('cbc_reportform_log', array('createTime' => $params));
            return $query->row_array();
        } else {
            $query = $this->db->select("*")->from('cbc_reportform_log')->order_by("createTime", "DESC")->get();
            return $query->result_array();
        }
    }
    //成交订单
    public function get_order(){
        $query = $this->db->where('status','1')->get('cbc_order');
        return $query->result_array();
    }
    //关联查询cbc_res
//    public function get_res($where = false){
//        if($where === false){
//            $query = $this->db->select("*")->from('cbc_res')->order_by("createTime", "DESC")->get();
//            return $query->result_array();
//        }else{
//            $query = $this->db->get_where('cbc_res', array('Id' => $where['Id']));
//            return $query->row_array();
//        }
//    }
    //关联查询cbc_user/cbc_attr/cbc_question
    public function get_supple_select(){
        $query = $this->db->select("cbc_supple.Id,
	cbc_supple.billHolder,
	cbc_supple.billHolderStatus,
	cbc_supple.brandName,
	cbc_supple.title,
	cbc_supple.content,
	cbc_supple.createTime,
	cbc_supple.distributionMode,
	cbc_supple.email,
	cbc_supple.startTime,
	cbc_supple.endTime,
	cbc_supple.granularity,
	cbc_supple.invoice,
  cbc_supple.location,
  cbc_supple.num, 
  cbc_supple.minOrderNum,
	cbc_supple.placeOrigin,
	cbc_supple.productPhoto,
	cbc_supple.residualNum,
	cbc_supple.tel,
	cbc_supple.unitPrice,
	cbc_supple.warehouseReceipt,
  cbc_user.username,
  cbc_attr.attrname")->from('cbc_supple')->join("cbc_user","cbc_supple.userId = cbc_user.Id","left")->join("cbc_attr","cbc_supple.attrId = cbc_attr.Id","left")->order_by("createTime", "ASC")->get();
        return $query->result_array();
    }
    //关联查询cbc_user/cbc_attr/cbc_question
    public function get_purchase_select(){
            $query = $this->db->select("cbc_purchase.Id,
	cbc_purchase.billHolder,
	cbc_purchase.billHolderStatus,
	cbc_purchase.brandName,
	cbc_purchase.companyType,
	cbc_purchase.title,
	cbc_purchase.content,
	cbc_purchase.createTime,
	cbc_purchase.distributionMode,
	cbc_purchase.email,
	cbc_purchase.startTime,
	cbc_purchase.endTime,
	cbc_purchase.granularity,
	cbc_purchase.invoice,
  cbc_purchase.location,
  cbc_purchase.num, 
	cbc_purchase.placeOrigin,
	cbc_purchase.residualNum,
	cbc_purchase.suppleCompanyType,
	cbc_purchase.tel,
	cbc_purchase.unitPrice,
	cbc_purchase.warehouseReceipt,
	cbc_purchase.status,
  cbc_user.username,
  cbc_attr.attrname")->from('cbc_purchase')->join("cbc_user","cbc_purchase.userId = cbc_user.Id","left")->join("cbc_attr","cbc_purchase.attrId = cbc_attr.Id","left")->order_by("cbc_purchase.status","ASC")->get();
            return $query->result_array();
    }
    //获取会员信息
    public function get_userinfos()
    {
        $query = $this->db->select("*")->from('cbc_user')->get();
        return $query->result_array();
    }
    //按条件检索用户数据
    public function get_userInfo_where($params=array()){
        if(!empty($params)){
            $this->db->select("*")->from('cbc_user');
            foreach ($params as $key=>&$val){
                $this->db->where($key,$val);
            }
            $query=$this->db->get();
            return $query->result_array();
        }else{
            return 0;
        }
    }
    //注册会员
    public function inset_userinfos($params=array()){
        if(empty($params)){
            return false;
        }else{
            $data = array(
                'username' => $params['tel'],
                'tel' =>$params['tel'],
                'pwd' => md5(md5($params['pwd'])),
                'createTime' =>time()
            );
            return $this->db->insert('cbc_user', $data);
        }
    }
    //更新数据
    public function update_userInfo($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->where('tel',$params['tel'])->update('cbc_user',array("pwd"=>$params['pwd']));
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    public function get_advertisement_select(){
        $query = $this->db->select("*")->from('cbc_advertisement')->get();
        return $query->result_array();
    }
    public function get_question_select(){
        $query = $this->db->select("*")->from('cbc_question')->get();
        return $query->result_array();
    }
    //获取当前用户关注的标签
    public function get_user_sign(){

    }
}