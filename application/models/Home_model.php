<?php
class Home_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    /**
     * tableRows
     * @notice 获取表内数据
     * @return	具体表的具体行号
     * @anthor Json.Wang
     */
    public function tableRows($params = "cbc_user")
    {
        return $this->db->count_all($params);
    }
    /**
     * modify_password
     * @notice 根据管理员Id修改后台账号信息
     * @return	具体表的具体行号
     * @anthor Json.Wang
     */
    public function modify_password($params=array()){
        return $this->db->where('Id',$_SESSION['UserId'])->update('cbc_admin', $params);
    }
    /**
     * count_table
     * @notice 获取表内数据
     * @return	获取指定表的总行数
     * @anthor Json.Wang
     */
    public function count_table($tableName=""){
        if(empty($tableName)){
            return 0;
        }else{
            return $this->db->count_all("cbc_".$tableName);
        }

    }
}