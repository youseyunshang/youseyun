<?php
class Entrust_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //关联查询cbc_user/cbc_entrust
    public function get_entrust_select($params = array()){
        $query = $this->db->select("cbc_entrust.Id,cbc_entrust.title,cbc_entrust.endTime,cbc_entrust.billHolder,cbc_entrust.status,cbc_entrust.tel,cbc_entrust.num,cbc_entrust.createTime,cbc_user.username")->from('cbc_entrust')->join("cbc_user","cbc_entrust.userId = cbc_user.Id","left")->where('type',$params['type'])->get();
        return $query->result_array();
    }
    public function epurchase_status($params=array()){
        if(!empty($params)){
            return $this->db->where('Id', $params['Id'])->update('cbc_entrust',array("status"=>$params['status']));
        }else{
            return 0;
        }

    }
    //删除问题
    public function del_entrust($params=0){
        return $this->db->where('Id',$params)->delete("cbc_entrust");
    }
}