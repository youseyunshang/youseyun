<?php
class Order_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    /**
     * get_order_all
     * @notice 根据订单表的具体字段进行数据检索
     * @params $data = array() 字段集合
     * @return	检索结果
     * @anthor Json.Wang
     */
    public function get_order_all($data=array()){
        $this->db->select("*")->from('cbc_order');
        if(!empty($data)){
            foreach ($data as $key=>$val){
                $this->db->where($key,$val);
            }
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    //获取问题信息
    public function get_order()
    {
        $query = $this->db->select("cbc_order.orderNumber,
    cbc_order.content,
	cbc_order.num,
	cbc_order.purchasePerson,
	cbc_order.purchaseTel,
	cbc_order.supplePerson,
	cbc_order.suppleTel,
	cbc_order.createTime,
	cbc_order.lastTime,
	cbc_order.`status`,
	cbc_admin.username")->from('cbc_order')->join("cbc_admin","cbc_order.createId = cbc_admin.Id","left")->get();
        return $query->result_array();
    }
    //删除问题
    public function del_order($params=0){
        return $this->db->where('Id',$params)->delete("cbc_order");
    }
}