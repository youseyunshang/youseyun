<?php
class Company_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取公司信息
    public function get_cominfos($params = array("comType"=>1))
    {
        $query = $this->db->select("*")->from('cbc_company')->where("cbc_company.comType",$params['comType'])->where("cbc_company.status","1")->get();
        return $query->result_array();
    }
    //获取公司信息
    public function get_cominfos_row($params = 0)
    {
        $query = $this->db->select("*")->from('cbc_company')->where("cbc_company.Id",$params)->where("cbc_company.status","1")->get();
        return $query->row_array();
    }
    //插入公司信息
    public function inset_cominfos($params = array())
    {
        if(empty($params)){
            return false;
        }else{
            $data = array(
                'comName' => isset($params['comName'])?$params['comName']:'',
                'comType' => isset($params['comType'])?$params['comType']:'',
                'comContent' => isset($params['comContent'])?$params['comContent']:'',
                'comOrigin' => isset($params['comOrigin'])?$params['comOrigin']:'',
                'provid' => isset($params['provid'])?$params['provid']:'',
                'cityid' => isset($params['cityid'])?$params['cityid']:'',
                'location' => isset($params['location'])?$params['location']:'',
                'comPerson' => isset($params['comPerson'])?$params['comPerson']:'',
                'comTel' => isset($params['comTel'])?$params['comTel']:'',
                'comLicense' => isset($params['comLicense'])?$params['comLicense']:'',
                'comLogo' => isset($params['comLogo'])?$params['comLogo']:'',
                'createId' => $_SESSION['UserId'],
                'createTime' => time(),
                'status' => 1
            );
            return $this->db->insert('cbc_company', $data);
        }
    }
    //更新公司信息
    public function update_cominfos($params = array()){
        if(empty($params)){
            return false;
        }else{
            $data = array(
                'comName' => isset($params['comName'])?$params['comName']:'',
                'comType' => isset($params['comType'])?$params['comType']:'',
                'comContent' => isset($params['comContent'])?$params['comContent']:'',
                'comOrigin' => isset($params['comOrigin'])?$params['comOrigin']:'',
                'provid' => isset($params['provid'])?$params['provid']:'',
                'cityid' => isset($params['cityid'])?$params['cityid']:'',
                'location' => isset($params['location'])?$params['location']:'',
                'comPerson' => isset($params['comPerson'])?$params['comPerson']:'',
                'comTel' => isset($params['comTel'])?$params['comTel']:'',
                'comLicense' => isset($params['comLicense'])?$params['comLicense']:'',
                'comLogo' => isset($params['comLogo'])?$params['comLogo']:'',
                'createId' => $_SESSION['UserId'],
                'createTime' => time(),
                'status' => 1
            );
            return $this->db->where('Id', $params['Id'])->update('cbc_company', $data);
        }
    }
    //删除公司信息
    public function del_cominfos($params=0){
        return $this->db->where('Id', $params)->update('cbc_company', array("status"=>0));
//        return $this->db->where('Id',$params)->delete("cbc_company");
    }
    //插入质检订单
    public function insert_quality_testing_order($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->insert('cbc_quality_testing_order',$params);

//            $qId = $this->db->insert_id();

            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //插入物流订单
    public function insert_logistics_order($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->insert('cbc_logistics_order',$params);

//            $qId = $this->db->insert_id();

            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }

}