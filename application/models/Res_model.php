<?php
class Res_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取金属属性
    public function get_define_attr()
    {
        $query = $this->db->where('type',0)->get('cbc_attr');
        return $query->result_array();
    }
    //获取关键词标签
    public function get_sign_attr(){
        $query = $this->db->where('type',1)->get('cbc_attr');
        return $query->result_array();
    }
    //插入资源数据
    public function insert_res($params=array()){
        $this->db->trans_start();
        $this->db->insert('cbc_res', $params);
        $resId = $this->db->insert_id();
        $data = array();
        if($resId>0&&$attrData = json_decode($params['attrLog'],true)){
            foreach ($attrData as $k=>&$v){
                $data[] = array("attrId"=>$v['Id'],"resId"=>$resId);
            }
        }
        if($resId>0&&$signData = json_decode($params['signLog'],true)){
            foreach ($signData as $k=>&$v){
                $data[] = array("attrId"=>$v['Id'],"resId"=>$resId);
            }
        }
        $this->db->insert_batch('cbc_res_attr', $data);
        return $this->db->trans_complete();
    }
    //更新资源数据
    public function update_res($params = array()){
        $this->db->trans_start();
        $this->db->where('Id',$_POST['Id'])->update('cbc_res', $params);
        $this->db->where('resId',$_POST['Id'])->delete("cbc_res_attr");
        $data = array();
        if($_POST['Id']>0&&$attrData = json_decode($params['attrLog'],true)){
            foreach ($attrData as $k=>&$v){
                $data[] = array("attrId"=>$v['Id'],"resId"=>$_POST['Id']);
            }
        }
        if($_POST['Id']>0&&$signData = json_decode($params['signLog'],true)){
            foreach ($signData as $k=>&$v){
                $data[] = array("attrId"=>$v['Id'],"resId"=>$_POST['Id']);
            }
        }
        $this->db->insert_batch('cbc_res_attr', $data);
        return $this->db->trans_complete();
    }
    //关联查询cbc_res/cbc_admin
    public function get_res($where = false){
        if($where === false){
            $query = $this->db->select("*")->from('cbc_res')->get();
            return $query->result_array();
        }else{
            $query = $this->db->get_where('cbc_res', array('Id' => $where['Id']));
            return $query->row_array();
        }
    }
    //查看信息
    public function get_res_all(){
        $query = $this->db->select("cbc_res.Id,cbc_res.title,cbc_res.notice,cbc_res.attrLog,cbc_res.signLog,cbc_res.createTime,cbc_res.type,cbc_admin.username")->from('cbc_res')->join("cbc_admin","cbc_res.createId = cbc_admin.Id","left")->order_by("cbc_res.Id", "DESC")->get();
        return $query->result_array();
    }
    //删除信息
    public function del_res($params=0){
        $this->db->trans_start();
        $this->db->where('Id',$params)->delete("cbc_res");
        $this->db->where('resId',$params)->delete("cbc_res_attr");
        $this->db->where('resId',$params)->delete("cbc_user_collection_res");
        return $this->db->trans_complete();
    }
    //展示报表信息
    public function get_report_all($params = 0){
        if($params>0){
            $query = $this->db->get_where('cbc_reportform', array('Id' => $params,"status"=>1));
            return $query->row_array();
        }else{
            $query = $this->db->select("*")->from('cbc_reportform')->where("status","1")->get();
            return $query->result_array();
        }
    }
    //插入报表信息
    public function insert_report($params=array()){
        $this->db->insert('cbc_reportform', $params);
        return $this->db->insert_id();
    }
    //假删除数据
    public function del_report(){
        return $this->db->where('Id',$_POST['Id'])->update('cbc_reportform', array("status"=>0));
    }
    //删除今日产生的报价表
    public function del_out_report(){
        return $this->db->where('Id',$_POST['Id'])->delete("cbc_reportform_log");
    }
    //更新报表数据
    public function update_report($params = array()){
        return $this->db->where('Id',$_POST['Id'])->update('cbc_reportform', $params);
    }
    //批量插入报表数据
    public function insert_report_log($data = array()){
        $this->db->insert('cbc_reportform_log',$data);
        return $this->db->insert_id();
    }
    //获取报表内容列表 按时间
    public function get_reportout_list($params=0){
//        if($params>0){
//            $query = $this->db->select("*")->from('cbc_reportform_log')->get();
//            return $query->result_array();
////            $query = $this->db->get_where('cbc_reportform_log', array('createTime' => $params));
////            return $query->row_array();
//        }else{
            $query = $this->db->select("*")->from('cbc_reportform_log')->order_by("cbc_reportform_log.Id","DESC")->get();
            return $query->result_array();
//        }
    }
    //通过Id获取报表内容
    public function get_reportout_byid($params=0){
            $query = $this->db->get_where('cbc_reportform_log', array('Id' => $params));
            return $query->row_array();
    }
    //更新报表内容
    public function update_report_out($params=array()){
        return $this->db->where('Id',$_POST['Id'])->update('cbc_reportform_log', $params);
    }
    //根据时间更新报表内容
    public function update_report_time($params=array(),$time=0){
        return $this->db->where('createTime',$time)->update('cbc_reportform_log', $params);
    }
    //插入资源数据
    public function insert_news_branch($params=array(),$data=array()){
        if(!empty($params)){
            $this->db->trans_start();
            if(!empty($data)){
                $this->db->insert_batch('cbc_attr', $data);
            }
            $this->db->insert_batch('cbc_res', $params);
            return $this->db->trans_complete();
        }else{
            return 0;
        }

    }

}