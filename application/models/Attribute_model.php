<?php
class Attribute_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //获取属性
    //$params=0金属属性 $params=1自定义属性
    public function get_attr($data = array("params"=>false,"type"=>0)){
        if ($data['params'] === FALSE)
        {
            $query = $this->db->where('type',$data['type'])->get('cbc_attr');
            return $query->result_array();
        }
        $query = $this->db->get_where('cbc_attr', array('Id' => $data['Id']));
        return $query->row_array();
    }
    //获取所有标签
    public function get_attr_all(){
        $query = $this->db->get('cbc_attr');
        return $query->result_array();
    }
    //获取最后一条数据
    public function last_row_data(){
        return $this->db->order_by('id desc')->get('cbc_attr')->row_array();
    }
    //保存属性
    //保存标签
    public function save_attr($data = array()){
        $this->db->insert('cbc_attr', $data);
        return $this->db->insert_id();
    }
    //批量保存属性
    public function save_attr_all($data = array()){
        if(!empty($data)){
            $this->db->insert_batch('cbc_attr', $data);
        }
    }
    //删除属性
    public function del_attr($params){
        $this->db->where('Id',$params)->delete("cbc_attr");
    }
    //更新属性
    public function update_attr($params = array()){
        $Id = $params['Id'];
        $data = unserialize(serialize($params));
        unset($data['Id']);
        return $this->db->where('Id',$Id)->update('cbc_attr', $data);
    }
    //插入粒度
    public function insert_granularity($params=array()){
        if(!empty($params)){
//            $this->db->trans_start();
            $this->db->insert('cbc_granularity', $params);
            return $this->db->insert_id();
//            $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //插入牌号
    public function insert_brandname($params=array()){
        if(!empty($params)){
//            $this->db->trans_start();
            $this->db->insert('cbc_brandName', $params);
            return $this->db->insert_id();
//            $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //获取粒度
    public function get_granularity($data=array()){
            $this->db->select("*")->from('cbc_granularity');
            if(!empty($data)){
                foreach ($data as $key=>$val){
                    $this->db->where($key,$val);
                }
            }
            $this->db->order_by('Id desc');
            $query = $this->db->get();
            return $query->result_array();
    }
    //获取牌号
    public function get_brandname($data=array()){
        $this->db->select("*")->from('cbc_brandname');
        if(!empty($data)){
            foreach ($data as $key=>$val){
                $this->db->where($key,$val);
            }
        }
        $this->db->order_by('Id desc');
        $query = $this->db->get();
        return $query->result_array();
    }
    //更新粒度
    public function update_granularity($data=array(),$params = 0){
        if($params>0){
            return $this->db->where('Id',$params)->update('cbc_granularity', $data);
        }else {
            return 0;
        }
    }
    //更新粒度
    public function update_brandname($data=array(),$params = 0){
        if($params>0){
            return $this->db->where('Id',$params)->update('cbc_brandName', $data);
        }else {
            return 0;
        }
    }
    //删除牌号
    public function del_brandname($params = 0){
        if($params>0){
            return $this->db->where('Id',$params)->delete("cbc_brandName");
        }else {
            return 0;
        }
    }
    //删除粒度
    public function del_granularity($params = 0){
        if($params>0){
            return $this->db->where('Id',$params)->delete("cbc_granularity");
        }else {
            return 0;
        }
    }
}