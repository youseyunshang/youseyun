<?php
class Index_user_model extends CI_Model {
    private static $instance;
    public function __construct()
    {
        self::$instance =& $this;
        $this->load->database();
        $this->load->driver('cache');
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //添加问题
    public function add_question($params=array()){
        if(!empty($params)){
            return $this->db->insert('cbc_question', $params);
        }else{
            return 0;
        }
    }
    //删除问题--假删除
    public function del_question( $params = 0 ){
        if($params>0){
            return $this->db->where('Id', $params)->update('cbc_question', array("status"=>0));
        }else{
            return 0;
        }
    }
    //添加答案
    public function add_answer($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            $query = $this->db->get_where('cbc_question', array('Id' => $params['qid']));
            $data = $query->row_array();
            $this->db->where('Id', $params['qid'])->update('cbc_question', array("answermun"=>$data['answermun']+1));
            $this->db->insert('cbc_answer', $params);
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
    //添加爱好
    public function add_hobby($params=array()){
        if(!empty($params)){
            $data = self::get_hobby();
            foreach ($data as $key=>$val){
                if($val['attrId']==$params['attrId']&&self::del_hobby($val['Id'])){
                    return 2;
                    die();
                }
            }
            if(count($data)<5){
                $this->db->trans_start();
                $this->db->insert('cbc_user_attr', $params);
                if($this->db->trans_complete()){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    //获取爱好
    public function get_hobby(){

            $query = $this->db->get_where('cbc_user_attr', array('userId' =>$_SESSION["indexUserData"]["Id"]));
            $data = $query->result_array();
            return $data;

    }
    //获取爱好
    public function del_hobby($params = 0){
        $this->db->trans_start();
        $this->db->where('Id',$params)->delete("cbc_user_attr");
        return $this->db->trans_complete();

    }
    //whereIn cbc_res_attr
    public function get_res_attr_wherein($params=array()){
        if(!empty($params)){
            $this->db->select("*")->from('cbc_res_attr');
            $this->db->where_in('attrId',$params);
            $query = $this->db->get();
            return $query->result_array();
        }else{
            return 0;
        }
    }
    //whereIn检索资源
    public function getResWhereIn($params=array()){
        if(!empty($params)){
            $this->db->select("*")->from('cbc_res');
            $this->db->where_in('cbc_res.Id',$params);
            $query = $this->db->get();
            return $query->result_array();
        }else{
            return 0;
        }
    }
    //获得当前用户收藏的资源
    public function getResSessionId(){
            $query = $this->db->select("cbc_res.Id as Id,cbc_res.title,cbc_res.notice,cbc_res.photoType,cbc_res.photo,cbc_res.attrLog,cbc_res.signLog,cbc_res.origin")->from('cbc_user_collection_res')->join("cbc_res","cbc_user_collection_res.resId=cbc_res.Id")->where('cbc_user_collection_res.userId',$_SESSION["indexUserData"]["Id"])->get();
            return $query->result_array();
    }
    //获得当前用户收藏的问题
    public function getQuestionSessionId(){
        $query = $this->db->select("cbc_question.unqid,cbc_question.attrId,cbc_question.title,cbc_question.content,cbc_question.light,cbc_question.answermun")->from('cbc_user_collection_que')->join("cbc_question","cbc_user_collection_que.qId = cbc_question.unqid")->where('cbc_user_collection_que.userId',$_SESSION["indexUserData"]["Id"])->get();
        return $query->result_array();
    }
    //whereIn检索问答
    public function getQuestionWhereIn($params=array()){
        if(!empty($params)){
            $this->db->select("*")->from('cbc_question');
            $this->db->where_in('cbc_question.unqid',$params);
            $query = $this->db->get();
            return $query->result_array();
        }else{
            return 0;
        }
    }
    //whereIn检索问答
    public function getQuestionWhere($params=array()){
        if(!empty($params)){
            $this->db->select("*")->from('cbc_question');
            $this->db->where_in('cbc_question.unqid',$params);
            $query = $this->db->get();
            return $query->result_array();
        }else{
            return 0;
        }
    }
    //更新数据
    public function update_userInfo($params=array()){
        if(!empty($params)){
            $this->db->trans_start();
            $this->db->where('Id',$_SESSION["indexUserData"]["Id"])->update('cbc_user',$params);
            return $this->db->trans_complete();
        }else{
            return 0;
        }
    }
}