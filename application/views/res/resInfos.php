<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>资讯详情</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
    <style>
        .content img{
            width:100%;
        }
    </style>
</head>
<body>
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<!--首页导航end-->
<!--返回start-->
<?php include 'application/views/smallBackIcon.php';?>
<!--返回end-->
<?php
    if(empty($_SESSION['indexUserData'])&&mb_strlen($resInfo['content'])>200){
        echo '<div class="pannel-content parent-relative">
            <div class="login-hidden">
                <a href="'.ROOT_INDEX_REQUEST.'/index/login">登录观看全部内容</a>
            </div>';
    }else{
        echo '<div class="pannel-content">';
    }
?>
    <!--    资讯-->
    <div class="pannel-index">
        <div class="pannel-index-res-title" style="font-size: 0.4rem;">
            <p>
                <?php
                if(!empty($resInfo)){
                    echo $resInfo['title'];
                }
                ?>
            </p>
        </div>
        <div class="pannel-index-res-title" style="font-size: 14px;">
            <span>来源：<span>
                 <?php
                 if(!empty($resInfo['origin'])){
                     echo $resInfo['origin'];
                 }else{
                     echo '有色云商金属网';
                 }
                 ?>
            </span>
            </span>
            <span style="display: flex;flex-direction: row;justify-content: center; align-items: center;float: right;margin-right: 20px;">
                <span class="foot-icon <?php echo $resInfo['save'];?>"></span>
                <a href="javascript:;" resId="<?php
                    if(!empty($resInfo['Id'])){
                        echo $resInfo['Id'];
                    }
                ?>" onclick="resInfoObj.saveRes(this)" style="text-decoration: none;margin-left: 5px;color:#7e8396;">收藏</a>
            </span>
        </div>
        <?php
            if($resInfo['photoType']>0){
                $data = json_decode($resInfo['photo'],true);
                if(!empty($data['coverLogoOne'])){
                    echo '<div class="pannel-index-res-img">
                            <img src="'.ROOT_URL_DEFINE.'/resource/uploads'.$data['coverLogoOne'].'" style="">
                        </div>';
                }
            }
            echo "<div class='content'>";
            if(!empty($resInfo)){
                echo $resInfo['content'];
            }
            echo "</div>";
        ?>
        <div class="pannel-index-res-attr">
            <?php
            switch ($resInfo['type']){
                case 1:
                    echo '<span >资讯</span>';
                break;
                case 2:
                    echo '<span >日评</span>';
                    break;
                case 3:
                    echo '<span >周评</span>';
                    break;
                case 4:
                    echo '<span >月评</span>';
                    break;
                case 5:
                    echo '<span >新闻</span>';
                    break;
                default:
                    echo '<span >资讯</span>';
                    break;
            }
            if(!empty($resInfo['attrLogo'])){
                $data = json_decode($resInfo['attrLogo'],true);
                if(!empty($data)){
                    foreach ($data as $key=>&$val){
                        if($key>1){
                            break;
                        }
                        echo '<span >'.$val['attrname'].'</span>';
                    }
                }
            }
            ?>
        </div>
    </div>
</div>
<div class="pannel-index-thickline">
</div>
<div class="pannel-content">
    <div class="pannel-index" style=" height:50px;display: flex;flex-direction: row;justify-content: space-between; align-items: center;">
        <label style="font-family: MicrosoftYaHei;font-size: 18px;font-stretch: normal;letter-spacing: 0px;color: #0053fb;">为您推荐</label>
    </div>
    <!--资讯推荐-->
    <?php
        if(!empty($forDatas)){
            unset($val);
            foreach ($forDatas as $key=>&$val){
                $val['notice'] = mb_substr($val['notice'],0,30)."...";
                $photo = json_decode($val['photo'],true);
                if(!empty($photo['coverLogoOne'])){
                  $str = ROOT_URL_DEFINE.'/resource/uploads'.$photo['coverLogoOne'];
                }else{
                  $str = ROOT_URL_DEFINE.'/resource/indexData/indexDefault/tc1.png';
                }
                echo '<div class="pannel-index">
                            <a href="'.ROOT_INDEX_REQUEST.'/indexRes/resInfos/'.$val['Id'].'" style="text-decoration: none;color: #000;">
                                <div class="pannel-index-que-answer" style="padding:10px 0px;display: flex;flex-direction: row;justify-content: space-between; align-items: center;">
                                    <div class="pannel-index-que-answer-left" style="width: 60%;">
                                            <p>'.$val['notice'].'</p>
                                    </div>
                                    <div class="pannel-index-que-answer-right">
                                        <img src="'.$str.'" style="width: 100%;height: 60px;border-radius: 5px;">
                                    </div>
                                </div>
                                <div class="pannel-index-underline">
                                </div>
                            </a>
                        </div>';
            }
        }else{
            echo '<div class="pannel-index">数据稀少，不好意思！</div>';
        }
    ?>
    <!--资讯推荐end-->
</div>
<script>
    var resInfoObj={
        init(){
            //js格式化数据
            $.trim($(".content p").html());
            $(".content p").css("text-indent","2em");
            // $(".content p>strong").parent("p").html("<h3>"+$("p>strong").html()+"</h3>");
            $.each($(".content p"),function(k,v){
                let html = $(v).html().replace(/<\/?.+?>/g, "");
                $(v).html(html);
            });
        },
        "saveRes":function(that){
            if($(".foot-icon-save").length>0){
                window.location.href=WINDOWS_INDEX_REQUEST+"/IndexUsercenter/myCollection";
                return false;
            }
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/IndexRes/collectionRes",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : {"resId":$(that).attr("resId")},
                dataType : "text",
                success: function (response) {
                    if(response == 'login'){
                        window.location.href=WINDOWS_INDEX_REQUEST+"/Index/login";
                        return false;
                    }
                    if(response.length>0){
                        alert("收藏成功！");
                        $(".foot-icon-nosave").addClass("foot-icon-save");
                        $(".foot-icon-nosave").removeClass("foot-icon-nosave");
                    }else{
                        alert("收藏失败！");
                    }
                }
            });
        }
    }
    resInfoObj.init();
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
