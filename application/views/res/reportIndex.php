<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>今日行情</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
</head>
<body>
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<!--首页导航end-->
<div class="pannel-index">
    <div style="width:100%;height:60px;display: flex;flex-direction: row;justify-content: center; align-items: center;">
        <a href="javascript:;" onclick="reportObj.returnBack()">
            <img src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/resReturn.png" style="width:100%;">
        </a>
    </div>
    <div class="pannel-index-underline"></div>
</div>
<div class="pannel-index">
    <div class="index-pannel" style="width: 100%;overflow: hidden;overflow-x: scroll;">
        <div class="report-navigation" style="">
            <?php
                $attrAllFonts = 0;
                if(!empty($shopAttr)){
                echo '<a href="javascript:void(0);" class="select-item" onclick="reportObj.selectData(this);">全部</a>';
                foreach ($shopAttr as $key=>&$val){
                    $attrname = explode("@",$val['attrname']);
                    $attrAllFonts += mb_strlen($attrname[0]);
                    echo '<a href="javascript:void(0);" onclick="reportObj.selectData(this);">'.$attrname[0].'</a>';
                }
            }?>
        </div>
    </div>
</div>
<div class="pannel-index">
    <div class="pannel-index-formpoint-title">
        <div class="title-item title-item-firstColumn">名称/价格范围</div>
        <div class="title-item title-item-secondColumn">均价</div>
        <div class="title-item title-item-thirdColumn">涨跌</div>
    </div>
    <div class="formpoint-body">
        <?php
            foreach ($reportData as $key=>&$val){
                //文字颜色
                if( $val['updown'] > 0 ){
                    $color = 'color: #009944;';
                }elseif ( $val['updown'] < 0 ){
                    $color = 'color: #ff0000;';
                }else{
                    $color = '';
                }
                //规格
                $Specifications = isset($val['Specifications'])?$val['Specifications']:"";
                //单位
                $measurement = isset($val['measurement'])?$val['measurement']:"";
                //name
                $reportName = $val['formname'].' '.$Specifications;
                $reportNameWidth = mb_strlen($reportName,"utf-8")*16;
                echo ' <div class="pannel-index-formpoint-list" attrname="'.$val['attrNames'].'">
                        <div style="width: 60%;float:left;text-align: left;padding-left:15px;box-sizing: border-box;">
                        <div style="height: 30px;width:100%;line-height: 30px;font-size: 15px;font-weight: bold;color: #535353;overflow-x: auto;"><span style="display: inline-block;width:'.$reportNameWidth.'px;">'.$reportName.'</span></div>
                        <div style="height: 30px;line-height: 30px;'.$color.'">'.$val['lastnum'].'-'.$val['topnum'].' '.$measurement.'</div>
                        </div>
                         <div style="width: 20%;float:left;text-align: center;'.$color.'">'.$val['middlenum'].'</div>
                         <div style="width: 20%;float:left;text-align: center;box-sizing: border-box;">
                            <div style="height: 30px;line-height: 30px;color: #535353;">'.date("m",time()).'-'.date("d",time()).'</div>
                            <div style="height: 30px;line-height: 30px;'.$color.'" class="updown">'.$val['updown'].'</div>
                        </div>
                    </div>';
            }
        ?>
    </div>
</div>
<script>
    var reportObj = {
        'init':function(){
            //页面属性导航栏长度
            var attrCount = '<?php echo count($shopAttr)*37+$attrAllFonts*13;?>';
            $(".report-navigation").css("width",attrCount+"px");
        },
        'selectData':function(that){
            if($(that).html()=='全部'){
                $('.pannel-index-formpoint-list').show();
                $('.select-item').removeClass("select-item");
                $(that).addClass("select-item");
                return false;
            }else{
                $.each($(".pannel-index-formpoint-list"),function(k,v){
                    if ($(v).attr('attrname')&&$(v).attr('attrname').indexOf($(that).html()) != -1) {
                        $(v).show();
                    }else{
                        $(v).hide();
                    }
                    $('.select-item').removeClass("select-item");
                    $(that).addClass("select-item");
                });
            }
        },
        'returnBack':function(that){
            window.history.back(-1);
        }
    }
    reportObj.init();
    //滚动事件
    $(window).scroll(function() {
        $(".fix-nav").hide();
        $("#fix-nav-control").attr('pid', '0');
    });
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
