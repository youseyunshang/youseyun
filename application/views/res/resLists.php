<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>资讯</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/js/part.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
</head>
<body>
<?php include 'application/views/smallIcon.php';?>
<style>
    .search-float-left{
        width:80%;
        min-height: 500px;
        display: flex;
        flex-direction: row;
        box-sizing: border-box;
        justify-content:center;
        background: #fff;
        position: fixed;
        left:100%;
        top:0px;
        z-index: 999;
        opacity: 0.5;
    }
    .search-float-left .search-float-left-backdefault{
        width:20%;
        background: #0C0C0C;
        opacity: 0.1;
    }
    .search-float-left .search-float-left-content{
        width:90%;
    }
    .search-float-left-item-footer{
        width: 100%;
        display: flex;
        height: 80px;
        flex-direction: row;
        align-items: center;
        justify-content: space-around;
    }
    .default-small-btn{
        width: 70px;
        height: 30px;
        background-color: #f2f2f2;
        border-radius: 15px;
        display: inline-block;
        font-family: MicrosoftYaHei;
        font-size: 12px;
        font-stretch: normal;
        letter-spacing: 0px;
        color: #535353;
        text-align: center;
        line-height: 30px;
        text-decoration: none;
        margin: 10px 5px;
        box-sizing: border-box;
        border: solid 1px #f2f2f2;
    }
    .blue-small-btn{
        background-color: #ffffff;
        border: solid 1px #0053fb;
        color: #0053fb;
    }
    .search-float-left-items-header{
        width:100%;
        height:50px;
        line-height: 50px;
        font-family: MicrosoftYaHei;
        font-size: 15px;
        font-weight: bold;
        font-stretch: normal;
        letter-spacing: 0px;
        color: #000000;
        box-sizing: border-box;
        padding-left:10px;
    }
    .search-float-left-item-footer a{
        width: 100px;
        text-align: center;
        text-decoration: none;
        height: 40px;
        line-height: 40px;
        background-color: #ffffff;
        border-radius: 21px;
        border: solid 1px #e5e5e5;
        font-family: MicrosoftYaHei;
        font-size: 12px;
        font-stretch: normal;
        letter-spacing: 0px;
        color: #959595;
    }
    .search-float-left-item-footer .redBut{
        background-color: #ff0000;
        color: #ffffff;
    }
</style>
<!--右侧侧边导航start-->
<div class="search-float-left">
    <div class="search-float-left-backdefault" style="display: none;">
        暂时没用！
    </div>
    <div class="search-float-left-content">
        <div class="search-float-left-list">
            <div class="search-float-left-item-header">

            </div>
            <div class="search-float-left-item-center" style="overflow-y: auto;height: 500px;">
                <div class="search-float-left-items" id="firstAttr">
                    <div class="search-float-left-items-header">
                        金属属性
                    </div>
                    <div class="search-float-left-items-content">
                        <?php if(!empty($infoAttr)){
                            foreach ($infoAttr as $key=>&$val){
                                if($val['pid']<1){
                                    $attrname = explode("@",$val['attrname']);
                                    echo '<a href="javascript:;" class="default-small-btn" id="'.$val['Id'].'" pid="0">'.$attrname[0].'</a>';
                                }
                            }
                        }?>
                    </div>
                </div>
                <div class="search-float-left-items" id="secondAttr">
                    <div class="search-float-left-items-header">
                        属性
                    </div>
                    <div class="search-float-left-items-content">
                        <?php if(!empty($infoAttr)){
                            foreach ($infoAttr as $key=>&$val){
                                if($val['pid']>1){
                                    $attrname = explode("@",$val['attrname']);
                                    echo '<a href="javascript:;" class="default-small-btn" id="'.$val['Id'].'" pid="'.$val['pid'].'">'.$attrname[0].'</a>';
                                }
                            }
                        }?>
                    </div>
                </div>
                <div class="search-float-left-items" id="resType">
                    <div class="search-float-left-items-header">
                       类型
                    </div>
                    <div class="search-float-left-items-content" style="overflow-y: auto;height:100px;">
                        <a href="javascript:;" class="default-small-btn blue-small-btn" value="all">全部</a>
                        <a href="javascript:;" class="default-small-btn" value="1">资讯</a>
                        <a href="javascript:;" class="default-small-btn" value="2">日评</a>
                        <a href="javascript:;" class="default-small-btn" value="3">周评</a>
                        <a href="javascript:;" class="default-small-btn" value="4">月评</a>
                        <a href="javascript:;" class="default-small-btn" value="5">新闻</a>
                    </div>
                </div>
            </div>
            <div class="search-float-left-item-footer">
                <a href="javascript:;" onclick="res.allAttrData();">全部</a>
                <a href="javascript:;" class="redBut" onclick="res.selectAttrData();">确定</a>
            </div>
        </div>
    </div>
</div>
<!--右侧侧边导航end-->
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<!--首页导航end-->
<div class="pannel-index sheet" id="sheet">
    <div class="index-navigation">
        <a href="<?php echo ROOT_INDEX_REQUEST?>/index/indexDefault"><span>推荐</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexRes/resLists" style="font-size: 18px;color: #0053fb;"><span>资讯</span><span class="underline-nav"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexQuestion/questionLists"><span>问答</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/indexProduct/index"><span>交易</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/indexUsercenter/index"><span>我的</span><span class="underline-nav" style="display: none;"></span></a>
    </div>
</div>
<div class="pannel-index" id="sheetAttr">
    <div class="index-pannel sheetAttr">
        <div class="report-navigation" style="">
            <?php
                $attrAllFonts = 0;
                if(!empty($infoAttr)){
                echo '<a href="javascript:void(0);" class="select-item" attrId="all" onclick="res.selectData(this);">全部</a>';
                foreach ($infoAttr as $key=>&$val){
                    $attrname = explode("@",$val['attrname']);
                    $len = $key*60;
                    $attrAllFonts += mb_strlen($attrname[0]);
                    echo '<a href="javascript:void(0);" len="'.$len.'" attrId="'.$val['Id'].'" onclick="res.selectData(this);">'.$attrname[0].'</a>';
                }
            }?>
        </div>
    </div>
    <span class="report-navigation-getMore">
    </span>
</div>
<div class="pannel-index">
    <div style="width:100%;height:60px;display: flex;flex-direction: row;justify-content: center; align-items: center;">
        <a href="javascript:;" onclick="res.showMoreReport(this);">
            <img src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/resLoading.png" style="width:100%;">
        </a>
    </div>
</div>
<div class="pannel-content">
</div>
<div class="pannel-index">
    <div style="width:100%;height:50px;display: flex;flex-direction: row;justify-content: center; align-items: center;" id="reload">
        <img src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/reload2.gif" style="width:40%;">
    </div>
</div>
<script>
    var totalHeight = $(document).height(),seeHeight = $(window).height(),scrollIn=true,scrollTop = $(window).scrollTop();
    // 资源对象数据
    //searchAttr 检索的属性
    //resType 检索的资源类型
    //init 初始化数据
    //showMoreReport 报表详情
    //fixNavShow 头部导航伸缩
    //selectData 检索数据
    //loadingData 加载数据
    var res = {
        'searchAttr':null,
        'filterArr':{},
        'resType':null,
        'init':function(){
            //页面属性导航栏长度
            var attrCount = '<?php echo count($infoAttr)*37+$attrAllFonts*13;?>';
            $(".report-navigation").css("width",attrCount+"px");
            //更新侧面导航高度
            let height = seeHeight+100;
            $(".search-float-left").css("min-height",height+"px");
            $(".search-float-left").css("overflow-y","scroll");
            let conHeight = seeHeight-100;
            $(".search-float-left-item-center").css("min-height",conHeight+"px");
            //侧面导航检索数据
            $("#firstAttr").find(".default-small-btn").click(function(){
                let id=$(this).attr("id");
                    //样式
                    $("#firstAttr").find(".blue-small-btn").removeClass("blue-small-btn");
                    $(this).addClass("blue-small-btn");
                    //二级分类
                    $("#secondAttr").find('a').hide();
                    $("#secondAttr").find('a[pid="'+id+'"]').show();
            });
            $("#secondAttr").find(".default-small-btn").click(function(){
                    //样式
                    $("#secondAttr").find(".blue-small-btn").removeClass("blue-small-btn");
                    $(this).addClass("blue-small-btn");
            });
            $("#resType").find(".default-small-btn").click(function(){
                //样式
                $("#resType").find(".blue-small-btn").removeClass("blue-small-btn");
                $(this).addClass("blue-small-btn");
            });
            //初始化数据
            $("#firstAttr").find(".default-small-btn").first().trigger("click");
            $(".report-navigation-getMore").click(function(){
                scrollIn = false;
                $("body").append("<div style='width: 100%;height: 800px;background: #000;opacity: 0.7;position: fixed;left: 0px;top: 0px;z-index: 990;' class='mask'></div>");
                $("body").attr("style","overflow:hidden;");
                $(".search-float-left").animate({left:"20%",opacity:'1'},500);
            });
            //事件绑定数据
            $("body").on("click",".mask",function(event){
                scrollIn = true;
                //消除动画
                $(".mask").remove();
                $("body").removeAttr("style");
                $(".search-float-left").animate({left:"100%",opacity: "0.7"},500);
            });
            //图片报错
            document.addEventListener("error", function (e) {
                var elem = e.target;
                if (elem.tagName.toLowerCase() == 'img') {
                    elem.src = "<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/error.png";
                }
            }, true);
            //默认进来加载数据
            $("#sheetAttr").find("a[attrId='all']").trigger("click");
        },
        'showMoreReport':function(that){
            //报表详情
            $(".pannel-index-formpoint-list").removeClass("hidden-block");
            $(that).attr("href",WINDOWS_INDEX_REQUEST+"/indexRes/reportIndex");
        },
        'selectData':function(that){
            //页面数据选择检索
            $(".select-item").removeClass("select-item");
            $(that).addClass("select-item");
            $("#sheetAttr").find(".sheetAttr").scrollLeft($(that).attr('len'));
            $(".pannel-content").empty();
            scrollIn = true;
            res.filterArr = {};
            res.searchAttr = null;
            if($(that).attr("attrId")!="all"){
                res.searchAttr = $(that).attr("attrId");
                res.loadingData({'indexMoreToken':'23478979345893654506','filterArr':res.filterArr,'searchAttr':res.searchAttr});
            }else{
                res.loadingData({'indexMoreToken':'23478979345893654506','filterArr':res.filterArr,'searchAttr':res.searchAttr,'resType':res.resType});
            }
        },
        'loadingData':function(obj){
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/IndexRes/resLists",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : obj,
                dataType : "json",
                success: function (response) {
                    if(response.code=="200"){
                        var data = response.data.response,html='';
                        $.each(data,function(k,v){
                            //存储数据
                            res.filterArr[v.totalUnqid]=1;
                            html='';
                            switch (v.totalType) {
                                case 'res':
                                    switch (v.photoType) {
                                        case "0":
                                            // 无图
                                            resPart.resNoPicture(v);
                                            break;
                                        case "1":
                                            // 大图
                                            resPart.resBigPicture(v);
                                            break;
                                        case "2":
                                            //小图
                                            resPart.resBigPicture(v);
                                            break;
                                        case "3":
                                            // 三图
                                            resPart.resThreePicture(v);
                                            break;
                                        default:
                                            // 无图
                                            resPart.resNoPicture(v);
                                            break;
                                    }
                                    break;
                                case 'advert':
                                    switch (v.photoType) {
                                        case "1":
                                            // 大图
                                            advertPart.advertBigPicture(v);
                                            break;
                                        case "2":
                                            //小图
                                            advertPart.advertBigPicture(v);
                                            break;
                                        case "3":
                                            // 三图
                                            advertPart.advertThreePicture(v);
                                            break;
                                        default:
                                            // 大图
                                            advertPart.advertBigPicture(v);
                                            break;
                                    }
                                    break;
                            }
                        });
                        scrollIn = true;
                        $("#reload").html('<img src="'+WINDOWS_URL_DEFINE+'/resource/indexData/indexDefault/reload2.gif" style="width:40%;">');
                    }else{
                        $("#reload").html("<span style='font-size: 12px;color: #BBB;'>没有更多数据了</span>");
                    }
                }
            });
        },
        'selectAttrData':function(){
            //消除动画
            $(".mask").remove();
            $("body").removeAttr("style");
            $(".search-float-left").animate({left:"100%",opacity: "0.5"},500);
            //属性Id
            res.searchAttr = $("#secondAttr").find(".blue-small-btn").attr("id");
            if(!res.searchAttr){
                res.searchAttr = $("#firstAttr").find(".blue-small-btn").attr("id");
            }
            //资源类型
            if($("#resType").find(".blue-small-btn").attr("value")!='all'){
                res.resType = $("#resType").find(".blue-small-btn").attr("value");
            }
            $("#sheetAttr").find("a[attrId='"+res.searchAttr+"']").trigger("click");
        },
        'allAttrData':function(){
            //消除动画
            $(".mask").remove();
            $("body").removeAttr("style");
            $(".search-float-left").animate({left:"100%",opacity: "0.5"},500);
            res.resType = null;
            //资源类型
            $("#sheetAttr").find("a[attrId='all']").trigger("click");
        }
    }
    res.init();
    //滚动事件
    $(window).scroll(function(){
        scrollTop = $(window).scrollTop();
        totalHeight = $(document).height();
        //导航显示
        if(scrollTop>100){
            $("#sheet").addClass('fix-nav-sheet');
            $("#sheetAttr").addClass('fix-nav-sheetAttr');
        }else{
            $("#sheet").removeClass('fix-nav-sheet');
            $("#sheetAttr").removeClass('fix-nav-sheetAttr');
        }
        if(scrollTop+seeHeight+100>totalHeight&&scrollIn){
            scrollIn = false;
            res.loadingData({'indexMoreToken':'23478979345893654506','filterArr':res.filterArr,'searchAttr':res.searchAttr});
        }
    });
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
