<!DOCTYPE html>
<html>
<head>
    <title>我的供求</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/myProduct/index.css">
</head>
<body>
    <div id="app" v-cloak>
        <div class="gray"></div>
        <tops :title="'我的供求'"></tops>
        <div class="taps">
            <span :class="{checked: taps === 'supple'}" @click="resClick('supple')">供应</span>
            <span :class="{checked: taps === 'purchase'}" @click="resClick('purchase')">求购</span>
        </div>
        <div class="main">
            <div v-if="taps === 'supple'">
                <div class="product" v-for="i in suppleList">
                    <img :src="i.productPhoto" alt="">
                    <div class="right">
                        <div class="title">{{i.title}}</div>
                        <div class="num">库存：{{i.num}}/吨({{i.minOrderNum}}吨起订)</div>
                        <div class="price"><i>{{i.unitPrice}}</i>元/吨</div>
                        <div class="more" @click="suppleSelected(i.Id)"></div>
                        <div class="operate" style="display: none;" :suppleId="i.Id" sonId="0">
                            <span v-if="i.status == 2" @click="update(i,'supple');">编辑</span>
                            <span>删除</span>
                            <span>下架</span>
                            <span>分享</span>
                        </div>
                    </div>
                </div>
            </div>
            <div v-if="taps === 'purchase'">
                <div class="product" v-for="i in purchaseList">
                    <img :src="i.productPhoto" alt="">
                    <div class="right">
                        <div class="title">{{i.title}}</div>
                        <div class="num">库存：{{i.num}}/吨({{i.minOrderNum}}吨起订)</div>
                        <div class="price"><i>{{i.unitPrice}}</i>元/吨</div>
                        <div class="more" @click="purchaseSelected(i.Id)"></div>
                        <div class="operate" style="display: none;" :purchaseId="i.Id" sonId="0">
                            <span v-if="i.status == 2" @click="update(i,'purchase');">编辑</span>
                            <span>删除</span>
                            <span>下架</span>
                            <span>分享</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var supple = eval('('+'<?php echo $supple?>'+')'),//供应列表
            purchase = eval('('+'<?php echo $purchase?>'+')');//采购列表
        console.log(supple);
        console.log(purchase);
    </script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/myProduct/index.js"></script>
</body>
</html>
    
