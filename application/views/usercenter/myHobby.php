<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>喜好标签</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/js/part.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
    <style>
        body,html{
            background: #eaeaea;
        }
        .hobby-labels{
            font-size: 14px;
            padding: 5px 20px;
            display: inline-block;
            color: #999;
        }
        .hobby-item-blue{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #0052fb;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-blue{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #0052fb;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-green{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background:#00b8ff;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-yellow{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #fec91b;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-black{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #bebebe;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-default{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #eaeaea;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item{
            text-decoration: none;
            display: inline-block;
            color: #0052fb;
            border: 1px solid #0C78DD;
            padding: 2px 10px;
            border-radius: 20px;
        }
    </style>
</head>
<body>
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<!--首页导航end-->
<!--返回start-->
<?php include 'application/views/smallBackIcon.php';?>
<!--返回end-->
<div class="pannel-index">
    <div style="    text-align: center;
    font-size: 18px;
    color: #00f;
    height: 30px;
    line-height: 30px;">喜好标签</div>
    <div style="text-align: center;
    font-size: 14px;
    height: 40px;
    line-height: 40px;
    color: #72d4ff;">选择您的喜好，定义您的内容</div>
</div>
<label class="hobby-labels">金属</label>
<div class="pannel-content">
    <?php
        if(!empty($allAttr)){
            foreach ($allAttr as $key=>&$val){
                $item = $key%5;
                $backClass = 'hobby-item-default';
                switch ($item){
                    case 0:
                        $backClass = 'hobby-item-blue';
                        break;
                    case 1:
                        $backClass = 'hobby-item-green';
                        break;
                    case 2:
                        $backClass = 'hobby-item-yellow';
                        break;
                    case 3:
                        $backClass = 'hobby-item-black';
                        break;
                    case 4:
                        $backClass = 'hobby-item-default';
                        break;
                }
                if($val['type']==0){
                    $attrname = explode("@",$val['attrname']);
                    if(count($attrname)>1){
                        $icon = $attrname[1];
                        $val['attrname'] = $attrname[0];
                    }else{
                        $icon = '云';
                    }
                    $swatch = "+ 关注";
                    if(isset($hobby[$val['Id']])){
                        $swatch = "<span style='color:#999;'>取消关注</span>";
                    }
                    echo '<div class="pannel-index">
                            <div class="pannel-index-res-content" style="height:50px;display: flex;flex-direction: row;align-items: center;box-sizing: border-box;justify-content:space-between;background: #fff;font-size: 14px;">
                                <span>
                                    <a href="javascript:;" class="'.$backClass.'">'.$icon.'</a>
                                    <span>'.$val['attrname'].'</span>
                                </span>
                                <a href="javascript:;" onclick="myhobby.attr(this);" attr="'.$val['Id'].'" class="hobby-item">'.$swatch.'</a>
                            </div>
                        </div>
                        <div class="pannel-index-underline">
                        </div>';
                }
            }
        }
    ?>
</div>
<label class="hobby-labels">金属形态</label>
<div class="pannel-content">
    <?php
    if(!empty($allAttr)){
        foreach ($allAttr as $key=>&$val){
            $item = $key%5;
            $backClass = 'hobby-item-default';
            switch ($item){
                case 0:
                    $backClass = 'hobby-item-blue';
                    break;
                case 1:
                    $backClass = 'hobby-item-green';
                    break;
                case 2:
                    $backClass = 'hobby-item-yellow';
                    break;
                case 3:
                    $backClass = 'hobby-item-black';
                    break;
                case 4:
                    $backClass = 'hobby-item-default';
                    break;
            }
            if($val['type']==3){
                $attrname = explode("@",$val['attrname']);
                if(count($attrname)>1){
                    $icon = $attrname[1];
                    $val['attrname'] = $attrname[0];
                }else{
                    $icon = '云';
                }
                $swatch = "+ 关注";
                if(isset($hobby[$val['Id']])){
                    $swatch = "<span style='color:#999;'>取消关注</span>";
                }
                echo '<div class="pannel-index">
                            <div class="pannel-index-res-content" style="height:50px;display: flex;flex-direction: row;align-items: center;box-sizing: border-box;justify-content:space-between;background: #fff;font-size: 14px;">
                                <span>
                                    <a href="javascript:;" class="'.$backClass.'">'.$icon.'</a>
                                    <span>'.$val['attrname'].'</span>
                                </span>
                                <a href="javascript:;" onclick="myhobby.attr(this);" attr="'.$val['Id'].'" class="hobby-item">'.$swatch.'</a>
                            </div>
                        </div>
                        <div class="pannel-index-underline">
                        </div>';
            }
        }
    }
    ?>
</div>
<label class="hobby-labels">喜好</label>
<div class="pannel-content">
    <?php
    if(!empty($allAttr)){
        foreach ($allAttr as $key=>&$val){
            $item = $key%5;
            $backClass = 'hobby-item-default';
            switch ($item){
                case 0:
                    $backClass = 'hobby-item-blue';
                    break;
                case 1:
                    $backClass = 'hobby-item-green';
                    break;
                case 2:
                    $backClass = 'hobby-item-yellow';
                    break;
                case 3:
                    $backClass = 'hobby-item-black';
                    break;
                case 4:
                    $backClass = 'hobby-item-default';
                    break;
            }
            if($val['type']==1){
                $attrname = explode("@",$val['attrname']);
                if(count($attrname)>1){
                    $icon = $attrname[1];
                    $val['attrname'] = $attrname[0];
                }else{
                    $icon = '云';
                }
                $swatch = "+ 关注";
                if(isset($hobby[$val['Id']])){
                    $swatch = "<span style='color:#999;'>取消关注</span>";
                }
                echo '<div class="pannel-index">
                            <div class="pannel-index-res-content" style="height:50px;display: flex;flex-direction: row;align-items: center;box-sizing: border-box;justify-content:space-between;background: #fff;font-size: 14px;">
                                <span>
                                    <a href="javascript:;" class="'.$backClass.'">'.$icon.'</a>
                                    <span>'.$val['attrname'].'</span>
                                </span>
                                <a href="javascript:;" onclick="myhobby.attr(this);" attr="'.$val['Id'].'" class="hobby-item">'.$swatch.'</a>
                            </div>
                        </div>
                        <div class="pannel-index-underline">
                        </div>';
            }
        }
    }
    ?>
</div>
<script>
    var myhobby={
        'attr':function(that){
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/IndexUsercenter/addMyHobby",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : {"attrId":$(that).attr("attr")},
                dataType : "text",
                success: function (response) {
                    switch (response) {
                        case "0":
                            alert("最多关注五个标签！");
                            break;
                        case "1":
                            $(that).html("<span style='color:#999;'>取消关注</span>");
                            break;
                        case "2":
                            $(that).html("+ 关注");
                            break;
                        default:
                            alert("最多关注五个标签！");
                            break;
                    }
                }
            });
        }
    }
</script>
</body>

</html>
