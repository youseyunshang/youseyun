<!DOCTYPE html>
<html>
<head>
    <title>我的问答</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/myQAndA/index.css">
</head>
<body>
<div id="app" v-cloak>
    <div class="gray"></div>
    <tops :title="'我的问答'"></tops>
    <div class="taps">
        <span :class="{checked: taps}" @click="taps = true">问题</span>
        <span :class="{checked: !taps}" @click="taps = false">回答</span>
    </div>
    <div class="QList" v-if="taps">
        <div class="question" v-for="i in QList">
            <div class="title">{{i.title}}</div>
            <div class="num">{{i.num}}个回答</div>
        </div>
    </div>
    <div class="AList" v-else>
        <div class="answer" v-for="i in AList">
            <div class="title">{{i.title}}</div>
            <div class="desc">{{i.desc}}</div>
            <div class="time">{{i.time}}</div>
        </div>
    </div>
    <div class="nomore">没有更多内容</div>
</div>

<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/myQAndA/index.js"></script>
</body>
</html>

