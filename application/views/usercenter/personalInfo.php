<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/config.js"></script>
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/croppimg/cropper.min.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/croppimg/ImgCropping.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/personalInfo/index.css">
</head>
<body>
<div id="app" v-cloak>
    <tops :title="'编辑个人资料'"></tops>
    <div class="notice" v-if="showNotice" @click="showNotice = false">
        <span>您填写的内容将用于个人页展示及内容推荐</span>
        <i></i>
    </div>
    <div class="head" @click="uploadPicture">
        <img :src="userInfo.avatar" id="userInfoImg" style="width: 100%;height: 100%;border-radius: 100px;"/>
    </div>
    <input type="file" style="display: none;" accept="image/*" @change="handleFile" class="hiddenInput"/>
    <div class="form">
        <div class="title">基本信息</div>
        <div class="item">
            <span>昵称</span>
            <input type="text" v-model="userInfo.realname" maxlength="10">
        </div>
        <div class="item">
            <span>性别</span>
            <div class="sexs">
                <label class="sex" @click="sexs('1')">
                    <input type="radio" name="sex" value="1" v-model='userInfo.sex'>
                    <span>男</span>
                </label>
                <label class="sex" @click="sexs('0')">
                    <input type="radio" name="sex" value="0" v-model='userInfo.sex'>
                    <span>女</span>
                </label>
            </div>
        </div>
<!--        <div class="item">-->
<!--            <span>居住地</span>-->
<!--            <div></div>-->
<!--            <i></i>-->
<!--        </div>-->
        <div class="item">
            <span>手机号</span>
            <div>{{userInfo.tel}}</div>
            <i></i>
        </div>
<!--        <div class="title mt64">企业信息</div>-->
<!--        <div class="item">-->
<!--            <span>所属企业</span>-->
<!--            <div>未填写</div>-->
<!--            <i></i>-->
<!--        </div>-->
<!--        <div class="item">-->
<!--            <span>商家类型</span>-->
<!--            <div>未填写</div>-->
<!--            <i></i>-->
<!--        </div>-->
        <div class="title mt64">添加认证</div>
<!--        <div class="item">-->
<!--            <span>个人认证</span>-->
<!--            <div>未认证</div>-->
<!--            <i></i>-->
<!--        </div>-->
        <div class="item" @click="companyValid()">
            <span>企业认证</span>
            <div>{{userInfo.grade}}</div>
            <i></i>
        </div>
    </div>
    <button @click="submit">保存</button>
</div>
<a href="javascript:;" id="replaceImg" style="display: none;" class="l-btn">更换图片</a>
<!--<div style="width: 80px;height: 80px;border: solid 1px #555;padding: 5px;margin-top: 10px">-->
<!--    <img id="finalImg" src="" width="100%">-->
<!--</div>-->


<!--图片裁剪框 start-->
<div style="display: none" class="tailoring-container">
    <div class="black-cloth" onclick="closeTailor(this)"></div>
    <div class="tailoring-content">
        <div class="tailoring-content-one">
            <label title="上传图片" for="chooseImg" class="l-btn choose-btn">
                <input type="file" accept="image/jpg,image/jpeg,image/png" name="file" id="chooseImg" class="hidden" onchange="selectImg(this)">
                选择图片
            </label>
            <div class="close-tailoring"  onclick="closeTailor(this)">×</div>
        </div>
        <div class="tailoring-content-two">
            <div class="tailoring-box-parcel">
                <img id="tailoringImg">
            </div>
            <div class="preview-box-parcel">
                <p>图片预览：</p>
                <div class="square previewImg"></div>
                <div class="circular previewImg"></div>
            </div>
        </div>
        <div class="tailoring-content-three">
            <a href="javascript:;" class="l-btn cropper-reset-btn">复位</a>
            <a href="javascript:;" class="l-btn cropper-rotate-btn">旋转</a>
            <a href="javascript:;" class="l-btn cropper-scaleX-btn">换向</a>
            <a href="javascript:;" class="l-btn sureCut" id="sureCut">确定</a>
        </div>
    </div>
</div>
<!--图片裁剪框 end-->
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/vue/croppimg/cropper.min.js" ></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/personalInfo/index.js"></script>
<script>
    //弹出框水平垂直居中
    (window.onresize = function () {
        var win_height = $(window).height();
        var win_width = $(window).width();
        if (win_width <= 768){
            $(".tailoring-content").css({
                "top": (win_height - $(".tailoring-content").outerHeight())/2,
                "left": 0
            });
        }else{
            $(".tailoring-content").css({
                "top": (win_height - $(".tailoring-content").outerHeight())/2,
                "left": (win_width - $(".tailoring-content").outerWidth())/2
            });
        }
    })();

    //弹出图片裁剪框
    $("#replaceImg").on("click",function () {
        $(".tailoring-container").toggle();
    });
    //图像上传
    function selectImg(file) {
        if (!file.files || !file.files[0]){
            return;
        }
        var reader = new FileReader();
        reader.onload = function (evt) {
            var replaceSrc = evt.target.result;
            //更换cropper的图片
            $('#tailoringImg').cropper('replace', replaceSrc,false);//默认false，适应高度，不失真
        }
        reader.readAsDataURL(file.files[0]);
    }
    //cropper图片裁剪
    $('#tailoringImg').cropper({
        aspectRatio: 1/1,//默认比例
        preview: '.previewImg',//预览视图
        guides: false,  //裁剪框的虚线(九宫格)
        autoCropArea: 0.3,  //0-1之间的数值，定义自动剪裁区域的大小，默认0.8
        movable: false, //是否允许移动图片
        dragCrop: true,  //是否允许移除当前的剪裁框，并通过拖动来新建一个剪裁框区域
        movable: true,  //是否允许移动剪裁框
        resizable: false,  //是否允许改变裁剪框的大小
        zoomable: true,  //是否允许缩放图片大小
        mouseWheelZoom: false,  //是否允许通过鼠标滚轮来缩放图片
        touchDragZoom: true,  //是否允许通过触摸移动来缩放图片
        rotatable: true,  //是否允许旋转图片
        crop: function(e) {
            // 输出结果数据裁剪图像。
        }
    });
    //旋转
    $(".cropper-rotate-btn").on("click",function () {
        $('#tailoringImg').cropper("rotate", 45);
    });
    //复位
    $(".cropper-reset-btn").on("click",function () {
        $('#tailoringImg').cropper("reset");
    });
    //换向
    var flagX = true;
    $(".cropper-scaleX-btn").on("click",function () {
        if(flagX){
            $('#tailoringImg').cropper("scaleX", -1);
            flagX = false;
        }else{
            $('#tailoringImg').cropper("scaleX", 1);
            flagX = true;
        }
        flagX != flagX;
    });

    //裁剪后的处理
    $("#sureCut").on("click",function () {
        if ($("#tailoringImg").attr("src") == null ){
            return false;
        }else{
            var cas = $('#tailoringImg').cropper('getCroppedCanvas');//获取被裁剪后的canvas
            var base64url = cas.toDataURL('image/png'); //转换为base64地址形式
            // $("#finalImg").prop("src",base64url);//显示为图片的形式
            $("#userInfoImg").prop("src",base64url);//显示为图片的形式
            //关闭裁剪框
            closeTailor();
        }
    });
    //关闭裁剪框
    function closeTailor() {
        $(".tailoring-container").toggle();
    }
</script>
</body>
</html>

