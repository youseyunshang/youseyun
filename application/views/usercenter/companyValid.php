<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/verification/index.css">
</head>
<body>
    <div id="app" v-cloak>
        <tops :title="'编辑个人资料'"></tops>
        <div class="notice" v-if="showNotice" @click="showNotice = false">
            <span>完善企业基本资料，经平台审核后，可升级成认证会员</span>
            <i></i>
        </div>
        <div class="form">
            <div class="wrap">
                <span>企业LOGO</span>
                <label class="logo">
                    <input v-show="false" type="file" accept="image/*" multiple @change="handleChange">
                    <img v-if="userInfo.comLicense" :src="IMG_URL + userInfo.comLicense" alt="">
                    <div v-else class="logos"></div>  
                </label>
            </div>
            <div class="item">
                <span>企业名称</span>
                <input type="text" v-model="userInfo.comName">
            </div>
            <div class="item" @click="show1">
                <span>企业类型</span>
                <div></div>
                <i></i>
            </div>
            <div class="wrap">
                <span>企业介绍</span>
                <textarea placeholder="请输入您的企业介绍 " v-model="userInfo.comContent"></textarea>
            </div>
            <div class="item" @click="show2">
                <span>主营铁合金</span>
                <div></div>
                <i></i>
            </div>
            <div class="item">
                <span>企业地址</span>
                <input type="text" v-model="userInfo.comLocation">
            </div>
            <div class="item">
                <span>企业联系人</span>
                <input type="text" v-model="userInfo.comPerson">
            </div>
            <div class="item">
                <span>手机</span>
                <input type="text" v-model="userInfo.comTel">
            </div>
            <div class="important">多个手机号必须用英文","分隔,并确保手机格式输入正确 </div>
        </div>
        <button @click="submit">保存</button>
        <div class="typeList" v-if="showType">
            <div class="title">企业类型</div>
            <div class="list">
                <div v-for="i in typeList" :class="{choose: i.id === chooseId}" @click="chooseType(i.id)">{{i.name}}</div>
            </div>
            <div class="close" @click="closeShow">确定</div>
        </div>
        <div class="typeList" v-if="showType1">
            <div class="title">主营范围</div>
            <div class="list1">
                <div class="left">
                    <div v-for="i in originList" :class="{choose: i.id === chooseId1}" @click="chooseType1(i)">{{i.name}}</div>
                </div>
                <div class="right">
                    <div v-for="e in childrenList" :class="{choose: e.id === chooseId2}" @click="chooseType2(e.id)">{{e.name}}</div>
                </div>
            </div>
            <div class="close" @click="closeShow">确定</div>
        </div>
    </div>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/verification/index.js"></script>
</body>
</html>
    
