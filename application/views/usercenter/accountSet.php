<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>喜好标签</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/js/part.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
    <style>
        body,html{
            background: #eaeaea;
        }
        .hobby-labels{
            font-size: 14px;
            padding: 5px 20px;
            display: inline-block;
            color: #999;
        }
        .hobby-item-blue{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #0052fb;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-blue{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #0052fb;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-green{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background:#00b8ff;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-yellow{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #fec91b;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-black{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #bebebe;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item-default{
            width: 40px;
            height: 40px;
            display: inline-block;
            line-height: 40px;
            text-align: center;
            border-radius: 10px;
            background: #eaeaea;
            text-decoration: none;
            color: #fff;
        }
        .hobby-item{
            text-decoration: none;
            display: inline-block;
            color: #0052fb;
            border: 1px solid #0C78DD;
            padding: 2px 10px;
            border-radius: 20px;
        }
    </style>
</head>
<body>
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<div class="pannel-content">
    <a href="javascript:;" onclick="myaccountSet.outlogin();" style="text-decoration: none;color: #666;">
        <div class="pannel-index">
            <div class="pannel-index-res-content" style="height:50px;display: flex;flex-direction: row;align-items: center;box-sizing: border-box;justify-content:space-between;background: #fff;font-size: 14px;">
                <span>
                    <span class="foot-icon" style="background-position: 300px -405px;width: 30px;height: 30px;"></span>
                    <span style="position: relative;left: 10px;top: -5px;">退出登录</span>
                </span>
                <a href="javascript:;">
                    <span class="foot-icon" style="background-position: 354px -126px;"></span>
                </a>
            </div>
        </div>
        <div class="pannel-index-underline">
        </div>
    </a>
</div>
<script>
    var myaccountSet={
        'outlogin':function(that){
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/index/logoutUser",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : {},
                dataType : "text",
                success: function (response) {
                    window.location.href=WINDOWS_INDEX_REQUEST+"/index/indexDefault";
                }
            });
        }
    }
</script>
</body>

</html>
