<!DOCTYPE html>
<html>
<head>
    <title>我的收藏</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/myCollection/index.css">
</head>
<body>
<div id="app" v-cloak>
    <div class="gray"></div>
    <tops :title="'我的收藏'"></tops>
    <div class="taps">
        <span :class="{checked: taps === 'information'}" @click="resClick('information')">资讯</span>
<!--        <span :class="{checked: taps === 'quotation'}" @click="resClick('quotation')">行情</span>-->
        <span :class="{checked: taps === 'QAndA'}" @click="resClick('QAndA')">问答</span>
    </div>
    <div class="informationList" v-if="taps === 'information'">
        <div class="information" v-for="i in informationList">
            <div class="title" @click="resInfos(i.id)">{{i.title}}</div>
            <div class="source">来源：{{i.source}} {{i.time}}</div>
            <div class="main" @click="resInfos(i.id)">
                <div class="oneBig" v-if="i.type === 'oneBig'">
                    <img :src="i.imgUrl" alt="">
                    <div class="desc">{{i.desc}}</div>
                </div>
                <div class="oneSmall" v-if="i.type === 'oneSmall'">
                    <div class="desc1">{{i.desc}}</div>
                    <img :src="i.imgUrl" alt="">
                </div>
                <div class="threeSmall" v-if="i.type === 'threeSmall'">
                    <div class="imgList">
                        <img v-for="el in i.imgUrl" :src="el" alt="">
                    </div>
                    <div class="desc">{{i.desc}}</div>
                </div>
                <div class="none" v-if="i.type === 'none'">
                    <div class="desc">{{i.desc}}</div>
                </div>
            </div>
            <div class="bottom">
                <div class="labelList">
                    <span v-for="item in i.label">{{item}}</span>
                </div>
                <div class="more" @click="showId = i.id">
                    <div class="operate" v-if="showId === i.id" @click.stop="showId = ''">
                        <span>删除</span>
                        <span>分享</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="quotationList" v-if="taps === 'quotation'" style="display: none;">
        <div class="quotation" v-for="i in quotationList">
            <div class="headers">
                <div class="time">
                    {{i.time}}
                    <span>{{i.type}}</span>
                </div>
                <div class="more" @click="showId1 = i.id">
                    <div class="operate" v-if="showId1 === i.id" @click.stop="showId1 = ''">
                        <span>删除</span>
                        <span>分享</span>
                    </div>
                </div>
            </div>
            <div class="main">
                <div class="title">
                    <span class="flex1">名称/价格范围</span>
                    <P><span>均价</span><span>跌涨</span></P>
                </div>
                <div class="item" v-for="item in i.children">
                    <div class="top">
                        <span class="title">{{item.title}}</span>
                        <P><span></span><span>{{item.time}}</span></P>
                    </div>
                    <div class="bottom">
                        <span class="price">{{item.price}}<i>元/吨</i></span>
                        <P :class="item.type === 'up' ? 'up' : 'down'"><span>{{item.average}}</span><span>{{item.float}}</span></P>
                    </div>
                </div>
                <div class="button">查看更多金属行情</div>
            </div>
        </div>
    </div>
    <div class="QAndAList" v-if="taps === 'QAndA'">
        <div class="QAndA" v-for="i in QAndAList">
            <div class="title" @click="questionInfos(i.qid)">{{i.title}}</div>
            <div class="desc">{{i.desc}}</div>
            <div class="flexRight">
                <div class="more" @click="showId1 = i.id">
                    <div class="operate" v-if="showId1 === i.id" @click.stop="showId1 = ''">
                        <span>删除</span>
                        <span>分享</span>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div>
                    <span class="share"></span>
                    <i>{{i.num1}}</i>
                </div>
                <div>
                    <span class="news"></span>
                    <i>{{i.num2}}</i>
                </div>
                <div>
                    <span class="like"></span>
                    <i class="blue">{{i.num3}}</i>
                </div>
            </div>
        </div>
    </div>
    <div class="nomore">没有更多内容</div>
</div>
<script>
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/myCollection/index.js"></script>
</body>
</html>

