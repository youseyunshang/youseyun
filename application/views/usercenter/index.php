<!DOCTYPE html>
<html>
<head>
    <title>个人中心</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/personalCenter/index.css">
</head>
<body>
<div id="app" style="padding-bottom: 2rem;" v-cloak>
    <div class="top">
        <div class="head" @click="goInfo">
            <?php
                if(!empty($_SESSION['indexUserData']['realname'])){
                    echo '<img src="'.ROOT_URL_DEFINE.$_SESSION["indexUserData"]["userLogo"].'" style="width: 100%;height: 100%;border-radius: 100px;"/>';
                }
            ?>
        </div>
        <div class="content" @click="goInfo">
            <div class="name"><span>
                    <?php
                        $realname = !empty($_SESSION['indexUserData']['realname'])?$_SESSION['indexUserData']['realname']:'昵称';
                        echo $realname;
                    ?>
                </span>
                <span class="vip" v-if="vip"></span><span class="novip" v-else></span></div>
            <div class="notice">
                    <?php
                    $comName = !empty($_SESSION['indexUserData']['comName'])?$_SESSION['indexUserData']['comName']:'所属企业待完善';
                    echo $comName;
                    ?>
            </div>
        </div>
        <div class="next"></div>
    </div>
    <div class="line"></div>
    <img class="join" v-if="showJoin" src="<?php echo ROOT_URL_DEFINE?>/resource/vue/images/8_03.png" alt="" @click="companyValid">
    <div class="bigTitle">待处理业务</div>
    <div class="wait">
        <div class="left">
            <div class="title">售出订单</div>
            <div class="bottom">
                <span></span>
                <i>3</i>
                <div>笔</div>
            </div>
        </div>
        <div class="right">
            <div class="title">购入订单</div>
            <div class="bottom">
                <span></span>
                <i>3</i>
                <div>笔</div>
            </div>
        </div>
    </div>
    <div class="list">
        <div class="item">
            <span class="member"></span>
            <div class="title">高级会员</div>
        </div>
        <div class="item" @click="collection">
            <span class="collection"></span>
            <div class="title">我的收藏</div>
        </div>
        <div class="item" @click="qanda">
            <span class="answer"></span>
            <div class="title">我的回答</div>
        </div>
        <div class="item" @click="sandp">
            <span class="product"></span>
            <div class="title">我的供求</div>
        </div>
    </div>
    <div class="list">
        <div class="item">
            <span class="entrust"></span>
            <div class="title">我的委托</div>
        </div>
        <div class="item">
            <span class="trusteeship"></span>
            <div class="title">我的托管</div>
        </div>
        <div class="item">
            <span class="order" @click="order"></span>
            <div class="title">我的订单</div>
        </div>
        <div class="item" @click="hobby">
            <span class="like"></span>
            <div class="title">我的喜好</div>
        </div>
    </div>
    <div class="list">
        <div class="item" @click="accountSet">
            <span class="member" style="background-position: -0.29rem -4.4rem;"></span>
            <div class="title">账号设置</div>
        </div>
        <div class="item" style="visibility: hidden;">
            <span class="trusteeship"></span>
            <div class="title"></div>
        </div>
        <div class="item" style="visibility: hidden;">
            <span class="order"></span>
            <div class="title"></div>
        </div>
        <div class="item" style="visibility: hidden;">
            <span class="like"></span>
            <div class="title"></div>
        </div>
    </div>
    <footers :page="'my'"></footers>
<!--    <div class="button" @click="indexDefault">返回首页</div>-->
</div>
<script>
    var userinfo = eval('('+'<?php echo $userinfo?>'+')');
    console.log(userinfo);
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/personalCenter/index.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>
</html>

