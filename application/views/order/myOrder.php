<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/order/index.css">
</head>
<body>
    <div id="app" v-cloak>
        <div class="topTaps">
            <span :class="{active: taps === 1}" @click="taps = 1">全部订单</span>
            <span :class="{active: taps === 2}" @click="taps = 2">买入订单</span>
            <span :class="{active: taps === 3}" @click="taps = 3">售出订单</span>
        </div>
        <div class="type">
            <div class="typeList">
                <div class="item" v-for="i in list" :class="{'checked': checkedId === i.id}" @click="checkedId = i.id">{{i.title}}</div>
            </div>
        </div>
        <div class="main">
            <div class="productList" v-for="i in productList">
                <div class="top">
                    <div><span>{{type[i.type]}}：</span><i>{{i.order}}</i></div>
                    <p>{{list[i.status].title}}</p>
                </div>
                <div v-for="(e, index) in i.list">
                    <div class="line" v-if="index > 0"></div>
                    <div class="list">
                        <img :src="e.img" alt="">
                        <div class="content">
                            <div class="title"><i>{{e.title}}</i><span>￥{{e.unit}}/吨×1</span></div>
                            <div class="name">{{e.name}}</div>
                            <div class="price">￥{{e.price}}</div>
                        </div>
                    </div>                    
                </div>
                <div class="total" v-if="i.list.length > 1">共{{i.list.length}}件商品  总计：￥{{i.total}}（含运费：￥0.00）</div>
                <div class="bottom" v-if="i.status !== 3">
                    <span class="c7" v-if="i.status === 4">确认收货</span>
                    <span class="c6" v-if="i.status === 4">退货退款</span>
                    <span class="c6" v-if="i.status === 1">取消订单</span>
                    <span class="c7" v-if="i.status === 5">评价</span>
                    <span class="c7" v-if="i.status === 2">去付款</span>
                    <span class="c8" v-if="i.status === 6">再次购买</span>
                </div>
                <div class="line"></div>
            </div>
        </div>
    </div>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/order/index.js"></script>
</body>
</html>
    
