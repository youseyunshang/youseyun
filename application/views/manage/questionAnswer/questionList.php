<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>问题管理</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/css/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<!--隐藏域Start-->
<div style="display: none;">
    <!--        修改-->
    <?php if(isset($_SESSION['url_permission']['/QuestionAnswer/AnswerList'])){
        echo '<input type="text" value="修改" id="show">';
    }else{
        echo '<input type="text" value="" id="show">';
    }?>
    <!--        删除-->
    <?php if(isset($_SESSION['url_permission']['/QuestionAnswer/DelQuestion'])){
        echo '<input type="text" value="删除" id="del">';
    }else{
        echo '<input type="text" value="" id="del">';
    }?>
</div>
<!--隐藏域End-->
<div class="cBody">
    <div class="console">
        <form class="layui-form" method="GET" action="#">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" id="selectName" name="selectName" required lay-verify="required" placeholder="问题内容" value="" autocomplete="off" class="layui-input">
                </div>
<!--                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>-->
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
<!--                <a href="--><?php //echo ROOT_URL_DEFINE;?><!--/UserPerson/excelOut" class="layui-btn">导出</a>-->
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>标题</th>
            <th>问题内容</th>
            <th>所属分类</th>
            <th>创建人员</th>
            <th>创建时间</th>
            <th>回答数量</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/QuestionAnswer/questionList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        var data = eval("("+response+")"),html = "";
                        //清空
                        $("#adminList").find("tbody").empty();
                        //填数据
                        $.each(data,function(k,v){
                            html = "";
                            html += "<tr>";
                            html += "<td>"+v.title+"</td>";
                            html += "<td>"+v.content+"</td>";
                            html += "<td>"+v.attrname+"</td>";
                            html += "<td>"+v.username+"</td>";
                            html += "<td>"+v.createTime+"</td>";
                            if($("#show").val()){
                                html += "<td><a style='color:#00f;' href='"+"<?php echo ROOT_ADMIN_REQUEST?>/QuestionAnswer/answerList/"+v.unqid+"'>"+v.answermun+"</a></td>";
                            }else{
                                html += "<td>"+v.answermun+"</td>";
                            }
                            html += "<td>";
                            if($("#del").val()){
                                html += "<a style='padding:0px 10px;color:#00f;' onclick='adminPersionObj.delCustomList(this)' Id ='"+v.Id+"' unQid='"+v.unqid+"' href='javascript:;'>删除</a>";
                            }else{
                                html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                            }
                            html += "</td>";
                            html += "</tr>";
                            $("tbody").append(html);
                            // $(html).prependTo('tbody');
                        });
                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);

            },
            'delCustomList':function(_this){
                layui.use(['form','laydate'], function() {
                    layer.confirm('确定要删除么？', {
                        btn: ['确定', '取消'] //按钮
                    }, function() {
                        var data = {
                            'Id':$(_this).attr("Id"),
                            'unqid':$(_this).attr("unQid")
                        }
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/QuestionAnswer/delQuestion",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                $(_this).parent().parent().remove();
                                layer.msg('删除成功', {
                                    icon: 1
                                });
                            }
                        });
                    }, function() {
                        layer.msg('取消删除', {
                            time: 2000 //20s后自动关闭
                        });
                    });
                });
            }
        }
    </script>
</div>
</body>

</html>