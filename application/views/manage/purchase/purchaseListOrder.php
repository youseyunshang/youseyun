<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>采购产品</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/css/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<div class="cBody">
    <div class="console" style="display: none;">
        <form class="layui-form" method="GET" action="#">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" id="selectName" name="selectName" required lay-verify="required" placeholder="标题" value="" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">

                        <select name="status" id="status">
                            <option value="">--所有--</option>
                            <option value="1">通过</option>
                            <option value="0">未通过</option>
                        </select>

                </div>
                <!--                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>-->
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
                <!--                <a href="--><?php //echo ROOT_URL_DEFINE;?><!--/UserPerson/excelOut" class="layui-btn">导出</a>-->
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th style="width: 10%;">报价</th>
            <th style="width: 20%;">送货方式</th>
            <th style="width: 20%;">付款方式</th>
            <th style="width: 20%;">交货时间</th>
            <th style="width: 10%;">联系人</th>
            <th style="width: 10%;">手机号</th>
            <th style="width: 10%;">编辑</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'Id':'<?php echo $Id;?>',
                    'curr':curr,
                    'limit':limit
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Purchase/showPurchaseOrder",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "json",
                    success: function (response) {
                        if(response){
                            //清空
                            $("#adminList").find("tbody").empty();
                            //填数据
                            $.each(response,function(k,v){
                                html = "";
                                html += "<tr>";
                                html += "<td>"+v.unitPrice+"</td>";
                                html += "<td>"+v.distributionMode+"</td>";
                                html += "<td>"+v.payment+"</td>";
                                html += "<td>"+v.endTime+"</td>";
                                html += "<td>"+v.billHolder+"</td>";
                                html += "<td>"+v.tel+"</td>";
                                html += "<td>";
                                html += "<a style='padding:0px 10px;color:#00f;' onclick='adminPersionObj.agree(this)' Id ='"+v.Id+"' status='"+v.status+"' href='javascript:;'>"+(v.status>0?"通过":"未通过")+"</a>";
                                html += "</td>";
                                html += "</tr>";
                                $("tbody").append(html);
                            });
                        }
                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);

            },
            'agree':function(_this){
                        var data = {
                            'Id':$(_this).attr("Id"),
                            'status':$(_this).attr("status")
                        }
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/Purchase/agreePurchaseOrder",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                adminPersionObj.selectList();
                            }
                        });
            }
        }
    </script>
</div>
</body>

</html>