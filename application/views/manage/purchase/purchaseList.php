<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>采购产品</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/css/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<div class="cBody">
    <!--隐藏域Start-->
    <div style="display: none;">
        <!--        审核-->
        <?php if(isset($_SESSION['url_permission']['/Purchase/showPurchase'])){
            echo '<input type="text" value="审核" id="show">';
        }else{
            echo '<input type="text" value="" id="show">';
        }?>
        <!--        报单人-->
        <?php if(isset($_SESSION['url_permission']['/Purchase/showPurchaseOrder'])){
            echo '<input type="text" value="报单人" id="showOrder">';
        }else{
            echo '<input type="text" value="" id="showOrder">';
        }?>
    </div>
    <!--隐藏域End-->
    <div class="console">
        <form class="layui-form" method="GET" action="#">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" id="selectName" name="selectName" required lay-verify="required" placeholder="标题" value="" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">

                        <select name="status" id="status">
                            <option value="">--是否通过--</option>
                            <option value="1">已通过</option>
                            <option value="2">待审核</option>
                        </select>

                </div>
                <div class="layui-input-inline">

                    <select name="endTime" id="endTime">
                        <option value="">--是否下架--</option>
                        <option value="2">已下架</option>
                        <option value="1">未下架</option>
                    </select>

                </div>
                <!--                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>-->
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
                <!--                <a href="--><?php //echo ROOT_URL_DEFINE;?><!--/UserPerson/excelOut" class="layui-btn">导出</a>-->
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>编号</th>
            <th>产品标题</th>
            <th>产品数量</th>
<!--            <th>剩余采购量</th>-->
<!--            <th>预期采购价格</th>-->
<!--            <th>企业类型</th>-->
<!--            <th>供应企业类型</th>-->
            <th>采购开始时间</th>
            <th>采购结束时间</th>
            <th>挂单人</th>
            <th>手机号</th>
            <th>邮箱</th>
            <th>创建时间</th>
            <th>状态</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val(),
                    'status':$("#status").val(),
                    'endTime':$("#endTime").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Purchase/purchaseList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "json",
                    success: function (response) {
                        if(response){
                            var html = "";
                            //清空
                            $("#adminList").find("tbody").empty();
                            //填数据
                            $.each(response,function(k,v){
                                html = "";
                                html += "<tr>";
                                html += "<td>"+v.unqidCode+"</td>";
                                html += "<td>"+v.title+"</td>";
                                html += "<td>"+v.num+"</td>";
                                // html += "<td>"+v.residualNum+"</td>";
                                // html += "<td>"+v.unitPrice+"</td>";
                                // html += "<td>"+v.companyType+"</td>";
                                // html += "<td>"+v.suppleCompanyType+"</td>";
                                html += "<td>"+v.startTime+"</td>";
                                html += "<td>"+v.endTime+"</td>";
                                html += "<td>"+v.billHolder+"</td>";
                                html += "<td>"+v.tel+"</td>";
                                html += "<td>"+v.email+"</td>";
                                html += "<td>"+v.createTime+"</td>";
                                html += "<td>"+(v.status>1?"待审核":"已通过")+"</td>";
                                html += "<td>";
                                if($("#show").val()){
                                    html += "<a style='padding:0px 10px;color:#00f;' href='<?php echo ROOT_ADMIN_REQUEST?>/Purchase/showPurchase?Id="+v.Id+"'>审核</a>";
                                }else{
                                    html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                                }
                                if($("#showOrder").val()){
                                    if(v.purchaseOrderNum){
                                        html += "<a style='padding:0px 10px;color:#00f;' href='<?php echo ROOT_ADMIN_REQUEST?>/Purchase/showPurchaseOrder?Id="+v.Id+"'>报价人</a>";
                                    }
                                }else{
                                    html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                                }
                                html += "</td>";
                                html += "</tr>";
                                $("tbody").append(html);
                            });
                        }
                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);

            }
        }
    </script>
</div>
</body>

</html>