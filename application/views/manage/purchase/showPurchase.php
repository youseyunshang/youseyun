<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>订单详情</title>
    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <style>
        .layui-form{
            margin-right: 30%;
        }
        .layui-form-item{
            margin:20px 0px;
        }
        .layui-form-label{
            width:150px;
        }
    </style>
</head>
<body>
<div class="cBody">
    <input type="text" name="Id" value="<?php echo $purchase['Id'];?>" placeholder="例:admin" autocomplete="off" style="display: none;" class="layui-input">
        <table class="layui-table" id="adminList">
            <tbody>
                <tr>
                    <td><label>企业类型：<?php echo $purchase['companyType'];?></label></td>
                    <td><label>产品标题：<?php echo $purchase['title'];?></label></td>
                    <td><label>产品详情：<?php echo $purchase['content'];?></label></td>
                </tr>
                <tr>
                    <td><label>产品数量：<?php echo $purchase['num'];?></label></td>
                    <td><label>预期采购价格：<?php echo $purchase['unitPrice'];?></label></td>
                    <td><label>牌号：<?php echo $purchase['brandName'];?></label></td>
                </tr>
                <tr>
                    <td><label>产品粒度：<?php echo $purchase['granularity'];?></label></td>
                    <td><label>开始时间：<?php echo $purchase['startTime'];?></label></td>
                    <td><label>结束时间：<?php echo $purchase['endTime'];?></label></td>
                </tr>
                <tr>
                    <td><label>挂单人：<?php echo $purchase['billHolder'];?></label></td>
                    <td><label>电话：<?php echo $purchase['tel'];?></label></td>
                    <td><label>邮箱：<?php echo $purchase['email'];?></label></td>
                </tr>
                <tr>
                    <td><label>配送方式：<?php echo $purchase['distributionMode'];?></label></td>
                    <td><label>挂单用户：<?php echo $person['username'];?></label></td>
                    <td><label>公司名称：<?php echo $person['comName'];?></label></td>
                </tr>
            </tbody>
        </table>
    <form class="layui-form">
        <div class="layui-form-item">
            <div class="layui-input-block">
                <a class="layui-btn" onclick="myObj.agree(1)">通过</a>
                <a class="layui-btn layui-btn-primary" onclick="myObj.agree(2)">驳回</a>
            </div>
        </div>
    </form>
    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        var myObj = {
            "agree":function(status){
                var data = {
                    'status':status,
                    "Id":$("input[name='Id']").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Purchase/purchaseStatus",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "json",
                    success: function (response) {
                        if(response.code=="200"){
                            window.location.href = "<?php echo ROOT_ADMIN_REQUEST?>/Purchase/purchaseList";
                        }
                    }
                });
            }
        }
    </script>

</div>
</body>

</html>