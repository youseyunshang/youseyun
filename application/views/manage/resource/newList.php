<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>报价列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .searchResult{
            position: absolute;
            top: 50px;
            left: 15px;
            z-index: 10;
            background-color: #FFF;
            width: 100px;
            border: 1px solid #d2d2d2;
            border-radius: 2px;
        }
        .searchResult li{
            padding: 0 10px;
            line-height: 36px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            cursor: pointer;
        }
        .searchResult li:hover{
            background-color: #eee;
        }
    </style>
</head>

<body>
<div class="cBody">
    <div class="console">
        <div class="layui-form-item">
            <a class="layui-btn" onclick="pushNew()">发布新闻</a>
        </div>
    </div>

    <table id="customList" class="layui-table">
        <thead>
        <tr>
<!--            <th><input type="checkbox" style="" value="" father="1"></th>-->
            <th>勾选</th>
            <th>标题</th>
<!--            <th>概要</th>-->
            <th>来源</th>
            <th>原文链接</th>
            <th>图片</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if(!empty($newData)){
            foreach ($newData as $key=>&$val){
                echo "<tr>";
                echo "<td><input type='checkbox' style='' son='1' value='".$val['id']."'></td>";
                echo "<td class='title'>".$val['title']."</td>";
//                echo "<td class='content'>".$val['content']."</td>";
                echo "<td class='appName'>".$val['appName']."</td>";
                echo "<td class='url'><a href='".$val['url']."' style='color: #0C78DD;' target='_blank'>详情</td>";
                if (!empty($val['imageUrls'])){
                    if(count($val['imageUrls'])>2) {
                        echo "<td class='imageUrls'><img src='".$val['imageUrls'][0]."'><img src='".$val['imageUrls'][1]."'><img src='".$val['imageUrls'][2]."'></td>";
                    }else{
                        echo "<td class='imageUrls'><img src='".$val['imageUrls'][0]."'></td>";
                    }
                }else{
                    echo "<td></td>";
                }
                echo "</tr>";
            }
        }
        ?>
        </tbody>
    </table>
</div>
<script>
    layui.use(['form','laydate','laypage', 'layer'], function() {
        var laypage = layui.laypage,layer = layui.layer,form = layui.form,laydate = layui.laydate;
    });
    function pushNew(){
        var ids = new Array();
        $.each($("input[son='1']"),function (k,v) {
            if($(v).prop("checked")){
                ids.push($(v).val());
            }
        });
        $.ajax({
            url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/pushNew",
            type : "POST",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data : {"data":ids},
            dataType : "json",
            success: function (response) {
                if(response.code=="200"){
                    alert(response.message);
                    window.location.reload();
                }else{
                    layer.msg(response.message, {
                        time: 20000, //20s后自动关闭
                        btn: ['明白了', '知道了', '哦']
                    });
                }

            }
        });
    }
</script>
</body>

</html>