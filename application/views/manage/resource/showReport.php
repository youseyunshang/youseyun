<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>报价详情</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
</head>

<body>
<div class="cBody">
    <div class="console">
        <div class="layui-form-item">
            <a class="layui-btn" href="<?php echo ROOT_ADMIN_REQUEST;?>/Resource/reportOutList">返回</a>
        </div>
    </div>
    <table id="customList" class="layui-table">
        <thead>
        <tr>
            <th>名称</th>
            <th>最低值</th>
            <th>最高值</th>
            <th>中间值</th>
            <th>涨跌</th>
        </tr>
        </thead>
        <tbody>
        <?php
            unset($val);
            foreach ($reportData as $key=>&$val){
                echo "<tr>";
                echo "<td class='formname'>".$val['formname']."</td>";
                echo "<td class='lastnum'>".$val['lastnum']."</td>";
                echo "<td class='topnum'>".$val['topnum']."</td>";
                echo "<td class='middlenum'>".$val['middlenum']."</td>";
                if($val['updown']>0){
                    echo "<td class='updown' style='color: #0f0;font-size: 20px;'><span style='position: relative;top:-2px;'>↑</span> ".$val['updown']."</td>";
                }else if($val['updown']<0){
                    echo "<td><span class='updown' style='color: #f00;font-size: 20px;'><span style='position: relative;top:-2px;'>↓</span> ".abs($val['updown'])."</td>";
                }else{
                    echo "<td class='updown'>".$val['updown']."</td>";
                }

                echo "</tr>";
            }
        ?>
        </tbody>
    </table>
</div>
</body>

</html>