<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>更新资源</title>
    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/wangEditor.min.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .layui-form{
            margin-right: 30%;
        }
        .layui-form-item .layui-form-checkbox{
            margin-right:50px ;
        }
    </style>
</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" method="post" action="#">
        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="title" value="<?php echo $resData['title'];?>" id="title" placeholder="例:铁的深度加工" autocomplete="off" class="layui-input">
                <input type="text" id="Id" style="display: none;" value="<?php echo $resData['Id'];?>">
                <input type="text" id="path" style="display: none;" value="<?php echo $resData['path'];?>">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">栏目分类</label>
            <div class="layui-input-block">
                <input type="radio" name="typeBar" value="1" title="资讯" <?php echo $resData['type']==1?"checked":"";?>>
                <input type="radio" name="typeBar" value="2" title="日评" <?php echo $resData['type']==2?"checked":"";?>>
                <input type="radio" name="typeBar" value="3" title="周评" <?php echo $resData['type']==3?"checked":"";?>>
                <input type="radio" name="typeBar" value="4" title="月评" <?php echo $resData['type']==4?"checked":"";?>>
                <input type="radio" name="typeBar" value="5" title="新闻" <?php echo $resData['type']==5?"checked":"";?>>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">概要</label>
            <div class="layui-input-block">
                <textarea name="notice" id="notice" style="width:1000px;" placeholder="请输入内容" class="layui-textarea"><?php echo $resData['notice']?></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">来源</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="origin" id="origin" value="<?php echo $resData['origin'];?>" placeholder="中央新闻" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item" style="padding: 10px 0px;">
            <label class="layui-form-label">金属类别</label>
            <div  class="layui-input-block" style="width:1000px;overflow: hidden;">
                <div id="metalAll" class="demo-tree">
                </div>
                <script>
                    var checkBoxData = new Array(),
                        str='<?php echo $attrData;?>';
                    dataAttr = eval("("+str+")");
                    layui.use(['tree', 'util'], function(){
                        var tree = layui.tree
                            ,layer = layui.layer
                            ,util = layui.util;
                        //开启复选框
                        tree.render({
                            elem: '#metalAll'
                            ,data: dataAttr
                            ,showCheckbox: true
                            ,oncheck: function(obj){
                                if(obj.data.children&&obj.data.children.length>0){//父级点击
                                    $.each(obj.data.children,function(k,v){
                                        if(v.children&&v.children.length>0){//子项里面还有子数据
                                            $.each(v.children,function(k2,v2){
                                                // console.log(obj.checked);
                                                if(obj.checked){//添加
                                                    checkBoxData.push({'Id':v2.id,'pid':v2.pid,"attrname":v2.title})
                                                }else{//删除
                                                    // console.log(v2);
                                                    $.each(checkBoxData,function(k1,v1){
                                                        if(v1.Id == v2.id){
                                                            checkBoxData.splice(k1,1);
                                                            return false;
                                                        }
                                                    });
                                                }
                                            });
                                        }else{
                                            //二级金属没有子金属情况
                                            if(obj.checked){//添加
                                                checkBoxData.push({'Id':v.id,'pid':v.pid,"attrname":v.title})
                                            }else{//删除
                                                $.each(checkBoxData,function(k1,v1){
                                                    if(v1.Id == v.id){
                                                        checkBoxData.splice(k1,1);
                                                        return false;
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }else{
                                    if(obj.checked){//添加
                                        checkBoxData.push({'Id':obj.data.id,'pid':obj.data.pid,"attrname":obj.data.title})
                                    }else{//删除
                                        $.each(checkBoxData,function(k1,v1){
                                            if(v1.Id == obj.data.id){
                                                checkBoxData.splice(k1,1);
                                                return false;
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    });
                </script>

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">内容</label>
            <div class="layui-input-inline shortInput">
                <div id="editor" style="width:1000px;">
                    <?php echo $resData['content']?>
                </div>
                <!-- 注意， 只需要引用 JS，无需引用任何 CSS ！！！-->
                <script type="text/javascript">
                    var E = window.wangEditor;
                    var editor = new E('#editor');
                    // 或者 var editor = new E( document.getElementById('editor') )
                    editor.create();
                </script>
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">封面图</label>
            <div class="layui-input-block">
                <input type="radio" name="photo" value="1" title="一张大图" <?php echo $resData['photoType']==1?"checked":"";?>>
                <input type="radio" name="photo" value="2" title="一张小图" <?php echo $resData['photoType']==2?"checked":"";?>>
                <input type="radio" name="photo" value="3" title="三张小图" <?php echo $resData['photoType']==3?"checked":"";?>>
            </div>
        </div>
        <?php
            //图片资源
            $photoData = json_decode($resData['photo'],true);
        ?>
        <div class="layui-form-item coverChart" id="coverChartOne">
            <label class="layui-form-label"></label>
            <div class="layui-upload-drag" id="resOne">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php if(!empty($photoData['coverLogoOne'])){
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads'.$photoData['coverLogoOne'].'" id="coverImgOne" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="coverImgOne" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">';
            }?>
            <input type="text" value="<?php echo $photoData['coverLogoOne'];?>" id="coverLogoOne" style="display: none;" name="userLogo">
        </div>
        <div class="layui-form-item coverChart" id="coverChartTwo" <?php echo $resData['photoType']==3?"":"style='display:none;'"?>>
            <label class="layui-form-label"></label>
            <div class="layui-upload-drag" id="resTwo">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php if(!empty($photoData['coverLogoTwo'])){
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads'.$photoData['coverLogoTwo'].'" id="coverImgTwo" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="coverImgTwo" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">';
            }?>
            <input type="text" value="<?php echo $photoData['coverLogoTwo'];?>" id="coverLogoTwo" style="display: none;" name="userLogo">
        </div>
        <div class="layui-form-item coverChart" id="coverChartThree" <?php echo $resData['photoType']==3?"":"style='display:none;'"?>>
            <label class="layui-form-label"></label>
            <div class="layui-upload-drag" id="resThree">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php if(!empty($photoData['coverLogoThree'])){
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads'.$photoData['coverLogoOne'].'" id="coverImgThree" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="coverImgThree" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">';
            }?>
            <input type="text" value="<?php echo $photoData['coverLogoThree'];?>" id="coverLogoThree" style="display: none;" name="userLogo">
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <a href="javascript:;" class="layui-btn" onclick="myObj.submit()">修改</a>
                <a href="javascript:;" class="layui-btn layui-btn-primary">重置</a>
            </div>
        </div>
    </form>
    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        layui.use(['layer', 'form','upload'], function(){
            var layer = layui.layer
                ,form = layui.form;
            form.on('radio(photo)', function(data){
                switch (data.value) {
                    case "1":
                        $("#coverChartOne").show();
                        $("#coverChartTwo").hide();
                        $("#coverChartThree").hide();
                        break;
                    case "2":
                        $("#coverChartOne").show();
                        $("#coverChartTwo").hide();
                        $("#coverChartThree").hide();
                        break;
                    case "3":
                        $("#coverChartOne").show();
                        $("#coverChartTwo").show();
                        $("#coverChartThree").show();
                        break;
                }
            });
            var upload = layui.upload;
            //拖拽上传
            upload.render({
                elem: '#resOne',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Resource/upload',
                done: function(res) {
                    if(res.upload_data){
                        $("#coverImgOne").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/res/"+res.upload_data.file_name);
                        $("#coverLogoOne").val("/res/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#resTwo',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Resource/upload',
                done: function(res) {
                    if(res.upload_data){
                        $("#coverImgTwo").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/res/"+res.upload_data.file_name);
                        $("#coverLogoTwo").val("/res/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#resThree',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Resource/upload',
                done: function(res) {
                    if(res.upload_data){
                        $("#coverImgThree").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/res/"+res.upload_data.file_name);
                        $("#coverLogoThree").val("/res/"+res.upload_data.file_name);
                    }
                }
            });
        });
        var myObj = {
            "submit":function(){
                //内容
                var html = editor.txt.html(),photoType=1,typeBar=1;
                //封面图片
                var ptoto = {
                    "coverLogoOne":$("#coverLogoOne").val(),
                    "coverLogoTwo":$("#coverLogoTwo").val(),
                    "coverLogoThree":$("#coverLogoThree").val()
                }
                //图片的类型
                $.each($("input[name='photo']"),function(k,v){
                    if($(v).prop("checked")){
                        photoType = $(v).val();
                        return false;
                    }
                });
                //栏目分类
                $.each($("input[name='typeBar']"),function(k,v){
                    if($(v).prop("checked")){
                        typeBar = $(v).val();
                    }
                });
                if(checkBoxData.length<1){
                    layer.msg('请选择金属属性！', {
                        time: 20000, //20s后自动关闭
                        btn: ['明白了', '知道了', '哦']
                    });
                    return false;
                }else{
                    $.each(checkBoxData,function(k1,v1){
                        if(v1.pid >0){
                            $.each(dataAttr,function(k2,v2){
                                if(v1.pid == v2.id){
                                    checkBoxData.push({'Id':v2.id,'pid':v2.pid,"attrname":v2.title});
                                    return false;
                                }
                            });
                        }
                    });
                }
                var obj = {
                    "Id":$("#Id").val(),
                    "path":$("#path").val(),
                    "title":$("#title").val(),
                    "content":html,
                    "notice":$("#notice").val(),
                    "photo":ptoto,
                    "attr":checkBoxData,
                    "photoType":photoType,
                    "type":typeBar,
                    "origin":$("#origin").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/updateRes",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : obj,
                    dataType : "text",
                    success: function (response) {
                        if(response>0){
                            window.location.href = "<?php echo ROOT_ADMIN_REQUEST?>/Resource/resList";
                        }else{
                            layer.msg('修改不成功,请检查网络！', {
                                time: 20000, //20s后自动关闭
                                btn: ['明白了', '知道了', '哦']
                            });
                        }

                    }
                });
            }
        }
    </script>

</div>
</body>

</html>