<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>报价列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .searchResult{
            position: absolute;
            top: 50px;
            left: 15px;
            z-index: 10;
            background-color: #FFF;
            width: 100px;
            border: 1px solid #d2d2d2;
            border-radius: 2px;
        }
        .searchResult li{
            padding: 0 10px;
            line-height: 36px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            cursor: pointer;
        }
        .searchResult li:hover{
            background-color: #eee;
        }
    </style>
</head>

<body>
<div class="cBody">
    <div class="console">
        <div class="layui-form-item">
            <a class="layui-btn" onclick="addCustomList('customList')">新增</a>
        </div>
    </div>

    <table id="customList" class="layui-table">
        <thead>
        <tr>
            <th>名称</th>
            <th>金属属性</th>
            <th>规格</th>
            <th>单位</th>
            <th>最低值</th>
            <th>最高值</th>
            <th>中间值</th>
            <th>涨跌</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($reportData as $key=>&$val){
                echo "<tr>";
                echo "<td class='formname' unqid='".$val['Id']."'>".$val['formname']."</td>";
                echo "<td class='attr' attrIds='".$val['attrIds']."'><a href='javascript:;' onclick='' rId='".$val['Id']."'>".($val['attrIds']!=''?$val['attrNames']:'暂无属性')."</a></td>";
                echo "<td class='Specifications'>".$val['Specifications']."</td>";
                echo "<td class='measurement'>".$val['measurement']."</td>";
                echo "<td class='lastnum'>".$val['lastnum']."</td>";
                echo "<td class='topnum'>".$val['topnum']."</td>";
                echo "<td class='middlenum'>".$val['middlenum']."</td>";
                echo "<td class='updown'>".$val['updown']."</td>";
                echo "<td><a class='layui-btn layui-btn-xs' onclick='delCustomList(this)'>删除</a><a class='layui-btn layui-btn-xs' onclick='updateCustomList(this)'>修改</a></td>";
                echo "</tr>";
            }
        ?>
        </tbody>
    </table>
</div>
<script>
    layui.use(['form','laydate','laypage', 'layer'], function() {
        var laypage = layui.laypage,layer = layui.layer,form = layui.form,laydate = layui.laydate;
    });
    //选择属性
    function selectAttr(that){
        // var Id = $(that).attr("rId");
        layer.open({
            type:2,
            area:['800px','600px'],
            title:'选择属性',
            zIndex:999,
            scrollbar:false,
            btn:['确认','取消'],
            content: '<?php echo ROOT_ADMIN_REQUEST?>/Resource/selectAttrIframe',
            yes:function(index,layero){
                var response = $(layero).find("iframe")[0].contentWindow.selectData(),ids=new Array(),names=new Array();
                $.each(response,function(k,v){
                    if(v){
                        ids.push(v.Id);
                        names.push(v.attrname);
                    }
                });
                // console.log(ids.join(","));
                // console.log(names.join(","));
                $(that).html(names.join(","));
                $(that).parent().attr("attrIds",ids.join(","));
                layer.close(index);
            },
            btn2:function(index){
                layer.close(index);
            }
        });
    }
    //新增
    function addCustomList(id){
        var str = '<tr>'+
            '<td class="formname">'+
            '<input type="text" name="formname" lay-verify="required" autocomplete="off" class="layui-input">'+
            '</td>'+
            '<td class="attr" attrIds=""><a href="javascript:;" style="color: #0C78DD;" onclick="selectAttr(this);">暂无属性</a></td>'+
            '<td class="Specifications">'+
            '<input type="text" name="Specifications" lay-verify="required" autocomplete="off" class="layui-input">'+
            '</td>'+
            '<td class="measurement">'+
            '<input type="text" name="measurement" lay-verify="required" autocomplete="off" class="layui-input">'+
            '</td>'+
            '<td class="lastnum">'+
            '<input type="text" name="lastnum" lay-verify="required" autocomplete="off" class="layui-input">'+
            '</td>'+
            '<td class="topnum">'+
            '<input type="text" name="topnum" lay-verify="required" autocomplete="off" class="layui-input">'+
            '</td>'+
            '<td class="middlenum">0</td>'+
            '<td class="updown">0</td>'+
            '<td>'+
            '<a class="layui-btn layui-btn-xs" onclick="delCustomList(this)">删除</a><a class="layui-btn layui-btn-xs" onclick="addOK(this)">确定</a>'+
            '</td>'+
            '</tr>';
        $("#"+id).append(str);
        //重新初始化Iframe的高度
        cframeInit();
    }
    //修改
    function updateCustomList(_this){
        $(_this).parent().parent().find(".attr").find("a").attr("onclick","selectAttr(this);");
        $(_this).parent().parent().find(".attr").find("a").attr("style","color: #0C78DD;");
        let lastnum = $(_this).parent().parent().find(".lastnum").html(),topnum = $(_this).parent().parent().find(".topnum").html(),Specifications = $(_this).parent().parent().find(".Specifications").html(),measurement = $(_this).parent().parent().find(".measurement").html();
        $(_this).parent().parent().find(".lastnum").html('<input type="text" name="lastnum" value="'+lastnum+'" class="layui-input">');
        $(_this).parent().parent().find(".topnum").html('<input type="text" name="topnum" value="'+topnum+'" class="layui-input">');
        $(_this).parent().parent().find(".Specifications").html('<input type="text" name="Specifications" value="'+Specifications+'" class="layui-input">');
        $(_this).parent().parent().find(".measurement").html('<input type="text" name="measurement" value="'+measurement+'" class="layui-input">');
        $(_this).parent().append('<a class="layui-btn layui-btn-xs" onclick="updateOK(this)">确定</a>');
        //重新初始化Iframe的高度
        cframeInit();
    }
    //删除
    function delCustomList(_this){
        layui.use(['form','laydate'], function() {
            layer.confirm('确定要删除么？', {
                btn: ['确定', '取消'] //按钮
            }, function() {
                var data = {
                    'Id':$(_this).parent().parent().find(".formname").attr("unqid")
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/delReport",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        $(_this).parent().parent().remove();
                        layer.msg('删除成功', {
                            icon: 1
                        });
                    }
                });
            }, function() {
                layer.msg('取消删除', {
                    time: 2000 //20s后自动关闭
                });
            });
        });
    }
    //更新确定
    function updateOK(_this){
        var lastnum=  $(_this).parent().parent().find("input[name='lastnum']").val(),topnum= $(_this).parent().parent().find("input[name='topnum']").val(),Specifications=  $(_this).parent().parent().find("input[name='Specifications']").val(),measurement= $(_this).parent().parent().find("input[name='measurement']").val();
        var data = {
            'Id':$(_this).parent().parent().find(".formname").attr("unqid"),
            'measurement':measurement,
            'Specifications':Specifications,
            'lastnum':lastnum,
            'topnum':topnum,
            'attrIds':$(_this).parent().parent().find(".attr").attr("attrIds"),
            'attrNames':$(_this).parent().parent().find(".attr").find("a").html()
        }
        $.ajax({
            url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/updateReport",
            type : "POST",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data : data,
            dataType : "text",
            success: function (response) {
                var data = eval("("+response+")"),html = "";
                $(_this).parent().parent().find(".lastnum").html(data.lastnum);
                $(_this).parent().parent().find(".measurement").html(data.measurement);
                $(_this).parent().parent().find(".Specifications").html(data.Specifications);
                $(_this).parent().parent().find(".topnum").html(data.topnum);
                $(_this).parent().parent().find(".middlenum").html(data.middlenum);
                $(_this).parent().parent().find(".attr").find("a").removeAttr("onclick");
                $(_this).parent().parent().find(".attr").find("a").removeAttr("style");
                $(_this).remove();
                //重新初始化Iframe的高度
                cframeInit();
            }
        });
    }
    //添加OK
    function addOK(_this){
        var formname=  $(_this).parent().parent().find("input[name='formname']").val(),lastnum=  $(_this).parent().parent().find("input[name='lastnum']").val(),topnum=  $(_this).parent().parent().find("input[name='topnum']").val(),Specifications=  $(_this).parent().parent().find("input[name='Specifications']").val(),measurement= $(_this).parent().parent().find("input[name='measurement']").val();
        var data = {
            'formname':formname,
            'Specifications':Specifications,
            'measurement':measurement,
            'lastnum':lastnum,
            'topnum':topnum,
            'attrIds':$(_this).parent().parent().find(".attr").attr("attrIds"),
            'attrNames':$(_this).parent().parent().find(".attr").find("a").html()
        }
        $.ajax({
            url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/addReport",
            type : "POST",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data : data,
            dataType : "text",
            success: function (response) {
                var data = eval("("+response+")"),html = "";
                $(_this).parent().parent().find(".formname").attr("unqid",data.Id);
                $(_this).parent().parent().find(".formname").html(data.formname);
                $(_this).parent().parent().find(".Specifications").html(data.Specifications);
                $(_this).parent().parent().find(".measurement").html(data.measurement);
                $(_this).parent().parent().find(".lastnum").html(data.lastnum);
                $(_this).parent().parent().find(".topnum").html(data.topnum);
                $(_this).parent().parent().find(".middlenum").html(data.middlenum);
                $(_this).parent().parent().find(".updown").html(data.updown);
                $(_this).parent().parent().find(".attr").find("a").removeAttr("onclick");
                $(_this).parent().parent().find(".attr").find("a").removeAttr("style");
                $(_this).html("修改");
                $(_this).attr("onclick","updateCustomList(this)");
                //重新初始化Iframe的高度
                cframeInit();
            }
        });
    }

</script>
</body>

</html>