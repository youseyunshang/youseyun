<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>添加资源</title>
    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/wangEditor.min.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .layui-form{
            margin-right: 30%;
        }
        .layui-form-item .layui-form-checkbox{
            margin-right:50px ;
        }
    </style>
</head>

<body>
        <div class="layui-form-item">
            <div  class="layui-input-block">
                <div id="metalAll" class="demo-tree">
                </div>
                <script>
                    var checkBoxData = new Array(),str='<?php echo $metal;?>';dataAttr = eval("("+str+")");
                    layui.use(['tree', 'util'], function(){
                        var tree = layui.tree
                            ,layer = layui.layer
                            ,util = layui.util;

                        //开启复选框
                        tree.render({
                            elem: '#metalAll'
                            ,data: dataAttr
                            ,showCheckbox: true
                            ,oncheck: function(obj){
                                if(obj.data.children&&obj.data.children.length>0){//父级点击
                                    $.each(obj.data.children,function(k,v){
                                        if(v.children&&v.children.length>0){//子项里面还有子数据
                                            $.each(v.children,function(k2,v2){
                                                if(obj.checked){//添加
                                                    checkBoxData.push({'Id':v2.id,'pid':v2.pid,"attrname":v2.title})
                                                }else{//删除
                                                    $.each(checkBoxData,function(k1,v1){
                                                        if(v1.Id == v2.id){
                                                            checkBoxData.splice(k1,1);
                                                            return false;
                                                        }
                                                    });
                                                }
                                            });
                                        }else{
                                            //二级金属没有子金属情况
                                            if(obj.checked){//添加
                                                checkBoxData.push({'Id':v.id,'pid':v.pid,"attrname":v.title})
                                            }else{//删除
                                                $.each(checkBoxData,function(k1,v1){
                                                    if(v1.Id == v.id){
                                                        checkBoxData.splice(k1,1);
                                                        return false;
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }else{
                                    if(obj.checked){//添加
                                        checkBoxData.push({'Id':obj.data.id,'pid':obj.data.pid,"attrname":obj.data.title})
                                    }else{//删除
                                        $.each(checkBoxData,function(k1,v1){
                                            if(v1.Id == obj.data.id){
                                                checkBoxData.splice(k1,1);
                                                return false;
                                            }
                                        });
                                    }
                                }
                            }
                        });
                    });
                    var selectData = function(){
                        $.each(checkBoxData,function(k1,v1){
                            if(v1.pid >0){
                                $.each(dataAttr,function(k2,v2){
                                    if(v1.pid == v2.id){
                                        checkBoxData.push({'Id':v2.id,'pid':v2.pid,"attrname":v2.title});
                                        return false;
                                    }
                                });
                            }
                        });
                        return checkBoxData;
                    }
                </script>
            </div>
        </div>
</body>

</html>