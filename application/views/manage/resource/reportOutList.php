<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>报价列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<!--隐藏域End-->
<form method="post" style="display: none;" action="<?php echo ROOT_ADMIN_REQUEST?>/Resource/excelInReport" enctype="multipart/form-data">
            <input type="file" name="userfile" id="">
            <input type="submit" name="excelIn" id="">
</form>
<!--隐藏域Start-->
<div style="display: none;">
    <!--        修改-->
    <?php if(isset($_SESSION['url_permission']['/Resource/UpdateReport'])){
        echo '<input type="text" value="修改" id="update">';
    }else{
        echo '<input type="text" value="" id="updateRes">';
    }?>
    <!--        删除-->
    <?php if(isset($_SESSION['url_permission']['/Resource/DelReport'])){
        echo '<input type="text" value="删除" id="del">';
    }else{
        echo '<input type="text" value="" id="delRes">';
    }?>
</div>
<!--隐藏域End-->
<div class="cBody">
    <div class="console">
        <form class="layui-form" method="GET" action="#">
            <div class="layui-form-item">
                <a href="javascript:;" onclick="adminPersionObj.createPoint()" class="layui-btn">生成报表</a>
                <a href="javascript:;" onclick="adminPersionObj.pushPoint()" class="layui-btn" style="display: none;">发布报表</a>
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>报表日期</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html()||"1";
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/reportOutList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        var data = eval("("+response+")"),html = "";
                        //清空
                        $("#adminList").find("tbody").empty();
                        //填数据
                        $.each(data,function(k,v){
                            html = "";
                            html += "<tr>";
                            html += "<td>"+v.createTime+"</td>";
                            html += "<td>";
                            if($("#update").val()){
                                html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php echo ROOT_ADMIN_REQUEST?>/Resource/updateOutReport?Id="+v.Id+"\">修改</a>";
                            }else{
                                html += "<a style=\"padding:0px 10px;color:#00f;\" href=\"javascript:;\">暂无权限</a>";
                            }
                            if(v.now){
                                html += "<a style='padding:0px 10px;color:#00f;' onclick='adminPersionObj.delCustomList(this)' Id ='"+v.Id+"' href='javascript:;'>删除</a>";
                                html += "<a style='padding:0px 10px;color:#00f;' href='"+"<?php echo ROOT_ADMIN_REQUEST?>/Resource/showReport?Id="+v.Id+"'>预览</a>";
                                html += "<a style='padding:0px 10px;color:#00f;' href='"+"<?php echo ROOT_ADMIN_REQUEST?>/Resource/excelOutReport?Id="+v.Id+"'>导出</a>";
                                html += "<a style='padding:0px 10px;color:#00f;' href='javaScript:;' onclick='adminPersionObj.upload();'>导入</a>";
                            }
                            html += "</td>";
                            html += "</tr>";
                            $('tbody').append(html);
                        });
                    }
                });
            },
            'createPoint':function(){
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/createReport",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : {},
                    dataType : "text",
                    success: function (response) {
                            if(response>0){
                                window.location.href="<?php echo ROOT_ADMIN_REQUEST?>/Resource/reportOutList";
                            }else{
                                alert("今日数据已生成！");
                            }
                    }
                });
            },
            'pushPoint':function(){
                alert("2345324");
            },
            'delCustomList':function(_this){
                layui.use(['form','laydate'], function() {
                    layer.confirm('确定要删除么？', {
                        btn: ['确定', '取消'] //按钮
                    }, function() {
                        var data = {
                            'Id':$(_this).attr("Id")
                        }
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/Resource/delOutReport",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                $(_this).parent().parent().remove();
                                layer.msg('删除成功', {
                                    icon: 1
                                });
                            }
                        });
                    }, function() {
                        layer.msg('取消删除', {
                            time: 2000 //20s后自动关闭
                        });
                    });
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="excelIn"]').click();
                    }
                },500);
            },
        }
    </script>
</div>
</body>

</html>