<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>属性列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
</head>

<body>
<div class="cBody">
    <div class="console">
        <form class="layui-form" method="GET" action="#">
            <div class="layui-form-item">
                <a href="javascript:;" onclick="addFirstAttr()" style="margin: 50px 50px 0px;" class="layui-btn">添加一级属性</a>
            </div>
        </form>
    </div>
    <div id="attr" class="demo-tree demo-tree-box" style="margin:50px 200px;">
    </div>
    <script>
        var addFirstAttr = function(){
            var data = {
                "type":'add',
                "Id":0,
                "attrname":"未命名"
            }
            $.ajax({
                url: "<?php echo ROOT_URL_DEFINE?>/Attribute/changgeAttrProgressShop",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : data,
                dataType : "text",
                success: function (response) {
                    window.location.href="<?php echo ROOT_ADMIN_REQUEST?>/Attribute/attrProgressShop";
                }
            });
        }
        var str='<?php echo $metal;?>';dataAttr = eval("("+str+")");
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;
        });
        layui.use(['tree', 'util'], function(){
            var tree = layui.tree
                ,layer = layui.layer
                ,util = layui.util;
            //开启节点操作图标
            tree.render({
                elem: '#attr'
                ,data: dataAttr
                ,edit: ['add', 'update', 'del'] //操作节点的图标
                ,operate: function(obj){
                    //Ajax 操作
                    var data = {
                        "type":obj.type,
                        "Id":obj.data.id,
                        "attrname":obj.data.title
                    }
                    if(data.type=='del'){
                        layer.confirm('确定要删除么？', {
                            btn: ['确定', '取消'] //按钮
                        }, function() {
                            $.ajax({
                                url: "<?php echo ROOT_URL_DEFINE?>/Attribute/changgeAttrProgressShop",
                                type : "POST",
                                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                                data : data,
                                dataType : "text",
                                success: function (response) {
                                    window.location.href="<?php echo ROOT_ADMIN_REQUEST?>/Attribute/attrProgressShop";
                                }
                            });
                        }, function() {
                            window.location.href="<?php echo ROOT_ADMIN_REQUEST?>/Attribute/attrProgressShop";
                        });
                    }else{
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/Attribute/changgeAttrProgressShop",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                window.location.href="<?php echo ROOT_ADMIN_REQUEST?>/Attribute/attrProgressShop";
                            }
                        });
                    }
                }
            });
        });
        //重新初始化Iframe的高度
        cframeInit();
    </script>
</div>
</body>

</html>