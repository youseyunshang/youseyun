<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>牌号列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .searchResult{
            position: absolute;
            top: 50px;
            left: 15px;
            z-index: 10;
            background-color: #FFF;
            width: 100px;
            border: 1px solid #d2d2d2;
            border-radius: 2px;
        }
        .searchResult li{
            padding: 0 10px;
            line-height: 36px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            cursor: pointer;
        }
        .searchResult li:hover{
            background-color: #eee;
        }
    </style>
</head>

<body>
<div class="cBody">
    <div class="console">
        <div class="layui-form-item">
            <a class="layui-btn" onclick="addCustomList('customList')">新增</a>
        </div>
    </div>

    <table id="customList" class="layui-table">
        <thead>
        <tr>
            <th>牌号</th>
            <th>金属属性</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <?php
            if(!empty($granularityData)){
                foreach ($granularityData as $key=>&$val){
                    echo "<tr>";
                    echo "<td class='granularity' unqid='".$val['Id']."'>".$val['granularity']."</td>";
                    echo "<td class='attr' attrIds='".$val['attrId']."'><a href='javascript:;' onclick='' rId='".$val['Id']."'>".($val['attrId']!=''?$val['attrname']:'暂无属性')."</a></td>";
                    echo "<td><a class='layui-btn layui-btn-xs' onclick='delCustomList(this)'>删除</a><a class='layui-btn layui-btn-xs' onclick='updateCustomList(this)'>修改</a></td>";
                    echo "</tr>";
                }
            }
        ?>
        </tbody>
    </table>
</div>
<script>
    layui.use(['form','laydate','laypage', 'layer'], function() {
        var laypage = layui.laypage,layer = layui.layer,form = layui.form,laydate = layui.laydate;
    });
    //选择属性
    function selectAttr(that){
        // var Id = $(that).attr("rId");
        layer.open({
            type:2,
            area:['800px','600px'],
            title:'选择属性',
            zIndex:999,
            scrollbar:false,
            btn:['确认','取消'],
            content: '<?php echo ROOT_ADMIN_REQUEST?>/Attribute/selectAttrIframe',
            yes:function(index,layero){
                var response = $(layero).find("iframe")[0].contentWindow.selectData(),ids=new Array(),names=new Array();
                $.each(response,function(k,v){
                    if(v){
                        ids.push(v.Id);
                        names.push(v.attrname);
                        return false;
                    }
                });
                $(that).html(names.join(","));
                $(that).parent().attr("attrIds",ids.join(","));
                layer.close(index);
            },
            btn2:function(index){
                layer.close(index);
            }
        });
    }
    //新增
    function addCustomList(id){
        var str = '<tr>'+
            '<td class="granularity">'+
            '<input type="text" name="granularity" lay-verify="required" autocomplete="off" class="layui-input">'+
            '</td>'+
            '<td class="attr" attrIds=""><a href="javascript:;" style="color: #0C78DD;" onclick="selectAttr(this);">暂无属性</a></td>'+
            '<td>'+
            '<a class="layui-btn layui-btn-xs" onclick="delCustomList(this)">删除</a><a class="layui-btn layui-btn-xs" onclick="addOK(this)">确定</a>'+
            '</td>'+
            '</tr>';
        $("#"+id).append(str);
        //重新初始化Iframe的高度
        cframeInit();
    }
    //修改
    function updateCustomList(_this){
        $(_this).parent().parent().find(".attr").find("a").attr("onclick","selectAttr(this);");
        $(_this).parent().parent().find(".attr").find("a").attr("style","color: #0C78DD;");
        let granularity = $(_this).parent().parent().find(".granularity").html();
        $(_this).parent().parent().find(".granularity").html('<input type="text" name="granularity" value="'+granularity+'" class="layui-input">');
        $(_this).parent().append('<a class="layui-btn layui-btn-xs" onclick="updateOK(this)">确定</a>');
        $(_this).hide();
        //重新初始化Iframe的高度
        cframeInit();
    }
    //删除
    function delCustomList(_this){
        layui.use(['form','laydate'], function() {
            layer.confirm('确定要删除么？', {
                btn: ['确定', '取消'] //按钮
            }, function() {
                var data = {
                    'Id':$(_this).parent().parent().find(".granularity").attr("unqid")
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Attribute/delGranularity",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "json",
                    success: function (response) {
                        if(response.code=="200"){
                            $(_this).parent().parent().remove();
                            layer.msg('删除成功', {
                                icon: 1
                            });
                        }else{
                            alert(response.message);
                        }

                    }
                });
            }, function() {
                layer.msg('取消删除', {
                    time: 2000 //20s后自动关闭
                });
            });
        });
    }
    //更新确定
    function updateOK(_this){
        var granularity =  $(_this).parent().parent().find("input[name='granularity']").val();
        var data = {
            'Id':$(_this).parent().parent().find(".granularity").attr("unqid"),
            'granularity':granularity,
            'attrId':$(_this).parent().parent().find(".attr").attr("attrIds"),
            'attrname':$(_this).parent().parent().find(".attr").find("a").html()
        }
        $.ajax({
            url: "<?php echo ROOT_ADMIN_REQUEST?>/Attribute/updateGranularity",
            type : "POST",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data : data,
            dataType : "json",
            success: function (response) {
                if(response.code=="200"&&response.data.num>0){
                    $(_this).parent().parent().find(".granularity").html(granularity);
                    $(_this).parent().parent().find(".attr").find("a").removeAttr("onclick");
                    $(_this).parent().parent().find(".attr").find("a").removeAttr("style");
                    $(_this).html("修改");
                    $(_this).attr("onclick","updateCustomList(this)");
                }else{
                    alert(response.message);
                }
                //重新初始化Iframe的高度
                cframeInit();
            }
        });
    }
    //添加OK
    function addOK(_this){
        var granularity =  $(_this).parent().parent().find("input[name='granularity']").val();
        var data = {
            'granularity':granularity,
            'attrId':$(_this).parent().parent().find(".attr").attr("attrIds"),
            'attrname':$(_this).parent().parent().find(".attr").find("a").html()
        }
        $.ajax({
            url: "<?php echo ROOT_ADMIN_REQUEST?>/Attribute/addGranularity",
            type : "POST",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data : data,
            dataType : "json",
            success: function (response) {
                if(response.code=="200"&&response.data.num>0){
                    $(_this).parent().parent().find(".granularity").attr("unqid",response.data.num);
                    $(_this).parent().parent().find(".granularity").html(granularity);
                    $(_this).parent().parent().find(".attr").find("a").removeAttr("onclick");
                    $(_this).parent().parent().find(".attr").find("a").removeAttr("style");
                    $(_this).html("修改");
                    $(_this).attr("onclick","updateCustomList(this)");
                }else{
                   alert(response.message);
                }
                //重新初始化Iframe的高度
                cframeInit();
            }
        });
    }

</script>
</body>

</html>