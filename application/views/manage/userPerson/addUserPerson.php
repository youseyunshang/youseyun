<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>添加用户</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .layui-form{
            margin-right: 30%;
        }
    </style>
</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" method="post" action="<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/addUserPerson/1">
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="username" lay-verify="required|userName" placeholder="例:admin" autocomplete="off" class="layui-input" >
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">电话</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="tel" lay-verify="required|phone|tel" autocomplete="off" class="layui-input">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">真实姓名</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="realname" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">登录密码</label>
            <div class="layui-input-inline shortInput">
                <input type="password" lay-verify="required" name="pwd" autocomplete="off" class="layui-input">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-inline shortInput">
                <input type="password" lay-verify="required" name="password2" autocomplete="off" class="layui-input">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">性别</label>
            <div class="layui-input-block">
                <input type="radio" name="gender" value="1" title="男" checked>
                <input type="radio" name="gender" value="0" title="女">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">单位</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="department" placeholder="例:lejaer" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="idCard" placeholder="例:492168228@qq.com" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">地区</label>
            <div class="layui-input-inline">
                <select name="provid" id="provid" lay-filter="provid">
                </select>
            </div>
            <div class="layui-input-inline">
                <select name="cityid" id="cityid" lay-filter="cityid">
                    <option value="">请选择市</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">身份证</label>
            <div class="layui-input-inline shortInput">
<!--                <input type="text" name="idCard" lay-verify="required|identity|identityTwo" placeholder="例:2101221991245543465" autocomplete="off" class="layui-input">-->
                <input type="text" name="idCard" placeholder="例:2101221991245543465" autocomplete="off" class="layui-input">
            </div>
<!--            <i class="iconfont icon-huaban bt"></i>-->
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">头像</label>
            <div class="layui-upload-drag" id="logo">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="logoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
           ?>
            <input type="text" value="" id="userLogo" style="display: none;" name="userLogo">
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司名称</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comName" value="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司类型</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comType" value="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司领域</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comOrigin" value="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司详情</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comContent" value="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司联系人</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comPerson" value="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">联系人电话</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comTel"  value="" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">营业执照</label>
            <div class="layui-upload-drag" id="binLicense">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="binLicenseImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
           ?>
            <input type="text" value="" id="comLicense" style="display: none;" name="comLicense">
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司头像</label>
            <div class="layui-upload-drag" id="binLogo">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="binLogoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            ?>
            <input type="text" value="" id="comLogo" style="display: none;" name="comLogo">
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submitBut">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        var data ='<?php echo $userInfo;?>',response = eval('('+data+')');
        //默认城市为：宁夏 - 银川
        var defaults = {
            s1: 'provid',
            s2: 'cityid',
            s3: 'areaid',
            v1: 210000,
            v2: 210100,
            v3: null
        };
        layui.use(['upload','form'], function() {
            var upload = layui.upload;
            var form = layui.form;
            form.verify({
                userName:function(value){
                    var flag = 0;
                    $.each(response,function(k,v){
                        if(value == v.username){
                            flag = 1;
                            return false;
                        }
                    });
                    if( flag > 0 ){
                        return "用户名已存在！";
                    }
                },
                tel:function(value){
                    var flag = 0;
                    $.each(response,function(k,v){
                        if(value == v.tel){
                            flag = 1;
                            return false;
                        }
                    });
                    if( flag > 0 ){
                        return "电话号码已存在！";
                    }
                },
                identityTwo:function(){
                    var flag = 0;
                    $.each(response,function(k,v){
                        if(value == v.idCard){
                            flag = 1;
                            return false;
                        }
                    });
                    if( flag > 0 ){
                        return "身份证号已存在！";
                    }
                }
            });

            //拖拽上传
            upload.render({
                elem: '#logo',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Home/upload/userlogo',
                done: function(res) {
                    if(res.upload_data){
                        $("#logoImg").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/"+res.upload_data.file_name);
                        $("#userLogo").val("/resource/uploads/userlogo/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#binLicense',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Home/upload/binLicense',
                done: function(res) {
                    if(res.upload_data){
                        $("#binLicenseImg").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/binLicense/"+res.upload_data.file_name);
                        $("#comLicense").val("/binLicense/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#binLogo',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Home/upload/binLogo',
                done: function(res) {
                    if(res.upload_data){
                        $("#binLogoImg").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/binLogo/"+res.upload_data.file_name);
                        $("#comLogo").val("/binLogo/"+res.upload_data.file_name);
                    }
                }
            });
        });
    </script>

</div>
</body>

</html>