<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>会员列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
</head>

<body>
<!--隐藏域Start-->
<div style="display: none;">
    <!--        修改-->
    <?php if(isset($_SESSION['url_permission']['/UserPerson/UpdateUser'])){
        echo '<input type="text" value="修改" id="updateUser">';
    }else{
        echo '<input type="text" value="" id="updateUser">';
    }?>
    <!--        删除-->
    <?php if(isset($_SESSION['url_permission']['/UserPerson/DelUser'])){
        echo '<input type="text" value="删除" id="delUser">';
    }else{
        echo '<input type="text" value="" id="delUser">';
    }?>
</div>
<!--隐藏域End-->
<form method="post" style="display: none;" action="<?php echo ROOT_URL_DEFINE?>/UserPerson/addUserAll" enctype="multipart/form-data">
        <input type="file" name="userfile" id="">
        <input type="submit" name="submit" id="">
</form>
<div class="cBody">
    <div class="console" style="padding:30px 0px;">
        <form class="layui-form" method="GET" action="<?php echo ROOT_URL_DEFINE?>/UserPerson/UserPersonList">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" id="selectName" name="selectName" required lay-verify="required" placeholder="真实姓名/用户名/电话" value="" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <select name="grade" id="grade" lay-filter="grade">
                        <option value="">所有会员</option>
                        <option value="1">普通会员</option>
                        <option value="2">高级会员</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <input type="text" name="startTime" id="startTime" placeholder="开始时间" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <input type="text" name="endTime" id="endTime" placeholder="结束时间" autocomplete="off" class="layui-input">
                </div>
<!--                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>-->
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
                <a href="javascript:;" onclick="adminPersionObj.selectList('1')" style="display: none;" id="nearEnd" class="layui-btn">即将到期会员</a>
                <a href="<?php echo ROOT_URL_DEFINE;?>/resource/UserInfo.xlsx" class="layui-btn">下载导入模板</a>
                <a href="javascript:;" onclick="adminPersionObj.upload()" class="layui-btn">导入</a>
                <a href="<?php echo ROOT_URL_DEFINE;?>/UserPerson/excelOut" class="layui-btn">导出</a>
            </div>
        </form>

        <script>
            layui.use(['form','laydate'], function() {
                var form = layui.form,laydate = layui.laydate;
                //监听提交
                form.on('select(grade)', function(data){
                    if(data.value=="2"){
                        $("#nearEnd").show();
                    }else{
                        $("#nearEnd").hide();
                    }
                });
                //日期
                laydate.render({
                    elem: '#startTime',
                    type: 'datetime'
                });
                laydate.render({
                    elem: '#endTime',
                    type: 'datetime'
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>用户名</th>
            <th>电话</th>
<!--            <th>真实姓名</th>-->
<!--            <th>性别</th>-->
<!--            <th>邮箱</th>-->
<!--            <th>单位</th>-->
            <th>会员等级</th>
            <th>创建日期</th>
            <th>到期时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;
            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });

        });
        var adminPersionObj = {
            'selectList':function (nearEndTime) {
                var nearEndTime = nearEndTime || '';
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val(),
                    'grade':$("#grade").val(),
                    'startTime':$("#startTime").val(),
                    'endTime':$("#endTime").val(),
                    'nearEndTime':nearEndTime
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/UserPersonList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        if(response){
                            var data = eval("("+response+")"),html = "";
                            //清空
                            $("#adminList").find("tbody").empty();
                            //填数据
                            $.each(data,function(k,v){
                                html = "";
                                html += "<tr>";
                                html += "<td>"+v.username+"</td>";
                                html += "<td>"+v.tel+"</td>";
                                // html += "<td>"+v.realname+"</td>";
                                // html += "<td>"+(v.gender>0?"男":"女")+"</td>";
                                // html += "<td>"+v.email+"</td>";
                                // if(v.comName){
                                //     html += "<td>"+v.comName+"</td>";
                                // }else{
                                //     html += "<td>暂无</td>";
                                // }
                                html += "<td>"+v.grade+"</td>";
                                html += "<td>"+v.createTime+"</td>";
                                html += "<td>"+v.endTime+"</td>";
                                html += "<td>";
                                if($("#updateUser").val()){
                                    html += "<a style='padding:0px 10px;color:#00f;' href='"+"<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/updateUser?Id="+v.Id+"'>修改</a>";
                                    html += "<a style='padding:0px 10px;color:#00f;' href='"+"<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/updateUser?Id="+v.Id+"'>查看</a>";
                                }else{
                                    html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                                }
                                if($("#delUser").val()){
                                    html += "<a style='padding:0px 10px;color:#00f;' onclick='adminPersionObj.delCustomList(this)' Id ='"+v.Id+"' href='javascript:;'>"+$("#delUser").val()+"</a>";
                                }else{
                                    html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                                }
                                html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/userAllInfo?Id="+v.Id+"&status="+v.status+"\">详情</a>";
                                html += "</td>";
                                html += "</tr>";
                                $('tbody').append(html);
                            });
                        }

                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);
            },
            'delCustomList':function(_this){
                layui.use(['form','laydate'], function() {
                    layer.confirm('确定要删除么？', {
                        btn: ['确定', '取消'] //按钮
                    }, function() {
                        var data = {
                            'Id':$(_this).attr("Id")
                        }
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/delUser",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                $(_this).parent().parent().remove();
                                layer.msg('删除成功', {
                                    icon: 1
                                });
                            }
                        });
                    }, function() {
                        layer.msg('取消删除', {
                            time: 2000 //20s后自动关闭
                        });
                    });
                });
            }
        }
    </script>
</div>
</body>

</html>