<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>用户详情</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <style>
        .layui-form{
            margin-right: 30%;
        }
        .layui-form-item{
            margin:20px 0px;
        }
    </style>

</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" action="#">
        <div class="layui-form-item" style="display: none;">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="UserId" value="<?php echo $userInfo['Id'];?>" placeholder="例:admin" autocomplete="off" class="layui-input">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="username" value="<?php echo $userInfo['username'];?>" style="border: 0px;" readonly lay-verify="required" placeholder="例:admin" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">电话号码：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="tel" value="<?php echo $userInfo['tel'];?>" style="border: 0px;" readonly lay-verify="required|phone" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">真实姓名：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="realname" value="<?php echo $userInfo['realname'];?>" style="border: 0px;" readonly lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item" style="display: none;">
            <label class="layui-form-label">登录密码</label>
            <div class="layui-input-inline shortInput">
                <input type="password" name="pwd" value="123456" lay-verify="required" autocomplete="off" class="layui-input">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item" style="display: none;">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-inline shortInput">
                <input type="password" name="password2" value="123456" lay-verify="required" autocomplete="off" class="layui-input">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">性别：</label>
            <div class="layui-input-block">
                <input type="radio" name="gender" value="1" title="男" style="border: 0px;" readonly <?php if($userInfo['gender']>0){ echo "checked";}?>>
                <input type="radio" name="gender" value="0" title="女" style="border: 0px;" readonly <?php if($userInfo['gender']<1){ echo "checked";}?>>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">部门：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="department" value="<?php echo $userInfo['department']?>" style="border: 0px;" readonly placeholder="例:研发部" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="email" required value="<?php echo $userInfo['email']?>" style="border: 0px;" readonly lay-verify="required|email" placeholder="例:492168228@qq.com" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">地区：</label>
            <div class="layui-input-inline">
                <select name="provid" id="provid" lay-filter="provid">
                </select>
            </div>
            <div class="layui-input-inline">
                <select name="cityid" id="cityid" lay-filter="cityid">
                    <option value="">请选择市</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">身份证：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="idCard" style="border: 0px;" readonly required value="<?php echo $userInfo['idCard']?>" lay-verify="required|identity" placeholder="例:18341264232" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">头像：</label>
            <?php if(!empty($userInfo['userLogo'])){
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads'.$userInfo['userLogo'].'" id="logoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="logoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }?>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司名称：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comName" style="border: 0px;" readonly value="<?php echo $userInfo['comName']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司类型：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comType" style="border: 0px;" readonly value="<?php echo $userInfo['comType']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司领域：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comOrigin" style="border: 0px;" readonly value="<?php echo $userInfo['comOrigin']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司详情：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comContent" style="border: 0px;" readonly value="<?php echo $userInfo['comContent']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">联系人：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comPerson" style="border: 0px;" readonly value="<?php echo $userInfo['comPerson']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">电话：</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comTel" style="border: 0px;" readonly value="<?php echo $userInfo['comTel']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">营业执照：</label>
            <?php if(!empty($userInfo['comLicense'])){
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads'.$userInfo['comLicense'].'" id="binLicenseImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="binLicenseImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }?>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司头像：</label>
            <?php if(!empty($userInfo['comLogo'])){
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads'.$userInfo['comLogo'].'" id="binLogoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="binLogoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }?>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">喜好标签：</label>
            <div class="layui-input-inline shortInput">
            <?php
               if(!empty($userInfo['signall'])&&$signall = json_decode($userInfo['signall'],true)){
                    $joinArr = array();
                    foreach ($signall as $key=>&$val){
                        $joinArr[] = $val['attrname'];
                    }
                    echo implode("、",$joinArr);
               }
            ?>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <a class="layui-btn" onclick="myObj.agree(1)">通过</a>
                <a class="layui-btn layui-btn-primary" onclick="myObj.agree(0)">驳回</a>
            </div>
        </div>
        <div class="layui-form-item">
            <div style="width:100%;height:100px;">
            </div>
        </div>
    </form>

    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        //默认城市为：宁夏 - 银川
        var defaults = {
            s1: 'provid',
            s2: 'cityid',
            s3: 'areaid',
            v1: '<?php echo $userInfo['provid']?>',
            v2: '<?php echo $userInfo['cityid']?>',
            v3: null
        };
        layui.use(['upload'], function() {
            var upload = layui.upload;
        });
        var myObj = {
            "agree":function(status){
                var data = {
                    'status':status,
                    "Id":$("input[name='UserId']").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/userStatus",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        //在原来的页面跳转到下载word模板的页面
                        if(response>0){
                            window.location.href = "<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/UserPersonList";
                        }
                    }
                });
            },
            "noagree":function(){

            }
        }
    </script>

</div>
</body>

</html>