<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>修改物流质检</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <style>
        .layui-form{
            margin-right: 30%;
        }
    </style>

</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" method="post" action="<?php echo ROOT_ADMIN_REQUEST?>/Company/updateCompany">
        <div class="layui-form-item" style="display: none;">
                <input type="text" name="comType" value="<?php echo $userInfo['comType'];?>" autocomplete="off" class="layui-input">
                <input type="text" name="Id" value="<?php echo $userInfo['Id'];?>" placeholder="例:admin" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司名称</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comName" value="<?php echo $userInfo['comName']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司领域</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comOrigin" value="<?php echo $userInfo['comOrigin']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">地区</label>
            <div class="layui-input-inline">
                <select name="provid" id="provid" lay-filter="provid">
                </select>
            </div>
            <div class="layui-input-inline">
                <select name="cityid" id="cityid" lay-filter="cityid">
                    <option value="">请选择市</option>
                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">详细地址</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="location" value="<?php echo $userInfo['location']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司详情</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comContent" value="<?php echo $userInfo['comContent']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司联系人</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comPerson" value="<?php echo $userInfo['comPerson']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">联系人电话</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="comTel"  value="<?php echo $userInfo['comTel']?>" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">营业执照</label>
            <div class="layui-upload-drag" id="binLicense">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php if(!empty($userInfo['comLicense'])){
                echo '<img src="'.$userInfo['comLicense'].'" id="binLicenseImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="binLicenseImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }?>
            <input type="text" value="<?php echo $userInfo['comLicense']?>" id="comLicense" style="display: none;" name="comLicense">
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">公司头像</label>
            <div class="layui-upload-drag" id="binLogo">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php if(!empty($userInfo['comLogo'])){
                echo '<img src="'.$userInfo['comLogo'].'" id="binLogoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="binLogoImg" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            ';
            }?>
            <input type="text" value="<?php echo $userInfo['comLogo']?>" id="comLogo" style="display: none;" name="comLogo">
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submitBut">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        //默认城市为：宁夏 - 银川
        var defaults = {
            s1: 'provid',
            s2: 'cityid',
            s3: 'areaid',
            v1: '<?php echo $userInfo['provid']?>',
            v2: '<?php echo $userInfo['cityid']?>',
            v3: null
        };
        layui.use(['upload'], function() {
            var upload = layui.upload;
            //拖拽上传
            upload.render({
                elem: '#binLicense',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Home/upload/binLicense',
                done: function(res) {
                    if(res.upload_data){
                        $("#binLicenseImg").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/binLicense/"+res.upload_data.file_name);
                        $("#comLicense").val("<?php echo ROOT_URL_DEFINE;?>/resource/uploads/binLicense/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#binLogo',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Home/upload/binLogo',
                done: function(res) {
                    if(res.upload_data){
                        $("#binLogoImg").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/binLogo/"+res.upload_data.file_name);
                        $("#comLogo").val("<?php echo ROOT_URL_DEFINE;?>/resource/uploads/binLogo/"+res.upload_data.file_name);
                    }
                }
            });
        });
    </script>

</div>
</body>

</html>