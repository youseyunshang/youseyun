<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>物流企业列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
</head>

<body>
<!--隐藏域Start-->
<div style="display: none;">
    <!--        修改-->
    <?php if(isset($_SESSION['url_permission']['/Company/UpdateCompany'])){
        echo '<input type="text" value="修改" id="update">';
    }else{
        echo '<input type="text" value="" id="update">';
    }?>
    <!--        删除-->
    <?php if(isset($_SESSION['url_permission']['/Company/DelCompany'])){
        echo '<input type="text" value="删除" id="del">';
    }else{
        echo '<input type="text" value="" id="del">';
    }?>
</div>
<!--隐藏域End-->
<div class="cBody">
    <div class="console">
        <form class="layui-form" method="GET" action="<?php echo ROOT_URL_DEFINE?>/UserPerson/UserPersonList">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" id="selectName" name="selectName" placeholder="公司名称" value="" autocomplete="off" class="layui-input">
                </div>
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>公司名称</th>
            <th>公司领域</th>
            <th>公司详情</th>
<!--            <th>地址</th>-->
            <th>联系人</th>
            <th>联系电话</th>
<!--            <th>创建人</th>-->
            <th>创建时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val(),
                    'comType':'<?php echo $comType;?>'
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Company/companyList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        var data = eval("("+response+")"),html = "";
                        //清空
                        $("#adminList").find("tbody").empty();
                        //填数据
                        $.each(data,function(k,v){
                            html = "";
                            html += "<tr>";
                            html += "<td>"+v.comName+"</td>";
                            html += "<td>"+v.comOrigin+"</td>";
                            html += "<td>"+v.comContent+"</td>";
                            // html += "<td>"+v.provid+" "+v.cityid+"</td>";
                            html += "<td>"+v.comPerson+"</td>";
                            html += "<td>"+v.comTel+"</td>";
                            // html += "<td>"+v.username+"</td>";
                            html += "<td>"+v.createTime+"</td>";
                            html += "<td>";
                             if($("#update").val()){
                                html += "<a style='padding:0px 10px;color:#00f;' href='"+"<?php echo ROOT_ADMIN_REQUEST?>/Company/updateCompany?Id="+v.Id+"'>修改</a>";
                                html += "<a style='padding:0px 10px;color:#00f;' href='"+"<?php echo ROOT_ADMIN_REQUEST?>/Company/updateCompany?Id="+v.Id+"'>查看</a>";
                            }else{
                                html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                             }
                            if($("#del").val()){
                                html += "<a style='padding:0px 10px;color:#00f;' onclick='adminPersionObj.delCustomList(this)' Id ='"+v.Id+"' href='javascript:;'>删除</a>";
                             }else{
                                html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                            }
                            html += "</td>";
                            html += "</tr>";
                            $(html).prependTo('tbody');
                        });
                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);

            },
            'delCustomList':function(_this){
                layui.use(['form','laydate'], function() {
                    layer.confirm('确定要删除么？', {
                        btn: ['确定', '取消'] //按钮
                    }, function() {
                        var data = {
                            'Id':$(_this).attr("Id")
                        }
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/Company/delCompany",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                $(_this).parent().parent().remove();
                                layer.msg('删除成功', {
                                    icon: 1
                                });
                            }
                        });
                    }, function() {
                        layer.msg('取消删除', {
                            time: 2000 //20s后自动关闭
                        });
                    });
                });
            }
        }
    </script>
</div>
</body>

</html>