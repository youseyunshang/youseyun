<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>管理员列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<div class="cBody">
    <div style="display: none;">
<!--        修改-->
        <?php if(isset($_SESSION['url_permission']['/Role/ShowRole'])){
            echo '<input type="text" value="修改" id="updateRole">';
        }else{
            echo '<input type="text" value="" id="updateRole">';
        }?>
<!--        查看-->
        <?php if(isset($_SESSION['url_permission']['/Role/ShowRole'])){
            echo '<input type="text" value="查看" id="showRole">';
        }else{
            echo '<input type="text" value="" id="showRole">';
        }?>
<!--        删除-->
        <?php if(isset($_SESSION['url_permission']['/Role/DelRole'])){
            echo '<input type="text" value="删除" id="delRole">';
        }else{
            echo '<input type="text" value="" id="delRole">';
        }?>
    </div>
    <div class="console">
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <select name="status" id="status">
                        <option value="">--所有--</option>
                        <option value="1">待处理</option>
                        <option value="2">处理中</option>
                        <option value="3">已完成</option>
                    </select>
                </div>
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
            </div>
        </form>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>订单编号</th>
            <th>投诉公司</th>
            <th>投诉内容</th>
            <th>业务员名称</th>
            <th>业务员电话</th>
            <th>投诉人</th>
            <th>投诉时间</th>
            <th>状态</th>
<!--            <th>操作</th>-->
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'status':$("#status").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Dispute/disputeList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        if(response){
                            var data = eval("("+response+")"),html = "";
                            // console.log(data);
                            //清空
                            $("#adminList").find("tbody").empty();
                            //填数据
                            $.each(data,function(k,v){
                                html = "";
                                html += "<tr>";
                                html += "<td>"+v.ordernumber+"</td>";
                                html += "<td>"+v.disputeCom+"</td>";
                                html += "<td>"+v.content+"</td>";
                                html += "<td>"+v.saleman+"</td>";
                                html += "<td>"+v.saleTel+"</td>";
                                html += "<td>"+v.username+"</td>";
                                html += "<td>"+v.createTime+"</td>";
                                html += "<td>待处理</td>";
                                //html += "<td>";
                                // html += "<a style='padding:0px 10px;color:#00f;' href='#'>解决</a>";
                                // html += "<a style='padding:0px 10px;color:#00f;' href='#'>驳回</a>";
                                //html += "</td>";
                                html += "</tr>";
                                $(html).prependTo('tbody');
                            });
                        }
                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);

            }
        }
    </script>
</div>
</body>

</html>