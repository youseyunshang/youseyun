<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>后台首页</title>

    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <!--[if lt IE 9]>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/html5shiv.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/frameStyle.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/frame.js" ></script>
    <style>
        .oneItem{
            display: inline-block;
            width: 10px;
            height: 10px;
            float: right;margin-right: 10px;
        }
    </style>
</head>

<body>
<!-- 左侧菜单 - 开始 -->
<div class="frameMenu">
    <div class="logo">
        <a href="<?php echo ROOT_ADMIN_REQUEST?>/home/indexDefault" style="display: inline-block;">
        <img src="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/logo.png"/>
        <div class="logoText">
            <h1>有色云</h1>
            <p>youseyun</p>
        </div>
        </a>
    </div>
    <div class="menu">
        <ul>
            <?php foreach ($permission as $key=>&$val){
                echo '<li><a class="menuFA" href="javascript:void(0)" onclick="menuCAClick(\''.ROOT_ADMIN_REQUEST.$val['purl'].'\',this)"><i class="iconfont '.$val['picon'].' left"></i>'.$val['pname'].'<span class="oneItem"><i class="icon iconfont icon-arrow-down"></i></span></a>';
                if(isset($val['sub'])){
                    echo "<dl>";
                    foreach ($val['sub'] as $k=>&$v){
                        if(!empty($v['picon'])){
                            echo '<dt><a href="javascript:void(0)" onclick="menuCAClick(\''.ROOT_ADMIN_REQUEST.$v['purl'].'\',this)">'.$v['pname'].'</a></dt>';
                        }
                    }
                    echo "</dl>";
                }
                echo "</li>";
            }?>
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>会员管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>角色管理</a>-->
<!--                <dl>-->
<!--                    <dt><a href="javascript:void(0)" onclick="menuCAClick('http://127.0.0.1/ci/admin.php/Role/addRole',this)">添加角色</a></dt>-->
<!--                    <dt><a href="javascript:void(0)" onclick="menuCAClick('tgls/agent/agent_list.html',this)">角色列表</a></dt>-->
<!--                </dl>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>管理员管理</a>-->
<!--                <dl>-->
<!--                    <dt><a href="javascript:void(0)" onclick="menuCAClick('tgls/agent/agent_add.html',this)">添加管理员</a></dt>-->
<!--                    <dt><a href="javascript:void(0)" onclick="menuCAClick('tgls/agent/agent_list.html',this)">管理员列表</a></dt>-->
<!--                </dl>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>金属管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>信息管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>问答管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>供应信息管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>求购信息管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>平台委托管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>平台业务员管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>物流企业管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>质检企业管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>订单管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>喜好标签管理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>订单争议处理</a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a class="menuFA" href="javascript:void(0)" onclick="menuCAClick('tgls/qdAPI.html',this)"><i class="iconfont icon-zhishi left"></i>网站基本信息设置</a>-->
<!--            </li>-->

        </ul>
    </div>
</div>
<!-- 左侧菜单 - 结束 -->

<div class="main">
    <!-- 头部栏 - 开始 -->
    <div class="frameTop">
        <img class="jt" src="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/top_jt.png"/>
        <div class="topMenu">
            <ul>
                <li><a href="javascript:void(0)" onclick="menuCAClick('<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/updateAdmin?username=<?php echo $_SESSION['username'];?>',this)"><i class="iconfont icon-yonghu1"></i><?php echo $realname;?></a></li>
                <li><a href="javascript:void(0)" onclick="menuCAClick('<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/updateAdmin?username=<?php echo $_SESSION['username'];?>',this)"><i class="iconfont icon-xiugaimima"></i>修改密码</a></li>
                <li><a href="<?php echo ROOT_ADMIN_REQUEST?>/home/logoutUser"><i class="iconfont icon-084tuichu"></i>注销</a></li>
            </ul>
        </div>
    </div>
    <!-- 头部栏 - 结束 -->

    <!-- 核心区域 - 开始 -->
    <div class="frameMain">
        <div class="title" id="frameMainTitle">
            <span><i class="iconfont icon-xianshiqi"></i>后台首页</span>
        </div>
        <div class="con">
            <iframe id="mainIframe" src="<?php echo ROOT_ADMIN_REQUEST?>/home/indexData"></iframe>
        </div>
    </div>
    <!-- 核心区域 - 结束 -->
</div>
</body>

</html>