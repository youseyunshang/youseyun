<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>首页内容</title>

    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <!--[if lt IE 9]>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/html5shiv.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/frameStyle.css">

</head>
<style>
    html,body{
        background-color: #f2f2f2;
        color: #666;
    }
    .layui-card-header-icon{
        float: right;
        top:12px;
    }
</style>
<body>
<div class="cBody">
    <!-- 左侧菜单 - 开始 -->
    <div class="layui-fluid" style="margin:50px 0px;">
        <div class="layui-row layui-col-space15">
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header" style="font-size: 14px;">
                        <span>会员数量</span>
                        <span class="layui-badge layui-bg-blue layuiadmin-badge layui-card-header-icon">全</span>
                    </div>
                    <div class="layui-card-body layuiadmin-card-list">
                        <p style="font-size: 35px;text-align:center;line-height: 50px;height:50px;"><a style="cursor: pointer;" href="<?php echo ROOT_ADMIN_REQUEST?>/UserPerson/UserPersonList"><?php echo $usercount;?></a></p>
                        <p style="font-size: 14px;line-height: 50px;height:50px;">
                            <span>总量</span>
                            <span style="float: right;"><?php echo $usercount;?> <i class="layui-icon"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header" style="font-size: 14px;">
                        <span>成交订单数量</span>
                        <span class="layui-badge layui-bg-yellow layuiadmin-badge layui-card-header-icon">全</span>
                    </div>
                    <div class="layui-card-body layuiadmin-card-list">
                        <p style="font-size: 35px;text-align:center;line-height: 50px;height:50px;"><?php echo $ordercount;?></p>
                        <p style="font-size: 14px;line-height: 50px;height:50px;">
                            <span>总量</span>
                            <span style="float: right;"><?php echo $ordercount;?><i class="layui-icon"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header" style="font-size: 14px;">
                        <span>今日成交吨数</span>
                        <span class="layui-badge layui-bg-blue layuiadmin-badge layui-card-header-icon">日</span>
                    </div>
                    <div class="layui-card-body layuiadmin-card-list">
                        <p style="font-size: 35px;text-align:center;line-height: 50px;height:50px;"><?php echo $dayNum;?></p>
                        <p style="font-size: 14px;line-height: 50px;height:50px;">
                            <span>总量</span>
                            <span style="float: right;">88万 <i class="layui-icon"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header" style="font-size: 14px;">
                        <span>本周成交吨数</span>
                        <span class="layui-badge layui-bg-cyan layuiadmin-badge layui-card-header-icon">周</span>
                    </div>
                    <div class="layui-card-body layuiadmin-card-list">
                        <p style="font-size: 35px;text-align:center;line-height: 50px;height:50px;"><?php echo $weekNum;?></p>
                        <p style="font-size: 14px;line-height: 50px;height:50px;">
                            <span>总量</span>
                            <span style="float: right;">88万 <i class="layui-icon"></i></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!--        供应求购-->
        <div class="layui-row layui-col-space15">
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header" style="font-size: 14px;">
                        <span>供应信息数量</span>
                        <span class="layui-badge layui-bg-yellow layuiadmin-badge layui-card-header-icon">全</span>
                    </div>
                    <div class="layui-card-body layuiadmin-card-list">
                        <p style="font-size: 35px;text-align:center;line-height: 50px;height:50px;"><?php echo $purchasecount;?></p>
                        <p style="font-size: 14px;line-height: 50px;height:50px;">
                            <span>总量</span>
                            <span style="float: right;"><?php echo $purchasecount;?><i class="layui-icon"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header" style="font-size: 14px;">
                        <span>求购信息数量</span>
                        <span class="layui-badge layui-bg-yellow layuiadmin-badge layui-card-header-icon">全</span>
                    </div>
                    <div class="layui-card-body layuiadmin-card-list">
                        <p style="font-size: 35px;text-align:center;line-height: 50px;height:50px;"><?php echo $supplecount;?></p>
                        <p style="font-size: 14px;line-height: 50px;height:50px;">
                            <span>总量</span>
                            <span style="float: right;"><?php echo $supplecount;?> <i class="layui-icon"></i></span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm6 layui-col-md3">
                <div class="layui-card">
                    <div class="layui-card-header" style="font-size: 14px;">
                        <span>问题数量</span>
                        <span class="layui-badge layui-bg-yellow layuiadmin-badge layui-card-header-icon">全</span>
                    </div>
                    <div class="layui-card-body layuiadmin-card-list">
                        <p style="font-size: 35px;text-align:center;line-height: 50px;height:50px;"><?php echo $questioncount;?></p>
                        <p style="font-size: 14px;line-height: 50px;height:50px;">
                            <span>总量</span>
                            <span style="float: right;"><?php echo $questioncount;?><i class="layui-icon"></i></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-row layui-col-space15" style="background: #fff;margin:30px 0px;">
            <script src="<?php echo ROOT_URL_DEFINE?>/resource/layui/frappe-charts.min.js"></script>
            <div id="chart"></div>
            <script>
                // Javascript
                // let chart = new Chart( "#chart", { // or DOM element
                //     data: {
                //         labels: ["2019-08-01","2019-08-02","2019-08-03","2019-08-04","2019-08-05","2019-08-06","2019-08-07","2019-08-08","2019-08-09","2019-08-10","2019-08-11","2019-08-12","2019-08-13","2019-08-14","2019-08-15", "2019-08-16", "2019-08-17", "2019-08-18",
                //             "2019-08-19", "2019-08-20", "2019-08-21","2019-08-22", "2019-08-23", "2019-08-24","2019-08-25", "2019-08-26", "2019-08-27","2019-08-28", "2019-08-29", "2019-08-30","2019-08-31"],
                //         datasets: [
                //             {
                //                 title: "信息总量",
                //                 type: 'axis-mixed',
                //                 values: [25, 40, 30, 35, 8, 52, 17,25, 40, 30, 35, 8, 52, 17,25, 40, 30, 35, 8, 52, 17,25, 40, 30, 35, 8, 52, 17,25, 40, 30]
                //             },
                //             {
                //                 title: "问答总量",
                //                 type: 'axis-mixed',
                //                 values: [25, 50, 10, 15, 18, 32, 27,25, 50, 10, 15, 18, 32, 27,25, 50, 10, 15, 18, 32, 27,25, 50, 10, 15, 18, 32, 27,25, 50, 10]
                //             },
                //             {
                //                 title: "订单总量",
                //                 type: 'axis-mixed',
                //                 values: [15, 20, 3, 15, 58, 12, 17,15, 20, 3, 15, 58, 12, 17,15, 20, 3, 15, 58, 12, 17,15, 20, 3, 15, 58, 12, 17,15, 20, 3]
                //             }
                //         ]
                //     },
                //
                //     title: "数据统计图",
                //     type: 'axis-mixed', // or 'bar', 'line', 'pie', 'percentage'
                //     height: 250,
                //     colors: ['purple', '#ffa3ef', 'red']
                // });
                // $("document").ready(function(){
                //     $(".indicator:eq(0)").html("<i style='background: purple'></i>信息总量");
                //     $(".indicator:eq(1)").html("<i style='background: #ffa3ef'></i>问答总量");
                //     $(".indicator:eq(2)").html("<i style='background: #ff5858'></i>订单总量");
                // });
                let chart = new Chart( "#chart", { // or DOM element
                    data: {
                        labels: ["2019-08-01","2019-08-02","2019-08-03","2019-08-04","2019-08-05","2019-08-06","2019-08-07","2019-08-08","2019-08-09","2019-08-10","2019-08-11","2019-08-12","2019-08-13","2019-08-14","2019-08-15"],
                        datasets: [
                            {
                                title: "信息总量",
                                type: 'axis-mixed',
                                values: [25, 40, 30, 35, 8, 52, 17,25, 40, 30, 35, 8, 52, 17,25]
                            },
                            {
                                title: "问答总量",
                                type: 'axis-mixed',
                                values: [25, 50, 10, 15, 18, 32, 27,25, 50, 10, 15, 18, 32, 27,25]
                            },
                            {
                                title: "订单总量",
                                type: 'axis-mixed',
                                values: [15, 20, 3, 15, 58, 12, 17,15, 20, 3, 15, 58, 12, 17,15]
                            }
                        ]
                    },

                    title: "数据统计图",
                    type: 'axis-mixed', // or 'bar', 'line', 'pie', 'percentage'
                    height: 250,
                    colors: ['purple', '#ffa3ef', 'red']
                });
                $("document").ready(function(){
                    $(".indicator:eq(0)").html("<i style='background: purple'></i>信息总量");
                    $(".indicator:eq(1)").html("<i style='background: #ffa3ef'></i>问答总量");
                    $(".indicator:eq(2)").html("<i style='background: #ff5858'></i>订单总量");
                });
            </script>
        </div>
    </div>

    <div style="width: 100%;height:200px;">

    </div>
</div>
</body>

</html>
