<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>修改标签</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <style>
        .layui-form{
            margin-right: 30%;
        }
    </style>

</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" method="post" action="<?php echo ROOT_ADMIN_REQUEST?>/Sign/updateSign/1">
        <div class="layui-form-item" style="display: none;">
            <label class="layui-form-label">标签Id</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="Id" value="<?php echo $data['Id'];?>" >
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">标签名称</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="sign" value="<?php echo $data['attrname'];?>" placeholder="例:特朗普" autocomplete="off" class="layui-input" lay-verify="signName">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submitBut">修改</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        var data ='<?php echo json_encode($allSign);?>',response = eval('('+data+')');
        layui.use('form',function(){
            var form = layui.form;
            form.verify({
                signName:function(value){
                    var flag = 0;
                    $.each(response,function(k,v){
                        if(value == v.attrname){
                            flag = 1;
                            return false;
                        }
                    });
                    if( flag > 0 ){
                        return "标签已存在！";
                    }
                }
            });
        });
        //默认城市为：宁夏 - 银川
        var defaults = {
            s1: 'provid',
            s2: 'cityid',
            s3: 'areaid',
            v1: 510000,
            v2: 510100,
            v3: null
        };
    </script>

</div>
</body>

</html>