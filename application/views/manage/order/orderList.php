<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>订单列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/css/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<div class="cBody">
    <div class="console" style="padding: 30px 0px;">
        <form class="layui-form" method="GET" action="#">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" id="selectOrderNumber" name="selectOrderNumber" placeholder="订单编号" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <input type="text" id="selectName" name="selectName" placeholder="业务人员" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <input type="text" name="startTime" id="startTime" placeholder="开始时间" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <input type="text" name="endTime" id="endTime" placeholder="结束时间" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <select name="status" id="status" lay-filter="status">
                        <option value="">所有订单</option>
                        <option value="1">待审查</option>
                        <option value="2">待付款</option>
                        <option value="3">待收货</option>
                        <option value="4">待发货</option>
                        <option value="5">交易成功</option>
                        <option value="6">交易取消</option>
                        <option value="7">售后处理</option>
                    </select>
                </div>
<!--                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>-->
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
<!--                <a href="--><?php //echo ROOT_URL_DEFINE;?><!--/UserPerson/excelOut" class="layui-btn">导出</a>-->
            </div>
        </form>
    </div>
    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>订单编号</th>
            <th>说明</th>
            <th>数量</th>
            <th>采购人</th>
            <th>采购电话</th>
            <th>供应人</th>
            <th>供应电话</th>
            <th>业务人员</th>
            <th>创建时间</th>
            <th>更新日期</th>
            <th>状态</th>
<!--            <th>操作</th>-->
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer','form','laydate'], function() {
            var laypage = layui.laypage,layer = layui.layer,form = layui.form,laydate = layui.laydate;
            //监听提交
            form.on('submit(formDemo)', function(data) {
                layer.msg(JSON.stringify(data.field));
                return false;
            });
            //日期
            laydate.render({
                elem: '#startTime',
                type: 'datetime'
            });
            laydate.render({
                elem: '#endTime',
                type: 'datetime'
            });
            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html()||'1';
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val(),
                    'selectOrderNumber':$("#selectOrderNumber").val(),
                    'startTime':$("#startTime").val(),
                    'endTime':$("#endTime").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_URL_DEFINE?>/Order/orderList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        if(response){
                            var data = eval("("+response+")"),html = "";
                            //清空
                            $("#adminList").find("tbody").empty();
                            //填数据
                            $.each(data,function(k,v){
                                html = "";
                                html += "<tr>";
                                html += "<td>"+v.orderNumber+"</td>";
                                html += "<td>"+v.content+"</td>";
                                html += "<td>"+v.num+"</td>";
                                html += "<td>"+v.purchasePerson+"</td>";
                                html += "<td>"+v.purchaseTel+"</td>";
                                html += "<td>"+v.supplePerson+"</td>";
                                html += "<td>"+v.suppleTel+"</td>";
                                html += "<td>"+v.username+"</td>";
                                html += "<td>"+v.createTime+"</td>";
                                html += "<td>"+v.lastTime+"</td>";
                                html += "<td>"+v.status+"</td>";
                                // html += "<td>";
                                //if($("#delUser").val()){
                                //    html += "<a style='padding:0px 10px;color:#00f;' onclick='adminPersionObj.delCustomList(this)' Id ='"+v.Id+"' href='javascript:;'>"+$("#delUser").val()+"</a>";
                                //    html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php //echo ROOT_URL_DEFINE?>///Order/delOrder?Id="+v.Id+"\">删除</a>";
                                //}else{
                                //    html += "<a style='padding:0px 10px;color:#00f;' href='javascript:;'>权限不足</a>";
                                //}
                                //
                                //html += "</td>";
                                html += "</tr>";
                                $(html).prependTo('tbody');
                            });
                        }
                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);

            }
        }
    </script>
</div>
</body>

</html>