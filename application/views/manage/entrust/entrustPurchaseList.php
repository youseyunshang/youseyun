<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>采购列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<div class="cBody">
    <div class="console">
        <form class="layui-form" method="GET" action="#">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" id="selectName" name="selectName" required lay-verify="required" placeholder="名称" value="" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <select name="status" id="status">
                        <option value="">--所有--</option>
                        <option value="1">待处理</option>
                        <option value="2">处理中</option>
                        <option value="3">已完成</option>
                        <option value="4">已驳回</option>
                    </select>
                </div>
                <div class="layui-input-inline">
                    <input type="text" name="startTime" id="startTime" placeholder="开始时间" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <input type="text" name="endTime" id="endTime" placeholder="结束时间" autocomplete="off" class="layui-input">
                </div>
                <!--                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>-->
                <a href="javascript:;" onclick="adminPersionObj.selectList()" class="layui-btn">检索</a>
                <!--                <a href="--><?php //echo ROOT_URL_DEFINE;?><!--/UserPerson/excelOut" class="layui-btn">导出</a>-->
            </div>
        </form>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>产品名称</th>
            <th>产品数量</th>
            <th>委托结束时间</th>
            <th>联系人</th>
            <th>手机号</th>
            <th>委托创建人</th>
            <th>创建创建时间</th>
            <th>状态</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['form','laydate','laypage', 'layer'], function() {
            var laypage = layui.laypage,layer = layui.layer,form = layui.form,laydate = layui.laydate;
            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: '<?php echo $count;?>'
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.selectList();
                }
            });
            //日期
            laydate.render({
                elem: '#startTime',
                type: 'datetime'
            });
            laydate.render({
                elem: '#endTime',
                type: 'datetime'
            });
        });
        var adminPersionObj = {
            'selectList':function () {
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val(),
                    'status':$("#status").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Entrust/entrustPurchaseList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        if(response){
                            var data = eval("("+response+")"),html = "";
                            //清空
                            $("#adminList").find("tbody").empty();
                            //填数据
                            $.each(data,function(k,v){
                                html = "";
                                html += "<tr>";
                                html += "<td>"+v.title+"</td>";
                                html += "<td>"+v.num+"</td>";
                                html += "<td>"+v.endTime+"</td>";
                                html += "<td>"+v.billHolder+"</td>";
                                html += "<td>"+v.tel+"</td>";
                                html += "<td>"+v.username+"</td>";
                                html += "<td>"+v.createTime+"</td>";
                                html += "<td><span>"+v.status+"</span><a style='padding:0px 10px;color:#00f;margin-left:20px;' href='javascript:;' eId='"+v.Id+"' onclick='adminPersionObj.updateStatus(this)'>状态调整</a></td>";
                                html += "</tr>";
                                $(html).prependTo('tbody');
                            });
                        }
                    }
                });
            },
            'updateStatus':function(that){
                var Id = $(that).attr("eId");
                layer.open({
                    type:2,
                    area:['300px','300px'],
                    title:'委托状态',
                    zIndex:999,
                    scrollbar:false,
                    btn:['确认','取消'],
                    content: '<?php echo ROOT_URL_DEFINE?>/Entrust/entrustIframe',
                    yes:function(index,layero){
                        var data ={"Id":Id,"status":$(layero).find("iframe")[0].contentWindow.selectData()};

                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/Entrust/entrustIframeStatus",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                switch (data.status) {
                                    case '1':
                                        $(that).parent().find('span').html("待处理");
                                        break;
                                    case '2':
                                        $(that).parent().find('span').html("处理中");
                                        break;
                                    case '3':
                                        $(that).parent().find('span').html("已完成");
                                        break;
                                    case '4':
                                        $(that).parent().find('span').html("已驳回");
                                        break;
                                }
                                layer.close(index);
                            }
                        });
                    },
                    btn2:function(index){
                        layer.close(index);
                    }
                });
            },
            'upload':function () {
                $('input[name="userfile"]').click();
                setInterval(function () {
                    if($('input[name="userfile"]').val()){
                        $('input[name="submit"]').click();
                    }
                },500);

            }
        }
    </script>
</div>
</body>

</html>