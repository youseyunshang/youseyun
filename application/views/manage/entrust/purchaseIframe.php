<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>状态</title>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
</head>

<body>
    <div style="text-align: center;margin-top:30px;">
        <select name="status" id="status" style="    width: 180px;
    height: 40px;">
            <option value="">--所有--</option>
            <option value="1">待处理</option>
            <option value="2">处理中</option>
            <option value="3">已完成</option>
            <option value="4">已驳回</option>
        </select>
    </div>
    <script>
        var selectData = function(){
            return $('select').val();
        }
    </script>
</body>

</html>