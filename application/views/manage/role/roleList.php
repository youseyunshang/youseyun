<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>管理员列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<div class="cBody">
    <div style="display: none;">
<!--        修改-->
        <?php if(isset($_SESSION['url_permission']['/Role/ShowRole'])){
            echo '<input type="text" value="修改" id="updateRole">';
        }else{
            echo '<input type="text" value="" id="updateRole">';
        }?>
<!--        查看-->
        <?php if(isset($_SESSION['url_permission']['/Role/ShowRole'])){
            echo '<input type="text" value="查看" id="showRole">';
        }else{
            echo '<input type="text" value="" id="showRole">';
        }?>
<!--        删除-->
        <?php if(isset($_SESSION['url_permission']['/Role/DelRole'])){
            echo '<input type="text" value="删除" id="delRole">';
        }else{
            echo '<input type="text" value="" id="delRole">';
        }?>
    </div>
    <div class="console">
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" name="name" required lay-verify="required" placeholder="输入分管名称" autocomplete="off" class="layui-input">
                </div>
                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;
                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>角色名称</th>
            <th>创建时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>龙九山</td>
            <td>DLS201802281450280741</td>
            <td>
                <button class="layui-btn layui-btn-xs">修改</button>
                <button class="layui-btn layui-btn-xs">基本信息</button>
            </td>
        </tr>

        <tr>
            <td>龙九山</td>
            <td>DLS201802281450280741</td>
            <td>
                <button class="layui-btn layui-btn-xs">修改</button>
                <button class="layui-btn layui-btn-xs">基本信息</button>
            </td>
        </tr>
        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: <?php echo $count;?>
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.getList(obj);
                }
            });
        });
        var adminPersionObj = {
            "getList":function(obj){
                var data = {
                    'curr':obj.curr,
                    'limit':obj.limit
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Role/roleList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "text",
                    success: function (response) {
                        var data = eval("("+response+")"),html = "";
                        //清空
                        $("#adminList").find("tbody").empty();
                        //填数据
                        $.each(data,function(k,v){
                            html = "";
                            html += "<tr>";
                            html += "<td>"+v.rolename+"</td>";
                            html += "<td>"+v.createTime+"</td>";
                            html += "<td>";
                            if($("#updateRole").val()){
                                html += "<a style='padding:0px 10px;color:#00f;' href=\""+"<?php echo ROOT_ADMIN_REQUEST?>/Role/showRole?Id="+v.Id+"\">修改</a>";
                                html += "<a style='padding:0px 10px;color:#00f;' href=\""+"<?php echo ROOT_ADMIN_REQUEST?>/Role/showRole?Id="+v.Id+"\">查看</a>";
                            }
                            if($("#delRole").val()){
                                html += "<a style='padding:0px 10px;color:#00f;'onclick='adminPersionObj.delCustomList(this)' Id ='"+v.Id+"' href='javascript:;'>删除</a>";
                            }
                            html += "</td>";
                            html += "</tr>";
                            $("tbody").append(html);
                            // $(html).prependTo('tbody');
                        });
                    }
                });
            },
            'delCustomList':function(_this){
                layui.use(['form','laydate'], function() {
                    layer.confirm('确定要删除么？', {
                        btn: ['确定', '取消'] //按钮
                    }, function() {
                        var data = {
                            'Id':$(_this).attr("Id")
                        }
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/Role/delRole",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                $(_this).parent().parent().remove();
                                layer.msg('删除成功', {
                                    icon: 1
                                });
                            }
                        });
                    }, function() {
                        layer.msg('取消删除', {
                            time: 2000 //20s后自动关闭
                        });
                    });
                });
            }
        }
    </script>
</div>
</body>

</html>