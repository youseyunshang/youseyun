<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>添加角色</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE;?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE;?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE;?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE;?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE;?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE;?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE;?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE;?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE;?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE;?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <style>
        .layui-form{
            margin-right: 30%;
        }
        .layui-form-label{
            width: 100px;
        }
        .layui-input-block{
            margin-left: 130px;
        }
    </style>

</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" method="post" action="<?php echo ROOT_ADMIN_REQUEST;?>/Role/insertRole">
        <div class="layui-form-item">
            <label class="layui-form-label">角色名称</label>
            <div class="layui-input-block">
                <input type="text" name="title" required lay-verify="required|ZHCheck" placeholder="例:系统管理员" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">权限内容</label>
            <div  class="layui-input-block" style="width:1000px;overflow: hidden;">
                <?php
                    echo '<div style="box-sizing: border-box;padding: 5px 10px;margin: 10px;"><input type="checkbox" value="all" id="allChecked" lay-skin="primary" lay-filter="roleCheckBox" title="全选"></div>';
                    foreach ($permission as $key=>$val){
                        echo "<div style='border: 1px solid #999;box-sizing: border-box;padding: 5px 10px;margin: 10px;'>";
                        echo '<input type="checkbox" son="true" value="'.$val['Id'].'" lay-skin="primary" lay-filter="roleCheckBox" name="'.$val['Id'].'" title="'.$val['pname'].'">';
                        if(isset($val['sub'])){
                            echo "<br/>";
                            foreach ($val['sub'] as $k=>$v){
                                echo '<input type="checkbox" lay-skin="primary" son="true" pid="'.$val['Id'].'" lay-filter="lastSon" value="'.$v['Id'].'" name="'.$v['Id'].'" title="'.$v['pname'].'">';
                            }
                        }
                        echo "</div>";
                    }
                ?>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submitBut">添加</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>
<script>
    layui.use(['layer', 'form'], function(){
        var layer = layui.layer
            ,form = layui.form;
        form.on('checkbox(roleCheckBox)', function(data){
            if(data.value == "all"){
                if($("input[value='all']").prop('checked')){
                    $("input[son='true']").prop("checked",true);
                }else{
                    $("input[son='true']").prop("checked",false);
                }
            }else{
                if($("input[value='"+data.value+"']").prop('checked')){
                    $("input[pid='"+data.value+"']").prop("checked",true);
                }else{
                    $("input[pid='"+data.value+"']").prop("checked",false);
                }

            }
            form.render();
        });
        form.on('checkbox(lastSon)', function(data){
            var pid = $("input[value='"+data.value+"']").attr('pid');

            var flag = 0;
            //遍历过程当中如果发现allSonChecked = 1;那么就是理想状态
            $.each($("input[pid='"+pid+"']"),function(k,v){
                if($(v).prop("checked")){
                    flag = 1;
                }
            });
            //如果flag = 0 表示最低子选项中没有数据checked
            if(flag < 1){
                $("input[value='"+pid+"']").prop("checked",false);
            }else{
                $("input[value='"+pid+"']").prop("checked",true);
            }
            form.render();
        });
    });
</script>
</body>

</html>