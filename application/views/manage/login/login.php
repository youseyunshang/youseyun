<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="JsonWang" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>有色云后台</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 公共样式 结束 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/login1.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/adminData/js/adminLogin/login1.js"></script>
</head>

<body>
<div class="loginBg"></div>
<div class="login_main">
    <div class="box">
        <div class="left">
            <img src="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/logo.png" />
            <p>有色云</p>
        </div>
        <div class="right">
            <form class="layui-form layui-form-pane" action="#">
                <div class="layui-form-item">
                    <label class="layui-form-label login_title"><i class="iconfont icon-gerenzhongxin-denglu"></i></label>
                    <div class="layui-input-block login_input">
                        <input type="text" name="name" id="username" required lay-verify="required" autocomplete="off" placeholder="请输入您的用户名" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label login_title"><i class="iconfont icon-mima1"></i></label>
                    <div class="layui-input-block login_input">
                        <input type="password" name="password" id="password" required lay-verify="required" autocomplete="off" placeholder="请输入密码" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <a href="javaScript:;" id="ButtonSubmit" class="layui-btn layui-btn-fluid login_but" lay-submit lay-filter="loginBut" style="line-height: 45px;">登 录</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    layui.use(['form', 'layer'], function() {
        var form = layui.form,layer = layui.layer;
    });
    $(document).ready(function(){
        //回车触发
        document.onkeydown = function(e){
            var ev = document.all ? window.event : e;
            if(ev.keyCode==13) {
                $("#ButtonSubmit").click();
            }
        }
        $("#ButtonSubmit").click(function(){
            var data = {
                'username':$("#username").val(),
                'pwd':$("#password").val()
            }
            $.ajax({
                url: "<?php echo ROOT_ADMIN_REQUEST?>/Login/validLogin",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : data,
                dataType : "text",
                success: function (response) {
                    //在原来的页面跳转到下载word模板的页面
                    switch (response) {
                        case '0':
                            layer.msg("请填写用户名或用户名不存！", {
                                time: 2000 //20s后自动关闭
                            });
                            break;
                        case '1':
                            window.location.href = "<?php echo ROOT_ADMIN_REQUEST?>/home/indexDefault";
                            return false;
                            break;
                        case '2':
                            layer.msg("密码错误！", {
                                time: 2000 //20s后自动关闭
                            });
                            break;
                    }
                }
            });
        });
    });
</script>
</body>

</html>