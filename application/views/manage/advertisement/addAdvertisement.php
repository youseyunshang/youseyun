<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>添加资源</title>
    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/wangEditor.min.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .layui-form{
            margin-right: 30%;
        }
        .layui-form-item .layui-form-checkbox{
            margin-right:50px ;
        }
    </style>
</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" method="post" action="#">
        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="title" id="title" placeholder="例:广告" lay-verify="required" autocomplete="off" class="layui-input">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">概要</label>
            <div class="layui-input-block">
                <textarea name="notice" id="notice" style="width:1000px;" placeholder="请输入内容" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">内容</label>
            <div class="layui-input-inline shortInput">
                <div id="editor" style="width:1000px;">
                    <p>发布文章内容</p>
                </div>
                <!-- 注意， 只需要引用 JS，无需引用任何 CSS ！！！-->
                <script type="text/javascript">
                    var E = window.wangEditor;
                    var editor = new E('#editor');
                    // 或者 var editor = new E( document.getElementById('editor') )
                    editor.create();
                </script>
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">封面图</label>
            <div class="layui-input-block">
                <input type="radio" name="photo" value="1" title="一张大图" checked lay-filter="photo">
                <input type="radio" name="photo" value="2" title="一张小图" lay-filter="photo">
                <input type="radio" name="photo" value="3" title="三张小图" lay-filter="photo">
            </div>
        </div>
        <div class="layui-form-item coverChart" id="coverChartOne">
            <label class="layui-form-label"></label>
            <div class="layui-upload-drag" id="resOne">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <img src="<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/logo.png" id="coverImgOne" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            <input type="text" value="" id="coverLogoOne" style="display: none;" name="userLogo">
        </div>
        <div class="layui-form-item coverChart" id="coverChartTwo" style="display: none;">
            <label class="layui-form-label"></label>
            <div class="layui-upload-drag" id="resTwo">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <img src="<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/logo.png" id="coverImgTwo" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            <input type="text" value="" id="coverLogoTwo" style="display: none;" name="userLogo">
        </div>
        <div class="layui-form-item coverChart" id="coverChartThree" style="display: none;">
            <label class="layui-form-label"></label>
            <div class="layui-upload-drag" id="resThree">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <img src="<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/logo.png" id="coverImgThree" style="display: inline-block;width: 80px;position: relative;left: 60px;top: -30px;">
            <input type="text" value="" id="coverLogoThree" style="display: none;" name="userLogo">
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <a href="javascript:;" class="layui-btn" onclick="myObj.submit()">添加</a>
                <a href="javascript:;" class="layui-btn layui-btn-primary">重置</a>
            </div>
        </div>
    </form>
    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        layui.use(['layer', 'form','upload'], function(){
            var layer = layui.layer
                ,form = layui.form;
            form.on('select(myselectOne)', function(data){
                //
                $("#attrIdtwo").empty();
                $.each(attrObj,function(k,v){
                    if( v.pid === data.value ){
                        $("#attrIdtwo").append("<option value='"+v.Id+"'>"+v.attrname+"</option>");
                    }
                });
                form.render();
            });
            form.on('select(myselectTwo)', function(data){
                $("#attrIdthree").empty();
                $.each(attrObj,function(k,v){
                    if( v.pid === data.value ){
                        $("#attrIdthree").append("<option value='"+v.Id+"'>"+v.attrname+"</option>");
                    }
                });
                form.render();
            });
            form.on('radio(photo)', function(data){
                switch (data.value) {
                    case "1":
                        $("#coverChartOne").show();
                        $("#coverChartTwo").hide();
                        $("#coverChartThree").hide();
                        break;
                    case "2":
                        $("#coverChartOne").show();
                        $("#coverChartTwo").hide();
                        $("#coverChartThree").hide();
                        break;
                    case "3":
                        $("#coverChartOne").show();
                        $("#coverChartTwo").show();
                        $("#coverChartThree").show();
                        break;
                }
            });
            var upload = layui.upload;
            //拖拽上传
            upload.render({
                elem: '#resOne',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Resource/upload',
                done: function(res) {
                    if(res.upload_data){
                        $("#coverImgOne").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/res/"+res.upload_data.file_name);
                        $("#coverLogoOne").val("/res/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#resTwo',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Resource/upload',
                done: function(res) {
                    if(res.upload_data){
                        $("#coverImgTwo").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/res/"+res.upload_data.file_name);
                        $("#coverLogoTwo").val("/res/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#resThree',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Resource/upload',
                done: function(res) {
                    if(res.upload_data){
                        $("#coverImgThree").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/res/"+res.upload_data.file_name);
                        $("#coverLogoThree").val("/res/"+res.upload_data.file_name);
                    }
                }
            });
        });
        var myObj = {
            "submit":function(){
                //内容
                var html = editor.txt.html(),photoType=1;
                //封面图片
                var ptoto = {
                    "coverLogoOne":$("#coverLogoOne").val(),
                    "coverLogoTwo":$("#coverLogoTwo").val(),
                    "coverLogoThree":$("#coverLogoThree").val()
                }
                //图片的类型
                $.each($("input[name='photo']"),function(k,v){
                    if($(v).prop("checked")){
                        photoType = $(v).val();
                        return false;
                    }
                });
                //广告标题不能为空
                if(!$("#title").val()){
                    layer.msg('广告标题不能为空！', {
                        time: 2000, //20s后自动关闭
                        btn: ['明白了', '知道了', '哦']
                    });
                    return false;
                }
                var obj = {
                    "title":$("#title").val(),
                    "content":html,
                    "notice":$("#notice").val(),
                    "photo":ptoto,
                    "photoType":photoType
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/Advertisement/addAdvertisement",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : obj,
                    dataType : "text",
                    success: function (response) {
                        if(response>0){
                            window.location.href = "<?php echo ROOT_ADMIN_REQUEST?>/Advertisement/advertisementList";
                        }else{
                            layer.msg('修改不成功,请检查网络！', {
                                time: 20000, //20s后自动关闭
                                btn: ['明白了', '知道了', '哦']
                            });
                        }
                    }
                });
            }
        }
    </script>

</div>
</body>

</html>