<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>业务员列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/css/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

</head>

<body>
<div class="cBody">
    <div class="console">
        <form class="layui-form" action="">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" name="name" required lay-verify="required" placeholder="输入分管名称" autocomplete="off" class="layui-input">
                </div>
                <button class="layui-btn" lay-submit lay-filter="formDemo">检索</button>
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>用户名</th>
            <th>电话</th>
            <th>真实姓名</th>
            <th>性别</th>
            <th>工作年限</th>
            <th>邮箱</th>
            <th>职位</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>龙九山</td>
            <td>DLS201802281450280741</td>
            <td>无锡市</td>
            <td>龙九山</td>
            <td>龙九山</td>
            <td>18600001111</td>
            <td>123456789@qq.com</td>
            <td>
                <button class="layui-btn layui-btn-xs">修改</button>
                <button class="layui-btn layui-btn-xs">基本信息</button>
            </td>
        </tr>

        <tr>
            <td>龙九山</td>
            <td>DLS201802281450280741</td>
            <td>无锡市</td>
            <td>龙九山</td>
            <td>龙九山</td>
            <td>18600001111</td>
            <td>123456789@qq.com</td>
            <td>
                <button class="layui-btn layui-btn-xs">修改</button>
                <button class="layui-btn layui-btn-xs">基本信息</button>
            </td>
        </tr>
        <tr>
            <td>龙九山</td>
            <td>DLS201802281450280741</td>
            <td>无锡市</td>
            <td>龙九山</td>
            <td>龙九山</td>
            <td>18600001111</td>
            <td>123456789@qq.com</td>
            <td>
                <button class="layui-btn layui-btn-xs">修改</button>
                <button class="layui-btn layui-btn-xs">基本信息</button>
            </td>
        </tr>
        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: <?php echo $count;?>
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.getList(obj);
                }
            });
        });
        var adminPersionObj = {
            "getList":function(obj){
                var data = {
                    'curr':obj.curr,
                    'limit':obj.limit
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/salemanList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "json",
                    success: function (response) {
                        //清空
                        $("#adminList").find("tbody").empty();
                        //填数据
                        $.each(response,function(k,v){
                            html = "";
                            html += "<tr>";
                            html += "<td>"+v.username+"</td>";
                            html += "<td>"+v.tel+"</td>";
                            html += "<td>"+v.realname+"</td>";
                            html += "<td>"+(v.gender>0?"男":"女")+"</td>";
                            html += "<td>"+v.workYear+"</td>";
                            html += "<td>"+v.email+"</td>";
                            html += "<td>"+v.rolename+"</td>";
                            html += "<td>";
                            //html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php //echo ROOT_URL_DEFINE?>///AdminPerson/updateAdmin?username="+v.username+"\">修改</a>";
                            html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/updateAdmin?username="+v.username+"\">查看</a>";
                            //html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php //echo ROOT_URL_DEFINE?>///AdminPerson/delAdmin?userId="+v.UserId+"\">删除</a>";
                            html += "</td>";
                            html += "</tr>";
                            $(html).prependTo('tbody');
                        });
                    }
                });
            }
        }
    </script>
</div>
</body>

</html>