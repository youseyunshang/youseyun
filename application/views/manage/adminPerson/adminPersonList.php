<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="穷在闹市" />
    <!-- 作者 -->
    <meta name="revised" content="穷在闹市.v3, 2019/05/01" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="网站简介" />
    <!-- 网站简介 -->
    <meta name="keywords" content="搜索关键字，以半角英文逗号隔开" />
    <title>管理员列表</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
</head>

<body>

<!--隐藏域Start-->
<div style="display: none;">
    <!--        修改-->
    <?php if(isset($_SESSION['url_permission']['/AdminPerson/UpdateAdmin'])){
        echo '<input type="text" value="修改" id="update">';
    }else{
        echo '<input type="text" value="" id="update">';
    }?>
    <!--        删除-->
    <?php if(isset($_SESSION['url_permission']['/AdminPerson/DelAdmin'])){
        echo '<input type="text" value="删除" id="del">';
    }else{
        echo '<input type="text" value="" id="del">';
    }?>
</div>
<!--隐藏域End-->
<div class="cBody">
    <div id="test9" class="demo-tree demo-tree-box" style="width: 200px; height: 300px; overflow: scroll;display: none;">
    </div>
    <div class="console" style="padding: 30px 0px;">
        <form class="layui-form" action="#">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" name="selectName" id="selectName" placeholder="名称/真实姓名/电话" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-input-inline">
                    <?php
                        if(!empty($roles)){
                            echo '<select name="role" id="role">';
                            echo '<option value="">--所有--</option>';
                            foreach ($roles as $key=>&$val){
                                echo '<option value="'.$val['Id'].'">'.$val['rolename'].'</option>';
                            }
                            echo '</select>';
                        }
                    ?>

                </div>
                <a class="layui-btn" onclick="adminPersionObj.getList();">检索</a>
            </div>
        </form>
    </div>

    <table class="layui-table" id="adminList">
        <thead>
        <tr>
            <th>用户名</th>
            <th>电话</th>
            <th>真实姓名</th>
            <th>性别</th>
            <th>工作年限</th>
            <th>邮箱</th>
            <th>职位</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        layui.use(['laypage', 'layer'], function() {
            var laypage = layui.laypage,
                layer = layui.layer;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: <?php echo $count;?>
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
                    adminPersionObj.getList(obj);
                }
            });
        });
        var adminPersionObj = {
            "getList":function(){
                var curr = $(".layui-laypage-curr").find("em:last-child").html();
                var limit = $(".layui-laypage-limits").find("select").val();
                var data = {
                    'curr':curr,
                    'limit':limit,
                    'selectName':$("#selectName").val(),
                    'role':$("#role").val()
                }
                $.ajax({
                    url: "<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/adminList",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : data,
                    dataType : "json",
                    success: function (response) {
                        //清空
                        $("#adminList").find("tbody").empty();
                        //填数据
                        $.each(response,function(k,v){
                            html = "";
                            html += "<tr>";
                            html += "<td>"+v.username+"</td>";
                            html += "<td>"+v.tel+"</td>";
                            html += "<td>"+v.realname+"</td>";
                            html += "<td>"+(v.gender>0?"男":"女")+"</td>";
                            html += "<td>"+v.workYear+"</td>";
                            html += "<td>"+v.email+"</td>";
                            html += "<td>"+v.rolename+"</td>";
                            html += "<td>";
                            if($("#update").val()){
                                html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/updateAdmin?username="+v.username+"\">修改</a>";
                                html += "<a style=\"padding:0px 10px;color:#00f;\" href=\""+"<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/updateAdmin?username="+v.username+"\">查看</a>";
                            }
                            if($("#del").val()){
                                html += "<a style='padding:0px 10px;color:#00f;' onclick='adminPersionObj.delCustomList(this)' Id ='"+v.UserId+"' href='javascript:;'>删除</a>";
                            }
                            html += "</td>";
                            html += "</tr>";
                            $(html).prependTo('tbody');
                        });
                    }
                });
            },
            'delCustomList':function(_this){
                layui.use(['form','laydate'], function() {
                    layer.confirm('确定要删除么？', {
                        btn: ['确定', '取消'] //按钮
                    }, function() {
                        var data = {
                            'Id':$(_this).attr("Id")
                        }
                        $.ajax({
                            url: "<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/delAdmin",
                            type : "POST",
                            contentType: "application/x-www-form-urlencoded;charset=utf-8",
                            data : data,
                            dataType : "text",
                            success: function (response) {
                                $(_this).parent().parent().remove();
                                layer.msg('删除成功', {
                                    icon: 1
                                });
                            }
                        });
                    }, function() {
                        layer.msg('取消删除', {
                            time: 2000 //20s后自动关闭
                        });
                    });
                });
            }
        }
    </script>
    <script>
        layui.use(['tree', 'util'], function(){
            var tree = layui.tree
                ,layer = layui.layer
                ,util = layui.util

                //模拟数据
                ,data = [{
                    title: '一级1'
                    ,id: 1
                    ,checked: true
                    ,spread: true
                    ,children: [{
                        title: '二级1-1 可允许跳转'
                        ,id: 3
                        ,href: 'https://www.layui.com/'
                        ,children: [{
                            title: '三级1-1-3'
                            ,id: 23
                            ,children: [{
                                title: '四级1-1-3-1'
                                ,id: 24
                                ,children: [{
                                    title: '五级1-1-3-1-1'
                                    ,id: 30
                                },{
                                    title: '五级1-1-3-1-2'
                                    ,id: 31
                                }]
                            }]
                        },{
                            title: '三级1-1-1'
                            ,id: 7
                            ,children: [{
                                title: '四级1-1-1-1 可允许跳转'
                                ,id: 15
                                ,href: 'https://www.layui.com/doc/'
                            }]
                        },{
                            title: '三级1-1-2'
                            ,id: 8
                            ,children: [{
                                title: '四级1-1-2-1'
                                ,id: 32
                            }]
                        }]
                    },{
                        title: '二级1-2'
                        ,id: 4
                        ,spread: true
                        ,children: [{
                            title: '三级1-2-1'
                            ,id: 9
                            ,disabled: true
                        },{
                            title: '三级1-2-2'
                            ,id: 10
                        }]
                    },{
                        title: '二级1-3'
                        ,id: 20
                        ,children: [{
                            title: '三级1-3-1'
                            ,id: 21
                        },{
                            title: '三级1-3-2'
                            ,id: 22
                        }]
                    }]
                },{
                    title: '一级2'
                    ,id: 2
                    ,spread: true
                    ,children: [{
                        title: '二级2-1'
                        ,id: 5
                        ,spread: true
                        ,children: [{
                            title: '三级2-1-1'
                            ,id: 11
                        },{
                            title: '三级2-1-2'
                            ,id: 12
                        }]
                    },{
                        title: '二级2-2'
                        ,id: 6
                        ,children: [{
                            title: '三级2-2-1'
                            ,id: 13
                        },{
                            title: '三级2-2-2'
                            ,id: 14
                            ,disabled: true
                        }]
                    }]
                },{
                    title: '一级3'
                    ,id: 16
                    ,children: [{
                        title: '二级3-1'
                        ,id: 17
                        ,fixed: true
                        ,children: [{
                            title: '三级3-1-1'
                            ,id: 18
                        },{
                            title: '三级3-1-2'
                            ,id: 19
                        }]
                    },{
                        title: '二级3-2'
                        ,id: 27
                        ,children: [{
                            title: '三级3-2-1'
                            ,id: 28
                        },{
                            title: '三级3-2-2'
                            ,id: 29
                        }]
                    }]
                }]

                //模拟数据1
                ,data1 = [{
                    title: '江西'
                    ,id: 1
                    ,children: [{
                        title: '南昌'
                        ,id: 1000
                        ,children: [{
                            title: '青山湖区'
                            ,id: 10001
                        },{
                            title: '高新区'
                            ,id: 10002
                        }]
                    },{
                        title: '九江'
                        ,id: 1001
                    },{
                        title: '赣州'
                        ,id: 1002
                    }]
                },{
                    title: '广西'
                    ,id: 2
                    ,children: [{
                        title: '南宁'
                        ,id: 2000
                    },{
                        title: '桂林'
                        ,id: 2001
                    }]
                },{
                    title: '陕西'
                    ,id: 3
                    ,children: [{
                        title: '西安'
                        ,id: 3000
                    },{
                        title: '延安'
                        ,id: 3001
                    }]
                }]

                //模拟数据2
                ,data2 = [{
                    title: '早餐'
                    ,id: 1
                    ,children: [{
                        title: '油条'
                        ,id: 5
                    },{
                        title: '包子'
                        ,id: 6
                    },{
                        title: '豆浆'
                        ,id: 7
                    }]
                },{
                    title: '午餐'
                    ,id: 2
                    ,checked: true
                    ,children: [{
                        title: '藜蒿炒腊肉'
                        ,id: 8
                    },{
                        title: '西湖醋鱼'
                        ,id: 9
                    },{
                        title: '小白菜'
                        ,id: 10
                    },{
                        title: '海带排骨汤'
                        ,id: 11
                    }]
                },{
                    title: '晚餐'
                    ,id: 3
                    ,children: [{
                        title: '红烧肉'
                        ,id: 12
                        ,fixed: true
                    },{
                        title: '番茄炒蛋'
                        ,id: 13
                    }]
                },{
                    title: '夜宵'
                    ,id: 4
                    ,children: [{
                        title: '小龙虾'
                        ,id: 14
                        ,checked: true
                    },{
                        title: '香辣蟹'
                        ,id: 15
                        ,disabled: true
                    },{
                        title: '烤鱿鱼'
                        ,id: 16
                    }]
                }];
            //开启复选框
            tree.render({
                elem: '#test7'
                ,data: data2
                ,showCheckbox: true
                ,oncheck: function(obj){
                console.log(obj.data); //得到当前点击的节点数据
                console.log(obj.checked); //得到当前节点的展开状态：open、close、normal
                console.log(obj.elem); //得到当前节点元素
            }
            });
            //开启节点操作图标
            tree.render({
                elem: '#test9'
                ,data: data1
                ,edit: ['add', 'update', 'del'] //操作节点的图标
                // ,click: function(obj){
                //     layer.msg(JSON.stringify(obj.data));
                // }
                ,operate: function(obj){
                    var type = obj.type; //得到操作类型：add、edit、del
                    var data = obj.data; //得到当前节点的数据
                    var elem = obj.elem; //得到当前节点元素

                    //Ajax 操作
                    var id = data.id; //得到节点索引
                    if(type === 'add'){ //增加节点
                        //返回 key 值
                        console.log("add");
                        console.log(obj);
                    } else if(type === 'update'){ //修改节点
                        console.log(obj);
                    } else if(type === 'del'){ //删除节点
                        console.log(obj);
                    };
                }
            });
        });
    </script>
</div>
</body>

</html>