<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>修改管理员</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/jquery.mCustomScrollbar.css">
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <style>
        .layui-form{
            margin-right: 30%;
        }
        .layui-tree-icon ~ .layui-form-checkbox{
            display: none;
        }
        .layui-form-item img{
            display: inline-block;
            width: 80px;
            height: 80px;
            position: relative;
            left: 60px;
            top: -30px;
            border-radius: 80px;
        }
    </style>

</head>

<body>
<div class="cBody">
    <form id="addForm" class="layui-form" method="post" action="<?php echo ROOT_ADMIN_REQUEST?>/AdminPerson/updateAdmin/1">
        <div class="layui-form-item" style="display: none;">
            <label class="layui-form-label">用户Id</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="UserId" value="<?php echo $adminInfo['UserId'];?>" placeholder="例:admin" autocomplete="off" class="layui-input" lay-verify="required">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="username" value="<?php echo $adminInfo['username'];?>" placeholder="例:admin" autocomplete="off" class="layui-input" lay-verify="required">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">电话号码</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="tel" value="<?php echo $adminInfo['tel'];?>" required autocomplete="off" class="layui-input" lay-verify="required|phone">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">真实姓名</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="realname" value="<?php echo $adminInfo['realname'];?>" required autocomplete="off" class="layui-input" lay-verify="required">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">登录密码</label>
            <div class="layui-input-inline shortInput">
                <input type="password" name="pwd" value="123456" autocomplete="off" class="layui-input" lay-verify="required">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">确认密码</label>
            <div class="layui-input-inline shortInput">
                <input type="password" name="password2" value="123456" autocomplete="off" class="layui-input" lay-verify="required">
            </div>
            <i class="iconfont icon-huaban bt"></i>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">性别</label>
            <div class="layui-input-block">
                <input type="radio" name="gender" value="1" title="男" <?php if($adminInfo['gender']>0){ echo "checked";}?>>
                <input type="radio" name="gender" value="0" title="女" <?php if($adminInfo['gender']<1){ echo "checked";}?>>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">工作年限</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="workYear" value="<?php echo $adminInfo['workYear']?>" placeholder="例:10" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">邮箱</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="email" lay-verify="required|email" value="<?php echo $adminInfo['email']?>" placeholder="例:492168228@qq.com" autocomplete="off" class="layui-input">
            </div>
        </div>
<!--        <div class="layui-form-item">-->
<!--            <label class="layui-form-label">地区</label>-->
<!--            <div class="layui-input-inline">-->
<!--                <select name="provid" id="provid" lay-filter="provid">-->
<!--                </select>-->
<!--            </div>-->
<!--            <div class="layui-input-inline">-->
<!--                <select name="cityid" id="cityid" lay-filter="cityid">-->
<!--                    <option value="">请选择市</option>-->
<!--                </select>-->
<!--            </div>-->
<!--        </div>-->
        <div class="layui-form-item">
            <label class="layui-form-label">微信号</label>
            <div class="layui-input-inline shortInput">
                <input type="text" name="wechart" required value="<?php echo $adminInfo['wechart']?>" placeholder="例:18341264232" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">角色</label>
            <div class="layui-input-inline">
                <select name="roleId" lay-filter="myselect">
                    <?php foreach ($role as $key=>$val){
                        if($val['Id']==$adminInfo['roleId']){
                            echo '<option value="'.$val['Id'].'" selected>'.$val['rolename'].'</option>';
                        }else{
                            echo '<option value="'.$val['Id'].'">'.$val['rolename'].'</option>';
                        }
                    }?>
                </select>
            </div>
        </div>
        <div class="layui-form-item" id="saleman" <?php if($adminInfo['roleId']!="21"){echo 'style="display: none;"';}?>>
            <label class="layui-form-label">所涉金属</label>
            <input type="text" name="saleman" value="<?php echo $adminInfo['attrId'];?>" id="asdf" style="display: none;">
            <div class="layui-input-inline">
                <div id="metalAll" class="demo-tree">
                </div>
                <script>
                    var str='<?php echo $metal;?>';dataAttr = eval("("+str+")");
                    //console.log(dataAttr);
                    layui.use(['tree', 'util'], function(){
                        var tree = layui.tree
                            ,layer = layui.layer
                            ,util = layui.util;
                        //开启复选框
                        tree.render({
                            elem: '#metalAll'
                            ,data: dataAttr
                            ,showCheckbox: true
                            ,oncheck: function(obj){
                                if(!obj.data.children){//父级点击
                                    let metalStr = $("input[name='saleman']").val().split(",");
                                    if(obj.checked){//添加
                                        if(!metalStr.includes(obj.data.id)){
                                            metalStr.push(obj.data.id);
                                        }
                                    }else{//删除
                                        $.each(metalStr,function(k,v){
                                            if(v==obj.data.id){
                                                metalStr.splice(k,1);
                                            }
                                        });
                                    }
                                    $("input[name='saleman']").val(metalStr.join(","));
                                }
                            }
                        });
                    });
                </script>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">微信头像</label>
            <div class="layui-upload-drag" id="wechartLogo">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php if(!empty($adminInfo['wechartpicture'])){
                echo '<img src="'.$adminInfo['wechartpicture'].'" id="wechartLogoImg" style="">';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="wechartLogoImg">';
            }?>
            <input type="text" value="" id="wechartLogoUrl" style="display: none;" name="wechartLogoUrl">
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">默认头像</label>
            <div class="layui-upload-drag" id="logo">
                <i class="layui-icon"></i>
                <p>点击上传，或将文件拖拽到此处</p>
            </div>
            <?php if(!empty($adminInfo['logo'])){
                echo '<img src="'.$adminInfo['logo'].'" id="logoImg" >';
            }else{
                echo '<img src="'.ROOT_URL_DEFINE.'/resource/uploads/userlogo/logo.png" id="logoImg" >';
            }?>
            <input type="text" value="" id="logoUrl" style="display: none;" name="logoUrl">
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="submitBut">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>

    <!-- 三级省市 插件 -->
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/area.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/province.js"></script>
    <script>
        //默认城市为：宁夏 - 银川
        var defaults = {
            s1: 'provid',
            s2: 'cityid',
            s3: 'areaid',
            v1: 510000,
            v2: 510100,
            v3: null
        };
        layui.use(['upload','layer', 'form'], function(){
            var layer = layui.layer
                ,form = layui.form
                ,upload = layui.upload;
            form.on('select(myselect)', function(data){
                if(data.value=="21"){
                    $("#saleman").show();
                }else{
                    $("#saleman").hide();
                }
            });
            form.verify({
                username: function(value, item){ //value：表单的值、item：表单的DOM对象
                    if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
                        return '用户名不能有特殊字符';
                    }
                    if(/(^\_)|(\__)|(\_+$)/.test(value)){
                        return '用户名首尾不能出现下划线\'_\'';
                    }
                    if(/^\d+\d+\d$/.test(value)){
                        return '用户名不能全为数字';
                    }
                },
                'pass':function(value, item){
                    if( /^[\S]{6,12}$/.test(value)){
                        return '密码必须6到12位，且不能出现空格';
                    }
                },
                'tel':function(value){
                    if(!(/^1[3456789]\d{9}$/.test(value))){
                        return '手机号码有误';
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#wechartLogo',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Home/upload/userlogo',
                done: function(res) {
                    if(res.upload_data){
                        $("#wechartLogoImg").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/"+res.upload_data.file_name);
                        $("#wechartLogoUrl").val("<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/"+res.upload_data.file_name);
                    }
                }
            });
            //拖拽上传
            upload.render({
                elem: '#logo',
                url: '<?php echo ROOT_ADMIN_REQUEST;?>/Home/upload/userlogo',
                done: function(res) {
                    if(res.upload_data){
                        $("#logoImg").attr("src","<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/"+res.upload_data.file_name);
                        $("#logoUrl").val("<?php echo ROOT_URL_DEFINE;?>/resource/uploads/userlogo/"+res.upload_data.file_name);
                    }
                }
            });
        });
    </script>

</div>
</body>

</html>