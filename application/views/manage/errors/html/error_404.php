<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>404-2</title>
    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/base.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/css/adminlogin/iconfont.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/layui/css/layui.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/layui/layui.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-ui-1.10.4.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mousewheel.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <style>
        body{
            width: 100%;
            height: 100%;
            overflow: hidden;
            background: url(<?php echo ROOT_URL_DEFINE;?>/resource/adminData/img/adminLogin/other/maintain_bottom.png) no-repeat center bottom;
            background-size: 100% auto;
        }
        .midd{
            width: 500px;
            height: 320px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-top: -250px;
            margin-left: -250px;
        }
        .midd img{
            width: 500px;
            height: 243px;
        }
        .midd p{
            width: 100%;
            height: 77px;
            line-height: 77px;
            text-align: center;
            font-size: 24px;
            color: #00bfb4;
            background: url(<?php echo ROOT_URL_DEFINE;?>/resource/adminData/img/adminLogin/other/maintain_pic_bg.png) no-repeat center bottom;
            background-size: 80% auto;
        }
    </style>
</head>

<body>
<div class="midd">
    <img src="<?php echo ROOT_URL_DEFINE;?>/resource/adminData/img/adminLogin/other/maintain_pic.png" />
    <p>您访问的页面正在升级维护中...</p>
</div>
<script>
    $(function(){
        var win_h = $(window).height();
        $("body").height(win_h);
    })
</script>
</body>

</html>