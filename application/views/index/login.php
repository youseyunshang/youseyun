<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>登录</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
    <style>
        p{
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin:0rem;
            height:1.2rem;
        }
        .pannel-index{
            padding: 5px 20px;
        }
        .pannel-index .return-back{
            display: inline-block;
            width: 20px;
            height: 20px;
            overflow: hidden;
            background:  url("<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/icon.png");
            background-size: 300px;
            background-position: 140px -90px;
        }
        .pannel-index .rule-OK{
            display: inline-block;
            width: 20px;
            height: 20px;
            overflow: hidden;
            background:  url("<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/icon.png");
            background-size: 300px;
            background-position: 155px -145px;
        }
        .pannel-index .phone,.verification,.pwd{
            font-size: 0.28rem;
            padding: 16px 0px;
            width: 60%;
            border: 0px;
            outline: none;
        }
        .pannel-index .register-verification,.register-pwd,.register-phone{
            display: flex;
            justify-content: space-between;
        }
        .pannel-index .register-verification .blue-back{
            display: inline-block;
            text-decoration: none;
            width: 100px;
            height: 40px;
            line-height: 40px;
            background-color: #00aae8;
            border-radius: 20px;
            text-align: center;
            font-size: 0.28rem;
            color: #FFF;
            float: right;
        }
        .pannel-index .default-back{
            display: inline-block;
            text-decoration: none;
            width: 100px;
            height: 40px;
            line-height: 40px;
            border-radius: 20px;
            text-align: center;
            font-size: 0.28rem;
            background-color: #eeeeee;
            color: #7e8396;
            float: right;
        }
        .pannel-index .login-rule{
            font-size: 0.28rem;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between;
            width: 100%;
        }
        .pannel-index .login-rule a{
            text-decoration: none;
            color: #8d8a9c;
        }
        .pannel-index .register-default{
            width: 100%;
            background-color: #ececec;
            border-radius: 5px;
            font-size: 1.1rem;
            color: #7e8396;
            height: 60px;
            line-height: 60px;
            text-decoration: none;
            text-align: center;
            margin-top: 60px;
        }
        .pannel-index .register-OK{
            width: 100%;
            background-color: #00aae8;
            border-radius: 5px;
            font-size: 0.4rem;
            color: #fff;
            height: 60px;
            line-height: 60px;
            text-decoration: none;
            text-align: center;
            margin-top: 60px;
        }
    </style>
</head>
<body>
<div class="pannel-index" id="">
    <p><a href="<?php echo ROOT_INDEX_REQUEST?>/index/indexDefault" style="display: flex;"><span class="return-back"></span></a></p>
</div>
<div class="pannel-index" id="">
    <span style="display: flex;font-size: 0.4rem;">您好,</span>
</div>
<div class="pannel-index" id="">
    <p style="font-size: 0.3rem;justify-content: end;color: #7e8396;">欢迎来到有色云商，立即<a href="<?php echo ROOT_INDEX_REQUEST?>/index/register" style="text-decoration: none;color: #0053fb;">注册</a></p>
</div>
<div class="pannel-content" id="phoneLogin" style="display: none;">
    <div class="pannel-index">
        <div class="register-phone">
            <input type="text" name="phone" placeholder="请输入手机号码" class="phone" maxlength="11" />
        </div>
        <div class="pannel-index-underline">
        </div>
    </div>
    <div class="pannel-index">
        <div class="register-verification">
            <input type="text" name="verification" placeholder="请输入验证码" class="verification" maxlength="4" />
            <a href="javascript:;" class="blue-back" onclick="login.getVerificationCode()" id="verificationShow">获取验证码</a>
        </div>
        <div class="pannel-index-underline">
        </div>
    </div>
    <div class="pannel-index">
        <p class="login-rule"><a href="javascript:;" onclick="login.loginType(0)">密码登录</a><a href="<?php echo ROOT_INDEX_REQUEST?>/index/resetPassword">密码找回</a></p>
    </div>
</div>
<div class="pannel-content" id="pwdLogin">
    <div class="pannel-index">
        <div class="register-phone">
            <input type="text" name="phone" placeholder="请输入手机号码" class="phone" maxlength="11" />
        </div>
        <div class="pannel-index-underline">
        </div>
    </div>
    <div class="pannel-index">
        <div class="register-pwd">
            <input type="password" placeholder="请输入密码" name="pwd" class="pwd" maxlength="16" />
        </div>
        <div class="pannel-index-underline">
        </div>
    </div>
    <div class="pannel-index">
        <p class="login-rule"><a href="javascript:;" onclick="login.loginType(1)">快捷登录</a><a href="<?php echo ROOT_INDEX_REQUEST?>/index/resetPassword">密码找回</a></p>
    </div>
</div>
<div class="pannel-index">
    <a href="javascript:;" id="blue" class="register-OK" onclick="login.userLogin()">登录</a>
</div>
<script>
    //init 初始化数据
    var login = {
        'selectType':'pwdLogin',
        'init':function(){
        },
        'loginType':function(type) {
            if(type>0){
                login.selectType = "phoneLogin";
                $("#phoneLogin").show();
                $("#pwdLogin").hide();
            }else{
                login.selectType = "pwdLogin";
                $("#phoneLogin").hide();
                $("#pwdLogin").show();
            }
        },
        'userLogin':function(){
            switch (login.selectType) {
                case "phoneLogin":
                    login.phoneLogin();
                    break;
                case "pwdLogin":
                    login.pwdLogin();
                    break;
                default:
                    login.pwdLogin();
                    break;
            }
        },
        'pwdLogin':function(){
            var data = {
                "loginType":"0",
                "tel":$("#pwdLogin").find(".phone").val(),
                "pwd":$("#pwdLogin").find(".pwd").val()
            }
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/index/login",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : data,
                dataType : "text",
                success: function (response) {
                    if(response.length>1){
                        var data = eval('('+response+')');
                        switch (data.status) {
                            case "success":
                                alert(data.message);
                                self.location=document.referrer;
                                // window.location.href=WINDOWS_INDEX_REQUEST+"/indexUsercenter/index";
                                break;
                            case "false":
                                alert(data.message);
                                break;
                        }
                    }else{
                        alert("输入数据不正确！");
                    }
                }
            });
        },
        'phoneLogin':function(){
            // {"Message":"\u89e6\u53d1\u5929\u7ea7\u6d41\u63a7Permits:10","RequestId":"75651B2D-0370-4E40-9106-A91EA60AD9D9","Code":"isv.BUSINESS_LIMIT_CONTROL"}
            var data = {
                "loginType":"1",
                "tel":$("#phoneLogin").find(".phone").val(),
                "verification":$("#phoneLogin").find(".verification").val()
            }
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/index/login",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : data,
                dataType : "text",
                success: function (response) {
                    if(response.length>1){
                        var data = eval('('+response+')');
                        switch (data.status) {
                            case "success":
                                alert(data.message);
                                self.location=document.referrer;
                                // window.location.href=WINDOWS_INDEX_REQUEST+"/indexUsercenter/index";
                                break;
                            case "false":
                                alert(data.message);
                                break;
                        }
                    }else{
                        alert("输入数据不正确！");
                    }
                }
            });
            console.log(data);
        },
        'getVerificationCode':function(){
            var phone = $("#phoneLogin").find(".phone").val();
            if(!(/^1[3456789]\d{9}$/.test(phone))){
                alert("手机号码有误，请重填");
                return false;
            }else{
                $.ajax({
                    url: WINDOWS_INDEX_REQUEST+"/index/sendLoginMessage",
                    type : "POST",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data : {'phone':phone},
                    dataType : "text",
                    success: function (response) {
                        if(response.length>1){
                            var data = eval('('+response+')');
                            if(data.Code=="OK"){
                                //定时器
                                var i=60,setInt = setInterval(function(){
                                    if(i<1){
                                        $("#verificationShow").html("获取验证码").removeClass("blue-back").addClass("default-back").removeAttr("onclick");
                                        clearInterval(setInt);
                                    }else{
                                        $("#verificationShow").html(i);
                                        i--;
                                    }
                                },1000);
                            }
                            if(data.Code=="false"){
                                //定时器
                                alert(data.Message);
                            }
                        }else{
                            alert("输入数据不正确！");
                        }
                    }
                });
            }
        }
    }
    login.init();
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
