<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>首页</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script typet="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js"></script>
    <?php
    if(empty($weixinValid)){
	  echo '<script type="text/javascript" src="'.ROOT_URL_DEFINE.'/resource/configWeixin.js" ></script>';
	}
    ?>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/js/part.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
</head>
<body>
<?php include 'application/views/smallIcon.php';?>
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<!--首页导航end-->
<div class="pannel-index" id="sheet">
    <div class="index-navigation">
        <a href="<?php echo ROOT_INDEX_REQUEST?>/index/indexDefault" style="font-size: 18px;color: #0053fb;"><span>推荐</span><span class="underline-nav"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexRes/resLists"><span>资讯</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexQuestion/questionLists"><span>问答</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/indexProduct/index"><span>交易</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/indexUsercenter/index"><span>我的</span><span class="underline-nav" style="display: none;"></span></a>
    </div>
</div>
<div class="pannel-index">
    <div class="index-notice">
        <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexRes/resLists">
            <span>刚刚更新 <?php echo $newResCount;?> 条资讯</span>
            <span style="float: right;">查看</span>
        </a>
    </div>
</div>
<div class="pannel-index">
    <div class="formpoint-lable">
        <label class="formpoint-lable-font">最新行情</label>
        <span class="foot-icon-two formpoint-lable-icon" style="">
        </span>
    </div>
    <div class="pannel-index-formpoint-title">
        <div class="title-item title-item-firstColumn">名称/价格范围</div>
        <div class="title-item title-item-secondColumn">均价</div>
        <div class="title-item title-item-thirdColumn">涨跌</div>
    </div>
    <div class="formpoint-body">
        <?php
            foreach ($reportData as $key=>&$val){
                //规格
                $Specifications = isset($val['Specifications'])?$val['Specifications']:"";
                //单位
                $measurement = isset($val['measurement'])?$val['measurement']:"";
                //name
                $reportName = $val['formname'].' '.$Specifications;
                $reportNameWidth = mb_strlen($reportName,"utf-8")*16;
                echo ' <div class="pannel-index-formpoint-list">
                        <div style="width: 60%;float:left;text-align: left;padding-left:15px;box-sizing: border-box;">
                            <div style="height: 30px;width:100%;line-height: 30px;font-size: 15px;font-weight: bold;color: #535353;overflow-x: auto;"><span style="display: inline-block;width:'.$reportNameWidth.'px;">'.$reportName.'</span></div>
                        <div style="height: 30px;line-height: 30px;'.$val['color'].'">'.$val['lastnum'].'-'.$val['topnum'].' '.$measurement.'</div>
                        </div>
                         <div style="width: 20%;float:left;text-align: center;'.$val['color'].'">'.$val['middlenum'].'</div>
                         <div style="width: 20%;float:left;text-align: center;box-sizing: border-box;">
                            <div style="height: 30px;line-height: 30px;color: #535353;">'.date("m",time()).'-'.date("d",time()).'</div>
                            <div style="height: 30px;line-height: 30px;'.$val['color'].'" class="updown">'.$val['updown'].'</div>
                        </div>
                    </div>';
            }
        ?>
    </div>
    <div class="pannel-index-formpoint-foot">
        <a href="javascript:;" onclick="index.showMoreReport(this);">查看更多金属行情</a>
    </div>
</div>
<div class="pannel-index">
    <div id="news" style="height: 1rem;overflow:hidden;line-height: 1rem;">
        <ul style="margin-top:0!important;list-style: none;padding:5px;font-size: 0.3rem;">
            <li>
                <div class="go-items">
                    <span class="foot-icon" style="background-position: 347px -95px;margin-right:10px;"></span>
                    <span>本周成交量</span><span id="weekOrder" style="float: right;font-size: 0.4rem;color: #f00;">5吨</span>
                </div>
            </li>
            <li>
                <div class="go-items">
                    <span class="foot-icon" style="background-position: 347px -90px;margin-right:10px;"></span>
                    <span>本周订单量</span><span id="weekOrder" style="float: right;font-size: 0.4rem;color: #f00;">10吨</span>
                </div>
            </li>
            <li>
                <div class="go-items">
                    <span class="foot-icon" style="background-position: 347px -90px;margin-right:10px;"></span>
                    <span>本周问题数量</span><span id="weekOrder" style="float: right;font-size: 0.4rem;color: #f00;">10个</span>
                </div>
            </li>
        </ul>
    </div>
    <div class="pannel-index-underline">
    </div>
</div>
<div class="pannel-content">
    <!--商品-->
    <div class="pannel-index" id="shopInfo">
        <?php
            if(!empty($shopInfo)){
                unset($val);
                foreach ($shopInfo as $key=>&$val){
                    if($val['itemType']=='supple'){
                        //供应
                        echo ' <a href="'.ROOT_INDEX_REQUEST.'/indexProduct/index" class="supple-list" style="">
                                <img src="'.$val['productPhoto'].'">
                                <div style="margin-left: 0.3rem;"> 
                                    <div class="supple-name">'.$val['title'].'</div>
                                    <div class="supple-item">售价：<span style="font-size: 20px;color: red;">￥'.$val['unitPrice'].'</span>/吨</div>
                                    <div class="supple-item">库存：'.$val['num'].'吨</div>
                                    <div class="supple-item">所在地：'.$val['location'].'</div>
                                </div>       
                        </a>';
                    }else{
                        //采购
                        echo ' <a href="'.ROOT_INDEX_REQUEST.'/indexProduct/index" class="supple-list" style="">
                                <img src="#">
                                <div style="margin-left: 0.3rem;">
                                    <div class="supple-name">'.$val['title'].'</div>
                                    <div class="supple-item">采购价：<span style="font-size: 20px;color: red;">￥'.$val['unitPrice'].'</span>/吨</div>
                                    <div class="supple-item">粒度：'.$val['granularity'].' 牌号：'.$val['brandName'].'</div>
                                    <div class="supple-item">所在地：'.$val['location'].'</div>
                                </div>
                        </a>';
                    }

                }
                echo '<div class="showMoreProduct">查看更多商品信息</div>';
            }
        ?>
    </div>
    <!--商城入口-->
    <!--    登录注册start-->
    <div class="pannel-index-thickline"></div>
    <?php
        if(!isset($_SESSION['indexUserData'])){
            echo '<div class="pannel-index promptLogin">
                        <div class="index-login">
                            <span class="foot-icon-two iconOk"></span>
                            <span style="width:5.6rem;">
                                <a>登录可以查看更多信息</a><a href="'.ROOT_INDEX_REQUEST.'/index/login" class="index-login-notice">立即登录</a>
                            </span>
                            <span id="promptLoginClose" class="foot-icon-two iconFox"></span>
                        </div>
                </div>
                <div class="pannel-index-thickline promptLogin">
                </div>
                <div class="pannel-index promptRegister">
                    <div class="index-login">
                        <span class="foot-icon-two iconVip"></span>
                        <span style="width:5.6rem;">
                            <a>成为会员可以发布供求信息</a><a href="'.ROOT_INDEX_REQUEST.'/index/register" class="index-login-notice">立即注册</a>
                        </span>
                        <span id="promptRegisterClose" class="foot-icon-two iconFox"></span>
                    </div>
                </div>
                <div class="pannel-index-thickline promptRegister">
                </div>';
        }
    ?>
    <!--    登录注册end-->
</div>
<div class="pannel-index">
    <div style="width:100%;height:50px;display: flex;flex-direction: row;justify-content: center; align-items: center;" id="reload">
        <img src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/reload2.gif" style="width: 40%;">
    </div>
</div>
<script>
    function autoScroll(obj){
        $(obj).find('ul').animate({
            marginTop: '-1rem'
        },1000,function(){
            $(this).css({marginTop : "0px"});
            var li  =$("ul").children().first().clone();
            $("ul li:last").after(li );
            $("ul li:first").remove();

        })
    }
    $(function(){
        setInterval('autoScroll("#news")',2000);
    });
    var scrollIn = true,index = {
        'filterArr':{},
        'init':function(){
            //关闭登录提示
            $("#promptLoginClose").click(function(){
                $(".promptLogin").remove();
            });
            //关闭注册提示
            $("#promptRegisterClose").click(function(){
                $(".promptRegister").remove();
            });
            //进入商城首页
            $(".showMoreProduct").click(function(){
                window.location.href = WINDOWS_INDEX_REQUEST+"/indexProduct/index";
            });
            //初始加载数据
            index.loadingData();
            // 初始化字体大小
        },
        'loadingData':function(){
            scrollIn = false;
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/index/indexDefault",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : {'indexMoreData':'23478979345893654506','filterArr':index.filterArr},
                dataType : "json",
                success: function (response) {
                    if(response){
                        $.each(response,function(k,v){
                            //存储数据
                            index.filterArr[v.totalUnqid]=1;
                            switch (v.totalType) {
                                case 'res':
                                    index.showResData(v);
                                    break;
                                case 'advert':
                                    index.showAdvertData(v);
                                    break;
                                case 'question':
                                    index.showQuestionData(v);
                                    break;
                            }
                        });
                        scrollIn = true;
                    }else{
                        $("#reload").html("<span style='font-size: 12px;color: #bbb;'>没有更多数据了</span>");
                    }
                }
            });
            if(Object.keys(index.filterArr).length>10){
                $(".pannel-content").append('<div class="pannel-index" id="shopInfo">'+$("#shopInfo").html()+'<div>');
            }
        },
        'showMoreReport':function(that){
            $(".pannel-index-formpoint-list").removeClass("hidden-block");
            $(that).attr("href",WINDOWS_INDEX_REQUEST+"/indexRes/reportIndex");
        },
        'showResData':function(obj){
            switch (obj.photoType) {
                case "0":
                    resPart.resNoPicture(obj);
                break;
                case "1":
                //一张大图
                    resPart.resBigPicture(obj);
                break;
                case "2":
                //一张小图
                    resPart.resBigPicture(obj);
                //     resPart.resSmallPicture(obj);
                break;
                case "3":
                //三张图
                    resPart.resThreePicture(obj);
                break;
            }
        },
        'showAdvertData':function(obj){
            switch (obj.photoType) {
                case "1":
                    advertPart.advertBigPicture(obj);
                    break;
                case "2":
                    advertPart.advertSmallPicture(obj);
                    break;
                case "3":
                    advertPart.advertThreePicture(obj);
                    break;
            }
        },
        'showQuestionData':function(obj){
            if(obj.photo){
                var photo = eval('('+obj.photo+')');
                if(photo.coverLogoTwo!=""){
                    html = questionPart.threePicture(obj,photo);
                }else if(photo.coverLogoOne!=""){
                    html = questionPart.onePicture(obj,photo);
                }else{
                    html = questionPart.noPicture(obj);
                }
            }else{
                html = questionPart.noPicture(obj);
            }
            $(".pannel-content").append(html);
        }
    }
    index.init();
    var totalHeight = $(document).height();//整个文档高度
    var seeHeight = $(window).height();//浏览器可视窗口高度
    // var thisBodyHeight = $(document.body).height();//浏览器当前窗口文档body的高度
    // var totalBodyHeight = $(document.body).outerHeight(true);//浏览器当前窗口文档body的总高度 包括border padding margin
    // var thisWidth = $(window).width(); //浏览器当前窗口可视区域宽度
    // var thisDocumentWidth = $(document).width();//浏览器当前窗口文档对象宽度
    // var thisBodyWidth = $(document.body).width();//浏览器当前窗口文档body的宽度
    // var totalBodyWidth = $(document.body).outerWidth(true);//浏览器当前窗口文档body的总宽度 包括border padding margin
    var scrollTop = $(window).scrollTop();//浏览器可视窗口顶端距离网页顶端的高度（垂直偏移）
    // console.log(totalHeight,seeHeight,thisBodyHeight,totalBodyHeight,thisWidth,thisDocumentWidth,thisBodyWidth,totalBodyWidth,scrollTop);
    //添加滚动事件
    $(window).scroll(function(){
        scrollTop = $(window).scrollTop();
        totalHeight = $(document).height();
        //导航显示
        if(scrollTop>100){
            $("#sheet").addClass('fix-nav-sheet');
        }else{
            $("#sheet").removeClass('fix-nav-sheet');
        }
        if(scrollTop+seeHeight+100>totalHeight&&scrollIn){
            index.loadingData();
        }
    });
    document.addEventListener("error", function (e) {
        var elem = e.target;
        if (elem.tagName.toLowerCase() == 'img') {
            elem.src = "<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/error.png";
        }
    }, true);
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
