<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>搜索</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/js/part.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
</head>
<body>
<!--搜索结果页start-->
<div class="pannel-header" style="justify-content: left;">
    <a href="javascript:history.back();" class="pannel-header-search-icon"></a>
    <span style="position: relative;width: 90%;display: flex;">
        <input type="text" placeholder="请输入您要搜索的内容" id="search">
        <a href="javascript:;" onclick="index.searchAll();" class="pannel-header-input-seach"></a>
    </span>
</div>
<!--搜索结果页end-->
<div class="pannel-index" style="padding: 5px 50px;">
    <div class="index-navigation">
        <a href="javascript:;" onclick="index.tabType('res');"><span class="tab" id="res" style="color:#0053fb;">资讯</span></a>
        <a href="javascript:;" onclick="index.tabType('que');"><span class="tab" id="que">问答</span></a>
        <a href="javascript:;" onclick="index.tabType('shop');"><span class="tab" id="shop">交易</span></a>
    </div>
</div>
<div class="pannel-content" id="content">

</div>
<div class="pannel-index">
    <div style="width:100%;height:50px;display: flex;flex-direction: row;justify-content: center; align-items: center;" id="reload">
        <img src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/reload2.gif" style="width: 40%;">
    </div>
</div>
<script>
    var scrollIn = true,index = {
        'filterArr':{},
        'resType':"res",
        'init':function(){
            //初始加载数据
            index.loadingData();
        },
        'loadingData':function(){
            scrollIn = false;
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/index/searchDefault",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : {'indexMoreData':'23478979345893654506','filterArr':index.filterArr,'resType':index.resType,'content':$("#search").val()},
                dataType : "json",
                success: function (response) {
                    console.log(response);
                    if(response){
                        $.each(response,function(k,v){
                            //存储数据
                            index.filterArr[v.totalUnqid]=1;
                            switch (v.totalType) {
                                case 'res':
                                    index.showResData(v);
                                    break;
                                case 'advert':
                                    index.showAdvertData(v);
                                    break;
                                case 'question':
                                    index.showQuestionData(v);
                                    break;
                            }
                        });
                        scrollIn = true;
                        if(response.length<3){
                            $("#reload").html("<span style='font-size: 12px;color: #BBB;'>没有更多数据了</span>");
                        }
                    }else{
                        $("#reload").html("<span style='font-size: 12px;color: #BBB;'>没有更多数据了</span>");
                    }
                }
            });
        },
        'showMoreReport':function(that){
            $(".pannel-index-formpoint-list").removeClass("hidden-block");
            $(that).attr("href",WINDOWS_INDEX_REQUEST+"/indexRes/reportIndex");
        },
        'showResData':function(obj){
            switch (obj.photoType) {
                case "0":
                    resPart.resNoPicture(obj);
                break;
                case "1":
                //一张大图
                    resPart.resBigPicture(obj);
                break;
                case "2":
                //一张小图
                    resPart.resBigPicture(obj);
                //     resPart.resSmallPicture(obj);
                break;
                case "3":
                //三张图
                    resPart.resThreePicture(obj);
                break;
            }
        },
        'showAdvertData':function(obj){
            switch (obj.photoType) {
                case "1":
                    advertPart.advertBigPicture(obj);
                    break;
                case "2":
                    advertPart.advertSmallPicture(obj);
                    break;
                case "3":
                    advertPart.advertThreePicture(obj);
                    break;
            }
        },
        'showQuestionData':function(obj){
            if(obj.photo){
                var photo = eval('('+obj.photo+')');
                if(photo.coverLogoTwo!=""){
                    html = questionPart.threePicture(obj,photo);
                }else if(photo.coverLogoOne!=""){
                    html = questionPart.onePicture(obj,photo);
                }else{
                    html = questionPart.noPicture(obj);
                }
            }else{
                html = questionPart.noPicture(obj);
            }
            $(".pannel-content").append(html);
        },
        'tabType':function(type){
            if(type=="shop"){
                alert("暂未开通！");
                return false;
            }
            //项切换
            $(".tab").css("color","#282828");
            $("#"+type).css("color","#0053fb");
            //过滤
            index.filterArr = {};
            index.resType = type;
            //清空原数据
            $("#content").empty();
            index.loadingData();
        },
        'searchAll':function(){
            //清空原数据
            index.filterArr = {};
            $("#content").empty();
            index.loadingData();
        }
    }
    index.init();
    var totalHeight = $(document).height();//整个文档高度
    var seeHeight = $(window).height();//浏览器可视窗口高度
    // var thisBodyHeight = $(document.body).height();//浏览器当前窗口文档body的高度
    // var totalBodyHeight = $(document.body).outerHeight(true);//浏览器当前窗口文档body的总高度 包括border padding margin
    // var thisWidth = $(window).width(); //浏览器当前窗口可视区域宽度
    // var thisDocumentWidth = $(document).width();//浏览器当前窗口文档对象宽度
    // var thisBodyWidth = $(document.body).width();//浏览器当前窗口文档body的宽度
    // var totalBodyWidth = $(document.body).outerWidth(true);//浏览器当前窗口文档body的总宽度 包括border padding margin
    var scrollTop = $(window).scrollTop();//浏览器可视窗口顶端距离网页顶端的高度（垂直偏移）
    // console.log(totalHeight,seeHeight,thisBodyHeight,totalBodyHeight,thisWidth,thisDocumentWidth,thisBodyWidth,totalBodyWidth,scrollTop);
    //添加滚动事件
    $(window).scroll(function(){
        scrollTop = $(window).scrollTop();
        totalHeight = $(document).height();
        if(scrollTop+seeHeight+100>totalHeight&&scrollIn){
            index.loadingData();
        }
    });
    document.addEventListener("error", function (e) {
        var elem = e.target;
        if (elem.tagName.toLowerCase() == 'img') {
            elem.src = "<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/error.png";
        }
    }, true);
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
