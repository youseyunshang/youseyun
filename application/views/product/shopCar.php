<!DOCTYPE html>
<html>
<head>
    <title>购物车</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/mobiletime.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/shopCar/index.css">

</head>
<body>
<div id="app" v-cloak>
    <tops :title="'购物车'"></tops>
    <div v-if="shopCarList">
        <div class="edits" @click="editItem()">
            <span></span>
            <i>编辑</i>
        </div>
        <div class="commodityList" v-for="i in shopCarList">
            <div class="commodity">
                <span class="checks active" attrId="son" :pid="i.Id" @click="checkbox(i.Id)"></span>
                <div class="content">
                    <div class="title" @click="hrefProductInfo(i.suppleId)">{{i.title}}</div>
                    <div class="type">牌号 : {{i.brandName||'暂无牌号'}} 粒度 : {{i.granularity||'暂无粒度'}}</div>
                    <div class="bottom">
                        <div class="money">单价 : ￥<span class="onePrice" style="display: inline-block;">{{i.unitPrice}}</span></div>
                        <div class="num">
                            <span @click="priceReduce(i.Id)">-</span>
                            <span class="count">{{i.minOrderNum}}</span>
                            <span @click="priceAdd(i.Id)">+</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="settlement" v-if="edit">
            <span class="check" attrId="parent"></span>
            <span class="text">全选</span>
            <span class="text blue"></span>
            <span class="price blue"></span>
            <span class="notice"></span>
            <div class="btn" @click="deleteShopCar">删除</div>
        </div>
        <div class="settlement" v-else>
            <span class="check active" attrId="parent"></span>
            <span class="text">全选</span>
            <span class="text blue">应付金额:</span>
            <span class="price blue">￥{{allMoney}}</span>
<!--            <span class="notice"></span>-->
            <div class="btn" @click="Settlement">结算</div>
        </div>
    </div>
    <div class="nothing" v-else>
        <div class="img"></div>
        <p>购物车空空如也~~</p>
        <span @click="goShop">去逛逛</span>
    </div>
</div>
<script type="text/javascript">
    var shopCarData = eval('('+'<?php echo $shopCarData?>'+')');//购物车内数据
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/shopCar/index.js"></script>
</body>
</html>

