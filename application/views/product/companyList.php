<!DOCTYPE html>
<html>
<head>
    <title>物流公司</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/swiper.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/company/index.css">
    
</head>
<body>
    <div id="app" style="padding-bottom: 2rem" v-cloak>
        <headers></headers>
        <div class="companyList" v-for="i in companyList">
            <div class="btns" @click="placeAnOrder(i)">下单</div>
            <div class="companyName">{{i.comName}}</div>
            <div>
                <span class="name"></span>
                <p>联系人：{{i.comPerson}}</p>
            </div>
            <div>
                <span class="tel"></span>
                <p>联系方式：{{i.comTel}}</p>
            </div>
            <div>
                <span class="address"></span>
                <p>{{i.location}}</p>
            </div>
        </div>
        <footers :page="'shop'"></footers>
    </div>
    <script>
        var data = eval('('+'<?php echo $data?>'+')'),title = '<?php echo $title?>';
        $("title").html(title);
    </script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/swiper.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/company/index.js"></script>
</body>
</html>
    

