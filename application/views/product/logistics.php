<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/mobiletime.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/logistics/index.css">

</head>
<body>
<div id="app" style="padding-bottom: 2rem;" v-cloak>
    <div class="titles">物流委托</div>
    <form-input red :title="'公司名称'" :placeholder="'请输入公司名称'" v-model="form.companyName"></form-input>
    <div class="form-select">
        <div class="form-title"><span class="red">装货时间</span></div><div id="times" class="form-content form-checked">{{form.time}}</div><div class="form-img"></div>
    </div>
    <form-input red :title="'运送货物'" :placeholder="'请输入运送货物'" v-model="form.goodsName"></form-input>
    <form-input red :title="'货物包装'" :placeholder="'请输入货物包装'" v-model="form.goodsPack"></form-input>
    <form-input red :title="'货物重量'" :placeholder="'请输入货物重量'" :unit="'吨'" v-model="form.goodsWeight"></form-input>
    <div @click="form.loadAreaShow = true">
        <form-select
            red
            :title="'装货地址'"
            :checked="!!form.loadArea"
            :placeholder="form.loadArea ? form.loadArea : '请选择装货地址'"
        ></form-select>
    </div>
    <form-input :title="'详细地址'" :placeholder="'请输入详细地址'" v-model="form.loadAreaDetails"></form-input>
    <form-input red :title="'装货联系人'" :placeholder="'请输入装货联系人'" v-model="form.loadContactName"></form-input>
    <form-input red :title="'联系电话'" :placeholder="'请输入联系电话'" v-model="form.loadContactTel"></form-input>
    <div @click="form.unloadAreaShow = true">
        <form-select
                red
                :title="'卸货地址'"
                :checked="!!form.unloadArea"
                :placeholder="form.unloadArea ? form.unloadArea : '请选择卸货地址'"
        ></form-select>
    </div>
    <form-input :title="'详细地址'" :placeholder="'请输入详细地址'" v-model="form.unloadAreaDetails"></form-input>
    <form-input red :title="'卸货联系人'" :placeholder="'请输入卸货联系人'" v-model="form.unloadContactName"></form-input>
    <form-input red :title="'联系电话'" :placeholder="'请输入联系电话'" v-model="form.unloadContactTel"></form-input>
    <form-input :title="'备注说明'" :placeholder="'请输入备注说明'" v-model="form.remarks"></form-input>
    <div class="titles">被委托方</div>
    <div class="company">
        <p>{{logisticsCom.comName||有色云商}}</p>
        <p>{{logisticsCom.comPerson||有色云商}} {{logisticsCom.comTel||有色云商}}</p>
        <p v-show="showMore">公司详情：{{logisticsCom.comContent||有色云商}}</p>
        <p v-show="showMore">公司领域：{{logisticsCom.comOrigin||有色云商}}</p>
        <span v-show="showMore" @click="showMore = false">关闭</span>
        <span v-show="!showMore" @click="showMore = true">查看更多>></span>
    </div>
    <div class="next mt1" @click="submit">提交委托</div>
    <check-area @change="loadAreaChange" @close="form.loadAreaShow = false" v-if="form.loadAreaShow"></check-area>
    <check-area @change="unloadAreaChange" @close="form.unloadAreaShow = false" v-if="form.unloadAreaShow"></check-area>
    <footers :page="'shop'"></footers>
</div>
<script>
    var companyData = eval('('+'<?php echo $companyData?>'+')');
    console.log(companyData);
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/mobiletime.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/logistics/index.js"></script>
</body>
</html>

