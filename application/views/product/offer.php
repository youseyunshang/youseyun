<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/mobiletime.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/offer/index.css">
</head>
<body>
<div id="app" v-cloak>
    <div class="titles">报价</div>
    <div @click="showType = true">
        <form-select
                red
                :title="'含税类别'"
                :checked="!!type"
                :placeholder="type ? type : '请选择含税类别'"
        ></form-select>
    </div>
    <form-input red :title="'报价'" v-model="unitPrice" :placeholder="'请输入报价'"></form-input>
    <div @click="showSend = true">
        <form-select
                red
                :title="'送货方式'"
                :checked="!!send"
                :placeholder="send ? send : '请选择送货方式'"
        ></form-select>
    </div>
    <div @click="showPay = true">
        <form-select
                red
                :title="'付款方式'"
                :checked="!!pay"
                :placeholder="pay ? pay : '请选择付款方式'"
        ></form-select>
    </div>
    <div class="form-select">
        <div class="form-title"><span class="red">交货时间</span></div><div id="times" class="form-content form-checked">{{form.time}}</div><div class="form-img"></div>
    </div>
    <form-input red :title="'联系姓名'" v-model="billHolder" :placeholder="'请输入联系姓名'"></form-input>
    <form-input red :title="'手机号码'" v-model="tel" :placeholder="'请输入手机号码'"></form-input>
    <div class="next mt1" @click="offer">发送报价</div>
    <select-list :title="'含税类别'" :list="typeList" @change="(e) => { type = e.name;showType = false;}" @close="showType = false" v-if="showType"></select-list>
    <select-list :title="'送货方式'" :list="sendList" @change="(e) => { send = e.name;showSend = false;}" @close="showSend = false" v-if="showSend"></select-list>
    <select-list :title="'付款方式'" :list="payList" @change="(e) => { pay = e.name;showPay = false;}" @close="showPay = false" v-if="showPay"></select-list>
</div>
<script>
    var typeList = eval('('+'<?php echo $typeList?>'+')'),
        payList = eval('('+'<?php echo $payList?>'+')'),
        sendList = eval('('+'<?php echo $sendList?>'+')'),
        purchaseId = '<?php echo $purchaseId?>';
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/mobiletime.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/offer/index.js"></script>
</body>
</html>

