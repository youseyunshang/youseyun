<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductThird/index.css">
</head>
<body>
<div id="app" v-cloak>
    <div class="titles">完善产品资料</div>
    <!-- 上传图片 -->
    <div class="form-radio1">
        <div class="title"><span class="red">添加质检单</span></div>
        <div class="content">
            <div class="radio">
                <i><div :class="datas === '1' ? 'check' : ''"></div></i>
                <label :class="datas === '1' ? '' : 'uncheck'">有质检单</label>
                <input type="radio" name="radio" value="1" v-model="datas">
            </div>
            <div class="radio">
                <i><div :class="datas === '2' ? 'check' : ''"></div></i>
                <label :class="datas === '2' ? '' : 'uncheck'">无质检单</label>
                <input type="radio" name="radio" value="2" v-model="datas">
            </div>
        </div>
    </div>
    <div class="uploadImages" v-if="datas === '1'">
        <div class="left"></div>
        <div class="right">
            <label class="photo">
                <input v-show="false" type="file" id="qualityInspectionSheet" accept="image/*" multiple @change="handleChange">
                <img v-if="qualityInspectionSheet" :src="qualityInspectionSheet" alt="">
                <img v-else src="<?php echo ROOT_INDEX_REQUEST?>/resource/vue/images/addImage.png" alt="">
            </label>
            <p>提示：（点击图片可查看原图哦）</p>
        </div>
    </div>
    <div v-else>
        <textarea placeholder="请输入质检情况说明" v-model="reason"></textarea>
    </div>
    <div class="uploadImages">
        <div class="left">添加入库清单</div>
        <div class="right">
            <label class="photo">
                <input v-show="false" type="file" id="warehouseReceipt" accept="image/*" multiple @change="handleChange">
                <img v-if="warehouseReceipt" :src="warehouseReceipt" alt="">
                <img v-else src="<?php echo ROOT_INDEX_REQUEST?>/resource/vue/images/addImage.png" alt="">
            </label>
            <!-- <p>提示：（点击图片可查看原图哦）</p> -->
        </div>
    </div>

    <div class="btns">
        <div class="pre" @click="pre()">上一步</div><div class="next" @click="next()">下一步</div>
    </div>
    <select-list :title="'产品系'" :list="typesList" @change="(e) => { types = e.name}" @close="showTypes = false" v-if="showTypes"></select-list>
</div>
<script>
    var preData = eval('('+'<?php echo $preData?>'+')');
    console.log(preData);
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductThird/index.js"></script>
</body>
</html>