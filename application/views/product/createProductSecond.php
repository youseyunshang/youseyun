<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
<!--    <link rel="stylesheet" href="--><?php //echo ROOT_URL_DEFINE?><!--/resource/vue/css/mobiletime.css" />-->
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductSecond/index.css">
</head>
<body>
<div id="app" v-cloak>
    <div class="titles">选择要发布的产品</div>
    <form-input red :title="'产品名称'" :id="'productName'" :value="productName" :placeholder="'请输入产品名称'"></form-input>
    <form-input red :title="'产品数量'" :id="'num'" :value="num" :placeholder="'请输入产品数量'" :unit="'吨'"></form-input>
    <form-input red :title="'最低订量'" :id="'lastNum'" :value="lastNum" :placeholder="'请输入产品数量'" :unit="'吨'"></form-input>
    <form-input red :title="'产品价格'" :id="'lastPrice'" :value="lastPrice" :placeholder="'请输入产品价格'"></form-input>
	<!--/* <form-input red :title="'实际含量'" :id="'truePer'" :value="truePer" :placeholder="'请输入实际结算含量'" :unit="'%'"></form-input> */-->
    <div @click="showTypes = true" style="display: none;">
        <form-select
                red
                :title="'产品编号'"
                :checked="!!types"
                :placeholder="types ? types : '请选择产品编号'"
        ></form-select>
    </div>
    <div @click="showGranularity = true">
        <form-select
                :title="'产品粒度'"
                :checked="!!granularityType"
                :placeholder="granularityType ? granularityType : '请选择产品粒度'"
        ></form-select>
    </div>
	<div @click="showbrandName = true">
        <form-select
                :title="'产品牌号'"
                :checked="!!brandNameType"
                :placeholder="brandNameType ? brandNameType : '请选择产品牌号'"
        ></form-select>
    </div>
    <div @click="showProductOrigin = true">
        <form-select
                red
                :title="'产品产地'"
                :checked="!!ProductOrigin"
                :placeholder="ProductOrigin ? ProductOrigin : '请选择产品产地'"
        ></form-select>
    </div>
    <div @click="showProductLoation = true">
        <form-select
                red
                :title="'货物地点'"
                :checked="!!ProductLoation"
                :placeholder="ProductLoation ? ProductLoation : '请选择货物地点'"
        ></form-select>
    </div>
<!--    <form-input :title="'其他指标'" v-model="otherParams" :placeholder="'请输入其他指标'" ></form-input>-->
    <!-- 上传图片 -->
    <div class="uploadImages">
        <div class="left">产品图片</div>
        <div class="right">
            <label class="photo">
                <input v-show="false" type="file" accept="image/*" multiple @change="handleChange">
                <img v-if="photo" :src="photo" alt="">
                <img v-else src="<?php echo ROOT_INDEX_REQUEST?>/resource/vue/images/addImage.png" alt="">
            </label>
            <p></p>
        </div>
    </div>
    <form-input red :title="'截止时间'" :id="'dateSelectorOne'" :value="form.time" :placeholder="'输入日期'"></form-input>
    <form-radio
            red
            :id="'distributionMode'"
            :title="'配送方式'"
            :checked="sendType"
            :list="sendTypeList"
            @change="(e) => sendType = e"
    ></form-radio>
    <form-radio
            red
            :id="'invoice'"
            :title="'发票要求'"
            :checked="require"
            :list="requireList"
            @change="(e) => require = e"
    ></form-radio>
    <form-input :title="'产品详情'" :id="'productNotice'" :value="productNotice" :placeholder="'请输入产品详情'"></form-input>
    <div class="btns">
        <div class="pre" @click="pre()">上一步</div><div class="next" @click="next()">下一步</div>
    </div>
    <select-list :title="'产品系'" :list="typesList" @change="(e) => { types = e.name}" @close="showTypes = false" v-if="showTypes"></select-list>
    <select-list :title="'粒度'" :list="granularityList" @change="(e) => { granularityType = e.name;granularityTypeId = e.id;showGranularity = false;}" @close="showGranularity = false" v-if="showGranularity"></select-list>
    <select-list :title="'牌号'" :list="brandNameList" @change="(e) => { brandNameType = e.name;brandNameTypeId = e.id;showbrandName = false;}" @close="showbrandName = false" v-if="showbrandName"></select-list>
	<check-area @change="ProductLoations" @close="showProductLoation = false" v-if="showProductLoation"></check-area>
    <check-area @change="ProductOrigins" @close="showProductOrigin = false" v-if="showProductOrigin"></check-area>
</div>
<script>
    var preData = eval('('+'<?php echo $preData?>'+')'),granularityList = eval('('+'<?php echo $granularityList?>'+')'),brandNameList = eval('('+'<?php echo $brandNameList?>'+')');
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<!--<script src="--><?php //echo ROOT_URL_DEFINE?><!--/resource/vue/js/mobiletime.js"></script>-->
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductSecond/index.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/vue/Mdate/iScroll.js" ></script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/vue/Mdate/Mdate.js" ></script>
<script>
    new Mdate("dateSelectorOne", {
        acceptId: "dateSelectorOne",
        beginYear: "2018",
        beginMonth: "10",
        beginDay: "24",
        endYear: "2030",
        endMonth: "1",
        endDay: "1",
        format: "-"
    })
</script>
</body>
</html>
    
