<!DOCTYPE html>
<html>
<head>
    <title>发布供应</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductFirst/index.css">
</head>
<body>
<div id="app" v-cloak>
    <div class="titles">选择要发布的产品</div>
    <div id="shopAttr" @click="shopFirst = true">
        <form-select
                red
                :title="'产品系'"
                :checked="!!shopFirstName"
                :placeholder="shopFirstName ? shopFirstName : '请选择产品系'"
        ></form-select>
    </div>
    <div v-if="shopSecondShow" @click="shopSecond = true">
        <form-select
                :title="''"
                :checked="!!shopSecondName"
                :placeholder="shopSecondName ? shopSecondName : '请选择产品系'"
        ></form-select>
    </div>
    <div v-if="shopThirdShow" @click="shopThird = true">
        <form-select
                :title="''"
                :checked="!!shopThirdName"
                :placeholder="shopThirdName ? shopThirdName : '请选择产品系'"
        ></form-select>
    </div>
    <div @click="shopStatus = true">
        <form-select
                red
                :title="'产品形态'"
                :checked="!!shopStatusName"
                :placeholder="shopStatusName ? shopStatusName : '请选择产品形态'"
        ></form-select>
    </div>
    <div @click="comType = true">
        <form-select
                red
                :title="'企业类型'"
                :checked="!!comTypeName"
                :placeholder="comTypeName ? comTypeName : '请选择企业类型'"
        ></form-select>
    </div>
    <div class="next mt1" @click="next">下一步</div>
    <select-list :title="'产品系'" :list="shopFirstList" @change="(e) => { shopFirstName = e.name;shopFirstId = e.id;shopChange(shopFirstId,2);shopFirst = false;}" @close="shopFirst = false" v-if="shopFirst"></select-list>
    <select-list :title="'产品系'" :list="shopSecondList" @change="(e) => { shopSecondName = e.name;shopSecondId = e.id;shopChange(shopSecondId,3);shopSecond = false;}" @close="shopSecond = false" v-if="shopSecond"></select-list>
    <select-list :title="'产品系'" :list="shopThirdList" @change="(e) => { shopThirdName = e.name;shopThirdId = e.id;shopChange(shopThirdId,4);shopThird = false;}" @close="shopThird = false" v-if="shopThird"></select-list>
    <select-list :title="'商品形态'" :list="shopStatusList" @change="(e) => { shopStatusName = e.name;shopStatusId = e.id;shopStatus = false;}" @close="shopStatus = false" v-if="shopStatus"></select-list>
    <select-list :title="'企业类型'" :list="comTypeList" @change="(e) => { comTypeName = e.name;comTypeId = e.id;comType = false;}" @close="comType = false" v-if="comType"></select-list>
</div>
<script>
    var shopFirst = eval('('+'<?php echo $shopFirst?>'+')'),shopSon = eval('('+'<?php echo $shopSon?>'+')'),shopStatus = eval('('+'<?php echo $shopStatus?>'+')'),preData = eval('('+'<?php echo $preData?>'+')');
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductFirst/index.js"></script>
</body>
</html>
    
