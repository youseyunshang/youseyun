<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductFinish/index.css">

</head>
<body>
<div id="app" v-cloak>
    <div class="titles">填写发布人信息</div>
    <form-input red :title="'挂单人'" :id="'billHolder'" :value="billHolder" :placeholder="'请输入挂单人'"></form-input>
    <form-input red :title="'手机号码'" :id="'tel'" :value="tel" :placeholder="'请输入手机号码'"></form-input>
    <form-input :title="'邮箱地址'" :id="'email'" :value="email" :placeholder="'请输入邮箱地址'"></form-input>
    <div class="checks">
        <div class="checkbox mt70">
            <i><div :class="showBillHolder ? 'check' : ''"></div></i>
            <label class="f3">不显示企业信息、发布人信息</label>
            <input type="checkbox" v-model="showBillHolder">
        </div>
        <div class="checkbox mt46">
            <i><div :class="check2 ? 'check' : ''"></div></i>
            <label class="f3"><span class="colorE">我已经阅读并同意</span><span class="colorB">《服务协议》</span></label>
            <input type="checkbox" v-model="check2">
        </div>
    </div>
    <div class="next mt1" @click="finish">确认并发布</div>
</div>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/createProductFinish/index.js"></script>
</body>
</html>
    
