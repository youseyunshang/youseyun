<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/swiper.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/shopDefault/index.css">
    
</head>
<body>
    <div id="app" v-cloak>
        <headers></headers>
        <div class="today">
            <div class="left">
                <span>今日</span>
                <span class="mt10">布告</span>
            </div>
            <div class="right">
                <div class="swiper-container swiper1">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide" v-for="i in todayList">
                            <div class="list">
                                <span class="title">成交</span>
                                <p class="content">{{i}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="want" v-if="select">
            <div class="tap">
                <span class="active">帮我找货</span>
                <span @click="(e) => {select = false;}">我的供货</span>
            </div>
            <div class="content">
                <p>只需简单几个选项</p>
                <div>剩下的请交给我们专业的交易员</div>
            </div>
            <div class="btn" @click="publishInfo('purchase');">探寻理想货源</div>
        </div>
        <div class="want" v-else>
            <div class="tap">
                <span @click="(e) => {select = true;}">帮我找货</span>
                <span class="active">我的供货</span>
            </div>
            <div class="content">
                <p>查看行情走势</p>
                <div>随时掌握货品价格与行情动态</div>
            </div>
            <div class="btn" @click="publishInfo('supple');">我的供货文案</div>
        </div>
        <div style="height: 1.12rem;overflow: hidden;">
            <div class="swiper-container swiper2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" v-for="i in weekList">
                        <div class="deal">
                            <div>
                                <span></span>
                                <p>{{i.message}}</p>
                            </div>
                            <span class="num">{{i.num}}吨</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="line"></div>
        <div class="taps-list mt24">
            <div class="item" @click="productList">
                <span class="types"></span>
                <div class="title">全部类别</div>
            </div>
            <div class="item" @click="productList">
                <span class="basic-metals"></span>
                <div class="title">基本金属</div>
            </div>
            <div class="item" @click="productList">
                <span class="small-metals"></span>
                <div class="title">小金属</div>
            </div>
            <div class="item" @click="productList">
                <span class="ferroalloy"></span>
                <div class="title">铁合金</div>
            </div>
            <div class="item" @click="productList">
                <span class="rare-earth"></span>
                <div class="title">稀土类</div>
            </div>
        </div>
        <div class="taps-list mb14">
            <div class="item" @click="linkSupple">
                <span class="supply"></span>
                <div class="title">供应</div>
            </div>
            <div class="item" @click="linkPurchase">
                <span class="want-buy"></span>
                <div class="title">求购</div>
            </div>
            <div class="item" @click="company('2')">
                <span class="logistics"></span>
                <div class="title">物流公司</div>
            </div>
            <div class="item" @click="company('1')">
                <span class="quality-testing"></span>
                <div class="title">质检企业</div>
            </div>
            <div class="item">
            </div>
        </div>
        <div class="line"></div>
        <div class="metals-type">
            <div>矿</div>
            <div>化工金属</div>
            <div>铁合金</div>
            <div>金属制品</div>
        </div>
        <div class="line"></div>
        <div class="titles mt70">最新供应</div>
        <div class="main">
            <div class="product" v-for="i in newList" @click="goDetails('supple',i.Id)">
                <img :src="i.productPhoto" alt="">
                <div class="right">
                    <div class="title">{{i.title}}</div>
                    <div class="num">库存：{{i.num}}/吨({{i.minOrderNum}}吨起订)</div>
                    <div class="address">所在地 {{i.location || '未知'}}</div>
                    <div class="price"><i>{{i.unitPrice}}</i>元/基吨</div>
                </div>
            </div>
        </div>
        <div class="line"></div>
        <div class="advert"><img style="width: 100%;" src="<?php echo ROOT_INDEX_REQUEST."/resource/indexData/indexDefault/Advertisement1.png";?>"></div>
        <div class="titles mt50">为你推荐</div>
        <div class="main">
            <div class="product" v-for="i in list" @click="goDetails(i.itemType,i.Id)">
                <img :src="i.productPhoto" alt="">
                <div class="right">
                    <div class="title">{{i.title}}</div>
                    <div v-if="i.itemType === 'supple'">
                        <div class="num">库存：{{i.num}}/吨({{i.minOrderNum}}吨起订)</div>
                        <!-- <div class="label">
                            <span>品质卖家</span>
                        </div> -->
                        <div class="price"><i>{{i.unitPrice}}</i>元/60基吨</div>
                        <div class="texts"><span></span>{{i.comName}}</div>
                    </div>
                    <div v-if="i.itemType === 'purchase'">
                        <div class="num">牌&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号：{{i.brandName}}</div>
                        <div class="label">
                            <p>求购</p><p>{{i.distributionMode}}</p><p>{{i.invoice}}</p>
                        </div>
                        <div class="price"><i>{{i.unitPrice}}</i>吨 产地：{{i.placeOrigin}}</div>
                        <div class="texts">粒&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;度：{{i.granularity}}</div>
                        <div class="texts">到期时间：{{i.endTime}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="gomore" @click="getMore" v-if="showMore && !loading">更多商品信息</div>
        <img v-if="loading" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/reload2.gif" style="margin: .5rem auto">
        <footers :page="'shop'"></footers>
    </div>
    <script>
        var todayPublish = eval('('+'<?php echo $todayPublish?>'+')'),weekData = eval('('+'<?php echo $weekData?>'+')'),newSupple = eval('('+'<?php echo $newSupple?>'+')');
    </script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/swiper.js"></script>
    <script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/shopDefault/index.js"></script>
</body>
</html>
    
