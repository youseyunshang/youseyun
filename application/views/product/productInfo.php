<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
	<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/swiper.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/productDetails/index.css">
  
</head>
<body>
    <div id="app" v-cloak>
        <div class="banner">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" v-for="i in bannerList"><img :src="i" alt=""></div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <div class="main">
            <div class="label">品质商家</div>
            <div class="title">{{productData.title}}</div>
            <div class="price">供应价格：<span>{{productData.unitPrice}}</span>元/吨</div>
            <div class="content">
                <div><span>牌号：</span><i>{{productData.brandName}}</i></div>
                <div><span>粒度：</span><i>{{productData.granularity}}</i></div>
            </div>
            <div class="content">
                <div><span>产地：</span><i>{{productData.placeOrigin}}</i></div>
                <div><span>供货数量：</span><i>{{productData.num}}吨</i></div>
            </div>
            <div class="content">
                <div><span>货物所在地：</span><i>{{productData.location}}</i></div>
            </div>
            <div class="content">
                <div><span>发布日期：</span><i>{{productData.startTime}}</i></div>
            </div>
            <div class="content">
                <div><span>更新日期：</span><i>{{productData.startTime}}</i></div>
            </div>
            <div class="head-title mt17">
                <div>
                    <span>产品参数</span>
                    <i>DETAIL</i>
                </div>
                <div class="lines"></div>
            </div>
            <table border="0" cellspacing="1" cellpadding="0">
                <tr>
                    <td class="w21">
                        <div>名称：</div>
                        <div>{{productData.title}}</div>
                    </td>
                    <td class="w24">
                        <div>吨数：</div>
                        <div>{{productData.num}}吨</div>
                    </td>
                    <td class="w215">
                        <div>价格：</div>
                        <div>{{productData.unitPrice}}元/吨</div>
                    </td>
                </tr>
                <tr>
                    <td class="w24">
                        <div>牌号：</div>
                        <div>{{productData.brandName|| '--'}}</div>
                    </td>
                    <td class="w215">
                        <div>粒度：</div>
                        <div>{{productData.granularity|| '--'}}</div>
                    </td>
                    <td class="w21">
                        <div>产地：</div>
                        <div>{{productData.placeOrigin}}</div>
                    </td>
                </tr>
                <tr>
                    <td class="w21">
                        <div>发布日期：</div>
                        <div>2019-08-16</div>
                    </td>
                    <td class="w24">
                        <div>更新日期：</div>
                        <div>2019-08-16</div>
                    </td>
                    <td class="w215">
                        <div>发票要求：</div>
                        <div>{{productData.invoice}}</div>
                    </td>
                </tr>
                <tr>
                    <td class="w21">
                        <div>其他指标：</div>
                        <div>{{productData.other || '--'}}</div>
                    </td>
                    <td class="w24">
                        <div>检验：</div>
                        <div></div>
                    </td>
                    <td class="w215">
                        <div>货物所在地：</div>
                        <div>{{productData.location}}</div>
                    </td>
                </tr>
                <tr>
                    <td class="w21">
                        <div>入库清单：</div>
                        <div></div>
                    </td>
                    <td class="w215">
                        <div>配送方式：</div>
                        <div>{{productData.distributionMode}}</div>
                    </td>
                </tr>
            </table>
            <div class="head-title mt54">
                <div>
                    <span>产品图片</span>
                    <i>IMAGE</i>
                </div>
                <div class="lines"></div>
            </div>
            <img class="img1" :src="productData.productPhoto" alt="">
            <div class="head-title mt54">
                <div>
                    <span>产品质检</span>
                    <i>QUALITY</i>
                </div>
                <div class="lines"></div>
            </div>
            <img class="img2" :src="productData.qualityInspectionSheet" alt="">
        </div>
        <div class="line"></div>
        <div class="list">
            <div class="list-titles mt50">为你推荐</div>
            <div class="list-main">
                <div class="product" v-for="i in recommend">
                    <img :src="i.productPhoto" alt="">
                    <div class="right">
                        <div class="title">{{i.title}}</div>
                        <div class="num">库存：{{i.num}}/吨({{i.minOrderNum}}吨起订)</div>
                        <div class="label">
                            <span>品质卖家</span>
                        </div>
                        <div class="price"><i>{{i.unitPrice}}</i>元/60基吨</div>
                        <div class="texts"><span></span>{{i.comName}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="white"></div>
        <div class="shop-cart" @click="insertShopCar">
            <span></span>
            <p>加入购物车</p>
        </div>
        <div class="footers">
            <img class="photo" :src="manager.logo" alt="" @click="show = true">
            <div class="content" @click="show = true">
                <div class="name">
                    <span>{{manager.realname}}</span>
                    <i></i>
                </div>
                <div class="type">交易员</div>
            </div>
            <div class="btns call"><a id="forPhone" style="color: #fff;">电话咨询</a></div>
            <div class="btns buy">立即下单</div>
        </div>
        <div class="show-box" v-if="show">
            <div class="box">
                <div class="top">
                    <img class="photo" :src="manager.logo" alt="">
                    <div class="content">
                        <div class="name">
                            <span>{{manager.realname}}</span>
                            <i></i>
                        </div>
                        <div class="type">交易员</div>
                    </div>
                </div>
                <p>微信号：{{manager.wechart}}</p>
                <p>手机号：{{manager.tel}}</p>
                <img class="ercode" :src="manager.wechartpicture" alt="">
                <p>加微信询问业务更方便</p>
            </div>
            <img @click="show = false" class="close" src="<?php echo ROOT_URL_DEFINE?>/resource/vue/images/close2.png" alt="">
        </div>
    </div>
<script>
    var swiperPhone = eval('('+'<?php echo $swiperPhone?>'+')'),//轮播图数据
        data = eval('('+'<?php echo $data?>'+')'),//信息数据
        shopManager = eval('('+'<?php echo $shopManager?>'+')'),//业务人员信息
        forYouData = eval('('+'<?php echo $forYouData?>'+')');//为你推荐数据
        console.log(data);
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/swiper.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/productDetails/index.js"></script>
</body>
</html>