<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
<!--    <link rel="stylesheet" href="--><?php //echo ROOT_URL_DEFINE?><!--/resource/vue/css/mobiletime.css" />-->
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/getProductSecond/index.css">
</head>
<body>
<div id="app" v-cloak>
    <div class="titles">选择要购买的产品</div>
    <form-input red :title="'产品名称'" v-model="productName" :value="productName" :placeholder="'请输入产品名称'"></form-input>
    <form-input red :title="'产品数量'" v-model="num" :value="num" :placeholder="'请输入产品数量'" :unit="'吨'"></form-input>
    <form-input red :title="'预期价格'" v-model="price" :placeholder="'请输入预期采购价格'" :unit="'元/吨'"></form-input>
    <div @click="showbrandName = true">
        <form-select
                :title="'产品牌号'"
                :checked="!!brandNameType"
                :placeholder="brandNameType ? brandNameType : '请选择产品牌号'"
        ></form-select>
    </div>
    <div @click="showGranularity = true">
        <form-select
                :title="'产品粒度'"
                :checked="!!granularityType"
                :placeholder="granularityType ? granularityType : '请选择产品粒度'"
        ></form-select>
    </div>
    <div @click="showTypes = true" style="display: none;">
        <form-select
                red
                :title="'供应商地点'"
                :checked="!!types"
                :placeholder="types ? types : '请选择供应商地点'"
        ></form-select>
    </div>
    <div @click="showArea = true">
        <form-select
                red
                :title="'产品产地'"
                :checked="!!area"
                :placeholder="area ? area : '请选择产品产地'"
        ></form-select>
    </div>
    <form-input :title="'其他指标'" v-model="other" :placeholder="'请输入其他指标'"></form-input>
    <form-input red :title="'截止时间'" :id="'times'" :value="form.time" :placeholder="'输入日期'"></form-input>
<!--    <div class="form-select">-->
<!--        <div class="form-title"><span class="red">截止时间</span></div><div id="times" class="form-content form-checked">{{form.time}}</div><div class="form-img"></div>-->
<!--    </div>-->
<!--    <div class="desc">采购时间从初次审核通过时间开始计算。如采购方撤销询价，则有效期自动终止</div>-->
    <form-radio
            red
            :title="'配送方式'"
            :checked="sendType"
            :list="sendTypeList"
            @change="(e) => sendType = e"
    ></form-radio>
    <form-radio
            red
            :title="'发票要求'"
            :checked="require"
            :list="requireList"
            @change="(e) => require = e"
    ></form-radio>
    <form-input :title="'产品详情'" v-model="productNotice" :placeholder="'请输入产品详情'"></form-input>
    <div class="btns">
        <div class="pre" @click="pre()">上一步</div><div class="next" @click="next()">下一步</div>
    </div>
    <select-list :title="'粒度'" :list="granularityList" @change="(e) => { granularityType = e.name;granularityTypeId = e.id;showGranularity = false;}" @close="showGranularity = false" v-if="showGranularity"></select-list>
    <select-list :title="'牌号'" :list="brandNameList" @change="(e) => { brandNameType = e.name;brandNameTypeId = e.id;showbrandName = false;}" @close="showbrandName = false" v-if="showbrandName"></select-list>
    <check-area @change="checkArea" @close="showArea = false" v-if="showArea"></check-area>
</div>
<script>
    var preData = eval('('+'<?php echo $preData?>'+')'),
        granularityList = eval('('+'<?php echo $granularityList?>'+')'),
        brandNameList = eval('('+'<?php echo $brandNameList?>'+')');
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<!--<script src="--><?php //echo ROOT_URL_DEFINE?><!--/resource/vue/js/mobiletime.js"></script>-->
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/getProductSecond/index.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/vue/Mdate/iScroll.js" ></script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/vue/Mdate/Mdate.js" ></script>
<script>
    new Mdate("times", {
        acceptId: "times",
        beginYear: "2018",
        beginMonth: "10",
        beginDay: "24",
        endYear: "2030",
        endMonth: "1",
        endDay: "1",
        format: "-"
    })
</script>
</body>
</html>
    
