<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/mobiletime.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/qualityTesting/index.css">
</head>
<body>
<div id="app" style="padding-bottom: 2rem;" v-cloak>
    <div class="titles">质检委托</div>
    <form-input red :title="'公司名称'" v-model="comName" :placeholder="'请输入公司名称'"></form-input>
    <div class="form-select">
        <div class="form-title"><span class="red">需求时间</span></div><div id="times" class="form-content form-checked">{{form.time}}</div><div class="form-img"></div>
    </div>
    <form-input red :title="'质检货物'" v-model="goodsName" :placeholder="'请输入质检货物'"></form-input>
    <form-input red :title="'货物重量'" v-model="goodsNum" :placeholder="'请输入货物重量'" :unit="'吨'"></form-input>
    <div @click="showArea = true">
        <form-select
                red
                :title="'货物地址'"
                :checked="!!area"
                :placeholder="area ? area : '请选择货物地址'"
        ></form-select>
    </div>
    <form-input :title="'详细地址'" v-model="detailLocation" :placeholder="'请输入详细地址'"></form-input>
    <form-input red :title="'联系人'" v-model="person" :placeholder="'请输入联系人'"></form-input>
    <form-input red :title="'联系电话'" v-model="tel" :placeholder="'请输入联系电话'"></form-input>
    <form-input :title="'备注说明'" v-model="notice" :placeholder="'请输入备注说明'"></form-input>
    <div class="titles" :id="qualityCom.Id||0">被委托方</div>
    <div class="company">
        <p>{{qualityCom.comName||有色云商}}</p>
        <p>{{qualityCom.comPerson||有色云商}} {{qualityCom.comTel||有色云商}}</p>
        <p v-show="showMore">公司详情：{{qualityCom.comContent||有色云商}}</p>
        <p v-show="showMore">公司领域：{{qualityCom.comOrigin||有色云商}}</p>
        <span v-show="showMore" @click="showMore = false">关闭</span>
        <span v-show="!showMore" @click="showMore = true">查看更多>></span>
    </div>
    <div class="next mt1" @click="submit">提交委托</div>
    <check-area @change="checkArea" @close="showArea = false" v-if="showArea"></check-area>
    <footers :page="'shop'"></footers>
</div>
<script>
    var companyData = eval('('+'<?php echo $companyData?>'+')');
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/mobiletime.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/qualityTesting/index.js"></script>
</body>
</html>
    
