<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/swiper.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/productList/index.css">
</head>
<body>
<div id="app" style="padding-bottom: 2rem;" v-cloak>
    <headers></headers>
    <div class="banner">
        <div class="swiper-container swiper1">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="<?php echo ROOT_URL_DEFINE?>/resource/vue/images/img2.png" alt=""></div>
                <div class="swiper-slide"><img src="<?php echo ROOT_URL_DEFINE?>/resource/vue/images/img3.png" alt=""></div>
                <div class="swiper-slide"><img src="<?php echo ROOT_URL_DEFINE?>/resource/vue/images/img4.png" alt=""></div>
            </div>
        </div>
    </div>
    <div style="height: 1.12rem;overflow: hidden;">
        <div class="swiper-container swiper2">
            <div class="swiper-wrapper">
                <div class="swiper-slide" v-for="i in weekList">
                    <div class="deal">
                        <div>
                            <span></span>
                            <p>{{i.message}}</p>
                        </div>
                        <span class="num">{{i.num}}吨</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="type">
        <div class="typeList">
            <div class="item" v-for="i in firstAttr" :class="{'checked': checkedId === i.id}" @click="attrChange(i)">{{i.name}}</div>
        </div>
        <div class="more" @click="showMenu"><span></span></div>
    </div>
    <div class="main">
        <div class="product" v-for="i in list" @click="goDetails(i.itemType,i.Id)">
            <img :src="i.productPhoto" alt="">
            <div class="right">
                <div class="title">{{i.title}}</div>
                <div v-if="i.itemType === 'supple'">
                    <div class="num">库存：{{i.num}}/吨({{i.minOrderNum}}吨起订)</div>
                    <!-- <div class="label">
                        <span>品质卖家</span>
                    </div> -->
                    <div class="price"><i>{{i.unitPrice}}</i>元/60基吨</div>
                    <div class="texts"><span></span>{{i.comName}}</div>
                </div>
                <div v-if="i.itemType === 'purchase'">
                    <div class="num">牌&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;号：{{i.brandName}}</div>
                    <div class="label">
                        <p>求购</p><p>{{i.distributionMode}}</p><p>{{i.invoice}}</p>
                    </div>
                    <div class="price"><i>{{i.unitPrice}}</i>吨 产地：{{i.placeOrigin}}</div>
                    <div class="texts">粒&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;度：{{i.granularity}}</div>
                    <div class="texts">到期时间：{{i.endTime}}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="gomore" @click="getMore" v-if="showMore && !loading">更多商品信息</div>
    <img v-if="loading" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/reload2.gif" style="margin: .5rem auto">
    <div class="menu" v-if="menu">
        <div class="left"></div>
        <div class="right">
            <div class="top">
                <div class="box" v-for="(item,index) in typeList" v-if="item.list[0]">
                    <div class="title">
                        <span>产品类别</span>
                        <i></i>
                    </div>
                    <div class="list">
                        <span v-for="(i,index1) in item.list" :class="{checked: item.checkId === i.id}" @click="firstChange(index, i)">{{i.name}}</span>
                    </div>
                </div>
                <div class="box" v-if="brandList[0]">
                    <div class="title">
                        <span>产品牌号</span>
                        <i></i>
                    </div>
                    <div class="list">
                        <span v-for="i in brandList" :class="{checked: brandName === i.id}" @click="brandName = i.id">{{i.name}}</span>
                    </div>
                </div>
                <div class="box" v-if="granularityList[0]">
                    <div class="title">
                        <span>产品粒度</span>
                        <i></i>
                    </div>
                    <div class="list">
                        <span v-for="i in granularityList" :class="{checked: granularity === i.id}" @click="granularity = i.id">{{i.name}}</span>
                    </div>
                </div>
                <div class="box">
                    <div class="title">
                        <span>产品状态</span>
                        <i></i>
                    </div>
                    <div class="list">
                        <span v-for="i in statusList" :class="{checked: styleId === i.id}" @click="styleId = i.id">{{i.name}}</span>
                    </div>
                </div>
                
            </div>
            <div class="btns">
                <span class="reset" @click="resetId">重置</span>
                <span class="submit" @click="submit">确定</span>
            </div>
        </div>
    </div>
    <footers :page="'shop'"></footers>
</div>
<script>
    var shopFirst = eval('('+'<?php echo $shopFirst?>'+')'),//一级属性
    shopSon = eval('('+'<?php echo $shopSon?>'+')'),//子属性
    shopStatus = eval('('+'<?php echo $shopStatus?>'+')'),//状态属性
    weekData = eval('('+'<?php echo $weekData?>'+')'),
    brandName = eval('('+'<?php echo $brandName?>'+')'),
    granularity = eval('('+'<?php echo $granularity?>'+')'),
    where = eval('('+'<?php echo $where?>'+')');//where条件
</script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/swiper.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/productList/index.js"></script>
</body>
</html>