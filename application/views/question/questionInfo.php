<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>问题详情</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/js/part.js" ></script>
    <!--    字体图标-->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/font-awesome.min.css">
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/question/questionInfo.css?v="+new Date().getTime()+"'>");
    </script>
</head>
<body>
<!--返回start-->
<?php include 'application/views/smallBackIcon.php';?>
<!--返回end-->
<script>
    function demo() {
        /***
         Add your demo script here...
         In this demo, click my button after 1000 milliseconds
         You have approx three seconds to show off your stuff.
         ***/

        demointerval = setInterval(function() {
            var chkbox = document.querySelector('input[type="checkbox"]:not([checked])');
            if (chkbox) {
                chkbox.click();
                chkbox.setAttribute('checked', 'checked');
            } else {
                clearInterval(demointerval);
            }
        }, 600);
    }
    // Run demo if in searchresult preview frame
    // http://codepen.io/jesperkc/post/trigger-animation-in-preview-frame
    if (document.location.pathname.indexOf('fullcpgrid') > -1) {
        demo();
    }
</script>
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<!--首页导航end-->
<!--问题详情开始-->
<div class="pannel-content">
    <div class="pannel-index">
        <div class="pannel-index-res-title">
            <p style="font-family: MicrosoftYaHei;font-size: 16px;font-weight: bold;font-stretch: normal;letter-spacing: 0px;color: #000000;">
                <?php
                if(!empty($question)){
                    echo $question[0]['title'];
                }
                ?>
            </p>
        </div>
    </div>
    <div class="pannel-index" style="font-family: MicrosoftYaHei;padding: 0px 24px 15px;font-size: 14px;font-weight: normal;font-stretch: normal;letter-spacing: 0px;color: #434343;">
        <a href="javascript:;" onclick="questionInfo.moreAnswer();" style="text-decoration: none;color: #000;">查看更多回答
            <span>
               <?php
                   if(!empty($answer)){
                       $count = intval(count($answer))+1;
                       echo $count;
                   }else{
                       echo '1';
                   }
               ?>
            </span>
        </a>
    </div>
    <div class="pannel-index" style="border-top:0.5px solid #eee;padding:10px 20px;">
        <a href="javascript:;" style="border-right:0.5px solid #eee;" onclick="questionPart.clickSave(this);" qid="<?php echo $question[0]['unqid'];?>" class="question-controll">
            <span class="foot-icon <?php if($save){ echo 'foot-icon-save';}else{ echo 'foot-icon-nosave';}?>" style="position: relative;left: -5px;top: 3px;"></span><span>收藏</span>
        </a>
        <a href="<?php echo ROOT_INDEX_REQUEST;?>/indexQuestion/addAnswer/<?php echo $question[0]['unqid'];?>" class="question-controll">
            <span class="foot-icon" style="background-position: 94px -125px;position: relative;left: -5px;top: 3px;"></span><span>回答</span>
        </a>
    </div>
    <div class="pannel-index" style="background-color: #eeeeee;">
    </div>
    <div class="pannel-index">
        <?php
            if(isset($firstanswer['createUserInfos'])){
                $userInfos = json_decode($firstanswer['createUserInfos'],true);
                echo '<div class="pannel-index-que-answer">
                        <div class="pannel-index-que-answer-left">
                            <div style="width: 100%;overflow: hidden;box-sizing: border-box;">
                                <div style="width: 20%;height: 50px;float: left;display: flex;flex-direction: row;justify-content: center; align-items: center;">
                                    <img src="'.ROOT_URL_DEFINE.$userInfos['userLogo'].'" style="width: 80%;margin-top: 5px;border-radius: 80%;">
                                </div>
                                <div style="width: 60%;float: left;margin-top:5px;">
                                    <p style="margin: 0px;height: 25px;line-height: 25px;">'.$userInfos['username'].'</p>
                                    <p style="font-size: 12px;margin: 0px;">'.(!empty($userInfos['comName'])?$userInfos['comName']:'暂无企业').'</p>
                                </div>
                            </div>
                            <div class="content">
                            </div>
                        </div>
                        <div class="pannel-index-que-answer-right">
                            <div class="anim-icon heart">
                                <input type="checkbox" aId="'.$firstanswer['Id'].'" class="heartUpdown" id="heart'.$firstanswer['Id'].'" '.(isset($upAnswer[$firstanswer['Id']])?'checked':'').'/>
                                <label for="heart'.$firstanswer['Id'].'"></label>
                            </div>
                            <span class="num '.(isset($upAnswer[$firstanswer['Id']])?'foot-icon-up':'foot-icon-noup').'">'.$firstanswer['up'].'</span>
                        </div>
                    </div>';
            }
        ?>
<!--        onclick="questionInfo.upAnswer(this);" aId="'.$firstanswer['Id'].'"-->
        <div class="pannel-index-res-content">
            <?php
                if(!empty($firstanswer)){
                    echo "<p>";
                    echo $firstanswer['content'];
                    echo "</p>";
                }else{
                    echo '<div class="no-answer"><p style=""><span class="foot-icon" style="display: block;width: 1rem;height: 1rem;background-position: 1.5rem -3rem;"></span></p><p>~无人回答,请留下您的高见~</p></div>';
                }
            ?>
        </div>
    </div>
</div>
<!--问题详情结束-->
<div class="pannel-index" style="background-color: #eeeeee;">
</div>
<!--更多答案开始-->
<div class="pannel-content" id="moreAnswer">
    <?php
        if(!empty($answer)){
            foreach ($answer as $key=>&$val){
                $firstanswer = $val;
                echo '<div class="pannel-index">';
                if(isset($firstanswer['createUserInfos'])){
                    $userInfos = json_decode($firstanswer['createUserInfos'],true);
                    echo '<div class="pannel-index-que-answer">
                        <div class="pannel-index-que-answer-left">
                            <div style="width: 100%;overflow: hidden;box-sizing: border-box;">
                                <div style="width: 20%;height: 50px;float: left;display: flex;flex-direction: row;justify-content: center; align-items: center;">
                                    <img src="'.ROOT_INDEX_REQUEST.$userInfos['userLogo'].'" style="width: 80%;margin-top: 5px;border-radius: 80%;">
                                </div>
                                <div style="width: 60%;float: left;margin-top:5px;">
                                    <p style="margin: 0px;height: 25px;line-height: 25px;">'.$userInfos['username'].'</p>
                                    <p style="font-size: 12px;margin: 0px;">'.(!empty($userInfos['comName'])?$userInfos['comName']:'暂无企业').'</p>
                                </div>
                            </div>
                            <div class="content">
                            </div>
                        </div>
                        <div class="pannel-index-que-answer-right">
                            <div class="anim-icon heart">
                                <input type="checkbox" aId="'.$firstanswer['Id'].'" class="heartUpdown" id="heart'.$firstanswer['Id'].'" '.(isset($upAnswer[$firstanswer['Id']])?'checked':'').'/>
                                <label for="heart'.$firstanswer['Id'].'"></label>
                            </div>
                             <span class="num '.(isset($upAnswer[$firstanswer['Id']])?'foot-icon-up':'foot-icon-noup').'">'.$firstanswer['up'].'</span>
                        </div>
                    </div>';
                }
                echo '<div class="pannel-index-res-content">';
                if(!empty($firstanswer)){
                    echo "<p>";
                    echo $firstanswer['content'];
                    echo "</p>";
                }
                echo '</div>';
                echo '</div>';
            }
        }
    ?>
</div>
<!--<a href="javascript:;" onclick="questionInfo.upAnswer(this);" aId="'.$firstanswer['Id'].'"><span class="foot-icon '.(isset($upAnswer[$firstanswer['Id']])?'foot-icon-up':'foot-icon-noup').'"></span><span class="upnum">'.$firstanswer['up'].'</span></a>-->
<!--更多答案结束-->
<script>
    var questionInfo = {
        'init':function(){
            $(".heartUpdown").click(function(){
                if($(this).is(':checked')){
                    questionInfo.upAnswer({'type':'up','aId':$(this).attr("aId")});
                    // console.log("已经选中啦！");
                }else{
                    questionInfo.upAnswer({'type':'down','aId':$(this).attr("aId")});
                    // console.log("没有选中！");
                }
            });
        },
        'upAnswer':function(that){
            var that = that ||{};
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/indexQuestion/upAnswer",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : that,
                dataType : "json",
                success: function (response) {
                    if(response.code = '200'){
                        console.log(response.data.response);
                        switch (response.data.response) {
                            case "login":
                                // alert("登录以后，方可点赞！");
                                window.location.href = WINDOWS_INDEX_REQUEST+"/index/login";
                                return false;
                                break;
                            case "1":
                                $("input[aId='"+that.aId+"']").parent().parent().find(".num").html(response.data.count);
                                if(that.type=="up"){
                                    $("input[aId='"+that.aId+"']").parent().parent().find(".num").removeClass("foot-icon-noup");
                                    $("input[aId='"+that.aId+"']").parent().parent().find(".num").addClass("foot-icon-up");
                                }else{
                                    $("input[aId='"+that.aId+"']").parent().parent().find(".num").removeClass("foot-icon-up");
                                    $("input[aId='"+that.aId+"']").parent().parent().find(".num").addClass("foot-icon-noup");
                                }
                                break;
                            default:
                                break;
                        }
                    }else{
                        alert(response.message);
                    }
                }
            });
        },
        'moreAnswer':function(){
            $("#moreAnswer").show();
            $("html, body").animate({scrollTop: $("#moreAnswer").offset().top -20+ "px"}, 500);
            return false;//不要这句会有点卡顿
        }
    }
    questionInfo.init();
    document.addEventListener("error", function (e) {
        var elem = e.target;
        if (elem.tagName.toLowerCase() == 'img') {
            elem.src = WINDOWS_URL_DEFINE+"/resource/indexData/indexDefault/error.png";
        }
    }, true);
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
