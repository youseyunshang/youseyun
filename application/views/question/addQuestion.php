<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/ask/index.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
</head>
<body>
<div id="app" v-cloak :class="{'showMask': showMask}">
    <headers></headers>
    <div class="main">
        <textarea placeholder="输入问题并以问号结尾" style="min-height:60px;" id="titles" v-model="form.title" maxlength="50" @change="textAreaChange" @focus="textAreafocus" @blur="textAreablur" @keydown="form.title = form.title.substring(0, 50)" @keyup="form.title = form.title.substring(0, 50)" ></textarea>
        <div class="notice" v-if="showNotice" @click="showNotice = false">
            <div class="title">
                <div class="notices"></div>
                <span>让你的第一个提问获得更多解答</span>
                <div class="close"></div>
            </div>
            <p>· 保持文字简练，表述清晰问题的关键点</p>
            <p>· 添加合适的话题，让问题更好的沟通</p>
            <p>· 确保问题没有被提问过</p>
        </div>
        <div id="content">
        </div>
        <div class="uploadImage"><input class="input-file" id="imageUpload" type="file" name="fileInput" accept="image/*" style="position:absolute;left:0;opacity:0;width:100%;"></div>
    </div>
    <div class="labels">
        <div class="title" @click="showMask = true;isshow = true">选择话题（最多显示5个）+</div>
        <div class="labelList">
            <div class="item" v-for="i in labelList">{{i.title}}</div>
        </div>
    </div>
    <!-- <div class="list">
        <div class="item"><span>所属金属类别</span><div class="next"></div></div>
        <div class="item"><span>所属话题</span><div class="next"></div></div>
    </div> -->
    <button @click="submit" :class="{noClick: isSubmitBtnDisabled}" :disabled="isSubmitBtnDisabled">发布</button>
    <div class="mask" :class="showMask ? '' : 'mask-none'" v-if="isshow">
        <div :class="showMask ? 'chooseBox mask-active' : 'mask-none chooseBox '">
            <div class="top">
                <span></span>
                <span class="title">选择话题</span>
                <span class="complete" @click.stop="showMask = false">完成</span>
            </div>
            <div class="searchBox">
                <input type="text" placeholder="搜索话题" @blur="changeSearchSign()" v-model="searchSignAttr">
                <span class="search" @click="searchSign"></span>
            </div>
            <div class="notice">至少添加一个话题</div>
            <div class="labelList">
                <div class="item" v-for="i in labelList">{{i.title}}</div>
            </div>
            <div class="chooseList">
                <div class="item" v-for="i in chooseList">
                    <p>{{i.title}}</p>
                    <span @click="addSign(i.pid)">添加</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>-->
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/arteditor.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/ask/index.js"></script>
<script>
    var attr = '<?php echo $attr?>',attrResponse='';
    if(attr){
        attrResponse = eval('('+attr+')');
    }
    $('#content').artEditor({
        imgTar: '#imageUpload',
        limitSize: 10,   // 兆
        showServer: true,
        uploadUrl: WINDOWS_INDEX_REQUEST+'/IndexQuestion/uploadPic',
        data: {},
        uploadField: 'image',
        placeholader: '写回答...',
        validHtml: ["br"],
        beforeUpload: function(imgBase64) {
            // console.log("66666");
            // 处理完之后，必须return图片数据
            // return imgBase64;
        },
        uploadSuccess: function(res) {
            // 这里是处理返回数据业务逻辑的地方
            // `res`为服务器返回`status==200`的`response`
            // 如果这里`return <path>`将会以`<img src='path'>`的形式插入到页面
            // 如果发现`res`不符合业务逻辑
            // 比如后台告诉你这张图片不对劲
            // 麻烦返回 `false`
            // 当然如果`showServer==false`
            // 无所谓咯
            return res;
        },
        uploadError: function(res) {
            //这里做上传失败的操作
            //也就是http返回码非200的时候
            console.log(res);
        }
    });
</script>
</body>
</html>