<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="author" content="" />
    <!-- 作者 -->
    <meta name="revised" content="" />
    <!-- 定义页面的最新版本 -->
    <meta name="description" content="" />
    <!-- 网站简介 -->
    <meta name="keywords" content="" />
    <title>问答</title>
    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/js/part.js" ></script>
    <!-- 公共样式 结束 -->
    <script type="text/javascript">
        document.write("<link rel='stylesheet' type='text/css' href='<?php echo ROOT_URL_DEFINE?>/resource/indexData/css/index.css?v="+new Date().getTime()+"'>");
    </script>
</head>
<body>
<?php include 'application/views/smallIcon.php';?>
<!--首页导航start-->
<?php include 'application/views/header.php';?>
<!--首页导航end-->
<div class="pannel-index" id="sheet">
    <div class="index-navigation">
        <a href="<?php echo ROOT_INDEX_REQUEST?>/index/indexDefault"><span>推荐</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexRes/resLists"><span>资讯</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexQuestion/questionLists" style="font-size: 18px;color: #0053fb;"><span>问答</span><span class="underline-nav"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/indexProduct/index"><span>交易</span><span class="underline-nav" style="display: none;"></span></a>
        <a href="<?php echo ROOT_INDEX_REQUEST?>/indexUsercenter/index"><span>我的</span><span class="underline-nav" style="display: none;"></span></a>
    </div>
</div>
<div class="pannel-content">
</div>
<div class="pannel-index">
    <div style="width:100%;height:50px;display: flex;flex-direction: row;justify-content: center; align-items: center;" id="reload">
        <img src="<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/reload2.gif" style="width: 40%;">
    </div>
</div>
<script>
    var totalHeight = $(document).height();//整个文档高度
    var seeHeight = $(window).height(),scrollIn=true;//浏览器可视窗口高度
    var thisBodyHeight = $(document.body).height();//浏览器当前窗口文档body的高度
    var totalBodyHeight = $(document.body).outerHeight(true);//浏览器当前窗口文档body的总高度 包括border padding margin
    var thisWidth = $(window).width(); //浏览器当前窗口可视区域宽度
    var thisDocumentWidth = $(document).width();//浏览器当前窗口文档对象宽度
    var thisBodyWidth = $(document.body).width();//浏览器当前窗口文档body的宽度
    var totalBodyWidth = $(document.body).outerWidth(true);//浏览器当前窗口文档body的总宽度 包括border padding margin
    var scrollTop = $(window).scrollTop();//浏览器可视窗口顶端距离网页顶端的高度（垂直偏移）
    // console.log(totalHeight,seeHeight,thisBodyHeight,totalBodyHeight,thisWidth,thisDocumentWidth,thisBodyWidth,totalBodyWidth,scrollTop);
    var question = {
        'filterArr':{},
        'init':function(){
            //图片出错
            document.addEventListener("error", function (e) {
                var elem = e.target;
                if (elem.tagName.toLowerCase() == 'img') {
                    elem.src = WINDOWS_URL_DEFINE+"/resource/indexData/indexDefault/error.png";
                }
            }, true);
            //加载数据
            scrollIn = false;
            question.loadingData();
            return false;
        },
        'loadingData':function(){
            $("#reload").show();
            var data = {
              'indexMoreToken':'23478979345893654506',
              'filterArr':question.filterArr
            };
            $.ajax({
                url: WINDOWS_INDEX_REQUEST+"/IndexQuestion/questionLists",
                type : "POST",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data : data,
                dataType : "json",
                success: function (response) {
                    if(response){
                        var html='';
                        $.each(response,function(k,v){
                            //存储数据
                            question.filterArr[v.unqid]=1;
                            if(v.photo){
                                var photo = eval('('+v.photo+')');
                                if(photo.coverLogoTwo!=""){
                                    html = questionPart.threePicture(v,photo);
                                }else if(photo.coverLogoOne!=""){
                                    html = questionPart.onePicture(v,photo);
                                }else{
                                    html = questionPart.noPicture(v);
                                }
                            }else{
                                html = questionPart.noPicture(v);
                            }
                            $(".pannel-content").append(html);
                        });
                        scrollIn = true;
                        $("#reload").hide();
                    }else{
                        $("#reload").html("<span style='font-size: 12px;color: #BBB;'>没有更多数据了</span>");
                    }
                }
            });
        }
    }
    question.init();
    //添加滚动事件
    $(window).scroll(function(){
        scrollTop = $(window).scrollTop();
        totalHeight = $(document).height();
        //导航显示
        if(scrollTop>100){
            $("#sheet").addClass('fix-nav-sheet');
        }else{
            $("#sheet").removeClass('fix-nav-sheet');
        }
        if(scrollTop+seeHeight+100>totalHeight&&scrollIn){
            scrollIn = false;
            question.loadingData();
        }
    });
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/wxShare.js" ></script>
</body>

</html>
