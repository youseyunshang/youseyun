<!DOCTYPE html>
<html>
<head>
    <title>有色云</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no, email=no, date=no, address=no" />
    <link rel="shortcut icon" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <link rel="bookmark" href="<?php echo ROOT_URL_DEFINE?>/resource/adminData/img/adminLogin/favicon.ico"/>
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/framework/jquery-1.11.3.min.js" ></script>
<!--    <script typet="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>-->
    <script type="text/javascript" src="<?php echo ROOT_URL_DEFINE?>/resource/config.js" ></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/css/common.css" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/answer/index.css">
    <style>
        #content img{
            width:50%;
        }
    </style>
</head>
<body>
<div id="app" v-cloak>
    <headers></headers>
    <div class="top">
        <span class="topBack" @click="question"></span>
        <span style="display: flex;"><span style="font-size: .25rem;margin-right: 0.1rem;font-weight: bold;" @click="submit">发布</span><span class="send" @click="submit"></span></span>
    </div>
    <div class="title" id="title"><?php echo $question['title'];?></div>
    <div style="display: none;" id="unqid"><?php echo $question['unqid'];?></div>
    <div class="line"></div>
    <div class="main">
        <div id="content">
        </div>
        <div class="uploadImage" style="top:0rem;height:0.51rem;width:0.58rem;"><input class="input-file" id="imageUpload" type="file" name="fileInput" accept="image/*" style="position:absolute;left:0;opacity:0;width:100%;"></div>
    </div>

    <div class="list" style="display: none;">
        <div class="item">
            <div>
                <p>评论权限</p>
                <p>允许任何人评论</p>
            </div>
            <span>更改</span>
        </div>
        <div class="item"><div>禁止转载</div><i class="check"></i></div>
        <div class="item"><div>匿名身份</div><i class="checked"></i></div>
    </div>
</div>
<!--<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>-->
<!--<script src="https://unpkg.com/arteditor"></script>-->
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/arteditor.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/axios.min.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/js/common.js"></script>
<script src="<?php echo ROOT_URL_DEFINE?>/resource/vue/pages/answer/index.js"></script>
<script>
    $('#content').artEditor({
        imgTar: '#imageUpload',
        limitSize: 10,   // 兆
        showServer: true,
        uploadUrl: WINDOWS_INDEX_REQUEST+'/IndexQuestion/uploadPic',
        // uploadUrl: "",
        data: {},
        uploadField: 'image',
        placeholader: '写回答...',
        validHtml: ["br"],
        beforeUpload: function(imgBase64) {
            // console.log("66666");
            // 处理完之后，必须return图片数据
            // return imgBase64;
        },
        uploadSuccess: function(res) {
            // 这里是处理返回数据业务逻辑的地方
            // `res`为服务器返回`status==200`的`response`
            // 如果这里`return <path>`将会以`<img src='path'>`的形式插入到页面
            // 如果发现`res`不符合业务逻辑
            // 比如后台告诉你这张图片不对劲
            // 麻烦返回 `false`
            // 当然如果`showServer==false`
            // 无所谓咯
            console.log(res);
            return res;
        },
        uploadError: function(res) {
            //这里做上传失败的操作
            //也就是http返回码非200的时候
            console.log(res);
        }
    });
</script>
</body>
</html>

