<style>
    /*动画导航start*/
    .handerSmallIcon{
        background: url('<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/floatRight.png');
        width: 136px;
        height: 54px;
        padding-right: 10px;
        background-size: 100%;
        background-repeat: no-repeat;
        position: fixed;
        right: 0px;
        bottom: 100px;
        display: none;
    }
    .bounce {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
        width: 100%;
        color: white;
        height: 100%;
        font: normal bold 0.9rem "Product Sans", sans-serif;
        white-space: nowrap;
    }
    .letter {
        animation: bounce 0.4s cubic-bezier(0.05, 0, 0.2, 1) 1 alternate;
        display: inline-block;
        transform: translate3d(0, 0, 0);
        /*margin-top: 0.5em;*/
        text-shadow: rgba(255, 255, 255, 0.4) 0 0 0.05em;
        font: normal 500 0.3rem 'Varela Round', sans-serif;
    }
    .letter:nth-child(1) {
        animation-delay: 0s;
    }
    .letter:nth-child(2) {
        animation-delay: 0.0833333333s;
    }
    .letter:nth-child(3) {
        animation-delay: 0.1666666667s;
    }
    .letter:nth-child(4) {
        animation-delay: 0.25s;
    }
    .letter:nth-child(5) {
        animation-delay: 0.3333333333s;
    }
    .letter:nth-child(6) {
        animation-delay: 0.4166666667s;
    }
    @keyframes bounce {
        0% {
            transform: translate3d(0, 0, 0);
            text-shadow: rgba(255, 255, 255, 0.4) 0 0 0.05em;
        }
        50% {
            transform: translate3d(0, -0.5em, 0);
            text-shadow: rgba(255, 255, 255, 0.4) 0 1em 0.35em;
        }
        100% {
            transform: translate3d(0, 0, 0);
            text-shadow: rgba(255, 255, 255, 0.4) 0 0 0.05em;
        }
    }
    #handerSmallshow{
        width: 100%;
        font-family: MicrosoftYaHei;
        font-size: 13px;
        font-weight: normal;
        font-stretch: normal;
        letter-spacing: 0px;
        color: #7e8396;
        box-sizing: border-box;
        position: fixed;
        left: 0px;
        top: 0px;
        z-index: 999999;
        min-height: 700px;
        background: #fff;
    }
    .handerSmallshow-row{
        width: 100%;
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 0px 25px;
        box-sizing: border-box;
        justify-content: space-between;
        background: #fff;
        height: 110px;

    }
    .handerSmallshow-row-header{
        width:100%;
        height:200px;
        background: #fff;
        opacity: 1;
    }
    .handerSmallshow-row-content{
        width:100%;
        height:400px;
        background: #fff;
        opacity: 1;
    }
    .handerSmallshow-row .handerSmallshow-row-items{
        width:20%;
        text-align: center;
        text-decoration: none;
        color:#7e8396;
    }
    .handerSmallshow-row-items-icon{
        display: inline-block;
        width: 55px;
        height: 55px;
        border-radius: 30px;
        overflow: hidden;
        background-color: #0053fb;
        background-image: url('<?php echo ROOT_URL_DEFINE?>/resource/indexData/indexDefault/icon.png');
        background-size: 400px;
    }
    /*动画导航end*/
</style>
<div id="handerSmallshow" style="display: none;">
    <div class="handerSmallshow-row-header">
    </div>
    <div class="handerSmallshow-row-content">
        <div class="handerSmallshow-row">
            <a href="javascript:;" onclick="smallIcon.returnHeader();" class="handerSmallshow-row-items">
                <span class="handerSmallshow-row-items-icon" style="background-position: 5px -350px;"></span>
                <span class="handerSmallshow-row-items-font">返回顶部</span>
            </a>
            <a href="<?php echo ROOT_INDEX_REQUEST?>/index/indexDefault" class="handerSmallshow-row-items" style="">
                <span class="handerSmallshow-row-items-icon" style="background-color: #0053fb;background-position: -41px -350px;"></span>
                <span class="handerSmallshow-row-items-font">搜索</span>
            </a>
            <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexQuestion/addQuestion" style="" class="handerSmallshow-row-items">
                <span class="handerSmallshow-row-items-icon" style="background-color: #3998f4;background-position: -89px -350px;"></span>
                <span class="handerSmallshow-row-items-font">提个问题</span>
            </a>
            <a href="<?php echo ROOT_INDEX_REQUEST?>/IndexQuestion/questionLists" style="" class="handerSmallshow-row-items">
                <span class="handerSmallshow-row-items-icon" style="background-color: #7ecef4;background-position: -135px -350px"></span>
                <span class="handerSmallshow-row-items-font">回答问题</span>
            </a>
        </div>
        <div class="handerSmallshow-row">
            <a href="<?php echo ROOT_INDEX_REQUEST?>/index/login" class="handerSmallshow-row-items">
                <span class="handerSmallshow-row-items-icon" style="background-position: -180px -350px;"></span>
                <span class="handerSmallshow-row-items-font">发布供应</span>
            </a>
            <a href="<?php echo ROOT_INDEX_REQUEST?>/index/login" class="handerSmallshow-row-items" >
                <span class="handerSmallshow-row-items-icon" style="background-color: #0053fb;background-position: -228px -350px;"></span>
                <span class="handerSmallshow-row-items-font">发布求购</span>
            </a>
            <a href="<?php echo ROOT_INDEX_REQUEST?>/index/register" style="visibility: <?php echo (isset($_SESSION['indexUserData'])?"hidden":"visible");?>;" class="handerSmallshow-row-items">
                <span class="handerSmallshow-row-items-icon" style="background-color: #3998f4;background-position: -275px -350px;"></span>
                <span class="handerSmallshow-row-items-font">会员注册</span>
            </a>
            <a href="<?php echo ROOT_INDEX_REQUEST?>/index/login" style="visibility: <?php echo (isset($_SESSION['indexUserData'])?"hidden":"visible");?>;" class="handerSmallshow-row-items">
                <span class="handerSmallshow-row-items-icon" style="background-color: #7ecef4;background-position: -322px -350px;"></span>
                <span class="handerSmallshow-row-items-font">会员登录</span>
            </a>
        </div>
        <div class="handerSmallshow-row" style="justify-content: center;height:80px;">
            <a href="javascript:;" onclick="smallIcon.hidden();" class="handerSmallshow-row-items">
                <span class="handerSmallshow-row-items-icon" style="    transform: rotate(45deg);
    background-position: -210px -14px;background-color: #fff;"></span>
            </a>
        </div>
    </div>
</div>
<a href="javascript:;" onclick="smallIcon.handerSmallshow()">
    <div class="handerSmallIcon" id="handerSmallIcon">
        <div class="bounce">
            <span class="letter">提</span>
            <span class="letter">个</span>
            <span class="letter">问</span>
            <span class="letter">题</span>
        </div>
    </div>
</a>
<script>
    var smallIcon = {
        'init':function(){
            // 小图标滚动
            var signle = 1,str='';
            var sigle = setInterval(function () {
                flag = signle%5;
                switch (flag) {
                    case 1:
                        str='<span class="letter">提</span><span class="letter">个</span><span class="letter">问</span><span class="letter">题</span>';
                        break;
                    case 2:
                        str='<span class="letter">搜</span><span class="letter">索</span><span class="letter">一</span><span class="letter">下</span>';
                        break;
                    case 3:
                        str='<span class="letter">发</span><span class="letter">布</span><span class="letter">商</span><span class="letter">品</span>';
                        break;
                    case 4:
                        str='<span class="letter">登</span><span class="letter">录</span><span class="letter">注</span><span class="letter">册</span>';
                        break;
                }
                $("#handerSmallIcon").find(".bounce").empty();
                $("#handerSmallIcon").find(".bounce").prepend(str);
                $("#handerSmallIcon").find(".bounce").find(".letter").css({"animation":"bounce 0.4s cubic-bezier(0.05, 0, 0.2, 1) 1 alternate"});
                $("#handerSmallIcon").find(".bounce").find(".letter:nth-child(1)").css({"animation-delay":"0s"});
                $("#handerSmallIcon").find(".bounce").find(".letter:nth-child(2)").css({"animation-delay":"0.0833333333s"});
                $("#handerSmallIcon").find(".bounce").find(".letter:nth-child(3)").css({"animation-delay":"0.1666666667s"});
                $("#handerSmallIcon").find(".bounce").find(".letter:nth-child(4)").css({"animation-delay":"0.25s"});
                $("#handerSmallIcon").find(".bounce").find(".letter:nth-child(5)").css({"animation-delay":"0.3333333333s"});
                signle++;
            },5000);
            //收起
            $(".handerSmallshow-row-header").click(function(){
                $("#handerSmallshow").hide("500");
                $("body").attr("style","");
            });
        },
        'handerSmallshow':function(){
            $("#handerSmallshow").show("500");
            $("body").attr("style","overflow:hidden;");
        },
        'hidden':function(){
            $("#handerSmallshow").hide("500");
            $("body").attr("style","");
        },
        "returnHeader":function(){
            smallIcon.hidden();
            $("html, body").animate({scrollTop: $(".pannel-header").offset().top -20+ "px"}, 1000);
            return false;//不要这句会有点卡顿
        }
    }
    smallIcon.init();
    $(window).scroll(function(){
        //导航显示
        if($(window).scrollTop()>100){
            $("#handerSmallIcon").show();
        }else{
            $("#handerSmallIcon").hide();
        }
    });
</script>
