<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class IndexRes extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Index_res_model');
        $this->load->driver('cache');
    }
    //资讯数据页
    public function reportIndex(){
        $data = self::getReportformLog();
//        $shopAttr = self::getInfoAttrFirst();
        //报价数据
        $result = array('reportData'=>$data,'shopAttr'=>self::getInfoAttr());
        $this->load->view('res/reportIndex', $result);
    }
    //报价接口
    public function getReportformLog($time=86400){
        $this->load->model('Index_model');
        $nowTime = strtotime(date("Y-m-d",time()));
        $nowData = Index_model::get_instance()->get_reportform_log($nowTime);
        $lastData = Index_model::get_instance()->get_reportform_log(($nowData['createTime']-$time));
        if(!empty($nowData)&&!empty($lastData)){
            $todayData = json_decode($nowData['content'],true);
            $yestodayData = json_decode($lastData['content'],true);
            $data = Array();
            unset($val);
            foreach ($yestodayData as $key=>&$val){
                $data[$val['idnumber']] = $val;
            }
            unset($val);
            foreach ($todayData as $key=>&$val){
                if(isset($data[$val['idnumber']])){
                    // $todayData[$key]['updown'] = $val['middlenum']-$data[$val['idnumber']]['middlenum'];
                    $todayData[$key]['updown'] = ($val['topnum']+$val['lastnum'])/2-($data[$val['idnumber']]['topnum']+$data[$val['idnumber']]['lastnum'])/2;
                    $todayData[$key]['updown'] = intval($todayData[$key]['updown']);
                }
            }
        }
        return $todayData;
    }
    //信息一级金属属性
    public function getInfoAttrFirst(){
        $this->load->model('Attribute_model');
        $data = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>0));
        return $data;
    }
    /**
     * resInfos
     * @notice 资源详情数据
     * @return	void
     * @anthor	Json.Wang
     * @email 591935719@qq.com
     */
    public function resInfos($id = 0){
        if($id>0){
            $result = self::getResInfos();
            $data = array();
	        if( $data['resInfo'] = Index_res_model::get_instance()->get_res(array("Id"=>$id))){
                $save = self::userSaveResource();
                $data['resInfo']['save'] = isset($save[$id])?'foot-icon-save':'foot-icon-nosave';
                //解析数据
                if(file_exists($data['resInfo']['content'])){
                    $str = file_get_contents($data['resInfo']['content']);//将整个文件内容读入到一个字符串中
                    $data['resInfo']['content'] = str_replace("\r\n","<br />",unserialize($str));
                }
            }
            if(!empty($data['resInfo']['attrLog'])){
                $attr = json_decode($data['resInfo']['attrLog'],true);
            }else{
                $attr = array();
            }
            if(!empty($data['resInfo']['signLog'])){
                $sign = json_decode($data['resInfo']['signLog'],true);
            }else{
                $sign = array();
            }
            $search = array_merge($attr,$sign);
            //推荐数据
            $forData = array();
            //备用推荐项
            $forDataSecond = array();
            unset($val);
            foreach ($result as $key=>&$val){
                //添加备用项
                if($key<5){
                    $forDataSecond[] = $val;
                }
                //推荐项
                if(count($forData)>4){
                    break;
                }
                unset($v);
                foreach ($search as $k=>&$v){
                    if(preg_match('/'.$v['attrname'].'/i',$val['title'])||preg_match('/'.$v['attrname'].'/i',$val['notice'])){
                        $forData[] = $val;
                        break;
                    }
                }
            }
            $data['forDatas'] = empty($forData)?$forDataSecond:$forData;
            $this->load->view('res/resInfos', $data);
        }else{
            echo "数据错误！";
        }
    }
    //当前用户收藏的哪些资源
    public function userSaveResource(){
        $result = array();
        if(isset($_SESSION["indexUserData"]["Id"])){
            if($data = CI_Redis::get_instance()->redis->get('collection_res_'.$_SESSION["indexUserData"]["Id"])){
                $result = json_decode($data,true);
            }else{
                $where = array(
                    "userId"=>$_SESSION["indexUserData"]["Id"]
                );
                //更新
                $result = Index_res_model::get_instance()->get_collection_res($where);
            }

        }
       return $result;
    }
    //获取资讯数据
    public function getResInfos(){
//        if($result = $this->cache->redis->get('resInfos')){
//            return json_decode($result,true);
//        }else{
            $result = Index_res_model::get_instance()->get_res();
            foreach ($result as $key=>&$val){
                unset($val['content']);
                $val['totalType'] = 'res';
                $val['totalUnqid'] = 'res'.$val['Id'];
            }
//            $this->cache->redis->save('resInfos',json_encode($result),3600);
            return $result;
//        }
    }
    //资讯列表
    public function resLists(){
        $post = $this->input->post();
        if(!empty($post)){
            //首页资源数据
            $data = self::getResInfos();
            //过滤处理
            //排序
            $last_names = array_column($data,'createTime');
            array_multisort($last_names,SORT_DESC,$data);
            if(!empty($post['filterArr'])){
                unset($val);
                foreach ($data as $key=>&$val){
                    if(isset($post['filterArr'][$val['totalUnqid']])){
                        unset($data[$key]);
                    }
                }
            }else{
                //前台没有过滤，证明是第一次使用
                //用户登录进行数据灌入
                if(isset($_SESSION['indexUserData'])&&$signSearch = self::userSignSearch()&&empty($post['searchAttr'])&&empty($post['resType'])){
                    //用户登录做操作
                    $firstData = array();
                    unset($val);
                    foreach ($data as $key=>$val){
                        if(isset($signSearch[$val['totalUnqid']])){
                            $firstData[] = $val;
                        }else{
                            //最新的数据加载
                            if($key<3){
                                $firstData[] = $val;
                            }
                        }
                    }
                    if(!empty($firstData)){
                        $data = $firstData;
                    }
                }
            }
            //金属属性
            if(!empty($post['searchAttr'])){
                unset($val);
                foreach ($data as $key=>&$val){
                    if(!preg_match("/".$post['searchAttr']."/i",$val['attrLog'])){
                        unset($data[$key]);
                    }
                }
            }
            //资源类型
            if(!empty($post['resType'])){
                unset($val);
                foreach ($data as $key=>&$val){
                    if($post['resType']!=$val['type']){
                        unset($data[$key]);
                    }
                }
            }
            //资源过滤
            shuffle($data);
            $data = array_slice($data,0,5);
            if(!empty($data)){
                self::outMessage("200","资源加载成功！",array("response"=>$data));
            }else{
                self::outMessage("404","没有可用资源！",array("response"=>0));
            }
        }else{
            $result = array("infoAttr"=>self::getInfoAttr());
            $this->load->view('res/resLists', $result);
        }
    }
    //获得金属的属性标签
    public function getInfoAttr(){
        $this->load->model('Index_attr_model');
        $infoAttr = Index_attr_model::get_instance()->get_info_attr();
        $firstData = array();
        $secondData = array();
        if(!empty($infoAttr)){
            unset($val);
            foreach ($infoAttr as $key=>&$val){
                if($val['pid']>0){
                    $secondData[] = array(
                        "Id"=>$val['Id'],
                        "pid"=>$val['pid'],
                        "attrname"=>$val['attrname']
                    );
                }else{
                    $firstData[] = array(
                        "Id"=>$val['Id'],
                        "pid"=>$val['pid'],
                        "attrname"=>$val['attrname']
                    );
                }
            }
        }
        return array_merge($firstData,$secondData);
    }
    //通过用户喜好标签检索数据
    public function userSignSearch(){
//            1.查询用户的喜好标签
//        signAttrRes_用户Id
        if(empty($_SESSION["indexUserData"]["Id"])){
            return array();
        }
        if($result = CI_Redis::get_instance()->redis->get('signAttrRes_'.$_SESSION["indexUserData"]["Id"])){
            return json_decode($result,true);
        }else{
            $this->load->model('Index_user_model');
            $data = array();
            if($hobbyQuery = Index_user_model::get_instance()->get_hobby()){
                $hobby = array();
                foreach ($hobbyQuery as $key=>&$val){
                    $hobby[] = $val['attrId'];
                }
                //资源数据
                if($result = Index_user_model::get_instance()->get_res_attr_wherein($hobby)){
                    unset($val);
                    foreach ($result as $key=>&$val){
                        if($key<10){
                            $data['res'.$val['resId']] = 1;
                        }else{
                            break;
                        }
                    }
                }
            }
            CI_Redis::get_instance()->redis->setex('signAttrRes_'.$_SESSION["indexUserData"]["Id"],3600,json_encode($data));
            return $data;
        }
    }
    //收藏资讯
    //return login 返回登录接口
    //return 0 已经收藏
    //return >0 收藏成功
    public function collectionRes(){
        if(empty($_SESSION['indexUserData'])){
            echo "login";
            die();
        }
        $post = $this->input->post();
        $params = array(
            "userId"=>$_SESSION["indexUserData"]["Id"],
            "resId"=>$post['resId']
        );
        echo Index_res_model::get_instance()->collection_res($params);
    }
}
