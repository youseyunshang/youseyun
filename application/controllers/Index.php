<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Index extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Index_model');
        $this->load->library('CI_Index');
    }
    /**
     * indexDefault
     *
     * 用户默认进入的首页
     *
     * @param  $params=array()
     * @return	1.用户首页 2.请求数据
     * @anthor Json.Wang
     */
    public function indexDefault($params=""){
        $post = $this->input->post();
        if(!empty($post)){
            //首页资源数据
            $data = self::getResInfos();
            $advert = self::getAdvertisementDatas();
            $question = self::getQuestionDatas();
            $data = array_merge($data,$advert,$question);
            //排序
            $last_names = array_column($data,'createTime');
            array_multisort($last_names,SORT_DESC,$data);
            //前台数据过滤
            if(!empty($post['filterArr'])){
                unset($val);
                foreach ($data as $key=>&$val){
                    if(isset($post['filterArr'][$val['totalUnqid']])){
                        unset($data[$key]);
                    }
                }
            }else{
                //前台没有过滤，证明是第一次使用
                //用户登录进行数据灌入
                if(isset($_SESSION['indexUserData'])&&$signSearch = self::userSignSearch()){
                    //用户登录做操作
                    $firstData = array();
                    unset($val);
                    foreach ($data as $key=>$val){
                        if(isset($signSearch[$val['totalUnqid']])){
                            $firstData[] = $val;
                        }else{
                            //最新的数据加载
                            if($key<3){
                                $firstData[] = $val;
                            }
                        }
                    }
                    $data = $firstData;
                }
            }
            //资源过滤
            $data = array_slice($data,0,10);
            shuffle($data);
            if(!empty($data)){
                echo json_encode($data);
            }else{
                echo 0;
            }
        }else{
            $res = self::getResInfos();
            $nowTime = strtotime(date("Y-m-d",time()));
            foreach ($res as $k=>&$v){
                if($v['createTime']<$nowTime){
                    unset($v);
                }
            }
            //报价数据
            $todayData  = self::getReportformLog();
            $forTodayData = array();
            foreach ($todayData as $key=>$val){
                if($key>=4){
                    break;
                }else{
                    //文字颜色
                    if( $val['updown'] > 0 ){
                        $val['color'] = 'color: #009944;';
                    }elseif ( $val['updown'] < 0 ){
                        $val['color'] = 'color: #ff0000;';
                    }else{
                        $val['color'] = '';
                    }
                }
                $forTodayData[] = $val;
            }
            //吨数
            $weekData = self::weekData();
            //商城信息
            $shopInfo = self::shopInfo();
            $result = array(
                "reportData"=>$forTodayData,
                "weekData"=>$weekData,
                "weixinValid"=>$params,
                "shopInfo"=>$shopInfo,
                "newResCount"=>count($res)
                );
            $this->load->view('index/indexDefault', $result);
        }
    }
    /**
     * shopInfo
     *
     * 默认首页商品信息
     *
     * @param  null
     * @return	俩条商品数据
     * @anthor Json.Wang
     */
    public function shopInfo(){
        $this->load->model('Supple_model');
        $this->load->model('Purchase_model');
        if($redisData = CI_Redis::get_instance()->redis->get('shopDefaultData')){
            $data = json_decode($redisData,true);
        }else{
            //供应通过的数据
            $Supple = Supple_model::get_instance()->get_order_where(array("status"=>1));
            unset($val);
            foreach($Supple as $key=>&$val){
                $val['itemType'] = "supple";
            }
            //采购通过的数据
            $Purchase = Purchase_model::get_instance()->get_order_where(array("status"=>1));
            unset($val);
            foreach($Purchase as $key=>&$val){
                $val['itemType'] = "purchase";
            }
            $data = array_merge($Supple,$Purchase);
            $last_names = array_column($data,'createTime');
            array_multisort($last_names,SORT_DESC,$data);
            CI_Redis::get_instance()->redis->setex('shopDefaultData',3600,json_encode($data));
        }
        return array_slice($data,0,2);
    }
    /**
     * searchDefault
     *
     * 用户默认进入搜索页
     *
     * @param  $params=array()
     * @return	1.用户首页 2.请求数据
     * @anthor Json.Wang
     */
    public function searchDefault($params=""){
        $post = $this->input->post();
        if(!empty($post)){
            switch($post["resType"]){
                case "res":
                    //首页资源数据
                    $data = self::getResInfos();
                    break;
                case "que":
                    //首页资源数据
                    $data = self::getQuestionDatas();
                    break;
                case "shop":
                    //首页资源数据
                    $data = self::getResInfos();
                    break;
                default:
                    //首页资源数据
                    $data = self::getResInfos();
                    break;
            }
            //前台数据过滤
            if(!empty($post['filterArr'])){
                unset($val);
                foreach ($data as $key=>&$val){
                    if(isset($post['filterArr'][$val['totalUnqid']])){
                        unset($data[$key]);
                    }
                }
            }
            if(!empty($post['content'])){
                $headData = array();
                unset($val);
                foreach ($data as $key=>&$val){
                    if(preg_match("/".$post['content']."/i", $val['title'])){
                        $headData[] = $val;
                    }
                }
                $data = $headData;
            }
            //资源过滤
//            shuffle($data);
            $data = array_slice($data,0,10);
            if(!empty($data)){
                echo json_encode($data);
            }else{
                echo 0;
            }
        }else{
            //报价数据
            $result = array();
            $this->load->view('index/searchDefault', $result);
        }
    }
    public function ajaxSignShare(){
        $post = $this->input->post();
        $jsapi_ticket = '';
        if($access_token = self::access_token()){
            $jsapi_ticket = self::jsapi_ticket();
        }
        $time = time();
        $strs="QWERTYUIOPASDFGHJKLZXCVBNM1234567890qwertyuiopasdfghjklzxcvbnm";
        $noncestr=substr(str_shuffle($strs),mt_rand(0,strlen($strs)-11),10);
        $data = array("jsapi_ticket"=>$jsapi_ticket,"noncestr"=>$noncestr,"timestamp"=>$time);
        $params = http_build_query($data)."&url=".$post['url'];
        $sign = sha1($params);
        $response = array(
            "appid"=>AppId,
            "timestamp"=>$time,
            "nonceStr"=>$noncestr,
            "signature"=>$sign
        );
        echo json_encode($response);
    }
    //获取access_token
    public function access_token(){
        if($result = CI_Redis::get_instance()->redis->get('access_token')){
            return $result;
        }else{
            $this->load->library('Curl_request');
            $header = array();
            $data = array("grant_type"=>'client_credential',"appid"=>AppId,"secret"=>AppSecret);
            $path = "https://api.weixin.qq.com/cgi-bin/token";
            $data = Curl_request::get_instance()->curl($path,$data,$header,0,1);
            $result = json_decode($data,true);
            if(isset($result['access_token'])){
                CI_Redis::get_instance()->redis->setex('access_token',7200,$result['access_token']);
                return $result['access_token'];
            }else{
                return false;
            }
        }
    }
    //获取jsapi_ticket
    public function jsapi_ticket(){
        if($result = CI_Redis::get_instance()->redis->get('jsapi_ticket')){
            return $result;
        }else{
            $this->load->library('Curl_request');
            $header = array();
            $data = array("access_token"=>CI_Redis::get_instance()->redis->get('access_token'),"type"=>"jsapi");
            $path = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";
            $data = Curl_request::get_instance()->curl($path,$data,$header,0,1);
            $result = json_decode($data,true);
            if(isset($result['ticket'])){
                CI_Redis::get_instance()->redis->setex('jsapi_ticket',7200,$result['ticket']);
                return $result['ticket'];
            }else{
                return false;
            }
        }
    }
    //通过用户喜好标签检索数据
    public function userSignSearch(){
//            1.查询用户的喜好标签
//        signAttrRes_用户Id
        if(empty($_SESSION["indexUserData"]["Id"])){
            return array();
        }
        if($result = CI_Redis::get_instance()->redis->get('signAttrRes_'.$_SESSION["indexUserData"]["Id"])){
            return json_decode($result,true);
        }else{
            $this->load->model('Index_user_model');
            $data = array();
            if($hobbyQuery = Index_user_model::get_instance()->get_hobby()){
                $hobby = array();
                foreach ($hobbyQuery as $key=>&$val){
                    $hobby[] = $val['attrId'];
                }
                //资源数据
                if($result = Index_user_model::get_instance()->get_res_attr_wherein($hobby)){
                    unset($val);
                    foreach ($result as $key=>&$val){
                        if($key<10){
                            $data['res'.$val['resId']] = 1;
                        }else{
                            break;
                        }
                    }
                }
            }
            CI_Redis::get_instance()->redis->setex('signAttrRes_'.$_SESSION["indexUserData"]["Id"],3600,json_encode($data));
            return $data;
        }
    }
    //报价接口
    public function getReportformLog(){
        $nowTime = strtotime(date("Y-m-d",time()));
        $nowData = Index_model::get_instance()->get_reportform_log($nowTime);
        $lastData = Index_model::get_instance()->get_reportform_log(($nowData['createTime']-86400));
        if(!empty($nowData)&&!empty($lastData)){
            $todayData = json_decode($nowData['content'],true);
            $yestodayData = json_decode($lastData['content'],true);
            $data = Array();
            unset($val);
            foreach ($yestodayData as $key=>&$val){
                $data[$val['idnumber']] = $val;
            }
            unset($val);
            foreach ($todayData as $key=>&$val){
                if(isset($data[$val['idnumber']])){
                    //$todayData[$key]['updown'] = $val['middlenum']-$data[$val['idnumber']]['middlenum'];
                     $todayData[$key]['updown'] = ($val['topnum']+$val['lastnum'])/2-($data[$val['idnumber']]['topnum']+$data[$val['idnumber']]['lastnum'])/2;
		     $todayData[$key]['updown'] = intval($todayData[$key]['updown']);
		}
            }
        }
        return $todayData;
    }
    //本周成交量
    public function weekData(){
        $homeOrderData = Index_model::get_instance()->get_order();
        //本周成单数量
        $weekNum = 0;
        $weekTime = strtotime(date("Y-m-d",time()))-604800;
        //日成单数量
        $dayNum = 0;
        $dayTime = strtotime(date("Y-m-d",time()));
        foreach ($homeOrderData as $key=>&$val){
            if($val['createTime']>$weekTime){
                $weekNum += $val['num'];
            }
            if($val['createTime']>$dayTime){
                $dayNum += $val['num'];
            }
        }
        $result = array(
            "weekNum"=>$weekNum,
            "dayNum"=>$dayNum
        );
        return $result;
    }
    //获取资讯数据
    public function getResInfos(){
        if($result = CI_Redis::get_instance()->redis->get('resInfos')){
            return json_decode($result,true);
        }else{
            $this->load->model('Index_res_model');
            $result = Index_res_model::get_instance()->get_res();
            foreach ($result as $key=>&$val){
                unset($val['content']);
                $val['totalType'] = 'res';
                $val['totalUnqid'] = 'res'.$val['Id'];
            }
            CI_Redis::get_instance()->redis->setex('resInfos',3600,json_encode($result));
            return $result;
        }
    }
    //获取供应数据
    public function getSuppleDatas(){
        $result = Index_model::get_instance()->get_supple_select();
        var_dump("<pre>",$result);
        die();
    }
    //获取供应数据
    public function getPurchaseDatas(){
        $result = Index_model::get_instance()->get_purchase_select();
        var_dump("<pre>",$result);
        die();
    }
    //获取广告信息
    public function getAdvertisementDatas(){
        if($result = CI_Redis::get_instance()->redis->get('advertisementDatas')){
            return json_decode($result,true);
        }else{
            $result = Index_model::get_instance()->get_advertisement_select();
            foreach ($result as $key=>&$val){
                $val['totalType'] = 'advert';
                $val['totalUnqid'] = 'advert'.$val['Id'];
            }
            CI_Redis::get_instance()->redis->setex('advertisementDatas',3600,json_encode($result));
            return $result;
        }
    }
    //获取提问信息
    public function getQuestionDatas(){
        $this->load->model('Index_que_model');
        if($result = CI_Redis::get_instance()->redis->get('questionInfos')){
            return json_decode($result,true);
        }else{
            $result = Index_que_model::get_instance()->get_question();
            $data = array();
            foreach ($result as $key=>&$val){
                $val['totalType'] = 'question';
                $val['content'] = preg_replace("/<img.*?>/si","",$val['content']);
                $val['totalUnqid'] = 'question'.$val['unqid'];
                $data[$val['unqid']] = $val;
            }
            CI_Redis::get_instance()->redis->setex('questionInfos',3600,json_encode($data));
            return $result;
        }
    }
    //用户注册
    public function register(){
        CI_Index::get_instance()->valid_index_login();
        $post = $this->input->post();
        if(!empty($params)){
            $post = $params;
        }
        if(!empty($post)){
            //验证码验证
            if($phone = CI_Redis::get_instance()->redis->get($post['phone'])){
                if($post['verification']!=$phone){
                    $error = array("status"=>"false","message"=>"验证码错误！");
                    echo json_encode($error);
                }else{
                    CI_Redis::get_instance()->redis->del($post['phone']);
                }
            }else{
                $error = array("status"=>"false","message"=>"没有发送验证码或验证码生成失败！");
                echo json_encode($error);
            }
            //注册用户
            $params = array(
                "tel"=>$post['phone'],
                "pwd"=>$post['pwd'],
                "createTime"=>time(),
                "realname"=>"未命名".time(),
                "openId"=>(isset($_SESSION['openId'])?$_SESSION['openId']:'')
            );
            if(Index_model::get_instance()->inset_userinfos($params)){
                //用户登录
                if($user = Index_model::get_instance()->get_userInfo_where(array(
                    "tel"=>$post['phone'],
                    "pwd"=>md5(md5($post['pwd']))
                ))){
                    $_SESSION["indexUserData"]=$user[0];
                    $_SESSION["indexUserData"]["tel"] = substr_replace($_SESSION["indexUserData"]["tel"],'****',3,4);
                }
                $success = array("status"=>"success","message"=>"用户新建成功！");
                echo json_encode($success);
            }else{
                $error = array("status"=>"false","message"=>"用户名已存在或系统繁忙！");
                echo json_encode($error);
            }
        }else{
            $result = array();
            $this->load->view('index/register', $result);
        }
    }
    //用户注册发送短信
    public function sendMessage(){
        $post = $this->input->post();
        if(!empty($post['phone'])){
            //数据库检测=>后期走缓存
            $where = array(
                "tel"=>$post['phone']
            );
            $user = Index_model::get_instance()->get_userInfo_where($where);
            if(!empty($user)){
                $success = array("Message"=>"手机号已经被注册！","msg"=>"","Code"=>"false");
                echo json_encode($success);
                die();
            }else{
                $this->load->library('CI_SMSG');
                $num = CI_SMSG::get_instance()->randMum(4);
                $data = array(
                    'code'=>$num,
                    'phone'=>$post['phone'],
                    'TemplateCode'=>'SMS_173472107'
                );
                $response = CI_SMSG::get_instance()->sendCode($data);
                if($response->Message=="OK"&&$response->Code=="OK"){
                    CI_Redis::get_instance()->redis->setex($post['phone'],300,$num);
                    echo json_encode($response);
                }else{
                    echo json_encode($response);
                }
            }
        }else{
            echo 0;
        }
    }
    //用户登录发送短信
    public function sendLoginMessage(){
        $post = $this->input->post();
        if(!empty($post['phone'])){
            //数据库检测=>后期走缓存
//            $where = array(
//                "tel"=>$post['phone']
//            );
//            $user = Index_model::get_instance()->get_userInfo_where($where);
//            if(empty($user)){
//                $success = array("Message"=>"用户不存在！","msg"=>"","Code"=>"false");
//                echo json_encode($success);
//                die();
//            }else{
                $this->load->library('CI_SMSG');
                $num = CI_SMSG::get_instance()->randMum(4);
                $data = array(
                    'code'=>$num,
                    'phone'=>$post['phone'],
                    'TemplateCode'=>'SMS_173472107'
                );
                $response = CI_SMSG::get_instance()->sendCode($data);
                if($response->Message=="OK"&&$response->Code=="OK"){
                    CI_Redis::get_instance()->redis->setex($post['phone'],300,$num);
                    echo json_encode($response);
                }else{
                    echo json_encode($response);
                }
//            }
        }else{
            echo 0;
        }
    }
    //用户找回密码发送短信
    public function sendResetPwdMessage(){
        $post = $this->input->post();
        if(!empty($post['phone'])){
            //数据库检测=>后期走缓存
            $where = array(
                "tel"=>$post['phone']
            );
            $user = Index_model::get_instance()->get_userInfo_where($where);
            if(empty($user)){
                $success = array("Message"=>"用户不存在！","msg"=>"","Code"=>"false");
                echo json_encode($success);
                die();
            }else{
                $this->load->library('CI_SMSG');
                $num = CI_SMSG::get_instance()->randMum(4);
                $data = array(
                    'code'=>$num,
                    'phone'=>$post['phone'],
                    'TemplateCode'=>'SMS_173472107'
                );
                $response = CI_SMSG::get_instance()->sendCode($data);
                if($response->Message=="OK"&&$response->Code=="OK"){
                    CI_Redis::get_instance()->redis->setex($post['phone'],300,$num);
                    echo json_encode($response);
                }else{
                    echo json_encode($response);
                }
            }
        }else{
            echo 0;
        }
    }
    //登录
    public function login(){
        CI_Index::get_instance()->valid_index_login();
        //验证
        $post = $this->input->post();
        if(!empty($post)){
            if($post["loginType"]>0){
                //验证码验证
                if($phone = CI_Redis::get_instance()->redis->get($post['tel'])){
                    if($post['verification']!=$phone){
                        $error = array("status"=>"false","message"=>"验证码错误！");
                        echo json_encode($error);
                    }else{
                        //短信登录
                        if($user = Index_model::get_instance()->get_userInfo_where(array(
                            "tel"=>$post['tel']
                        ))){
                            $_SESSION["indexUserData"]=$user[0];
                            $_SESSION["indexUserData"]["tel"] = substr_replace($_SESSION["indexUserData"]["tel"],'****',3,4);
                            if(empty($_SESSION["indexUserData"]["openId"])&&!empty($_SESSION['openId'])){
                                $this->load->model('Index_user_model');
                                Index_user_model::get_instance()->update_userInfo(array(
                                    "openId"=>$_SESSION['openId']
                                ));
                            }
                            $success = array("status"=>"success","message"=>"登录成功！");
                            echo json_encode($success);
                        }else{
                            //注册用户
                            if(Index_model::get_instance()->inset_userinfos(array(
                                "tel"=>$post['tel'],
                                "pwd"=>'123456',
                                "createTime"=>time(),
                                "realname"=>"未命名".time(),
                                "openId"=>(isset($_SESSION['openId'])?$_SESSION['openId']:'')
                            ))){
                                //用户登录
                                if($user = Index_model::get_instance()->get_userInfo_where(array(
                                    "tel"=>$post['tel'],
                                    "pwd"=>md5(md5('123456'))
                                ))){
                                    $_SESSION["indexUserData"]=$user[0];
                                    $_SESSION["indexUserData"]["tel"] = substr_replace($_SESSION["indexUserData"]["tel"],'****',3,4);
                                }
                                $success = array("status"=>"success","message"=>"登录成功！");
                                echo json_encode($success);
                            }else{
                                $error = array("status"=>"false","message"=>"系统繁忙！");
                                echo json_encode($error);
                            }
                        }
                    }
                }else{
                    $error = array("status"=>"false","message"=>"没有发送验证码或验证码生成失败！");
                    echo json_encode($error);
                }
            }else{
                //密码登录
                if($user = Index_model::get_instance()->get_userInfo_where(array(
                    "tel"=>$post['tel'],
                    "pwd"=>md5(md5($post['pwd']))
                ))){
                    $_SESSION["indexUserData"]=$user[0];
                    $_SESSION["indexUserData"]["tel"] = substr_replace($_SESSION["indexUserData"]["tel"],'****',3,4);
                    if(empty($_SESSION["indexUserData"]["openId"])&&!empty($_SESSION['openId'])){
                        $this->load->model('Index_user_model');
                        Index_user_model::get_instance()->update_userInfo(array(
                            "openId"=>$_SESSION['openId']
                        ));
                    }
                    //验证码验证
                    $success = array("status"=>"success","message"=>"登录成功！");
                    echo json_encode($success);
                }else{
                    $error = array("status"=>"false","message"=>"手机号不正确或则密码错误！");
                    echo json_encode($error);
                }
            }
        }else{
            $result = array();
            $this->load->view('index/login', $result);
        }
    }
    //退出登录
    //清除微信openId
    public function logoutUser(){
        //注册用户
        $this->load->model('Index_user_model');
        if(Index_user_model::get_instance()->update_userInfo(array(
            "openId"=>''
        ))){
            // 初始化会话。
            // 如果要使用会话，别忘了现在就调用：
            @session_start();
            // 重置会话中的所有变量
            $_SESSION = array();
            // 如果要清理的更彻底，那么同时删除会话 cookie
            // 注意：这样不但销毁了会话中的数据，还同时销毁了会话本身
            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(),"", -1,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }
            // 最后，销毁会话
            @session_destroy();
        }

    }
    //找回密码
    public function resetPassword(){
        //验证
        $post = $this->input->post();
        if(!empty($post)){
            //验证码验证
            if($phone = CI_Redis::get_instance()->redis->get($post['phone'])){
                if($post['verification']!=$phone){
                    $error = array("status"=>"false","message"=>"验证码错误！");
                    echo json_encode($error);
                }else{
                    CI_Redis::get_instance()->redis->del($post['phone']);
                }
            }else{
                $error = array("status"=>"false","message"=>"没有发送验证码或验证码生成失败！");
                echo json_encode($error);
            }
            //注册用户
            $params = array(
                "tel"=>$post['phone'],
                "pwd"=>md5(md5($post['pwd']))
            );
            if(Index_model::get_instance()->update_userInfo($params)){
                //用户登录
                if($user = Index_model::get_instance()->get_userInfo_where($params)){
                    $_SESSION["indexUserData"]=$user[0];
                    $_SESSION["indexUserData"]["tel"] = substr_replace($_SESSION["indexUserData"]["tel"],'****',3,4);
                }
                $success = array("status"=>"success","message"=>"密码修改成功！");
                echo json_encode($success);
            }else{
                $error = array("status"=>"false","message"=>"系统繁忙！");
                echo json_encode($error);
            }
        }else{
            $result = array();
            $this->load->view('index/resetPassword', $result);
        }
    }
    //微信调用接口
    public function getcode(){

        if(!empty($_SESSION['openId'])){
	    header("location:".ROOT_INDEX_REQUEST."/index/indexDefault/weixinValid");
        }else{
            //基本配置
            if(!empty($_GET)){
                //这里获取到了code
                $code   = $_GET['code'];
                $this->load->library('Curl_request');
                //第一步:取得openid
                $oauth2Url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".AppId."&secret=".AppSecret."&code=".$code."&grant_type=authorization_code";
                $oauth2 = Curl_request::get_instance()->curl($oauth2Url);
                //微信登录
                $oauth2 = json_decode($oauth2,true);
                $_SESSION['openId'] = $oauth2['openid'];
                if($user = Index_model::get_instance()->get_userInfo_where(array(
                    "openId"=>$_SESSION['openId']
                ))){
                    $_SESSION["indexUserData"]=$user[0];
                    $_SESSION["indexUserData"]["tel"] = substr_replace($_SESSION["indexUserData"]["tel"],'****',3,4);
                }
		        header("location:".ROOT_INDEX_REQUEST."/index/indexDefault/weixinValid");
            }else{
                $redirect_uri=urlencode(ROOT_INDEX_REQUEST."/index/getcode");
                $url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".AppId."&redirect_uri=".$redirect_uri."&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
                
		        header("location:".$url);
            }
        }
    }
   /* public function weixinUser(){
        $appid  = AppId;
        $secret = AppSecret;
        //这里获取到了code
        $code   = $_GET['code'];
        var_dump($_GET);
//	die();
	$this->load->library('Curl_request');
        //第一步:取得openid
        $oauth2Url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$secret."&code=".$code."&grant_type=authorization_code";
        $oauth2 = Curl_request::get_instance()->curl($oauth2Url);
	var_dump($oauth2);
	die();
//        $oauth2 = $this->http_curl($oauth2Url);
        //accestoken
        $access_token = $oauth2["access_token"];
        //openid
        $openid = $oauth2['openid'];
        //第二步:根据全局access_token和openid查询用户信息
        $get_user_info_url = "https://api.weixin.qq.com/sns/userinfo?access_token=".$access_token."&openid=".$openid."&lang=zh_CN";
        $userinfo = $this->http_curl($get_user_info_url);
        dump($userinfo);
        //打印用户信息
    }*/
}
