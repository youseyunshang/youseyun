<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class IndexQuestion extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Index_que_model');
    }
    public function uploadPic(){
        $post = $this->input->post();
        $this->load->library('CI_Picture');
        echo CI_Picture::get_instance()->compressImg($post['image']);
    }
    //添加问题
    public function addQuestion(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            $post = json_decode($request_body,true);
            $data = array(
                "title"=>$post['title'],
                "content"=>$post['content'],
                "createTime"=>time(),
                "status"=>1,
                "createUserId"=>$_SESSION["indexUserData"]["Id"],
                "unqid"=>time().'que',
                "answermun"=>0,
                "attrId"=>json_encode($post['attrId']),
                "light"=>1
            );
            //存在缓存先写缓存内
//            if($result = $this->cache->redis->get('questionInfos')){
//                $cacheData = json_decode($result,true);
//                //缓存类型为添加，后期需要此字段更新数据
//                $data['cacheType'] = 'add';
//                $cacheData[$data['unqid']] = $data;
//                echo $this->cache->redis->save('questionInfos',json_encode($cacheData));
//            }else{
                echo Index_que_model::get_instance()->add_question($data);
//            }
        }else{
            //标签库数据
            $this->load->model('Index_attr_model');
            $info = Index_attr_model::get_instance()->get_info_attr();
            $sign = Index_attr_model::get_instance()->get_sign_attr();
            $attr = array_merge($info,$sign);
            $result = array("attr"=>json_encode($attr));
            $this->load->view('question/addQuestion', $result);
        }
    }
    public function addQuestionData(){
        //标签库数据
        $this->load->model('Index_attr_model');
        $info = Index_attr_model::get_instance()->get_info_attr();
        $sign = Index_attr_model::get_instance()->get_sign_attr();
        $attr = array_merge($info,$sign);
        echo json_encode($attr);
    }
    //添加回答
    public function addAnswer($params=''){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            $post = json_decode($request_body,true);
            $preg = '/<img.*?src=[\"|\']?(.*?)[\"|\']?\s.*?>/i';
            preg_match_all($preg, $post['content'], $imgArr);
            $photo=array(
                "coverLogoOne"=>'',
                "coverLogoTwo"=>'',
                "coverLogoThree"=>''
            );
            if($count = count($imgArr[1])){
                if($count>2){
                    unset($val);
                    foreach ($imgArr[1] as $key=>&$val){
                        if($key>2){
                            break;
                        }
                        switch ($key){
                            case 0:
                                $photo['coverLogoOne'] = $val;
                                break;
                            case 1:
                                $photo['coverLogoTwo'] = $val;
                                break;
                            case 2:
                                $photo['coverLogoThree'] = $val;
                                break;
                        }
                    }
                }else{
                    $photo['coverLogoOne'] = $imgArr[1][0];
                }
            }
            $data = array(
                "qid"=>$post['unqid'],
                "content"=>$post['content'],
                "createTime"=>time(),
                "status"=>1,
                "createUserId"=>$_SESSION['indexUserData']['Id'],
                "photo"=>json_encode($photo),
                "createUserInfos"=>json_encode($_SESSION['indexUserData'])
            );
            echo Index_que_model::get_instance()->add_answer($data);
        }else{
            //把缓存内数据写入数据库
            $result = Index_que_model::get_instance()->get_question(array('unqid'=>$params));
            $data = array(
                "question"=>$result[0]
            );
            $this->load->view('question/addAnswer', $data);
        }
    }
    //删除问题
    public function delQuestion(){
        $post = $this->input->post();
        echo Index_que_model::get_instance()->del_question($post['Id']);
    }
    //问题列表
    public function questionLists(){
        $post = $this->input->post();
        if(!empty($post)){
            //首页资源数据
            $data = self::questionCachesList();
            //过滤已经加载数据
            if(!empty($post['filterArr'])){
                unset($val);
                foreach ($data as $key=>&$val){
                    if(isset($post['filterArr'][$val['unqid']])){
                        unset($data[$key]);
                    }
                }
            }
//            shuffle($data);
            $data = array_slice($data,0,5);
//            redis缓存方式
//            if(!empty($_SESSION["indexUserData"])&&$collection = CI_Redis::get_instance()->redis->get('collection_question_'.$_SESSION["indexUserData"]['Id'])){
//                $save = json_decode($collection,true);
//                unset($val);
//                foreach ($data as $key=>&$val){
//                    if(isset($save[$val['unqid']])){
//                        $data[$key]['save']=1;
//                    }else{
//                        $data[$key]['save']=0;
//                    }
//                };
//            }
//            数据库查询方式
            if(!empty($_SESSION["indexUserData"])){
                //用户收藏项
                $save = array();
                //问题收藏项
                $queItem = array();
                $allData = Index_que_model::get_instance()->get_collection_question();
                unset($val);
                foreach ($allData as $key=>&$val){
                    //用户是否收藏
                    if($val['userId']==$_SESSION["indexUserData"]['Id']){
                        $save[$val['qId']] = $val;
                    }
                    //问题数量项
                    $queItem[$val['qId']][] = $val;
                }
                unset($val);
                foreach ($data as $key=>&$val){
                    $data[$key]['save'] = isset($save[$val['unqid']])?1:0;
                    $data[$key]['savenum'] = isset($queItem[$val['unqid']])?count($queItem[$val['unqid']]):0;
                };
            }
            if(!empty($data)){
                echo json_encode($data);
            }else{
                echo 0;
            }
        }else{
            $result = array();
            $this->load->view('question/questionList', $result);
        }
    }
    //缓存永不过期除非手动删除 缓存中答案的更新时间为点开问题以后，提问更新时间为添加提问时
    public function questionCachesList(){
//        if($result = CI_Redis::get_instance()->redis->get('questionInfos')){
//            return json_decode($result,true);
//        }else{
            $result = Index_que_model::get_instance()->get_question();
            $data = array();
            foreach ($result as $key=>&$val){
                $val['totalType'] = 'question';
                $val['totalUnqid'] = 'question'.$val['unqid'];
                $val['content'] = preg_replace("/<img.*?>/si","",$val['content']);
                $data[$val['unqid']] = $val;
            }
            return $data;
//            CI_Redis::get_instance()->redis->setex('questionInfos',86400,json_encode($data));
//            return $result;
//        }
    }
    //问题详情页面
    public function questionInfo($params=0){
        //更新数据缓存
        $updown = self::updateAnswerUpDown();
        if($params>0){
            $question = Index_que_model::get_instance()->get_question(array('unqid'=>$params));
            $answer = Index_que_model::get_instance()->get_answers(array('qid'=>$params));
            //本用户收藏的问题
            $saveQuery = array();
            //本用户点赞的答案
            $upAnswer = array();
            if(isset($_SESSION["indexUserData"])){
                  unset($val);
                  foreach ($answer as $key=>&$val){
                      if(isset($updown[$val['Id']])){
                          $val['up'] = count($updown[$val['Id']]['userIds']);
                      }
                      if(isset($updown[$val['Id']]['userIds'][$_SESSION["indexUserData"]['Id']])){
                          $upAnswer[$val['Id']] = "1";
                      }
                  }
            }
            //最优回答
            if(!empty($answer)){
                $firstanswer = $answer[0];
            }else{
                $firstanswer = Array();
            }
            if(!empty($question)){
                //清除掉最优回答就是其余回答
                unset($answer[0]);
                $result = array(
                    'question'=>$question,
                    'answer'=>$answer,
                    'firstanswer'=>$firstanswer,
                    'save'=>empty($saveQuery)?false:true,
                    'upAnswer'=>$upAnswer
                );
                $this->load->view('question/questionInfo', $result);
            }else{
                echo "question Delete！";
                die();
            }

        }else{
            echo "Page Error！";
            die();
        }
    }
    //updateAnswerUpDown
    /*array(33){
    [7] =>
    array(1)
    {
    ["userIds"] =>
    array(0)
    {
    }
    }
    }*/
    public function updateAnswerUpDown(){
        if($result = CI_Redis::get_instance()->redis->get('answerUpDown')){
            return json_decode($result,true);
        }else{
            $answer = Index_que_model::get_instance()->get_answers();
            $updownData = Index_que_model::get_instance()->get_up_answer();
            $result = array();
            if(!empty($answer)){
                unset($val);
                foreach ($answer as $key=>&$val){
                    $result[$val['Id']] = array(
                        "userIds"=>array()
                    );
                }
                if(!empty($updownData)){
                    unset($val);
                    foreach ($updownData as $key=>&$val){
                        if(isset($result[$val['aId']])){
                            $result[$val['aId']]['userIds'][$val['userId']] = $val['userId'];
                        }
                    }
                }
            }
            CI_Redis::get_instance()->redis->setex('answerUpDown',86400,json_encode($result));
            return $result;
        }
    }
    //收藏问题
    public function collection(){
        if(empty($_SESSION['indexUserData'])){
            self::outMessage("404","当前使用者并没有登录，请登录！",array("response"=>"login"));
        }
        $post = $this->input->post();
//        if($collect = Index_que_model::get_instance()->collection_question(array("userId"=>$_SESSION["indexUserData"]["Id"],"qId"=>$post['unqId']))&&$num = Index_que_model::get_instance()->update_question_aboutnum(array("collectionNum"=>$post['num']+1),$post['unqId'])){
        if($collect = Index_que_model::get_instance()->collection_question(array("userId"=>$_SESSION["indexUserData"]["Id"],"qId"=>$post['unqId']))){
            self::outMessage("200","收藏成功！",array());
        }else{
            self::outMessage("404","收藏失败！",array());
        }
    }
    //点亮问题
    public function light(){
        $post = $this->input->post();
        $params = array(
            "light"=>$post['light']+1
        );
        if($light = Index_que_model::get_instance()->update_question_aboutnum($params,$post['unqId'])){
            self::outMessage("200","收藏成功！",array());
        }
    }
    //答案点赞
    public function upAnswer(){
        if(empty($_SESSION['indexUserData'])){
            self::outMessage("200","当前使用者并没有登录，请登录！",array("response"=>"login"));
        }else{
            $post = $this->input->post();
            if(!empty($post)){
                $result = self::updateAnswerUpDown();
                switch($post['type']){
                    case "up":
                        if(!isset($result[$post['aId']]['userIds'][$_SESSION["indexUserData"]["Id"]])){
                            $result[$post['aId']]['userIds'][$_SESSION["indexUserData"]["Id"]] = $_SESSION["indexUserData"]["Id"];
                        }
                        if($response = CI_Redis::get_instance()->redis->setex('answerUpDown',86400,json_encode($result))){
                            self::outMessage("200","点赞成功！",array("response"=>"1","count" =>count($result[$post['aId']]['userIds']) ));
                        }else{
                            self::outMessage("404","点赞过程失败，请检查网络！",array("response"=>"0"));
                        }
                        break;
                    case "down":
                        if(isset($result[$post['aId']]['userIds'][$_SESSION["indexUserData"]["Id"]])){
                            unset($result[$post['aId']]['userIds'][$_SESSION["indexUserData"]["Id"]]);
                        }
                        if($response = CI_Redis::get_instance()->redis->setex('answerUpDown',86400,json_encode($result))){
                            self::outMessage("200","取消点赞！",array("response"=>"1","count" =>count($result[$post['aId']]['userIds'])));
                        }else{
                            self::outMessage("404","点赞过程失败，请检查网络！",array("response"=>"0"));
                        }
                        break;
                    default:
                        self::outMessage("404","点赞过程失败，请检查网络！",array("response"=>"0"));
                        break;
                }
            }
        }
    }
    //将redis内的点赞的数据同步到数据库
    //更新点赞数最高的答案在问题中
    public function syncAnswerUpDown(){
        $updown = self::updateAnswerUpDown();
        if(!empty($updown)){
            //同步赞数据
            // 1.删除表数据内容
            Index_que_model::get_instance()->del_all_updown_answer();
            //2.批量插入数据
            $data = array();
            unset($val);
            foreach ($updown as $key=>&$val){
                if(!empty($val['userIds'])){
                    foreach ($val['userIds'] as $k=>$v){
                        $data[] = array(
                            "userId"=>$v,
                            "aId"=>$key
                        );
                    }
                }
                //更新点赞数据内容
                $params = array(
                    "up"=>count($val['userIds'])
                );
                Index_que_model::get_instance()->update_answer($key,$params);

            }
            Index_que_model::get_instance()->up_answer_branch($data);
        }
    }
}
