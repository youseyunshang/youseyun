<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class IndexUsercenter extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Index_user_model');
    }
    /**
     * index
     *
     * 个人中心
     *
     * @param  $params=array()
     * @return	void
     * @anthor Json.Wang
     */
    public function index(){
        $data = isset($_SESSION['indexUserData'])?$_SESSION["indexUserData"]:array();
        $result = array(
            "userinfo"=>json_encode($data)
        );
        $this->load->view('usercenter/index', $result);
    }
    /**
     * personalInfo
     *
     * 获取个人基本信息
     *
     * @param  $params=array()
     * @return	void
     * @anthor Json.Wang
     */
    public function personalInfo(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $result = array();
        $this->load->view('usercenter/personalInfo', $result);
    }
    /**
     * companyValid
     *
     * 公司验证
     *
     * @param  $params=array()
     * @return	void
     * @anthor Json.Wang
     */
    public function companyValid(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $result = array();
        $this->load->view('usercenter/companyValid', $result);
    }
    //初始化数据
    public function ajaxPersonalInfo(){
        echo json_encode($_SESSION["indexUserData"]);
    }
    //修改数据
    public function updateUserInfo(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            $post = json_decode($request_body,true);
            $data = array(
                'realname'=>$post['realname'],
                'gender'=>$post['sex']
            );
            if(substr($post['avatar'],0,4)=="data"){
                $array = array(
                    "pathUrl"=>"/resource/uploads/userlogo",
                    "column"=>&$post['avatar']
                );
                CI_Index::get_instance()->product_picture_upload($array);
                $data['userLogo'] = $post['avatar'];
                $pic = base_url($_SESSION['indexUserData']['userLogo']);
                if(file_exists($pic)&&unlink($pic)){
                    $_SESSION['indexUserData']['userLogo'] = $post['avatar'];
                }else{
                    $_SESSION['indexUserData']['userLogo'] = $post['avatar'];
                }
            }
            $_SESSION['indexUserData']['realname'] = $post['realname'];
            $_SESSION['indexUserData']['gender'] = $post['sex'];
            echo Index_user_model::get_instance()->update_userInfo($data);
        }
    }
    //修改数据
    public function updateComInfo(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            $post = json_decode($request_body,true);
            $data = array(
                'comName'=>$post['comName'],
                'comContent'=>$post['comContent'],
                'comPerson'=>$post['comPerson'],
                'comTel'=>$post['comTel'],
                'comType'=>$post['comType'],
                'comLocation'=>$post['comLocation']
            );
            if(substr($post['comLicense'],0,4)=="data"){
                $array = array(
                    "pathUrl"=>"/resource/uploads/binLicense",
                    "column"=>&$post['comLicense']
                );
                CI_Index::get_instance()->product_picture_upload($array);
                $data['comLicense'] = $post['comLicense'];
                $_SESSION['indexUserData']['comLicense'] = $post['comLicense'];
            }
            $_SESSION['indexUserData']['comName'] = $post['comName'];
            $_SESSION['indexUserData']['comContent'] = $post['comContent'];
            $_SESSION['indexUserData']['comPerson'] = $post['comPerson'];
            $_SESSION['indexUserData']['comTel'] = $post['comTel'];
            $_SESSION['indexUserData']['comLocation'] = $post['comLocation'];
            $_SESSION['indexUserData']['comType'] = $post['comType'];
            echo Index_user_model::get_instance()->update_userInfo($data);
        }
    }
    //我的收藏
    public function myCollection(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $result = array();
        $this->load->view('usercenter/myCollection', $result);
    }
    //我的问答
    public function myQAndA(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $result = array();
        $this->load->view('usercenter/myQAndA', $result);
    }
    //我的问答
    public function myQAndAData(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $this->load->model('Index_que_model');
        $params = array(
            "createUserId"=>$_SESSION["indexUserData"]["Id"]
        );
        $question = Index_que_model::get_instance()->get_question($params);
        $qResult = array();
        if(!empty($question)){
            unset($val);
            foreach ($question as $key=>&$val){
                $qResult[] =array(
                    'Id'=>$val['Id'],
                    'title'=>$val['title'],
                    'answermun'=>$val['answermun']
                );
            }
        }
        $params = array(
            "cbc_answer.createUserId"=>$_SESSION["indexUserData"]["Id"]
        );
        $answer = Index_que_model::get_instance()->get_answers_question($params);
        $result = array("qresult"=>$qResult,"answer"=>$answer);
        echo json_encode($result);
    }
    //个人中心中的我的供求列表
    public function myProduct(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $data = self::myProductData();
        $supple = array();
        $purchase = array();
        unset($val);
        foreach ($data as $key=>&$val){
            //默认不显示
            $val["showId"] = false;
            if($val['itemType']=="supple"){
                $supple[] = $val;
            }else{
                $purchase[] = $val;
            }
        }
        $result = array(
            "supple"=>json_encode($supple),
            "purchase"=>json_encode($purchase)
        );
        $this->load->view('usercenter/myProduct', $result);
    }
    /**
     * myProductData
     *
     * 用户自己发布的供求信息
     * @location 1.个人中心-我的供求
     * @param  null
     * @return	用户自己发布的供求信息
     * @anthor Json.Wang
     */
    public function myProductData(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        if($redisData = CI_Redis::get_instance()->redis->get('shopDefaultData')){
            $data = json_decode($redisData,true);
        }else{
            $this->load->model('Supple_model');
            $this->load->model('Purchase_model');
            //供应通过的数据
            $Supple = Supple_model::get_instance()->get_order_where();
            unset($val);
            foreach($Supple as $key=>&$val){
                $val['itemType'] = "supple";
            }
            //采购通过的数据
            $Purchase = Purchase_model::get_instance()->get_order_where();
            unset($val);
            foreach($Purchase as $key=>&$val){
                $val['itemType'] = "purchase";
            }
            $data = array_merge($Supple,$Purchase);
            $last_names = array_column($data,'createTime');
            array_multisort($last_names,SORT_DESC,$data);
            CI_Redis::get_instance()->redis->setex('shopDefaultData',3600,json_encode($data));
        }
        unset($val);
        foreach ($data as $key=>&$val){
            if($val['userId'] == $_SESSION["indexUserData"]["Id"]){
                unset($data[$key]['jsonData']);
                unset($data[$key]['attrAll']);
                unset($data[$key]['qualityInspectionSheet']);
                unset($data[$key]['warehouseReceipt']);
                unset($data[$key]['tel']);
            }else{
                unset($data[$key]);
            }
        }
        return $data;
    }
    //加载资源数据
    public function loadingData(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        //资讯
//        $whereIn = array();
//        if($myResSql = CI_Redis::get_instance()->redis->get('collection_res_'.$_SESSION["indexUserData"]["Id"])){
//            $myResData = json_decode($myResSql,true);
//        }else{
//           $where = array(
//               "userId"=>$_SESSION["indexUserData"]["Id"]
//           );
//            Index_user_model::get_instance()->getResWhereIn($where);
//        }
//        foreach ($myResData as $k=>&$v){
//            $whereIn[] = $v['resId'];
//        }
//        $myResData = Index_user_model::get_instance()->getResWhereIn($whereIn);
        //问答
//        $whereIn = array();
//        if($myQuestionSql = CI_Redis::get_instance()->redis->get('collection_question_'.$_SESSION["indexUserData"]["Id"])){
//            $myQuestionData = json_decode($myQuestionSql,true);
//            foreach ($myQuestionData as $k=>&$v){
//                $whereIn[] = $v['qId'];
//            }
//            $qResult = Index_user_model::get_instance()->getQuestionWhereIn($whereIn);
//        }else{
//
//        }
        $myResData = Index_user_model::get_instance()->getResSessionId();
        $qResult = Index_user_model::get_instance()->getQuestionSessionId();
        unset($val);
        foreach ($qResult as $key=>&$val){
            $val['content'] = strip_tags($val['content']);
        }
        $result = array("myRes"=>$myResData,"myquestion"=>$qResult);
        echo json_encode($result);
    }
    //喜好标签
    public function myHobby(){
        //验证登录数据
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $this->load->model('Index_attr_model');
        $hobby = Index_user_model::get_instance()->get_hobby();
        $attrId = array();
        if(!empty($hobby)){
            unset($val);
            foreach ($hobby as $key=>&$val){
                $attrId[$val['attrId']] = "1";
            }
        }
        $result=array("allAttr"=>Index_attr_model::get_instance()->get_all_attr(),"hobby"=>$attrId);
        $this->load->view('usercenter/myHobby', $result);
    }
    //喜好标签
    public function addMyHobby(){
        //验证登录数据
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $post = $this->input->post();
        $params = array(
            'userId' =>$_SESSION["indexUserData"]["Id"],
            'attrId' =>$post['attrId']
            );
        echo Index_user_model::get_instance()->add_hobby($params);
    }
    //用户设置
    public function accountSet(){
        //验证登录数据
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $result=array();
        $this->load->view('usercenter/accountSet', $result);
    }
	public function myOrder(){
		$result = array();
		$this->load->view('order/myOrder', $result);
	}
}
