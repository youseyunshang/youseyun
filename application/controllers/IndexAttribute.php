<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class IndexAttribute extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('Index_attr_model');
    }
    //信息一级金属属性
    public function getInfoAttrFirst(){
        $result = Index_attr_model::get_instance()->get_info_attr_first();
        var_dump("<pre>",$result);
    }
    //商城一级金属属性
    public function getShopAttrFirst(){
        $result = Index_attr_model::get_instance()->get_shop_attr_first();
        var_dump("<pre>",$result);
    }
    //信息金属属性
    public function getInfoAttr(){
        $result = Index_attr_model::get_instance()->get_info_attr();
        var_dump("<pre>",$result);
    }
    //商城金属属性
    public function getShopAttr(){
        $result = Index_attr_model::get_instance()->get_shop_attr();
        var_dump("<pre>",$result);
    }
}
