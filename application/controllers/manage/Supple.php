<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Supple extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Supple_model');
    }
    /**
     * suppleList
     *
     * 供应列表
     *
     * @param  $params=array()
     * @return	供应列表，检索，加载
     * @anthor Json.Wang
     */
    public function suppleList(){
        $post = $this->input->post();
        $result = Supple_model::get_instance()->get_order_where();
        if(!empty($post)){
            unset($val);
            foreach($result as $key=>&$val){
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['title'])){
                    unset($result[$key]);
                    continue;
                }
                if(!empty($post['status'])&&$post['status']!=$val['status']){
                    unset($result[$key]);
                    continue;
                }
                if(!empty($post['endTime'])&&$post['endTime']>1){
                    //查看下架内容排除上架的
                    $nowTime = time();
                    if($nowTime-$val['endTime']<0){
                        unset($result[$key]);
                        continue;
                    }
                }
                if(!empty($post['endTime'])&&$post['endTime']<2){
                    //查看上架内容排除下架的
                    $nowTime = time();
                    if($nowTime-$val['endTime']>0){
                        unset($result[$key]);
                        continue;
                    }
                }
                $result[$key]['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                $result[$key]['startTime'] = date("Y-m-d H:i:s",$val['startTime']);
                $result[$key]['endTime'] = date("Y-m-d H:i:s",$val['endTime']);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $data = array("count"=>count($result));
            $this->load->view('manage/supple/suppleList', $data);
        }
    }
    /**
     * showSupple
     *
     * 展示供应信息
     *
     * @param  $params=array()
     * @return	展示供应信息
     * @anthor Json.Wang
     */
    public function showSupple(){
        $get = $this->input->get();
        //查供应数据
        if($data = Supple_model::get_instance()->get_order_where(array("Id"=>$get['Id']))){
            self::formatShopData($data[0]);
            $result = array("supple"=>$data[0]);
            $this->load->view('manage/supple/showSupple', $result);
        }else{
            self::outMessage("404","数据库中暂无此数据，请检查接口！",array());
           die();
        }
    }
    /**
     * showSupple
     *
     * 删除供应信息
     *
     * @param  $params=array()
     * @return	删除供应信息
     * @anthor Json.Wang
     */
    public function delSupple(){
        $get = $this->input->get();
        echo Supple_model::get_instance()->del_order($get['Id']);
    }
    //供应信息状态
    /**
     * suppleStatus
     *
     * 修改供应信息状态
     *
     * @param  $params=array()
     * @return	修改供应信息状态
     * @anthor Json.Wang
     */
    public function suppleStatus(){
        $post = $this->input->post();
        if($supple = Supple_model::get_instance()->supple_status($post)){
            CI_Redis::get_instance()->redis->del("shopDefaultData");
            self::outMessage("200","操作成功！",array());
            exit();
        }
    }
    //格式化信息数据
    private function formatShopData(&$val = array()){
        $val['createTime'] = date("Y-m-d",$val['createTime']);
        $val['startTime'] = date("Y-m-d",$val['startTime']);
        $val['endTime'] = date("Y-m-d",$val['endTime']);
        //企业类型
        switch ($val['companyType']){
            case 1:
                $val['companyType'] = "生产商";
                break;
            case 2:
                $val['companyType'] = "贸易商";
                break;
            case 3:
                $val['companyType'] = "设备服务";
                break;
            case 4:
                $val['companyType'] = "钢厂";
                break;
            case 5:
                $val['companyType'] = "物流";
                break;
            default:
                $val['companyType'] = "生产商";
                break;
        }
        //发票
        switch($val['invoice']){
            case "1":
                $val['invoice'] = "无发票";
                break;
            case "2":
                $val['invoice'] = "增值税发票";
                break;
            case "3":
                $val['invoice'] = "普通发票";
                break;
            default:
                $val['invoice'] = "无发票";
        }
        //配送方式
        switch($val['distributionMode']){
            case "1":
                $val['distributionMode'] = "送到买家指定地点";
                break;
            case "2":
                $val['distributionMode'] = "上门自提";
                break;
            default:
                $val['distributionMode'] = "上门自提";
        }
        //公司
        $val['comName'] = "沈阳lejaer科技有限公司";
    }
}
