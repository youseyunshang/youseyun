<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Purchase extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Purchase_model');
    }
    //供应列表
    public function purchaseList(){
        $post = $this->input->post();
        if(!empty($post)){
            $result = Purchase_model::get_instance()->get_order_where();
            unset($val);
            foreach($result as $key=>&$val){
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['title'])){
                    unset($result[$key]);
                    continue;
                }
                if(!empty($post['status'])&&$post['status']!=$val['status']){
                    unset($result[$key]);
                    continue;
                }
                if(!empty($post['endTime'])&&$post['endTime']>1){
                    //查看下架内容排除上架的
                    $nowTime = time();
                    if($nowTime-$val['endTime']<0){
                        unset($result[$key]);
                        continue;
                    }
                }
                if(!empty($post['endTime'])&&$post['endTime']<2){
                    //查看上架内容排除下架的
                    $nowTime = time();
                    if($nowTime-$val['endTime']>0){
                        unset($result[$key]);
                        continue;
                    }
                }
                $result[$key]['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                $result[$key]['startTime'] = date("Y-m-d H:i:s",$val['startTime']);
                $result[$key]['endTime'] = date("Y-m-d H:i:s",$val['endTime']);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            unset($val);
            foreach ($response as $key=>&$val){
                $purchaseOrder = Purchase_model::get_instance()->get_all_purchaseorder($val['Id']);
                $response[$key]['purchaseOrderNum'] = count($purchaseOrder);
            }
            echo json_encode($response);
        }else{
            $result = Purchase_model::get_instance()->get_order();
            $data = array("count"=>count($result));
            $this->load->view('manage/purchase/purchaseList', $data);
        }
    }
    //供应列表
    public function showPurchase(){
        $get = $this->input->get();
        //查供应数据
        if($purchase = Purchase_model::get_instance()->get_order_where(array("Id"=>$get['Id']))){
            self::formatShopData($purchase[0]);
            if(!empty($purchase)){
                //查人员数据
                $person = Purchase_model::get_instance()->get_userinfos($purchase[0]['userId']);
            }else{
                $person = Array();
            }
            $data = array("purchase"=>$purchase[0],"person"=>$person);
            $this->load->view('manage/purchase/showPurchase', $data);
        }else{
            echo "数据库查询无果！";
            die();
        }
    }
    /**
     * purchaseStatus
     * @notice 采购信息的订单状态
     * @return	array
     * @anthor	Json.Wang
     */
    public function purchaseStatus(){
        $post = $this->input->post();
        if($purchase = Purchase_model::get_instance()->purchase_status($post)){
            CI_Redis::get_instance()->redis->del("shopDefaultData");
            self::outMessage("200","操作成功！",array());
            exit();
        }
    }
    /**
     * showPurchaseOrder
     * @notice 报价人信息
     * @return	array
     * @anthor	Json.Wang
     */
    public function showPurchaseOrder(){
        $get = $this->input->get();
        $post = $this->input->post();
        if(!empty($post)){
            $result = Purchase_model::get_instance()->get_all_purchaseorder($post['Id']);

            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            unset($val);
            foreach ($response as $key=>&$val){
                $response[$key]['endTime'] = date($val['endTime']);
            }
            echo json_encode($response);
        }else{
            $purchaseOrder = Purchase_model::get_instance()->get_all_purchaseorder($get['Id']);
            $data = array("count"=>count($purchaseOrder),"Id"=>$get['Id']);
            $this->load->view('manage/purchase/purchaseListOrder', $data);
        }

    }
    /**
     * showPurchaseOrder
     * @notice 是否同意报价人信息推送
     * @return	array
     * @anthor	Json.Wang
     */
    public function agreePurchaseOrder(){
        $post = $this->input->post();
        if(!empty($post)){
            if($result = Purchase_model::get_instance()->update_purchaseorder(array(
                "status"=>$post['status']>0?0:1
            ),$post['Id'])){
                self::outMessage("200","success",$result);
            }
        }else{
            self::outMessage("400","no post",array());
        }
    }
    //格式化信息数据
    private function formatShopData(&$val = array()){
        $val['createTime'] = date("Y-m-d",$val['createTime']);
        $val['startTime'] = date("Y-m-d",$val['startTime']);
        $val['endTime'] = date("Y-m-d",$val['endTime']);
        //企业类型
        switch ($val['companyType']){
            case 1:
                $val['companyType'] = "生产商";
                break;
            case 2:
                $val['companyType'] = "贸易商";
                break;
            case 3:
                $val['companyType'] = "设备服务";
                break;
            case 4:
                $val['companyType'] = "钢厂";
                break;
            case 5:
                $val['companyType'] = "物流";
                break;
            default:
                $val['companyType'] = "生产商";
                break;
        }
        //发票
        switch($val['invoice']){
            case "1":
                $val['invoice'] = "无发票";
                break;
            case "2":
                $val['invoice'] = "增值税发票";
                break;
            case "3":
                $val['invoice'] = "普通发票";
                break;
            default:
                $val['invoice'] = "无发票";
        }
        //配送方式
        switch($val['distributionMode']){
            case "1":
                $val['distributionMode'] = "送到买家指定地点";
                break;
            case "2":
                $val['distributionMode'] = "上门自提";
                break;
            default:
                $val['distributionMode'] = "上门自提";
        }
        //公司
        $val['comName'] = "沈阳lejaer科技有限公司";
    }
}
