<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        //CI_Controller::get_instance()->preValid();
        //self::preValid();
    }
    //登录首页
    public function login(){
        if( self::$allow>0 ){
        $this->load->view('manage/home/indexDefault', $_SESSION);
        }else{
            $data = array();
            $this->load->view('manage/login/login', $data);
        }
    }
    //验证登录
    public function validLogin(){
        //验证
        $post = $this->input->post();
        $data = Login_model::get_instance()->get_userinfos($post["username"]);
        if(empty($data)){
            echo 0;
        }else{
            if($data['pwd']==md5(md5($post['pwd']))){
                //管理员信息
                $this->session->set_userdata($data);
                echo 1;
                //登录限制
                //加权限
            }else{
                echo 2;
            }
        }

    }
}
