<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Order extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Order_model');
    }
    //供应列表
    public function orderList(){
        $post = $this->input->post();
        $result = Order_model::get_instance()->get_order();
        if(!empty($post)){
            unset($val);
            $startTime = !empty($post['startTime'])?strtotime(date($post['startTime'])):null;
            $endTime = !empty($post['endTime'])?strtotime(date($post['endTime'])):null;
            foreach($result as $key=>&$val){
                if(!empty($post['selectOrderNumber'])&&$val['orderNumber']!=$post['selectOrderNumber']){
                    unset($result[$key]);
                }
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['username'])){
                    unset($result[$key]);
                }
                //时间检测
                if(!empty($startTime)&&!($val['createTime']-$startTime>0)){
                    unset($result[$key]);
                    continue;
                }
                if(!empty($endTime)&&!($endTime-$val['createTime']>0)){
                    unset($result[$key]);
                    continue;
                }
                $val['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                $val['lastTime'] = date("Y-m-d H:i:s",$val['lastTime']);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $data = array("count"=>count($result));
            $this->load->view('manage/order/orderList', $data);
        }
    }
    //删除问题
    public function delOrder(){
        $get = $this->input->get();
        Supple_model::get_instance()->del_order($get['Id']);
    }
}
