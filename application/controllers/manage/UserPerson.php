<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class UserPerson extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->helper(array('form', 'url'));
    }
    //会员列表
    public function showRowsList(){
        $result = User_model::get_instance()->get_userinfos();
        $data = array("count"=>count($result));
        $this->load->view('manage/userPerson/userPersonList', $data);
    }
    /**
     * addUserPerson
     *
     * 后台添加用户
     *
     * @param  $params>0 添加 $params = 0 展示
     * @return	添加页面
     * @anthor Json.Wang
     */
    public function addUserPerson($params = 0){
        if(self::$allow>0){
            if( $params > 0 ){
                $post = $this->input->post();
                if(User_model::get_instance()->inset_userinfos($post)){
                   self::showRowsList();
                }
            }else{
                $userInfo = User_model::get_instance()->get_userinfos();
                $response = array();
                unset($val);
                foreach ($userInfo as $key=>$val){
                    $response[] = array(
                        "username"=>$val['username'],
                        "tel"=>$val['tel'],
                        "idCard"=>$val['idCard']
                    );
                }
                $result = array("userInfo"=>json_encode($response));
                $this->load->view('manage/userPerson/addUserPerson', $result);
            }
        }else{
            $data = array();
            $this->load->view('manage/login/login', $data);
        }
    }
    /**
     * UserPersonList
     *
     * 用户列表，用户检索
     *
     * @param  $params=array()
     * @return	json数据
     * @anthor Json.Wang
     */
    public function UserPersonList($params=array()){
        $post = $this->input->post();
        //此数据加载为缓存数据
        if(empty($post)){
            $this->load->model('Home_model');
            $count = Home_model::get_instance()->count_table("user");
            $data = array("count"=>$count);
            $this->load->view('manage/userPerson/userPersonList', $data);
        }else{//AJAX加载数据
                $result = User_model::get_instance()->get_userinfos();
                $selectName = !empty($post['selectName'])?$post['selectName']:null;
                $nearEndTime = !empty($post['nearEndTime'])?$post['nearEndTime']:null;
                $startTime = !empty($post['startTime'])?strtotime(date($post['startTime'])):null;
                $endTime = !empty($post['endTime'])?strtotime(date($post['endTime'])):null;
                unset($val);
                foreach($result as $key=>&$val){
                    //等级检测
                    if(!empty($post['grade'])&&$val['grade']!=$post['grade']){
                        unset($result[$key]);
                        continue;
                    }
                    //时间检测
                    if(!empty($startTime)&&!($val['createTime']-$startTime>0)){
                        unset($result[$key]);
                        continue;
                    }
                    if(!empty($endTime)&&!($endTime-$val['createTime']>0)){
                        unset($result[$key]);
                        continue;
                    }
                    //用户名,真实姓名,电话检测
                    if(!empty($selectName)){
                        if(preg_match("/".$selectName."/i", $val['realname'])||preg_match("/".$selectName."/i", $val['username'])||preg_match("/".$selectName."/i", $val['tel'])){
                        }else{
                            unset($result[$key]);
                            continue;
                        }
                    }
                    switch ($val['grade']){
                        case '1':
                            $result[$key]['grade'] = "普通会员";
                            $result[$key]['endTime'] = "--";
                            break;
                        case '2':
                            $result[$key]['grade'] = "高级会员";
                            $result[$key]['endTime'] = date("Y-m-d H:i:s",($result[$key]['createTime']+31536000));
                            break;
                    }
                    if(!empty($nearEndTime)&&(($result[$key]['createTime']+31536000-time()-2592000)>0)){
                        unset($result[$key]);
                        continue;
                    }
                    $result[$key]['createTime'] = date("Y-m-d H:i:s",$result[$key]['createTime']);
                }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }
    }
    //更新管理员
    //$type=0 查看 $type=1 更新
    public function updateUser($type=0){
        if($type>0){
            $post = $this->input->post();
            if(User_model::get_instance()->update_userinfos($post)){
               self::showRowsList();
            }
        }else{
            $get = $this->input->get();
            $userInfo = User_model::get_instance()->get_userinfos($get['Id']);
            $result = array();
            $result['userInfo'] = $userInfo;
            $this->load->view('manage/userPerson/updateUserPerson', $result);
        }
    }
    /**
     * UserPersonList
     *
     * 删除用户
     *
     * @param  $params=array()
     * @return	json数据
     * @anthor Json.Wang
     */
    public function delUser(){
        self::del_valid('/UserPerson/DelUser');
        $post = $this->input->post();
        echo User_model::get_instance()->del_userinfos($post['Id']);
    }
    /**
     * UserPersonList
     *
     * 用户列表，用户检索
     *
     * @param  $params=array()
     * @return	json数据
     * @anthor Json.Wang
     */
    public function userAllInfo(){
        $get = $this->input->get();
        $userInfo = User_model::get_instance()->get_userinfos($get['Id']);
        $result = array();
        $result['userInfo'] = $userInfo;
        $this->load->view('manage/userPerson/userAllInfo', $result);
    }
    //用户审核
    public function userStatus(){
        $post = $this->input->post();
        echo User_model::get_instance()->user_status($data=array("Id"=>$post['Id'],"status"=>$post['status'],"createTime"=>time()));
    }
    //批量导入用户
    public function addUserAll(){
        $config['upload_path']      = getcwd().'./resource/uploads/';
        $config['allowed_types']    = 'gif|jpg|png|xlsx|xls';
        $config['max_size']     = 2048;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
            $error = array('error' => $this->upload->display_errors());
            var_dump($error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            $this->load->library('CI_Excel');
            $result = CI_Excel::get_instance()->excel_in($data['upload_data']['full_path']);
            $sqlDataRow = array();
            //获取所有用户信息为做验证使用
            $allUser = array();
            $allUserSql = User_model::get_instance()->get_userinfos();
            unset($val);
            foreach ($allUserSql as $key=>&$val){
                $allUser[$val['tel']] = $val;
            }
            //错误数据
            $errorData = array();
            unset($val);
            foreach ($result as $key=>&$val){
                if($key>0){
                    //手机号码匹配
                    if(!preg_match('/^(1(([35789][0-9])|(47)))\d{8}$/',$val[0])){
                        $errorData[] = array(
                            'row'=>$key+1,
                            'question'=>"手机号码格式不正确！"
                        );
                        continue;
                    }
                    //手机号码存在不存在
                    if(isset($allUser[(string)$val[0]])){
                        $errorData[] = array(
                            'row'=>$key+1,
                            'question'=>"手机号码已存在！"
                        );
                        continue;
                    }
                    //如果没有错误的话会进行导入数据汇总
                    if(count($errorData)<1){
                        $sqlDataRow[] = array(
                            "username"=>$val[0],
                            "tel"=>$val[0],
                            "realname"=>$val[2],
                            "pwd"=>md5(md5('123456')),
                            "gender"=>$val[1]=="男"?'1':'0',
                            "grade"=>'1',
                            "createTime"=>time()
                        );
                    }
                }
            }
            if(!empty($errorData)){
                //存在错误
                $result = array();
                $result['errorInfo'] = $errorData;
                $this->load->view('manage/userPerson/excelInError', $result);
            }else{
                //不存在错误导入数据,更新用户
                User_model::get_instance()->insert_all_user($sqlDataRow);
                self::showRowsList();
            }

        }
    }
    public function excelInModel(){

    }
    //导出excel
    public function excelOut(){
              $this->load->library('CI_Excel');
//              数据库查找
              $head = array("用户名","真实姓名","电话","性别","身份证","邮箱","创建日期","公司名称","联系人","联系电话");
              $outData = array();
              $result = User_model::get_instance()->get_userinfos();
              foreach ($result as $key=>&$val){
                  $comName = empty($val['comName'])?"":$val['comName'];
                  $comPerson = empty($val['comPerson'])?"":$val['comPerson'];
                  $comTel = empty($val['comTel'])?"":$val['comTel'];
                  $outData[] = array($val['username'],$val['realname'],$val['tel'],($val['gender']>0?"男":"女"),$val['idCard'],$val['email'],date("Y-m-d H:i:s",$val['createTime']),$comName,$comPerson,$comTel);
              }
              CI_Excel::get_instance()->excel_out($head,$outData);
    }
}
