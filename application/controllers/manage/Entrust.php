<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Entrust extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Entrust_model');
    }
    //采购委托列表 type=0采购type=1销售
    public function entrustPurchaseList(){
        $post = $this->input->post();
        if(!empty($post)){
            $result = Entrust_model::get_instance()->get_entrust_select(array("type"=>0));
            unset($val);
            foreach($result as $key=>&$val){
//                标题检索
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['title'])){
                    unset($result[$key]);
                    continue;
                }
//                状态检索
                if(!empty($post['status'])&&$post['status']!=$val['status']){
                    unset($result[$key]);
                    continue;
                }
                //时间转换
                $val["endTime"] = date("Y-m-d H:i:s",$val["endTime"]);
                $val["createTime"] = date("Y-m-d H:i:s",$val["createTime"]);
                switch ($val['status']){
                    case "1":
                        $result[$key]['status'] = "待处理";
                        break;
                    case "2":
                        $result[$key]['status'] = "处理中";
                        break;
                    case "3":
                        $result[$key]['status'] = "已完成";
                        break;
                    case "4":
                        $result[$key]['status'] = "已驳回";
                        break;
                }
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $result = Entrust_model::get_instance()->get_entrust_select(array("type"=>0));
            $data = array("count"=>count($result));
            $this->load->view('manage/entrust/entrustPurchaseList', $data);
        }
    }
    //销售委托列表
    public function entrustSuppleList(){
        $post = $this->input->post();
        if(!empty($post)){
            $result = Entrust_model::get_instance()->get_entrust_select(array("type"=>1));
            unset($val);
            foreach($result as $key=>&$val){
                //                标题检索
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['title'])){
                    unset($result[$key]);
                    continue;
                }
//                状态检索
                if(!empty($post['status'])&&$post['status']!=$val['status']){
                    unset($result[$key]);
                    continue;
                }
                //时间转换
                $val["endTime"] = date("Y-m-d H:i:s",$val["endTime"]);
                $val["createTime"] = date("Y-m-d H:i:s",$val["createTime"]);
                switch ($val['status']){
                    case "1":
                        $result[$key]['status'] = "待处理";
                        break;
                    case "2":
                        $result[$key]['status'] = "处理中";
                        break;
                    case "3":
                        $result[$key]['status'] = "已完成";
                        break;
                    case "4":
                        $result[$key]['status'] = "已驳回";
                        break;
                }
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $result = Entrust_model::get_instance()->get_entrust_select(array("type"=>1));
            $data = array("count"=>count($result));
            $this->load->view('manage/entrust/entrustSuppleList', $data);
        }
    }
    //删除销售委托
    public function delEntrust(){
        $get = $this->input->get();
        Entrust_model::get_instance()->del_question($get['Id']);
    }
    public function entrustIframe(){
        $data = array();
        $this->load->view('manage/entrust/purchaseIframe', $data);
    }
    public function entrustIframeStatus(){
        $post = $this->input->post();
        echo Entrust_model::get_instance()->epurchase_status($post);
    }
}
