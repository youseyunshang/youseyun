<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class QuestionAnswer extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Que_ans_model');
        $this->load->helper(array('form', 'url'));
    }
    //问题列表
    public function questionList(){
        $post = $this->input->post();
        if(!empty($post)){
            $result = Que_ans_model::get_instance()->get_question_select();
            unset($val);
            foreach($result as $key=>&$val){
                $val['content'] = preg_replace('/<\s*img\s+[^>]*?src\s*=\s*(\'|\")(.*?)\\1[^>]*?\/?\s*>/i', '', $val['content']);
                $result[$key]['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['content'])){
                    unset($result[$key]);
                }
                $attr = json_decode($val['attrId'],true);
                if(!empty($attr)){
                    $sign = array();
                    foreach ($attr as $value){
                        $sign[] = $value['title'];
                    }
                    $val['attrname'] = implode(",",$sign);
                }else{
                    $val['attrname'] = '';
                }
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $result = Que_ans_model::get_instance()->get_question_select();
            $data = array("count"=>count($result));
            $this->load->view('manage/questionAnswer/questionList', $data);
        }
    }
    /**
     * answerList
     *
     * 删除问题
     *
     * @return	删除问题
     * @anthor Json.Wang
     */
    public function delQuestion(){
       $post = $this->input->post();
       echo Que_ans_model::get_instance()->del_question($post['Id']);
    }
    /**
     * answerList
     *
     * 答案列表
     *
     * @return	答案列表
     * @anthor Json.Wang
     */
    public function answerList($params=0){
        $post = $this->input->post();
        if(!empty($post)){
            $result = Que_ans_model::get_instance()->get_answer($post['Id']);
            unset($val);
            foreach($result as $key=>&$val){
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['content'])){
                    unset($result[$key]);
                    continue;
                }
                $result[$key]['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $result = Que_ans_model::get_instance()->get_answer($params);
            $data = array("count"=>count($result),"Id"=>$params);
            $this->load->view('manage/questionAnswer/answerList', $data);
        }
    }
    //删除答案
    public function delAnswer(){
        $post = $this->input->post();
        echo Que_ans_model::get_instance()->del_answer($post['Id']);
    }
}
