<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Advertisement extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Advertisement_model');
    }
    /**
     * addAdvertisement
     *
     * 添加广告
     *
     * @return	1 添加成功 0 添加失败
     * @anthor Json.Wang
     */
    public function addAdvertisement(){
        $post = $this->input->post();
        if(!empty($post)){
            $signData = Advertisement_model::get_instance()->get_sign_attr();
            $sign = array();
            foreach ($signData as $key=>&$val){
                if(preg_match("/".$val['attrname']."/i",$post['title'])||preg_match("/".$val['attrname']."/i",$post['content'])||preg_match("/".$val['attrname']."/i",$post['notice'])){
                    $sign[] = $val;
                }
            }
            $data = array(
                "title"=>$post['title'],
                "content"=>$post['content'],
                "createId"=>$_SESSION['UserId'],
                "createTime"=>time(),
                "photo"=>json_encode($post['photo']),
                "notice"=>$post['notice'],
                "photoType"=>$post['photoType'],
                "signLog"=>json_encode($sign)
            );
            echo Advertisement_model::get_instance()->insert_advertisement($data);
        }else{
            $result = array();
            $this->load->view('manage/advertisement/addAdvertisement', $result);
        }
    }
    /**
     * advertisementList
     *
     * 广告列表
     *
     * @return	成功后跳转列表页面
     * @anthor Json.Wang
     */
    public function advertisementList(){
        $post = $this->input->post();
        if(!empty($post)){
            $result = Advertisement_model::get_instance()->get_advertisement();
            unset($val);
            foreach($result as $key=>&$val){
                $result[$key]['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                $result[$key]['showNotice'] = substr($val['notice'],0,60);
                $result[$key]['signAll'] = "";
                //关键字
                if( !empty($val['signLog']) && $signLog = json_decode($val['signLog'],true)){
                    unset($v);
                    foreach ($signLog as $k=>&$v){
                        if(!empty($v['attrname'])){
                            $result[$key]['signAll'] .= $v['attrname'].",";
                        }
                    }
                    $result[$key]['signAll'] = substr($result[$key]['signAll'],0,-1);
                }
                //标题检索
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['title'])){
                    unset($result[$key]);
                    continue;
                }
                //概要检索
                if(!empty($post['selectNotice'])&&!preg_match("/".$post['selectNotice']."/i", $val['notice'])){
                    unset($result[$key]);
                    continue;
                }
                //属性检索
                if(!empty($post['selectAttrName'])&&!preg_match("/".$post['selectAttrName']."/i", $val['attrAll'])){
                    unset($result[$key]);
                    continue;
                }
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $result = Advertisement_model::get_instance()->get_advertisement();
            $data = array("count"=>count($result));
            $this->load->view('manage/advertisement/advertisementList', $data);
        }
    }
    /**
     * delAdvertisement
     *
     * 删除广告
     *
     * @return	1,0
     * @anthor Json.Wang
     */
    public function delAdvertisement(){
        $post = $this->input->post();
        echo Advertisement_model::get_instance()->del_advertisement($post['Id']);
    }
    /**
     * updateAdvertisement
     *
     * 更新资源
     *
     * @return	成功后跳转列表页面
     * @anthor Json.Wang
     */
    public function updateAdvertisement(){
        $post = $this->input->post();
        $get = $this->input->get();
        if(!empty($post)){
            //关键词检索匹配
            $signData = Advertisement_model::get_instance()->get_sign_attr();
            $sign = array();
            foreach ($signData as $key=>&$val){
                if(preg_match("/".$val['attrname']."/i",$post['title'])||preg_match("/".$val['attrname']."/i",$post['content'])||preg_match("/".$val['attrname']."/i",$post['notice'])){
                    $sign[] = $val;
                }
            }
            $data = array(
                "title"=>$post['title'],
                "content"=>$post['content'],
                "createId"=>$_SESSION['UserId'],
                "createTime"=>time(),
                "photo"=>json_encode($post['photo']),
                "notice"=>$post['notice'],
                "photoType"=>$post['photoType'],
                "signLog"=>json_encode($sign)
            );
            echo Advertisement_model::get_instance()->update_advertisement($data);
        }else{
            $result = array("resData"=>Advertisement_model::get_instance()->get_advertisement(array("Id"=>$get['Id'])));
            $this->load->view('manage/advertisement/updateAdvertisement', $result);
        }
    }
    //上传图片
    public function upload($params="res"){
        $config['upload_path']      = getcwd().'./resource/uploads/'.$params;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['encrypt_name']    = true;
        $config['max_size']     = 2048;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            echo json_encode($data);
        }
    }
}
