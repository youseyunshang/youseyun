<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Company extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Company_model');
        $this->load->helper(array('form', 'url'));
    }
    //公司列表
    public function showRowsList($params=array()){
        $result = Company_model::get_instance()->get_cominfos($params);
        $data = array("count"=>count($result),"comType"=>$params['comType']);
        $this->load->view('manage/company/companyList', $data);
    }
    //添加后台用户
    //$params = 1质检 2物流
    public function addCompany($params = 1){
        $post = $this->input->post();
        if(self::$allow>0){
            if( !empty($post) ){
                //添加
                if(Company_model::get_instance()->inset_cominfos($post)){
                   self::showRowsList(array("comType"=>$post['comType']));
                }
            }else{
                //显示添加页
                $result = array("comType"=>$params);
                $this->load->view('manage/company/addCompany', $result);
            }
        }else{
            $data = array();
            $this->load->view('manage/login/login', $data);
        }
    }
    //管理员列表
    public function companyList($params=1){
        $post = $this->input->post();
        //此数据加载为缓存数据
        if(empty($post)){
            $result = Company_model::get_instance()->get_cominfos(array("comType"=>$params));
            $data = array("count"=>count($result),"comType"=>$params);
            $this->load->view('manage/company/companyList', $data);
        }else{//AJAX加载数据
            $result = Company_model::get_instance()->get_cominfos(array("comType"=>$post['comType']));
            foreach($result as $key=>&$val){
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['comName'])){
                    unset($result[$key]);
                    continue;
                }
                $result[$key]['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }
    }
    //更新管理员
    //$type=0 查看 $type=1 更新
    public function updateCompany(){
        $post = $this->input->post();
        $get = $this->input->get();
        if(!empty($post)){
            if(Company_model::get_instance()->update_cominfos($post)){
               self::showRowsList(array("comType"=>$post['comType']));
            }
        }else{
            $userInfo = Company_model::get_instance()->get_cominfos_row($get['Id']);
            $result = array();
            $result['userInfo'] = $userInfo;
            $this->load->view('manage/company/updateCompany', $result);
        }
    }
    //删除管理员
    public function delCompany(){
        self::del_valid('/Company/DelCompany');
//        $get = $this->input->get();
//        if(Company_model::get_instance()->del_cominfos($get['Id'])){
//          self::showRowsList(array("comType"=>$get['comType']));
//        }
        $post = $this->input->post();
        echo Company_model::get_instance()->del_cominfos($post['Id']);
    }
    //上传图片
    public function upload($params="userlogo"){
        $config['upload_path']      = getcwd().'/resource/uploads/'.$params;
        $config['allowed_types']    = 'gif|jpg|png|xlsx|xls';
        $config['encrypt_name']    = true;
        $config['max_size']     = 2048;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            echo json_encode($data);
        }
    }
}
