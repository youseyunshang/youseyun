<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dispute extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Dispute_model');
    }
    //角色管理列表
    public function disputeList(){
        //获取角色内容
        $result = Dispute_model::get_instance()->get_dispute();

        $post = $this->input->post();
        //展示角色
        if(empty($post)){
            $data = array(
                "count"=>count($result)
            );
            $this->load->view("manage/dispute/disputeList",$data);
        }else{
            unset($val);
            foreach ($result as $key=>&$val){
                if(!empty($post['status'])&&$post['status']!=$val['status']){
                    unset($result[$key]);
                    continue;
                }
                $val['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                switch ($val['status']){
                    case "1":
                        $result[$key]['status'] = "待处理";
                        break;
                    case "2":
                        $result[$key]['status'] = "处理中";
                        break;
                    case "3":
                        $result[$key]['status'] = "已解决";
                        break;

                }
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }
    }
    //展示角色
//    public function showDispute(){
//        $get = $this->input->get();
//        if(isset($get['Id'])&&$result = Role_model::get_instance()->get_role($get['Id'])){
//            $roleData = unserialize($result["content"]);
//            //权限Id组
//            $Ids = array();
//            foreach ($roleData as $key=>$val){
//                $Ids[$val['Id']] = $val["Id"];
//                if(!empty($val['sub'])){
//                    foreach ($val['sub'] as $k=>$v){
//                        $Ids[$v['Id']] = $v["Id"];
//                    }
//                }
//            }
//            $content = array();
//            self::permissionInfo($content);
//            $response = array(
//                "roleId"=>$result['Id'],
//                "roleName"=>$result['rolename'],
//                "roleData"=>$Ids,
//                "permission"=>$content
//            );
//            $this->load->view('role/showRole', $response);
//        }
//    }
    //更新角色
//    public function updateRole(){
//        $content = array();
//        self::permissionInfo($content);
//        $post = $this->input->post();
//        $rolename = $post['title'];
//        $data = unserialize(serialize($post));
//        unset($data['title']);
//        foreach ($content as $key=>&$val){
//            if(isset($data[$val['Id']])){
//                if(isset($val['sub'])){//二级功能
//                    foreach ($val['sub'] as $k=>$v){
//                        if(!isset($data[$k])){
//                            unset($val['sub'][$k]);
//                        }
//                    }
//                }
//            }else{
//                unset($content[$key]);
//            }
//        }
//        $role = array("Id"=>$post['roleId'],"rolename"=>$rolename,"content"=>serialize($content),"createTime"=>time());
//        if(Role_model::get_instance()->update_role($role)){
//            //插入成功跳转角色管理
//            $result = Role_model::get_instance()->get_role();
//            $data = array(
//                "count"=>count($result)
//            );
//            $this->load->view("role/roleList",$data);
//        }
//    }
    //删除角色
//    public function delRole(){
//        $get = $this->input->get();
//        if(Role_model::get_instance()->del_role($get['Id'])){
//            //插入成功跳转角色管理
//            $result = Role_model::get_instance()->get_role();
//            $data = array(
//                "count"=>count($result)
//            );
//            $this->load->view("role/roleList",$data);
//        }
//    }
}
