<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Role extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Role_model');
    }
    /**
     * addRole
     *
     * 添加角色
     *
     * @param  $params=array()
     * @return	json数据
     * @anthor Json.Wang
     */
    public function addRole(){
        if( self::$allow>0 ){
            $result = array();
            $content = array();
            self::permissionInfo($content);
            //可以将此内容缓存处理
            $result['permission'] = $content;
            $this->load->view('manage/role/addRole', $result);
        }else{
            $data = array();
            $this->load->view('manage/login/login', $data);
        }
    }
    //管理权限内容
    /*return $params = array(
       "权限Id"=>array(
               "权限内容"
                "sub"=>array(
                    "权限Id"=>"权限内容"
                )
        )
    )*/
    public function permissionInfo(&$params){
        $data = Role_model::get_instance()->get_permissionInfo();
        foreach ($data as $key=>$val){
            if($val['pid']>0){//二级功能
                $params[$val['pid']]['sub'][$val['Id']] = $val;
            }else{
                $params[$val['Id']] = $val;
            }
        }
    }
    //插入一条数据
    public function insertRole(){
        $content = array();
        self::permissionInfo($content);
        $post = $this->input->post();
        $rolename = $post['title'];
        $data = unserialize(serialize($post));
        unset($data['title']);
        foreach ($content as $key=>&$val){
            if(isset($data[$val['Id']])){
                if(isset($val['sub'])){//二级功能
                    foreach ($val['sub'] as $k=>$v){
                        if(!isset($data[$k])){
                            unset($val['sub'][$k]);
                        }
                    }
                }
            }else{
                unset($content[$key]);
            }
        }
        $role = array("rolename"=>$rolename,"content"=>serialize($content));
        if(Role_model::get_instance()->add_role($role)){
            //插入成功跳转角色管理
            $result = Role_model::get_instance()->get_role();
            $data = array(
                "count"=>count($result)
            );
            $this->load->view("manage/role/roleList",$data);
        }else{
            self::addRole();
        }
    }
    //角色管理列表
    public function roleList(){
        //获取角色内容
        $result = Role_model::get_instance()->get_role();
        $post = $this->input->post();
        //展示角色
        if(empty($post)){
            $data = array(
                "count"=>count($result)
            );
            $this->load->view("manage/role/roleList",$data);
        }else{
            unset($val);
            foreach ($result as $key=>&$val){
                $val['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                unset($val['content']);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }
    }
    //展示角色
    public function showRole(){
        $get = $this->input->get();
        if(isset($get['Id'])&&$result = Role_model::get_instance()->get_role($get['Id'])){
            $roleData = unserialize($result["content"]);
            //权限Id组
            $Ids = array();
            foreach ($roleData as $key=>$val){
                $Ids[$val['Id']] = $val["Id"];
                if(!empty($val['sub'])){
                    foreach ($val['sub'] as $k=>$v){
                        $Ids[$v['Id']] = $v["Id"];
                    }
                }
            }
            $content = array();
            self::permissionInfo($content);
            $response = array(
                "roleId"=>$result['Id'],
                "roleName"=>$result['rolename'],
                "roleData"=>$Ids,
                "permission"=>$content
            );
            $this->load->view('manage/role/showRole', $response);
        }
    }
    //更新角色
    public function updateRole(){
        $content = array();
        self::permissionInfo($content);
        $post = $this->input->post();
        $rolename = $post['title'];
        $data = unserialize(serialize($post));
        unset($data['title']);
        foreach ($content as $key=>&$val){
            if(isset($data[$val['Id']])){
                if(isset($val['sub'])){//二级功能
                    foreach ($val['sub'] as $k=>$v){
                        if(!isset($data[$k])){
                            unset($val['sub'][$k]);
                        }
                    }
                }
            }else{
                unset($content[$key]);
            }
        }
        $role = array("Id"=>$post['roleId'],"rolename"=>$rolename,"content"=>serialize($content),"createTime"=>time());
        if(Role_model::get_instance()->update_role($role)){
            //插入成功跳转角色管理
            $result = Role_model::get_instance()->get_role();
            $data = array(
                "count"=>count($result)
            );
            $this->load->view("manage/role/roleList",$data);
        }
    }
    /**
     * addRole
     *
     * 删除角色
     *
     * @param  $params=array()
     * @return	json数据
     * @anthor Json.Wang
     */
    public function delRole(){
//        $get = $this->input->get();
//        if(Role_model::get_instance()->del_role($get['Id'])){
//            //插入成功跳转角色管理
//            $result = Role_model::get_instance()->get_role();
//            $data = array(
//                "count"=>count($result)
//            );
//            $this->load->view("role/roleList",$data);
//        }
        $post = $this->input->post();
        echo Role_model::get_instance()->del_role($post['Id']);
    }
}
