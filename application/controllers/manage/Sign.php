<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Sign extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Attribute_model');
    }
    //标签列表
    public function showRowsList(){
        //allSign所有标签
        $result = Attribute_model::get_instance()->get_attr_all();
        $data = array("count"=>count($result));
        $this->load->view('manage/sign/signList', $data);
    }
    //$params>0 添加
    //$params = 0 展示
    public function addSign($params = 0){
        if(self::$allow>0){
            if( $params > 0 ){
                $post = $this->input->post();
                $data = array(
                    'pid' => 0,
                    'attrname' =>$post['sign'],
                    'type'=>1,
                    'createTime' => time()
                );
                if(Attribute_model::get_instance()->save_attr($data)){
                    //allSign所有标签
                   self::showRowsList();
                }
            }else{
                //allSign所有标签
                $result = array(
                    "allSign"=>Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>1))
                );
                $this->load->view('manage/sign/addSign', $result);
            }
        }else{
            $data = array();
            $this->load->view('manage/login/login', $data);
        }
    }
    //标签列表

    public function signList($params=array()){
        $post = $this->input->post();
        //type=1 喜好标签 type =0 自定义属性
        $result = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>1));
        if(empty($post)){
            $data = array("count"=>count($result));
            $this->load->view('manage/sign/signList', $data);
        }else{//AJAX加载数据
            $signName = !empty($post['signName'])?$post['signName']:null;
            $startTime = !empty($post['startTime'])?strtotime(date($post['startTime'])):null;
            $endTime = !empty($post['endTime'])?strtotime(date($post['endTime'])):null;
            unset($val);
            foreach ($result as $key=>&$val){
                //标签检测
                if(!empty($signName)&&!preg_match("/".$signName."/i",$val["attrname"])){
                    unset($result[$key]);
                    continue;
                }
                //时间检测
                if(!empty($startTime)&&!($val['createTime']-$startTime>0)){
                    unset($result[$key]);
                    continue;
                }
                if(!empty($endTime)&&!($endTime-$val['createTime']>0)){
                    unset($result[$key]);
                    continue;
                }
                //时间戳转日期
                $val['createTime'] = date("Y-m-d H:i:s",$val["createTime"]);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }
    }
    //标签
    //$type=0 查看 $type=1 更新
    public function updateSign($type=0){
        if($type>0){
            $post = $this->input->post();
            $data = array(
                'Id'=>$post['Id'],
                'attrname' =>$post['sign'],
                'createTime' => time()
            );
            if(Attribute_model::get_instance()->update_attr($data)){
                self::showRowsList();
            }
        }else{
            echo "请检查参数是否正确！";
        }
    }
    public function showSign($params="0"){
        $result = array(
            "data" => Attribute_model::get_instance()->get_attr(array("params"=>true,"type"=>1,"Id"=>$params)),
            "allSign"=>Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>1))
        );
        $this->load->view('manage/Sign/updateSign', $result);
    }
    //删除标签
    public function delSign(){
//        $get = $this->input->get();
//        Attribute_model::get_instance()->del_attr($get['Id']);
//        self::showRowsList();
        $post = $this->input->post();
        echo Attribute_model::get_instance()->del_attr($post['Id']);
    }
}
