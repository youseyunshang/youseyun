<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class Resource extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Res_model');
    }
    /**
     * addRes
     *
     * 添加资源
     *
     * @return	1 添加成功 0 添加失败
     * @anthor Json.Wang
     */
    public function addRes(){
        $post = $this->input->post();
        if(!empty($post)){
            $signData = Res_model::get_instance()->get_sign_attr();
            $sign = array();
            foreach ($signData as $key=>&$val){
                if(preg_match("/".$val['attrname']."/i",$post['title'])||preg_match("/".$val['attrname']."/i",$post['content'])||preg_match("/".$val['attrname']."/i",$post['notice'])){
                    $sign[] = $val;
                }
            }
            $fileName = "2019_".date("His",time())."_".rand(1111,9999).'.log';
            $path = base_url("/resource/uploads/resLog/");
            if (!is_dir($path)){ //判断目录是否存在 不存在就创建
                mkdir($path,0777,true);
            }
            $num = file_put_contents ($path.$fileName,serialize($post['content']));
            if($num>0){
                $data = array(
                    "title"=>$post['title'],
                    "content"=>$path.$fileName,
                    "createId"=>$_SESSION['UserId'],
                    "createTime"=>time(),
                    "photo"=>json_encode($post['photo']),
                    "attrLog"=>json_encode($post['attr']),
                    "notice"=>$post['notice'],
                    "photoType"=>$post['photoType'],
                    "signLog"=>json_encode($sign),
                    "type"=>$post['type'],
                    "origin"=>$post['origin']
                );
                echo Res_model::get_instance()->insert_res($data);
            }else{
                echo 0;
            }

        }else{
            $this->load->model('Attribute_model');
            $data = Attribute_model::get_instance()->get_attr();
            $tree = self::createTree($data);
            $result = array("attrData"=>json_encode($tree));
            $this->load->view('manage/resource/addRes', $result);
        }
    }
    /**
     * addReport
     *
     * 添加报表
     *
     * @return	成功后跳转列表页面
     * @anthor Json.Wang
     */
    public function addReport(){
        $post = $this->input->post();
        if(!empty($post)){
            $middlenum = ($post['lastnum']+$post['topnum'])/2;
            $data = array(
                "formname"=>$post['formname'],
                "Specifications"=>$post['Specifications'],
                "measurement"=>$post['measurement'],
                "idnumber"=>"cbc".time().$post['lastnum'].$post['topnum'],
                "lastnum"=>$post['lastnum'],
                "topnum"=>$post['topnum'],
                "middlenum"=>$middlenum,
                "updown"=>'0',
                "createTime"=>time(),
                "createId"=>$_SESSION['UserId'],
                "attrIds"=>$post['attrIds'],
                "attrNames"=>$post['attrNames']
            );
            if($data['Id'] = Res_model::get_instance()->insert_report($data)){
                echo json_encode($data);
            }
        }else{
            $Data = Res_model::get_instance()->get_report_all();
            $result = array("reportData"=>json_encode($Data));
            $this->load->view('manage/resource/addReport', $result);
        }
    }
    /**
     * addReport
     *
     * 生成基础报价表
     *
     * @return	生成基础报价表
     * @anthor Json.Wang
     */
    public function addBaseReport(){
            $result = array("reportData"=>Res_model::get_instance()->get_report_all());
            $this->load->view('manage/resource/addBaseReport', $result);
    }
    /**
     * delReport
     *
     * 删除基础报表中的一行
     *
     * @return	删除基础报表中的一行
     * @anthor Json.Wang
     */
    public function delReport(){
        echo Res_model::get_instance()->del_report();
    }
    /**
     * delReport
     *
     * 删除报价中的一行
     *
     * @return	删除报价中的一行
     * @anthor Json.Wang
     */
    public function delOutReport(){
        echo Res_model::get_instance()->del_out_report();
    }
    /**
     * resList
     *
     * 资源列表
     *
     * @return	成功后跳转列表页面
     * @anthor Json.Wang
     */
    public function resList(){
        $post = $this->input->post();
        if(!empty($post)){
            $result = Res_model::get_instance()->get_res_all();
            unset($val);
            foreach($result as $key=>&$val){
                //标题检索
                if(!empty($post['selectName'])&&!preg_match("/".$post['selectName']."/i", $val['title'])){
                    unset($result[$key]);
                    continue;
                }
                //概要检索
                if(!empty($post['selectNotice'])&&!preg_match("/".$post['selectNotice']."/i", $val['notice'])){
                    unset($result[$key]);
                    continue;
                }
                //属性检索
                if(!empty($post['selectAttrName'])&&!preg_match("/".$post['selectAttrName']."/i", $val['attrAll'])){
                    unset($result[$key]);
                    continue;
                }
                //资讯类别
                if(!empty($post['typeBar'])&&$post['typeBar']!=$val['type']){
                    unset($result[$key]);
                    continue;
                }
                $result[$key]['createTime'] = date("Y-m-d H:i:s",$val['createTime']);
                $result[$key]['attrAll'] = '';
                //属性
                if(!empty($val['attrLog'])&&$attrLog = json_decode($val['attrLog'],true)){
                    foreach ($attrLog as $k=>&$v){
                        if(!empty($v['attrname'])){
                            $result[$key]['attrAll'] .= $v['attrname'].",";
                        }
                        if($k>=4){
                            break;
                        }
                    }
                    $result[$key]['attrAll'] = substr($result[$key]['attrAll'],0,-1);
                }
                $result[$key]['signAll'] = "";
                //关键字
                if( !empty($val['signLog']) && $signLog = json_decode($val['signLog'],true)){
                    unset($v);
                    foreach ($signLog as $k=>&$v){
                        if(!empty($v['attrname'])){
                            $result[$key]['signAll'] .= $v['attrname'].",";
                        }
                        if($k>=5){
                            break;
                        }
                    }
                    $result[$key]['signAll'] = substr($result[$key]['signAll'],0,-1);
                }
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $result = Res_model::get_instance()->get_res();
            $data = array("count"=>count($result));
            $this->load->view('manage/resource/resList', $data);
        }
    }
    /**
     * reportOutList
     *
     * 报表列表
     *
     * @return	成功后跳转列表页面
     * @anthor Json.Wang
     */
    public function reportOutList(){
        $post = $this->input->post();
        $result = Res_model::get_instance()->get_reportout_list();
        if(!empty($post)){
            $dayTime = strtotime(date("Y-m-d",time()));
            unset($val);
            foreach($result as $key=>&$val){
                if($dayTime==$result[$key]['createTime']){
                    $result[$key]['now'] = true;
                }
                $result[$key]['createTime'] = date("Y-m-d",$val['createTime']);
            }
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }else{
            $data = array("count"=>count($result));
            $this->load->view('manage/resource/reportOutList', $data);
        }
    }
    /**
     * delRes
     *
     * 删除资源
     *
     * @return	1,0
     * @anthor Json.Wang
     */
    public function delRes(){
        $post = $this->input->post();
        echo Res_model::get_instance()->del_res($post['Id']);
    }
    /**
     * updateRes
     *
     * 更新资源
     *
     * @return	成功后跳转列表页面
     * @anthor Json.Wang
     */
    public function updateRes(){
        $post = $this->input->post();
        $get = $this->input->get();
        if(!empty($post)){
            //关键词检索匹配
            $signData = Res_model::get_instance()->get_sign_attr();
            $sign = array();
            foreach ($signData as $key=>&$val){
                if(preg_match("/".$val['attrname']."/i",$post['title'])||preg_match("/".$val['attrname']."/i",$post['notice'])){
                    $sign[] = $val;
                }
                if(count($sign)>4){
                    break;
                }
            }
            if(empty($post['path'])||file_exists($post['path'])){
                $fileName = "2019_".date("His",time())."_".rand(1111,9999).'.log';
                $path = base_url("/resource/uploads/resLog/");
                if (!is_dir($path)){ //判断目录是否存在 不存在就创建
                    mkdir($path,0777,true);
                }
                $post['path'] = $path.$fileName;
            }
            $num = file_put_contents ($post['path'],serialize($post['content']));
            if($num>0){
                $data = array(
                    "title"=>$post['title'],
                    "content"=>$post['path'],
                    "createId"=>$_SESSION['UserId'],
                    "createTime"=>time(),
                    "photo"=>json_encode($post['photo']),
                    "attrLog"=>json_encode($post['attr']),
                    "notice"=>$post['notice'],
                    "photoType"=>$post['photoType'],
                    "signLog"=>json_encode($sign),
                    "type"=>$post['type'],
                    "origin"=>$post['origin']
                );
                echo Res_model::get_instance()->update_res($data);
            }else{
                echo 0;
            }
        }else{
            $this->load->model('Attribute_model');
            $resData = Res_model::get_instance()->get_res(array("Id"=>$get['Id']));
            if(file_exists($resData['content'])){
                $resData['path'] = $resData['content'];
                $str = file_get_contents($resData['content']);//将整个文件内容读入到一个字符串中
                $resData['content'] = str_replace("\r\n","<br />",unserialize($str));
            }else{
                $resData['path'] = '';
            }
            $attrData = array();
            if(!empty($resData['attrLog'])){
                $attrData = json_decode($resData['attrLog'],true);
            }
            $data = Attribute_model::get_instance()->get_attr();
            $tree = self::createTree($data,$attrData);
            $result = array("attrData"=>json_encode($tree),"resData"=>$resData);
            $this->load->view('manage/resource/updateRes', $result);
        }
    }
    /**
     * updateReport
     *
     * 更新报表
     *
     * @return	成功后跳转列表页面
     * @anthor Json.Wang
     */
    public function updateReport(){
        $post = $this->input->post();
        if(!empty($post)){
            $middlenum = ($post['lastnum']+$post['topnum'])/2;
            $data = array(
                "measurement"=>$post['measurement'],
                "Specifications"=>$post['Specifications'],
                "lastnum"=>$post['lastnum'],
                "topnum"=>$post['topnum'],
                "middlenum"=>$middlenum,
                "createTime"=>time(),
                "createId"=>$_SESSION['UserId'],
                "attrIds"=>$post['attrIds'],
                "attrNames"=>$post['attrNames']
            );
            if(Res_model::get_instance()->update_report($data)){
                echo json_encode($data);
            }
        }
    }
    public function updateOutReport(){
        $get = $this->input->get();
        $post = $this->input->post();
        if(!empty($post)){
            $reportData = Res_model::get_instance()->get_reportout_byid($post['Id']);
            $data = json_decode($reportData['content'],true);
            switch ($post['type']){
                case "del":
                    unset($val);
                    foreach ($data as $key=>&$val){
                        if($val['idnumber']==$post['unqid']){
                            unset($data[$key]);
                            break;
                        }
                    }
                    Res_model::get_instance()->update_report_out(array("content"=>json_encode($data)));
                    break;
                case "update":
                    $middlenum = ($post['lastnum']+$post['topnum'])/2;
                    $outData = array(
                        "lastnum"=>$post['lastnum'],
                        "topnum"=>$post['topnum'],
                        "Specifications"=>$post['Specifications'],
                        "measurement"=>$post['measurement'],
                        "middlenum"=>$middlenum,
                        "createTime"=>time(),
                        "createId"=>$_SESSION['UserId'],
                        "attrIds"=>$post['lastnum'],
                        "attrNames"=>$post['attrNames']
                    );
                    unset($val);
                    foreach ($data as $key=>&$val){
                        if($val['idnumber']==$post['unqid']){
                            $val['lastnum'] = $outData['lastnum'];
                            $val['topnum'] = $outData['topnum'];
                            $val['Specifications'] = $outData['Specifications'];
                            $val['measurement'] = $outData['measurement'];
                            $val['middlenum'] = $outData['middlenum'];
                            $val['createTime'] = $outData['createTime'];
                            $val['createId'] = $outData['createId'];
                            $val['attrIds'] = $outData['attrIds'];
                            $val['attrNames'] = $outData['attrNames'];
                            break;
                        }
                    }
                    if(Res_model::get_instance()->update_report_out(array("content"=>json_encode($data)))){
                        echo json_encode($outData);
                    }
                    break;
                case "add":
                    $middlenum = ($post['lastnum']+$post['topnum'])/2;
                    $outData = array(
                        "formname"=>$post['formname'],
                        "idnumber"=>"cbc".time().$post['lastnum'].$post['topnum'],
                        "lastnum"=>$post['lastnum'],
                        "topnum"=>$post['topnum'],
                        "Specifications"=>$post['Specifications'],
                        "measurement"=>$post['measurement'],
                        "middlenum"=>$middlenum,
                        "updown"=>'0',
                        "createTime"=>time(),
                        "createId"=>$_SESSION['UserId'],
                        "attrIds"=>$post['lastnum'],
                        "attrNames"=>$post['attrNames']
                    );
                    $data[] = $outData;
                    if(Res_model::get_instance()->update_report_out(array("content"=>json_encode($data)))){
                        echo json_encode($outData);
                    }
                    break;
            }
        }else{
            $reportData = Res_model::get_instance()->get_reportout_byid(intval($get['Id']));
            $data = json_decode($reportData['content'],true);
            if(isset($get['selectName'])){
                foreach ($data as $key=>&$val){
//                     if(!preg_match("/".$get['selectName']."/i",$val['attrNames'])){
//                         unset($data[$key]);
//                         continue;
//                     }
                     if(!preg_match("/".$get['selectName']."/i",$val['formname'])){
                         unset($data[$key]);
                         continue;
                     }
                }
            }
            $result = array("reportData"=>$data,"rId"=>$get['Id'],"Id"=>$reportData["Id"],"createTime"=>$reportData["createTime"]);
            $this->load->view('manage/resource/updateReport', $result);
        }
    }
    //上传图片
    public function upload($params="res"){
        $config['upload_path']      = getcwd().'/resource/uploads/'.$params;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['encrypt_name']    = true;
        $config['max_size']     = 2048;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            echo json_encode($data);
        }
    }
    //http://api01.idataapi.cn:8000/article/idataapi?KwPosition=3&size=20&catLabel2=宏观经济&publishDateRange=1563840000,1566432000&createDateRange=1563840000,1566432000&apikey=xd5BAH89sqMC3IH88OLs4BU7Yn8XhQhPwVysL0oBMjQDmsfXxJIsiNK3GNhOYRpr
    //app接口请求
    public function resApi(){
        //聚合数据
//        $get = $this->input->get();
//        $key = "fd9897316542138399bebce44e1511aa";
//        $this->load->library('Curl_request');
//        $data = array("key"=>$key,"type"=>$get['type']);
//        $data = Curl_request::get_instance()->curl("http://v.juhe.cn/toutiao/index",$data);
        $this->load->library('Curl_request');
        //阿里数据
        $get = $this->input->get();
//        $appcode = "3ba0765af33a4618952d70cd017405d0";
        $apiKey = "xd5BAH89sqMC3IH88OLs4BU7Yn8XhQhPwVysL0oBMjQDmsfXxJIsiNK3GNhOYRpr";
        $header = array();
//        array_push($header, "Authorization:APPCODE " . $appcode);
        $data = array("apikey"=>$apiKey,"catLabel2"=>"宏观经济","size"=>"50");
        $path = "http://api01.idataapi.cn:8000/article/idataapi";
        $data = Curl_request::get_instance()->curl($path,$data,$header);
        return json_decode($data,true);
//        var_dump("<pre>",json_decode($data,true));
    }

    //生成报价
    public function createReport(){
        $time = strtotime(date("Y-m-d",time()));
        $lastTime = $time-86400;
        if($data = Res_model::get_instance()->get_reportout_list()){
            $nowdata = array();
            $lastData = array();
            unset($val);
            foreach ($data as $key=>&$val){
                if($val['createTime']==$time){
                    $nowdata = $data[$key];
                }
                if($val['createTime']==$lastTime){
                    $lastData = $data[$key];
                }
            }
            if(!empty($nowdata)){
                //已经有当天数据啦
                echo 0;
            }else{
                //没有当天数据添加一条当天数据
                if(!empty($lastData)){
                    $arr = array(
                        "content"=>$lastData['content'],
                        "createTime"=>$time
                    );
                    echo Res_model::get_instance()->insert_report_log($arr);
                }else{
                    echo 0;
                }
            }
        }else{
            //如果在报价日志里没有数据
            //从基础数据中添加一条数据
            $Data = Res_model::get_instance()->get_report_all();
            $arr = array(
                "content"=>json_encode($Data),
                "createTime"=>$time
            );
            echo Res_model::get_instance()->insert_report_log($arr);
        }
    }
    public function showReport(){
	$get = $this->input->get();
        $time = strtotime(date("Y-m-d",time()));
        $lastTime = $time-86400;
        if($data = Res_model::get_instance()->get_reportout_list()) {
            $nowData = array();
            $lastData = array();
            unset($val);
            foreach ($data as $key => &$val) {
                if ($val['createTime'] == $time) {
                    $nowData = $data[$key];
                }
                if ($val['createTime'] == $lastTime) {
                    $lastData = $data[$key];
                }
            }
        }
        if(!empty($nowData)&&!empty($lastData)){
            $todayData = json_decode($nowData['content'],true);
            $yestodayData = json_decode($lastData['content'],true);
            $data = Array();
            unset($val);
            foreach ($yestodayData as $key=>&$val){
                if(isset($val['idnumber'])){
                    $data[$val['idnumber']] = $val;
                }
            }
            unset($val);
            foreach ($todayData as $key=>&$val){
                if(!empty($val['idnumber'])&&isset($data[$val['idnumber']])){
                            //$todayData[$key]['updown'] = $val['middlenum']-$data[$val['idnumber']]['middlenum'];
                    $todayData[$key]['middlenum'] = ($val['topnum']+$val['lastnum'])/2;
                    $yesMiddle = ($data[$val['idnumber']]['topnum']+$data[$val['idnumber']]['lastnum'])/2;
                    $todayData[$key]['updown'] = $todayData[$key]['middlenum']-$yesMiddle;
                }
            }
        }elseif (!empty($nowData)){
            $todayData = json_decode($nowData['content'],true);
        }
        $result = array("reportData"=>$todayData);
        $this->load->view('manage/resource/showReport', $result);
    }
	//excel导出
	 public function excelOutReport(){
        $get = $this->input->get();
        $time = strtotime(date("Y-m-d",time()));
        $lastTime = $time-86400;
        if($data = Res_model::get_instance()->get_reportout_list()) {
            $nowData = array();
            $lastData = array();
            unset($val);
            foreach ($data as $key => &$val) {
                if ($val['createTime'] == $time) {
                    $nowData = $data[$key];
                }
                if ($val['createTime'] == $lastTime) {
                    $lastData = $data[$key];
                }
            }
        }
        if(!empty($nowData)&&!empty($lastData)){
            $todayData = json_decode($nowData['content'],true);
            $yestodayData = json_decode($lastData['content'],true);
            $data = Array();
            unset($val);
            foreach ($yestodayData as $key=>&$val){
                $data[$val['idnumber']] = $val;
            }
            unset($val);
            foreach ($todayData as $key=>&$val){
                if(isset($data[$val['idnumber']])){
                   // $todayData[$key]['updown'] = $val['middlenum']-$data[$val['idnumber']]['middlenum'];
                   $todayData[$key]['updown'] = ($val['topnum']+$val['lastnum'])/2-($data[$val['idnumber']]['topnum']-$data[$val['idnumber']]['lastnum'])/2;
		}
		if(empty($val['idnumber'])||empty($val['formname'])){
		   unset($todayData[$key]);
		}
            }
            //$result = array("reportData"=>$todayData);
            //$this->load->view('manage/resource/showReport', $result);
        }elseif (!empty($nowData)){
            $todayData = json_decode($nowData['content'],true);
            //$result = array("reportData"=>$todayData);
            //$this->load->view('manage/resource/showReport', $result);
        }
        unset($val);
        foreach ($todayData as $key=>&$val){
            $todayData[$key]['updownPer'] = 0;
        }
	    $this->load->library('CI_Excel');
        $head = array("名称","编号","最低值","最高值","规格","价格","中间值","涨跌","创建时间","创建人Id","属性Id","属性名称","涨跌比");
	    CI_Excel::get_instance()->excel_out($head,$todayData);
    }
    //excel导入
    public function excelInReport(){
            $config['upload_path']      = getcwd().'/resource/uploads/';
            $config['allowed_types']    = '*';
            $config['max_size']     = 204800;
	        $this->load->library('upload', $config);

            if (!$this->upload->do_upload('userfile'))
            {
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
            }else{
                $data = array('upload_data' => $this->upload->data());
                $time = strtotime(date("Y-m-d",time()));
                $this->load->library('CI_Excel');
                $result = CI_Excel::get_instance()->excel_in($data['upload_data']['full_path'],0);
                $data = array();
                foreach ($result as $key=>&$val){
                    if($key<1){
                        continue;
                    }
                    if(isset($val[12])&&$val[12]>0){
                        //增
                        //最低值
                        $val[2] = intval($val[2]*(1+abs($val[12])));
                        self::formatNum($val[2]);
                        //最高值
                        $val[3] = intval($val[3]*(1+abs($val[12])));
                        self::formatNum($val[3]);
                        //中间值
                        $val[6] = intval(($val[2]+$val[3])/2);
                    }else if(isset($val[12])&&$val[12]<0){
                        //减
                        //最低值
                        $val[2] = intval($val[2]*(1-abs($val[12])));
                        self::formatNum($val[2]);
                        //最高值
                        $val[3] = intval($val[3]*(1-abs($val[12])));
                        self::formatNum($val[3]);
                        //中间值
                        $val[6] = intval(($val[2]+$val[3])/2);
                    }else{
                        self::formatNum($val[2]);
                        self::formatNum($val[3]);
                        $val[6] = intval(($val[2]+$val[3])/2);
                    }
                    $data[] = array(
                        'formname'=>$val[0],
                        'idnumber'=>$val[1],
                        'lastnum'=>$val[2],
                        'topnum'=>$val[3],
                        'Specifications'=>$val[4],
                        'measurement'=>$val[5],
                        'middlenum'=>$val[6],
                        'updown'=>$val[7],
                        'createTime'=>$val[8],
                        'createId'=>$val[9],
                        'attrIds'=>$val[10],
                        'attrNames'=>$val[11]
                    );
                }
                $time = strtotime(date("Y-m-d",time()));
                if($all = Res_model::get_instance()->update_report_time(array("content"=>json_encode($data)),$time)) {
                    $list = Res_model::get_instance()->get_reportout_list();
                    $this->load->view('manage/resource/reportOutList', array("count"=>count($list)));
                }else{
                    echo 0;
                }

            }
    }
    //报表选择数据
    public function selectAttrIframe(){
        $this->load->model('Attribute_model');
        $data = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>0));
        $tree = self::createTree($data);
        $result = array("metal"=>json_encode($tree));
        $this->load->view('manage/resource/selectAttrIframe', $result);
    }
    //生成树结构
    public function createTree($data=array(),$attrData=array()){
        $saveAttr = array();
        if(!empty($attrData)){
            foreach ($attrData as $key=>$val){
                $saveAttr[$val['Id']] = $val;
            }
        }
        $baseData = array();
        foreach ($data as $val){
            if($val['pid']>0){
                $flag = isset($saveAttr[$val['Id']])?true:false;
            }else{
                $flag = false;
            }

            $baseData[$val['Id']] = array(
                "title"=>$val['attrname'],
                "id"=>$val['Id'],
                "pid"=>$val['pid'],
                "spread"=>false,
                "checked"=>$flag
            );
        }
        $tree = array();
        unset($val);
        foreach ($baseData as $key=>$val){
            if(isset($baseData[$val['pid']])){
                $baseData[$val['pid']]['children'][] = &$baseData[$key];
            }else{
                $tree[] = &$baseData[$key];
            }
        }
        return $tree;
    }
    //新闻列表数据
    public function newList(){

        if($result = CI_Redis::get_instance()->redis->get('newInfos')){
            $data = json_decode($result,true);
            $result = array("newData"=>$data['data']);
            $this->load->view('manage/resource/newList', $result);
        }else{
            $result = self::resApi();
            CI_Redis::get_instance()->redis->setex('newInfos',7200,json_encode($result));
            $result = array("newData"=>$result['data']);
            $this->load->view('manage/resource/newList', $result);
        }
    }
    //发布新闻数据
    public function pushNew(){
        $post = $this->input->post();
        isset($_SESSION["UserId"])||self::outMessage("404","登录失效！",0);
        self::format_array($post['data']);
        $data = array();
        if(!empty($post['data'])){
            unset($val);
            foreach ($post['data'] as $key=>&$val){
                $data[$val]=1;
            }
            //标签数据
            $filterSignData = array();
            $signData = Res_model::get_instance()->get_sign_attr();
            unset($val);
            foreach ($signData as $key=>&$val){
                $filterSignData[$val['attrname']]=1;
            }
            //添加标签数据
            $addSignData = array();
            //资源数据
            $result = CI_Redis::get_instance()->redis->get('newInfos');
            $allData = json_decode($result,true);
            $params = array();
            unset($val);
            foreach ($allData['data'] as $key=>&$val){
                if(isset($val['id'])&&isset($data[$val['id']])){
                    $fileName = "2019_".date("His",time())."_".rand(1111,9999).'.log';
                    $path = base_url("/resource/uploads/resLog/");
                    if (!is_dir($path)){ //判断目录是否存在 不存在就创建
                        mkdir($path,0777,true);
                    }
                    $num = file_put_contents ($path.$fileName,serialize($val['html']));
                    if($num>0){
                        $params[] = array(
                            "title"=>$val['title'],
                            "content"=>$path.$fileName,
                            "createId"=>$_SESSION['UserId'],
                            "createTime"=>time(),
                            "photo"=>'{"coverLogoOne":"","coverLogoTwo":"","coverLogoThree":""}',
                            "attrLog"=>'',
                            "origin"=>$val['appName'],
                            "notice"=>$val['content'],
                            "photoType"=>0,
                            "signLog"=>'',
                            "type"=>5
                        );
                        foreach ($val['topkeyword'] as $k=>$v){
                            //只保留前20个
                            if($k>=20){
                                break;
                            }
                            if(!isset($filterSignData[$v])){
                                $addSignData[]= array(
                                    'pid' => 0,
                                    'attrname' =>$v,
                                    'type'=>1,
                                    'createTime' => time()
                                );
                            }
                        }
                    }
                    unset($allData['data'][$key]);
                }
            }
            CI_Redis::get_instance()->redis->setex('newInfos',7200,json_encode($allData));
            self::outMessage("200","发布成功！",Res_model::get_instance()->insert_news_branch($params,$addSignData));
        }else{
            self::outMessage("404","请选择数据！",0);
        }
    }
    //格式化数字逻辑
    private function formatNum(&$num){
//        if($num>=1000){
//            $num = intval(($num/100))*100;
//        }
//        else{
//            $num = intval(($num/10))*10;
//        }
    }
}
