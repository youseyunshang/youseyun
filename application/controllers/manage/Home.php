<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Home_model');
    }
    //后台首页
    public function indexDefault(){

        if( self::$allow>0 ){
            self::url_permission();
            $result = array(
                "realname"=>$_SESSION['realname'],
                "permission"=>unserialize($_SESSION['content'])
            );
            $this->load->view('manage/home/indexDefault',$result);
        }else{
            $data = array();
            $this->load->view('manage/login/login', $data);
        }
    }
    /**
     * indexData
     * @notice 后台首页加载数据
     * @return	void
     */
    public function indexData(){
        $this->load->model('Order_model');
        $homeOrderData = Order_model::get_instance()->get_order_all(array("status"=>1));
        //本周成单数量
        $weekNum = 0;
        $weekTime = strtotime(date("Y-m-d",time()))-604800;
        //日成单数量
        $dayNum = 0;
        $dayTime = strtotime(date("Y-m-d",time()));
        foreach ($homeOrderData as $key=>&$val){
            if($val['createTime']>$weekTime){
                $weekNum += $val['num'];
            }
            if($val['createTime']>$dayTime){
                $dayNum += $val['num'];
            }
        }
        $result = array(
            "data"=>'',
            "usercount"=>Home_model::get_instance()->tableRows("cbc_user"),
            "questioncount"=>Home_model::get_instance()->tableRows("cbc_question"),
            "purchasecount"=>Home_model::get_instance()->tableRows("cbc_purchase"),
            "supplecount"=>Home_model::get_instance()->tableRows("cbc_supple"),
            "ordercount"=>Home_model::get_instance()->tableRows("cbc_order"),
            "admincount"=>Home_model::get_instance()->tableRows("cbc_admin"),
            "weekNum"=>$weekNum,
            "dayNum"=>$dayNum
        );
        $this->load->view('manage/home/indexData',$result);
    }
    //修改密码
    public function passWord(){
        $post = $this->input->post();
        if(!empty($post)){
            $data = array(
                'pwd'=>md5(md5($post['pwd']))
            );
            Home_model::get_instance()->modify_password($data);
        }else{
            $result = array();
            $this->load->view('manage/home/password',$result);
        }
    }
    //系统设置
    public function systemInfo(){
        $result = array(
        );
        $this->load->view('manage/home/systemInfo',$result);
    }
    //退出登录
    public function logoutUser(){
        self::logout();
    }
    //上传图片
    public function upload($params="userlogo"){

        $config['upload_path']      = getcwd().'/resource/uploads/'.$params;
        $config['allowed_types']    = 'gif|jpg|png|xlsx|xls';
        $config['encrypt_name']    = true;
        $config['max_size']     = 2048;
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }else{
            $data = array('upload_data' => $this->upload->data());
            echo json_encode($data);
        }
    }
}
