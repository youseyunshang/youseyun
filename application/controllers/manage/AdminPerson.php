<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//总个数缓存方式 优化
class AdminPerson extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Role_model');
        $this->load->model('Login_model');
    }
    /**
     * addAdminPerson
     *
     * 添加后台用户
     *
     * @param  $params>0 添加 $params = 0 展示
     * @return	添加后台用户
     * @anthor Json.Wang
     */
    public function addAdminPerson($params = 0){
        if(self::$allow>0){
            if( $params > 0 ){
                $post = $this->input->post();
                if(Login_model::get_instance()->inset_userinfos($post)){
                    $result = Login_model::get_instance()->get_userinfos();
                    $data = array("count"=>count($result));
                    $this->load->view('manage/adminPerson/adminPersonList', $data);
                }
            }else{
                $this->load->model('Attribute_model');
                $attr = Attribute_model::get_instance()->get_attr();
                $this->load->model('Attribute_model');
                $data = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>2));
                $tree = self::createTree($data);
                $data = Role_model::get_instance()->get_role();
                $admin = Login_model::get_instance()->get_userinfos();
                unset($val);
                foreach ($data as $key=>&$val){
                    unset($val['content']);
                    unset($val['createtime']);
                }
                $result = array();
                $result['role'] = $data;
                $result['metal'] = json_encode($tree);
                $result['admin'] = $admin;
                $this->load->view('manage/adminPerson/addAdminPerson', $result);
            }
        }else{
            $data = array();
            $this->load->view('manage/login/login', $data);
        }
    }
    /**
     * adminList
     *
     * 管理员列表
     *
     * @param  $params=array()
     * @return	管理员列表
     * @anthor Json.Wang
     */
    public function adminList($params=array()){
        $post = $this->input->post();
        $result = Login_model::get_instance()->get_userinfos();
        if(empty($post)){
            $role = Login_model::get_instance()->get_role();
            foreach ($role as $key=>&$val){
                unset($val['content']);
                unset($val['createTime']);
            }
            $data = array("count"=>count($result),"roles"=>$role);
            $this->load->view('manage/adminPerson/adminPersonList', $data);
        }else{//AJAX加载数据
            if(isset($post['selectName'])){
                $selectName = $post['selectName'];
            }else{
                $selectName = '';
            }
            unset($val);
            foreach($result as $key=>&$val){
                //名称检索
                if(!empty($selectName)){
                        if(preg_match("/".$selectName."/i", $val['realname'])||preg_match("/".$selectName."/i", $val['username'])||preg_match("/".$selectName."/i", $val['tel'])){
                        }else{
                            unset($result[$key]);
                        }
                }
                //角色检索
                if(!empty($post['role'])&&$post['role']!=$val['roleId']){
                    unset($result[$key]);
                }
            }

            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }
    }
    //更新管理员
    //$type=0 查看 $type=1 更新
    public function updateAdmin($type=0){
        if($type>0){
            $post = $this->input->post();
            self::format_input($post['saleman']);
            if(Login_model::get_instance()->update_userinfos($post)){
                if($post['UserId']==$_SESSION['UserId']){
                    //个人中心，修改密码进入
                    echo "修改成功！";
                }else{
                    $result = Login_model::get_instance()->get_userinfos();
                    $data = array("count"=>count($result));
                    $this->load->view('manage/adminPerson/adminPersonList', $data);
                }
            }
        }else{
            $get = $this->input->get();
            $adminInfo = Login_model::get_instance()->get_userinfos($get['username']);
            $this->load->model('Attribute_model');
            $data = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>2));
            $select = array();
            if($params = explode(",",$adminInfo['attrId'])){
                foreach ($params as $val){
                    $select[] = array(
                        "Id"=>$val
                    );
                }
            }
            $tree = self::createTree($data,$select);
            $data = Role_model::get_instance()->get_role();
            unset($val);
            foreach ($data as $key=>&$val){
                unset($val['content']);
                unset($val['createTime']);
            }
            $result = array();
            $result['adminInfo'] = $adminInfo;
            $result['role'] = $data;
            $result['metal'] = json_encode($tree);
            $this->load->view('manage/adminPerson/updateAdminPerson', $result);
        }
    }
    /**
     * delAdmin
     *
     * 删除管理员
     *
     * @param
     * @return	删除管理员
     * @anthor Json.Wang
     */
    //
    public function delAdmin(){
        //做删除操作验证
        self::del_valid('/AdminPerson/DelAdmin');
        $post = $this->input->post();
        echo Login_model::get_instance()->del_userinfos($post['Id']);
    }
    //业务员管理
    public function salemanList(){
        $post = $this->input->post();
        $result = Login_model::get_instance()->get_userinfos();

        unset($val);
        foreach ($result as $key=>&$val){
            if($val['rolename']!="业务员"){
                unset($result[$key]);
            }
        }
        if(empty($post)){
            $data = array("count"=>count($result));
            $this->load->view('manage/adminPerson/salemanList', $data);
        }else{//AJAX加载数据
            $response = array_slice($result,($post['curr']-1)*$post['limit'],$post['limit']);
            echo json_encode($response);
        }
    }
    /**
     * createTree
     *
     * 生成树结构
     *
     * @param  $data = "排序结构数组" $attrData= "选中的数组"
     * @return	接口统一调用
     * @anthor Json.Wang
     */
    //生成树结构
    public function createTree($data=array(),$attrData=array()){
        $saveAttr = array();
        if(!empty($attrData)){
            foreach ($attrData as $key=>$val){
                $saveAttr[$val['Id']] = $val;
            }
        }
        $baseData = array();
        foreach ($data as $val){
            if($val['pid']>0){
                $flag = isset($saveAttr[$val['Id']])?true:false;
            }else{
                $flag = false;
            }

            $baseData[$val['Id']] = array(
                "title"=>$val['attrname'],
                "id"=>$val['Id'],
                "pid"=>$val['pid'],
                "spread"=>false,
                "checked"=>$flag
            );
        }
        $tree = array();
        unset($val);
        foreach ($baseData as $key=>$val){
            if(isset($baseData[$val['pid']])){
                $baseData[$val['pid']]['children'][] = &$baseData[$key];
            }else{
                $tree[] = &$baseData[$key];
            }
        }
        return $tree;
    }
}
