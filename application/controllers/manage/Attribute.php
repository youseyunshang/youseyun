<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Attribute extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Attribute_model');
    }
    //固有属性
    public function index(){
        $data = Attribute_model::get_instance()->get_attr();
        $tree = self::createTree($data);
        $result = array("metal"=>json_encode($tree));
        $this->load->view('manage/attr/attrDefine', $result);
    }
    //金属属性
    public function getMetalAttr(){
        $result = Attribute_model::get_instance()->get_metal_attr();
        var_dump($result);
    }
    //统一修改暂时屏蔽
    public function saveAttr(){
        $post = $this->input->post();
        $insert = array();
        foreach ($post as $key=>&$val){
            if(!empty($val)){
                switch ($key){
                    case "del":
                        unset($v);
                        foreach($val as $k=>&$v){
                            Attribute_model::get_instance()->del_attr($v['id']);
                        }
                        break;
                    case "add":
                        unset($v);
                        foreach($val as $k=>&$v){
                            $data= array(
                                'pid' => $v['pId'],
                                'attrname' =>$v['name'],
                                'type'=>0,
                                'createTime' => time()
                            );
                            $insert[$v['id']] = Attribute_model::get_instance()->save_attr($data);
                        }

                        break;
                    case "update":
                        unset($v);
                        foreach($val as $k=>&$v){
                                $data = array(
                                    "Id"=>$insert[$v['id']],
                                    'pid' => $v['pId'],
                                    'attrname' =>$v['name'],
                                    'type'=>0,
                                    'createTime' => time()
                                );
                            Attribute_model::get_instance()->update_attr($data);
                        }
                        break;
                }
            }
        }
        echo 1;
    }
    //新写法写属性添加-信息type=0
    public function changgeAttr(){
        $post = $this->input->post();
        switch ($post['type']){
            case "add":
                $data= array(
                    'pid' => $post['Id'],
                    'attrname' =>'未命名',
                    'type'=>0,
                    'createTime' => time()
                );
                echo Attribute_model::get_instance()->save_attr($data);
                break;
            case "del":
                echo Attribute_model::get_instance()->del_attr($post['Id']);
                break;
            case "update":
                $data = array(
                    "Id"=>$post['Id'],
                    'attrname' =>$post['attrname'],
                    'type'=>0,
                    'createTime' => time()
                );
                echo Attribute_model::get_instance()->update_attr($data);
                break;
        }
    }
    //商城属性
    public function attrDefineShop(){
        $data = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>2));
        $tree = self::createTree($data);
        $result = array("metal"=>json_encode($tree));
        $this->load->view('manage/attr/attrDefineShop', $result);
    }
    //生成树结构
    public function createTree($data){
        $baseData = array();
        foreach ($data as $val){
            $baseData[$val['Id']] = array(
                "title"=>$val['attrname'],
                "id"=>$val['Id'],
                "pid"=>$val['pid'],
                "spread"=>true
            );
        }
        $tree = array();
        unset($val);
        foreach ($baseData as $key=>$val){
            if(isset($baseData[$val['pid']])){
                $baseData[$val['pid']]['children'][] = &$baseData[$key];
            }else{
                $tree[] = &$baseData[$key];
            }
        }
        return $tree;
    }
    //新写法写属性添加-信息type=0
    public function changgeAttrShop(){
        $post = $this->input->post();
        switch ($post['type']){
            case "add":
                $data= array(
                    'pid' => $post['Id'],
                    'attrname' =>'未命名',
                    'type'=>2,
                    'createTime' => time()
                );
                echo Attribute_model::get_instance()->save_attr($data);
                break;
            case "del":
                echo Attribute_model::get_instance()->del_attr($post['Id']);
                break;
            case "update":
                $data = array(
                    "Id"=>$post['Id'],
                    'attrname' =>$post['attrname'],
                    'type'=>2,
                    'createTime' => time()
                );
                echo Attribute_model::get_instance()->update_attr($data);
                break;
        }
    }
    //铁合金属性
    public function attrProgressShop(){
        $data = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>3));
        $tree = self::createTree($data);
        $result = array("metal"=>json_encode($tree));
        $this->load->view('manage/attr/attrProgressShop', $result);
    }
    //新写法写属性添加-信息type=3 铁合金
    public function changgeAttrProgressShop(){
        $post = $this->input->post();
        switch ($post['type']){
            case "add":
                $data= array(
                    'pid' => $post['Id'],
                    'attrname' =>'未命名',
                    'type'=>3,
                    'createTime' => time()
                );
                echo Attribute_model::get_instance()->save_attr($data);
                break;
            case "del":
                echo Attribute_model::get_instance()->del_attr($post['Id']);
                break;
            case "update":
                $data = array(
                    "Id"=>$post['Id'],
                    'attrname' =>$post['attrname'],
                    'type'=>3,
                    'createTime' => time()
                );
                echo Attribute_model::get_instance()->update_attr($data);
                break;
        }
    }
    //报表选择数据
    public function selectAttrIframe(){
        $data = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>2));
        $tree = self::createTree($data);
        $result = array("metal"=>json_encode($tree));
        $this->load->view('manage/attr/selectAttrIframe', $result);
    }
    //牌号
    public function brandName(){
        $brandName = Attribute_model::get_instance()->get_brandname();
        $result = array(
            "brandNameData"=>$brandName
        );
        $this->load->view('manage/attr/brandName', $result);
    }
    //添加牌号
    public function addbrandName(){
        $post = $this->input->post();
        if(!empty($post['brandName'])&&!empty($post['attrId'])){
            $insert = array(
                'brandName' => $post['brandName'],
                'attrId' =>$post['attrId'],
                'attrname' =>$post['attrname']
            );
            if($result = Attribute_model::get_instance()->insert_brandname($insert)){
                self::outMessage("200","添加成功！",array("num"=>$result));
            }else{
                self::outMessage("404","插入有误！",array("num"=>0));
            }
        }else{
            self::outMessage("404","参数错误！",array("num"=>0));
        }
    }
    //修改牌号
    public function updatebrandName(){
        $post = $this->input->post();
        if(!empty($post['Id'])&&!empty($post['attrname'])){
            $update = array();
            if(!empty($post['brandName'])){
                $update['brandName'] = $post['brandName'];
            }
            if(!empty($post['attrId'])){
                $update['attrId'] = $post['attrId'];
            }
            if(!empty($post['attrname'])){
                $update['attrname'] = $post['attrname'];
            }
            if($result = Attribute_model::get_instance()->update_brandname($update,$post['Id'])){
                self::outMessage("200","修改成功！",array("num"=>$result));
            }else{
                self::outMessage("404","插入有误！",array("num"=>0));
            }
        }else{
            self::outMessage("404","参数错误！",array("num"=>0));
        }
    }
    //删除牌号
    public function delbrandName(){
        $post = $this->input->post();
        if(!empty($post['Id'])){
            if($result = Attribute_model::get_instance()->del_brandname($post['Id'])){
                self::outMessage("200","删除成功！",array("num"=>$result));
            }else{
                self::outMessage("404","插入有误！",array("num"=>0));
            }
        }else{
            self::outMessage("404","参数错误！",array("num"=>0));
        }
    }
    //粒度
    public function granularity(){
        $granularityData = Attribute_model::get_instance()->get_granularity();
        $result = array(
           "granularityData"=>$granularityData
        );
        $this->load->view('manage/attr/granularity', $result);
    }
    //添加粒度
    public function addGranularity(){
        $post = $this->input->post();
        if(!empty($post['granularity'])&&!empty($post['attrId'])){
            $insert = array(
                'granularity' => $post['granularity'],
                'attrId' =>$post['attrId'],
                'attrname' =>$post['attrname']
            );
            if($result = Attribute_model::get_instance()->insert_granularity($insert)){
                self::outMessage("200","添加成功！",array("num"=>$result));
            }else{
                self::outMessage("404","插入有误！",array("num"=>0));
            }
        }else{
            self::outMessage("404","参数错误！",array("num"=>0));
        }
    }
    //修改粒度
    public function updateGranularity(){
        $post = $this->input->post();
        if(!empty($post['Id'])&&!empty($post['attrname'])){
            $update = array();
            if(!empty($post['granularity'])){
                $update['granularity'] = $post['granularity'];
            }
            if(!empty($post['attrId'])){
                $update['attrId'] = $post['attrId'];
            }
            if(!empty($post['attrname'])){
                $update['attrname'] = $post['attrname'];
            }
            if($result = Attribute_model::get_instance()->update_granularity($update,$post['Id'])){
                self::outMessage("200","修改成功！",array("num"=>$result));
            }else{
                self::outMessage("404","插入有误！",array("num"=>0));
            }
        }else{
            self::outMessage("404","参数错误！",array("num"=>0));
        }
    }
    //删除粒度
    public function delGranularity(){
        $post = $this->input->post();
        if(!empty($post['Id'])){
            if($result = Attribute_model::get_instance()->del_granularity($post['Id'])){
                self::outMessage("200","删除成功！",array("num"=>$result));
            }else{
                self::outMessage("404","插入有误！",array("num"=>0));
            }
        }else{
            self::outMessage("404","参数错误！",array("num"=>0));
        }
    }
    //更新数据--暂时无用
    public function jiaoben(){
        //删除二级属性
        $shopData = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>2));
        $metalData = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>0));
        //var_dump("<pre>",$shopData,$metalData);die();
        //删除金属属性标签
        foreach ($metalData as $key=>$val){
            Attribute_model::get_instance()->del_attr($val['Id']);
        }
        //商城复制金属
        foreach ($shopData as $key=>$val){
            $val['idTwo'] = $val['Id'];
            $val['pidTwo'] = $val['pid'];
            $val['type']=0;
            $val['pid'] = 0;
            unset($val['Id']);
            Attribute_model::get_instance()->save_attr($val);
        }
    }
    //同步数据--暂时无用
    public function tongbu(){
        //删除二级属性
        $metalData = Attribute_model::get_instance()->get_attr(array("params"=>false,"type"=>0));
        $syncData = array();
        foreach ($metalData as $key=>&$val){
            if(isset($val['idTwo'])){
                $syncData[$val['idTwo']] = $val;
            }
        }
        foreach ($metalData as $key=>$val){
            if(isset($val['pidTwo'])&&$val['pidTwo']>0){
                $data = array(
                    "Id"=>$val['Id'],
                    'pid'=>$syncData[$val['pidTwo']]['Id']
                );
                Attribute_model::get_instance()->update_attr($data);
            }
        }
    }

}
