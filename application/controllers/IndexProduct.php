<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class IndexProduct extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Supple_model');
        $this->load->model('Purchase_model');
    }
    /**
     * index
     * @notice 商城首页加载数据
     * @return	void
     */
    public function index(){
		//今日布告数据
		$todayPublish = array();
		self::todayPublish($todayPublish);
		//滚屏数据
		$weekData = array();
		self::weekData($weekData);
		$newSupple = self::newSupple();
		
        $result = array(
			'todayPublish'=>json_encode($todayPublish),
			'weekData'=>json_encode($weekData),
			'newSupple'=>json_encode($newSupple)
		);
        $this->load->view('product/shopDefault', $result);
    }
    /**
     * todayPublish
     * @params &$data
     * @notice 商城首页中 - 今日布告
     * @return	void
     * @anthor Json.Wang
     */
	private function todayPublish(&$data=array()){
		$data = array("陈**,成交了一笔有机硅胶订单！","陈**,成交了一笔铁合金订单！","陈**,成交了一笔高碳烙铁订单！");
	}
    /**
     * weekData
     * @params &$data
     * @notice 商城首页中 - 成交量
     * @return	void
     * @anthor Json.Wang
     */
	private function weekData(&$data=array()){
		$data = array(array("message"=>"本周订单量","num"=>100),array("message"=>"本周成交量","num"=>10));
	}
    /**
     * weekData
     * @notice 商城首页中 - 最新供应
     * @return	void
     * @anthor Json.Wang
     */
	private function newSupple(){
		$result = array();
		if($data = Supple_model::get_instance()->get_order_where(array("status"=>1))){
			unset($val);
			foreach($data as $key=>$val){
				if($key<3){
                    //清除不必要字段
                    $result[] = array(
                        "Id"=>$val["Id"],
                        "productPhoto"=>$val["productPhoto"],
                        "title"=>$val["title"],
                        "num"=>$val["num"],
                        "address"=>$val["location"],
                        "minOrderNum"=>$val["minOrderNum"],
                        "unitPrice"=>$val["unitPrice"]
                    );
				}else{
					break;
				}
			}
		}
		return $result;
	}
    //发布产品
    public function createProductFirst(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            echo CI_Redis::get_instance()->redis->setex('createProductFirst_'.$_SESSION["indexUserData"]["Id"],3600,$request_body);
        }else{
            $shop = self::shopAttrData();
            //是否先前有数据
            $result = array(
                "shopFirst"=>json_encode($shop['shopFirst']),
                "shopSon"=>json_encode($shop['shopSon']),
                "shopStatus"=>json_encode($shop['shopStatus']),
                "preData"=>json_encode(array())
            );
            if($createProductFirstData = CI_Redis::get_instance()->redis->get('createProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                $result["preData"] = $createProductFirstData;
            }
            $this->load->view('product/createProductFirst', $result);
        }
    }
    public function createProductSecond(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            //判断是否完成第二部分
            if(!CI_Redis::get_instance()->redis->exists('createProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                self::outMessage("404","请完成上一步操作！");
            }
            self::outMessage("200","请求成功！",CI_Redis::get_instance()->redis->setex('createProductSecond_'.$_SESSION["indexUserData"]["Id"],3600,$request_body));
        }else{
            if(!CI_Redis::get_instance()->redis->exists('createProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                $url = ROOT_INDEX_REQUEST."/IndexProduct/createProductFirst";
                header("Location:$url");
            }else{
                $rData = CI_Redis::get_instance()->redis->get('createProductFirst_'.$_SESSION["indexUserData"]["Id"]);
                $firstData = json_decode($rData,true);
                if(!empty($firstData['attr'][2]['shopThirdId'])){
                    $productAttr = $firstData['attr'][2]['shopThirdId'];
                }elseif (!empty($firstData['attr'][1]['shopSecondId'])){
                    $productAttr = $firstData['attr'][1]['shopSecondId'];
                }else{
                    $productAttr = $firstData['attr'][0]['shopFirstId'];
                }
                $this->load->model('Attribute_model');
                $granularity = Attribute_model::get_instance()->get_granularity(array("attrId"=>$productAttr));
                $granularityList = array();
                if(!empty($granularity)){
                    unset($val);
                    foreach ($granularity as $key=>&$val){
                        $granularityList[]=array(
                            'id'=>$val['Id'],
                            'name'=>$val['granularity']
                        );
                    }
                }
                $brandName = Attribute_model::get_instance()->get_brandname(array("attrId"=>$productAttr));
                $brandNameList = array();
                if(!empty($brandName)){
                    unset($val);
                    foreach ($brandName as $key=>&$val){
                        $brandNameList[]=array(
                            'id'=>$val['Id'],
                            'name'=>$val['brandName']
                        );
                    }
                }
            }
            //是否先前有数据
             $result = array(
                "preData"=>json_encode(array()),
                "granularityList"=>empty($granularityList)?json_encode(array()):json_encode($granularityList),
                "brandNameList"=>empty($brandNameList)?json_encode(array()):json_encode($brandNameList)
            );
            if($Data = CI_Redis::get_instance()->redis->get('createProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                $result['preData'] = $Data;
            }
            $this->load->view('product/createProductSecond', $result);
        }
    }
    public function createProductThird(){
        //判断是否登录
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        //判断是否完成第二部分
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            if(!CI_Redis::get_instance()->redis->exists('createProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                self::outMessage("404","请完成上一步操作！");
            }
            self::outMessage("200","请求成功！",CI_Redis::get_instance()->redis->setex('createProductThird_'.$_SESSION["indexUserData"]["Id"],3600,$request_body));
        }else{
            if(!CI_Redis::get_instance()->redis->exists('createProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                $url = ROOT_INDEX_REQUEST."/IndexProduct/createProductSecond";
                header("Location:$url");
            }
            //是否先前有数据
            $result = array(
                "preData"=>json_encode(array())
            );
            if($Data = CI_Redis::get_instance()->redis->get('createProductThird_'.$_SESSION["indexUserData"]["Id"])){
                $result['preData'] = $Data;
            }
            $this->load->view('product/createProductThird', $result);
        }
    }
    public function createProductFinish(){
	
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            //判断是否完成第二部分
            if(!CI_Redis::get_instance()->redis->exists('createProductThird_'.$_SESSION["indexUserData"]["Id"])){
                self::outMessage("404","请完成上一步操作！");
            }
            $jsonData = array();
            $params = array();
            //第一部分
            if($data= CI_Redis::get_instance()->redis->get('createProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                $jsonData['createProductFirst_'.$_SESSION["indexUserData"]["Id"]] = $data;
                $FirstData = json_decode($data,true);
                $params['attrAll'] = !empty($FirstData['attr'])?json_encode($FirstData['attr']):"";
                $params['attrId'] = !empty($FirstData['shopStatus'][0]['shopStatusId'])?$FirstData['shopStatus'][0]['shopStatusId']:"";
                $params['companyType'] = !empty($FirstData['comType'][0]['comTypeId'])?$FirstData['comType'][0]['comTypeId']:"";
            }

            //第二部分
            if($data= CI_Redis::get_instance()->redis->get('createProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                $jsonData['createProductSecond_'.$_SESSION["indexUserData"]["Id"]] = $data;
                $SecondData = json_decode($data,true);
                self::reNamePicture($SecondData['productPhoto']);
                $params['productPhoto'] = $SecondData['productPhoto'];
				$params['title'] = !empty($SecondData['title'])?$SecondData['title']:"";
                $params['content'] = !empty($SecondData['content'])?$SecondData['content']:"";
                $params['num'] = !empty($SecondData['num'])?$SecondData['num']:"";
                $params['minOrderNum'] = !empty($SecondData['minOrderNum'])?$SecondData['minOrderNum']:"";
                $params['unitPrice'] = !empty($SecondData['unitPrice'])?$SecondData['unitPrice']:"";
                //$params['granularity'] = !empty($SecondData['granularity'])?$SecondData['granularity']:"";
                $params['distributionMode'] = !empty($SecondData['distributionMode'])?$SecondData['distributionMode']:"";
                $params['invoice'] = !empty($SecondData['invoice'])?$SecondData['invoice']:"";
				//产品产地
				$params['placeOrigin'] = !empty($SecondData['ProductOrigin'])?$SecondData['ProductOrigin']:"";
				//产品所在地
				$params['location'] = !empty($SecondData['ProductLoation'])?$SecondData['ProductLoation']:"";
				//粒度
				$params['granularity'] = !empty($SecondData['granularityType'])?$SecondData['granularityType']:"";
				//牌号
				$params['brandName'] = !empty($SecondData['brandNameType'])?$SecondData['brandNameType']:"";
				$params['otherParams'] = !empty($SecondData['otherParams'])?$SecondData['otherParams']:"";
				$params['startTime'] = time();
                $params['endTime'] = !empty($SecondData['endTime'])?strtotime($SecondData['endTime']):"";
            }
            //第三部分
            if($data= CI_Redis::get_instance()->redis->get('createProductThird_'.$_SESSION["indexUserData"]["Id"])){
                $jsonData['createProductThird_'.$_SESSION["indexUserData"]["Id"]] = $data;
                $ThirdData = json_decode($data,true);
                $params['hasqualitySheet'] = !empty($ThirdData['hasqualitySheet'])?$ThirdData['hasqualitySheet']:"";
                self::reNamePicture($ThirdData['qualityInspectionSheet']);
                $params['qualityInspectionSheet'] = $ThirdData['qualityInspectionSheet'];
                self::reNamePicture($ThirdData['warehouseReceipt']);
                $params['warehouseReceipt'] = $ThirdData['warehouseReceipt'];
                $params['reason'] =!empty($ThirdData['reason'])?$ThirdData['reason']:"";
            }
            //最后一步
            $lastData = json_decode($request_body,true);
            $params["billHolder"] = !empty($lastData['billHolder'])?$lastData['billHolder']:"";
            $params["tel"] = !empty($lastData['tel'])?$lastData['tel']:"";
            $params["email"] = !empty($lastData['email'])?$lastData['email']:"";
            $params["billHolderStatus"] = !empty($lastData['showBillHolder'])?intval($lastData['showBillHolder']):0;
            //谁创建的供应信息
            $params["userId"] = $_SESSION["indexUserData"]["Id"];
            //创建时间
            $params["createTime"] = time();
            //订单唯一编号
            if(CI_Redis::get_instance()->redis->exists('unqidCode_supple_'.$_SESSION["indexUserData"]["Id"])){
                $params["unqidCode"] = CI_Redis::get_instance()->redis->get('unqidCode_supple_'.$_SESSION["indexUserData"]["Id"]);
                $jsonData['unqidCode'] = $params["unqidCode"];
                $params['jsonData'] = serialize($jsonData);
                //更新供应信息
                if($a=Supple_model::get_instance()->update_Supple($params,$params["unqidCode"])){
                    //清除内存中的数据释放内存
                    $count = CI_Redis::get_instance()->redis->del('unqidCode_supple_'.$_SESSION["indexUserData"]["Id"],'createProductFirst_'.$_SESSION["indexUserData"]["Id"],'createProductSecond_'.$_SESSION["indexUserData"]["Id"],'createProductThird_'.$_SESSION["indexUserData"]["Id"]);
                    self::outMessage("200","请求成功！",$count);
                }else{
                    self::outMessage("404","插入数据库失败，请检查网络！",0);
                }
            }else{
                $params["unqidCode"] = "2019".date("His",time()).rand(1111,9999).$_SESSION["indexUserData"]["Id"];
                $jsonData['unqidCode'] = $params["unqidCode"];
                $params['jsonData'] = serialize($jsonData);
            }
            //发布商品信息
            if($a=Supple_model::get_instance()->insert_Supple($params)){
                //清除内存中的数据释放内存
                $count = CI_Redis::get_instance()->redis->del('createProductFirst_'.$_SESSION["indexUserData"]["Id"],'createProductSecond_'.$_SESSION["indexUserData"]["Id"],'createProductThird_'.$_SESSION["indexUserData"]["Id"]);
                self::outMessage("200","请求成功！",$count);
            }else{
                self::outMessage("404","插入数据库失败，请检查网络！",0);
            }
        }else{
            //判断是否完成第二部分
            if(!CI_Redis::get_instance()->redis->exists('createProductThird_'.$_SESSION["indexUserData"]["Id"])){
                $url = ROOT_INDEX_REQUEST."/IndexProduct/createProductThird";
                header("Location:$url");
            }
            $result = array();
            $this->load->view('product/createProductFinish', $result);
        }
    }
    public function updateCreateProduct(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            //最后一步
            $allData = json_decode($request_body,true);
            if(!empty($allData['type'])&&!empty($allData['Id'])){
                switch ($allData['type']){
                    case "supple":
                        if($productData = Supple_model::get_instance()->get_order_where(array("Id"=>$allData['Id']))){
                            $resultData = unserialize($productData[0]['jsonData']);
                            foreach ($resultData as $key=>&$val){
                                if($key=="unqidCode"){
                                    CI_Redis::get_instance()->redis->setex($key."_supple_".$_SESSION["indexUserData"]["Id"],3600,$val);
                                }else{
                                    CI_Redis::get_instance()->redis->setex($key,3600,$val);
                                }
                            }
                            self::outMessage("200","请求成功！",array());
                        }else{
                            self::outMessage("404","数据库中查询不到对应数据！");
                        }
                        break;
                    case "purchase":
                        if($productData = Purchase_model::get_instance()->get_order_where(array("Id"=>$allData['Id']))){
                            $resultData = unserialize($productData[0]['jsonData']);
                            foreach ($resultData as $key=>&$val){
                                if($key=="unqidCode"){
                                    CI_Redis::get_instance()->redis->setex($key."_purchase_".$_SESSION["indexUserData"]["Id"],3600,$val);
                                }else{
                                    CI_Redis::get_instance()->redis->setex($key,3600,$val);
                                }
                            }
                            self::outMessage("200","请求成功！",array());
                        }else{
                            self::outMessage("404","数据库中查询不到对应数据！");
                        }
                        break;
                    default:
                        break;
                }
            }else{
                self::outMessage("404","具体参数存在问题请检查！");
            }
        }
    }
    //采购产品
    public function getProductFirst(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            echo CI_Redis::get_instance()->redis->setex('getProductFirst_'.$_SESSION["indexUserData"]["Id"],3600,$request_body);
        }else{
            $shop = self::shopAttrData();
            //是否先前有数据
            $result = array(
                "shopFirst"=>json_encode($shop['shopFirst']),
                "shopSon"=>json_encode($shop['shopSon']),
                "shopStatus"=>json_encode($shop['shopStatus']),
                "preData"=>json_encode(array())
            );
            if($createProductFirstData = CI_Redis::get_instance()->redis->get('getProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                $result["preData"] = $createProductFirstData;
            }
            $this->load->view('product/getProductFirst', $result);
        }
    }
    public function getProductSecond(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            //判断是否完成第二部分
            if(!CI_Redis::get_instance()->redis->exists('getProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                self::outMessage("404","请完成上一步操作！");
            }
            self::outMessage("200","请求成功！",CI_Redis::get_instance()->redis->setex('getProductSecond_'.$_SESSION["indexUserData"]["Id"],3600,$request_body));
        }else{
            if(!CI_Redis::get_instance()->redis->exists('getProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                $url = ROOT_INDEX_REQUEST."/IndexProduct/getProductFirst";
                header("Location:$url");
            }else{
                $rData = CI_Redis::get_instance()->redis->get('getProductFirst_'.$_SESSION["indexUserData"]["Id"]);
                $firstData = json_decode($rData,true);

                if(!empty($firstData['attr'][2]['shopThirdId'])){
                    $productAttr = $firstData['attr'][2]['shopThirdId'];
                }elseif (!empty($firstData['attr'][1]['shopSecondId'])){
                    $productAttr = $firstData['attr'][1]['shopSecondId'];
                }else{
                    $productAttr = $firstData['attr'][0]['shopFirstId'];
                }
                $this->load->model('Attribute_model');
                $granularity = Attribute_model::get_instance()->get_granularity(array("attrId"=>$productAttr));
                $granularityList = array();
                if(!empty($granularity)){
                    unset($val);
                    foreach ($granularity as $key=>&$val){
                        $granularityList[]=array(
                            'id'=>$val['Id'],
                            'name'=>$val['granularity']
                        );
                    }
                }
                $brandName = Attribute_model::get_instance()->get_brandname(array("attrId"=>$productAttr));
                $brandNameList = array();
                if(!empty($brandName)){
                    unset($val);
                    foreach ($brandName as $key=>&$val){
                        $brandNameList[]=array(
                            'id'=>$val['Id'],
                            'name'=>$val['brandName']
                        );
                    }
                }
            }
            //是否先前有数据
            $result = array(
                "preData"=>json_encode(array()),
                "granularityList"=>empty($granularityList)?json_encode(array()):json_encode($granularityList),
                "brandNameList"=>empty($brandNameList)?json_encode(array()):json_encode($brandNameList)
            );
            if($Data = CI_Redis::get_instance()->redis->get('getProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                $result['preData'] = $Data;
            }
            $this->load->view('product/getProductSecond', $result);
        }

    }
    public function getProductFinish(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            //判断是否完成第二部分
            if(!CI_Redis::get_instance()->redis->exists('getProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                self::outMessage("404","请完成上一步操作！");
            }
            $jsonData = array();
            $params = array();
            //第一部分
            if($data= CI_Redis::get_instance()->redis->get('getProductFirst_'.$_SESSION["indexUserData"]["Id"])){
                $jsonData['getProductFirst_'.$_SESSION["indexUserData"]["Id"]] = $data;
                $FirstData = json_decode($data,true);
                $params['attrAll'] = !empty($FirstData['attr'])?json_encode($FirstData['attr']):"";
                $params['attrId'] = !empty($FirstData['shopStatus'][0]['shopStatusId'])?$FirstData['shopStatus'][0]['shopStatusId']:"";
                $params['companyType'] = !empty($FirstData['comType'][0]['comTypeId'])?$FirstData['comType'][0]['comTypeId']:"";
            }
            //第二部分
            if($data= CI_Redis::get_instance()->redis->get('getProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                $jsonData['getProductSecond_'.$_SESSION["indexUserData"]["Id"]] = $data;
                $SecondData = json_decode($data,true);
                $params['title'] = !empty($SecondData['productName'])?$SecondData['productName']:"";
                $params['content'] = !empty($SecondData['productNotice'])?$SecondData['productNotice']:"";
                $params['num'] = !empty($SecondData['num'])?$SecondData['num']:"";
                $params['unitPrice'] = !empty($SecondData['price'])?$SecondData['price']:"";
                $params['location'] = !empty($SecondData['area'])?$SecondData['area']:"";
                $params['distributionMode'] = !empty($SecondData['distributionMode'])?$SecondData['distributionMode']:"";
                $params['invoice'] = !empty($SecondData['invoice'])?$SecondData['invoice']:"";
                $params['endTime'] = !empty($SecondData['endTime'])?strtotime($SecondData['endTime']):"";
            }
            //最后一步
            $lastData = json_decode($request_body,true);
            $params["billHolder"] = !empty($lastData['billHolder'])?$lastData['billHolder']:"";
            $params["tel"] = !empty($lastData['tel'])?$lastData['tel']:"";
            $params["email"] = !empty($lastData['email'])?$lastData['email']:"";
            $params["billHolderStatus"] = !empty($lastData['showBillHolder'])?intval($lastData['showBillHolder']):0;
            //谁创建的供应信息
            $params["userId"] = $_SESSION["indexUserData"]["Id"];
            //创建时间
            $params["createTime"] = time();
            //订单唯一编号
            if(CI_Redis::get_instance()->redis->exists('unqidCode_purchase_'.$_SESSION["indexUserData"]["Id"])){
                $params["unqidCode"] = CI_Redis::get_instance()->redis->get('unqidCode_purchase_'.$_SESSION["indexUserData"]["Id"]);
                $jsonData['unqidCode'] = $params["unqidCode"];
                $params['jsonData'] = serialize($jsonData);
                //更新供应信息
                if($a=Purchase_model::get_instance()->update_Purchase($params,$params["unqidCode"])){
                    //清除内存中的数据释放内存
                    $count = CI_Redis::get_instance()->redis->del('unqidCode_purchase_'.$_SESSION["indexUserData"]["Id"],'createProductFirst_'.$_SESSION["indexUserData"]["Id"],'createProductSecond_'.$_SESSION["indexUserData"]["Id"],'createProductThird_'.$_SESSION["indexUserData"]["Id"]);
                    self::outMessage("200","请求成功！",$count);
                }else{
                    self::outMessage("404","插入数据库失败，请检查网络！",0);
                }
            }else{
                $params["unqidCode"] = "2019".date("His",time()).rand(1111,9999).$_SESSION["indexUserData"]["Id"];
                $jsonData['unqidCode'] = $params["unqidCode"];
                $params['jsonData'] = serialize($jsonData);
            }
            //订单唯一编号

            //发布商品信息
            if($a=Purchase_model::get_instance()->insertPurchase($params)){
                //清除内存中的数据释放内存
                $count = CI_Redis::get_instance()->redis->del('getProductFirst_'.$_SESSION["indexUserData"]["Id"],'getProductSecond_'.$_SESSION["indexUserData"]["Id"]);
                self::outMessage("200","请求成功！",$count);
                exit();
            }else{
                self::outMessage("404","插入数据库失败，请检查网络！",0);
            }
        }else{
            //判断是否完成第二部分
            if(!CI_Redis::get_instance()->redis->exists('getProductSecond_'.$_SESSION["indexUserData"]["Id"])){
                $url = ROOT_INDEX_REQUEST."/IndexProduct/getProductSecond";
                header("Location:$url");
            }
            $result = array();
            $this->load->view('product/getProductFinish', $result);
        }
    }
	//供应产品详情
	public function productInfo($params=0){
		//获取当前详情数据
        //供应通过的数据
        if( $Data = Supple_model::get_instance()->get_order_where(array("status"=>1))){
            unset($val);
            foreach ($Data as $key=>&$val){
                if($val['Id']==$params){
                    $Supple = $val;
                    self::formatShopData($Supple);
                    break;
                }
            }
            //产品属性
            $firstData = json_decode($Supple['attrAll'],true);
            if(!empty($firstData[2]['shopThirdId'])){
                $productAttr = $firstData[2]['shopThirdId'];
            }elseif (!empty($firstData[1]['shopSecondId'])){
                $productAttr = $firstData[1]['shopSecondId'];
            }else{
                $productAttr = $firstData[0]['shopFirstId'];
            }
            $shopManager = self::shopManager($productAttr);
            //根据属性获取数据
            $forYouData = array();
            unset($val);
            foreach ($Data as $key=>&$val){
                if(preg_match("/".$productAttr."/i",$val['attrAll'])){
                    unset($val['attrAll']);
                    unset($val['jsonData']);
                    $forYouData[] = $val;
                }
                if(count($forYouData)>4){
                    break;
                }
            }
            //轮播的图片
            unset($Supple['attrAll']);
            unset($Supple['jsonData']);
            $result = array(
                "swiperPhone"=>array(),
                "data"=>json_encode($Supple),
                "forYouData"=>json_encode($forYouData),
                "shopManager"=>json_encode($shopManager)
            );
            $result['swiperPhone'][] = $Supple['productPhoto'];
            $result['swiperPhone'][] = $Supple['warehouseReceipt'];
            $result['swiperPhone'][] = $Supple['qualityInspectionSheet'];
            $result['swiperPhone'] = json_encode($result['swiperPhone']);
            $this->load->view('product/productInfo', $result);
        }else{
            self::outMessage("404","参数错误！",0);
        }
	}
    //供应产品详情
    public function purchaseInfo($params=0){
        //获取当前详情数据
        //供应通过的数据
        if( $Data = Purchase_model::get_instance()->get_order_where(array("status"=>1))){
            unset($val);
            foreach ($Data as $key=>&$val){
                if($val['Id']==$params){
                    $Purchase = $val;
                    self::formatShopData($Purchase);
                    break;
                }
            }
            //产品属性
            $firstData = json_decode($Purchase['attrAll'],true);
            if(!empty($firstData[2]['shopThirdId'])){
                $productAttr = $firstData[2]['shopThirdId'];
            }elseif (!empty($firstData[1]['shopSecondId'])){
                $productAttr = $firstData[1]['shopSecondId'];
            }else{
                $productAttr = $firstData[0]['shopFirstId'];
            }
            $shopManager = self::shopManager($productAttr);
            //根据属性获取数据
            $forYouData = array();
            unset($val);
            foreach ($Data as $key=>&$val){
                if(preg_match("/".$productAttr."/i",$val['attrAll'])){
                    unset($val['attrAll']);
                    $forYouData[] = $val;
                }
                if(count($forYouData)>4){
                    break;
                }
            }
            //轮播的图片
            unset($Purchase['attrAll']);
            $result = array(
                "swiperPhone"=>json_encode(array()),
                "data"=>json_encode($Purchase),
                "forYouData"=>json_encode($forYouData),
                "shopManager"=>json_encode($shopManager)
            );
            $this->load->view('product/purchaseInfo', $result);
        }else{
            self::outMessage("404","参数错误！",0);
        }
    }
	// 商城业务人员信息
    public function shopManager($attrStr=""){
        if($attrStr==""){
           return array();
        }else{
            $this->load->model('Login_model');
            $roleMan = Login_model::get_instance()->get_manager(array("roleId"=>21));
            if(!empty($roleMan)){
                $result = array();
                unset($val);
                foreach ($roleMan as $key=>&$val){
                    if(preg_match("/".$attrStr."/i",$val['attrId'])){
                        $result = $val;
                        break;
                    }
                }
                return empty($result)?$roleMan[0]:$result;
            }else{
                return array();
            }
        }
    }
    //质检委托
    public function logistics(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        $this->load->model('Company_model');
        if(!empty($request_body)) {
            $post = json_decode($request_body, true);
            if(!empty($post)){
                $post['ordernumber'] = "logistics".date("His",time()).rand(1111,9999).$_SESSION["indexUserData"]["Id"];
                $post['createTime'] = time();
                if(Company_model::get_instance()->insert_logistics_order($post)){
                    self::outMessage("200","请求成功！",array());
                }else{
                    self::outMessage("404","发送请求失败！",array("error"=>"检测数据库连接！"));
                }
            }else{
                self::outMessage("404","发送请求失败！",array("error"=>"检测BODY体内容！"));
            }
        }else{
            $get = $this->input->get();
            $companyData = array();
            if(!empty($get['id'])){
                $companyData = Company_model::get_instance()->get_cominfos_row($get['id']);
            }
            $result = array(
                'companyData'=>json_encode($companyData)
            );
            $this->load->view('product/logistics', $result);
        }
    }
    //物流委托
    public function qualityTesting(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        $this->load->model('Company_model');
        if(!empty($request_body)) {
            $post = json_decode($request_body, true);
            if(!empty($post)){
                $post['ordernumber'] = "qualityTesting".date("His",time()).rand(1111,9999).$_SESSION["indexUserData"]["Id"];
                $post['createTime'] = time();
                if(Company_model::get_instance()->insert_quality_testing_order($post)){
                    self::outMessage("200","请求成功！",array());
                }else{
                    self::outMessage("404","发送请求失败！",array("error"=>"检测数据库连接！"));
                }
            }else{
                self::outMessage("404","发送请求失败！",array("error"=>"检测BODY体内容！"));
            }
        }else{
            $get = $this->input->get();
            $companyData = array();
            if(!empty($get['id'])){
                $companyData = Company_model::get_instance()->get_cominfos_row($get['id']);
            }
            $result = array(
                'companyData'=>json_encode($companyData)
            );
            $this->load->view('product/qualityTesting', $result);
        }

    }
    //供求信息列表
    public function productList(){
        $get = $this->input->get();
        //滚屏数据
        $weekData = array();
        self::weekData($weekData);
        //属性
        $shop = self::shopAttrData();
        //牌号
        $brandNameList = self::brandNameInfo();
        $brandName = array();
        foreach ($brandNameList as $key=>$val){
            $brandName[$val['attrId']][]=$val;
        }
        //粒度
        $granularityList = self::granularityInfo();
        $granularity = array();
        foreach ($granularityList as $key=>$val){
            $granularity[$val['attrId']][]=$val;
        }
        //是否先前有数据
        $result = array(
            "shopFirst"=>json_encode($shop['shopFirst']),
            "shopSon"=>json_encode($shop['shopSon']),
            "shopStatus"=>json_encode($shop['shopStatus']),
            'weekData'=>json_encode($weekData),
            'where'=>json_encode($get),
            'brandName'=>json_encode($brandName),
            'granularity'=>json_encode($granularity)
        );
        $this->load->view('product/productList', $result);
    }
    /**
     * allShopData
     *
     * 请求获取数据-根据经常关注的商品属性
     * @location 1.商城首页 2.供求列表页
     * @param  $post = array(
                     "where"=>array(
                      "itemType"=>"supple",
                      "brandName"=>"牌号2",
                     "granularity"=>"粒度2",
                       "attrId"=>304,
                       "styleId"=>
                    ),
                 "page"=>1
              );
     * @return	用户请求的供求接口数据
     * @anthor Json.Wang
     */
    public function allShopData($params=1){
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            $post = json_decode($request_body,true);
            if(!empty($post["page"])){
                if($redisData = CI_Redis::get_instance()->redis->get('shopDefaultData')){
                    $data = json_decode($redisData,true);
                }else{
                    //供应通过的数据
                    $Supple = Supple_model::get_instance()->get_order_where(array("status"=>1));
                    unset($val);
                    foreach($Supple as $key=>&$val){
                        $val['itemType'] = "supple";
                    }
                    //采购通过的数据
                    $Purchase = Purchase_model::get_instance()->get_order_where(array("status"=>1));
                    unset($val);
                    foreach($Purchase as $key=>&$val){
                        $val['itemType'] = "purchase";
                    }
                    $data = array_merge($Supple,$Purchase);
                    $last_names = array_column($data,'createTime');
                    array_multisort($last_names,SORT_DESC,$data);
                    CI_Redis::get_instance()->redis->setex('shopDefaultData',3600,json_encode($data));
                }
                //数据过滤检索如果检索数据存在where键
                if(!empty($post["where"])){
                    self::shopDataWhere($data,$post["where"]);
                }
                $index = ($post['page']-1)>0?($post['page']-1)*5:0;
                $result = array_slice($data,$index,5);
                if(!empty($result)){
                    unset($val);
                    foreach ($result as $key=>&$val){
                        //格式化数据
                        self::formatShopData($val);
                    }
                    self::outMessage("200","请求成功！",$result);
                }else{
                    self::outMessage("404","没有数据！",$result);
                }
            }else{
                self::outMessage("404","参数错误！",array());
            }
        }else{
            self::outMessage("404","参数空！",array());
        }
    }
    //格式化信息数据
    private function formatShopData(&$val = array()){
        $val['createTime'] = date("Y-m-d",$val['createTime']);
        $val['startTime'] = date("Y-m-d",$val['startTime']);
        $val['endTime'] = date("Y-m-d",$val['endTime']);
        //发票
        switch($val['invoice']){
            case "1":
                $val['invoice'] = "无发票";
                break;
            case "2":
                $val['invoice'] = "增值税发票";
                break;
            case "3":
                $val['invoice'] = "普通发票";
                break;
            default:
                $val['invoice'] = "无发票";
        }
        //配送方式
        switch($val['distributionMode']){
            case "1":
                $val['distributionMode'] = "送到买家指定地点";
                break;
            case "2":
                $val['distributionMode'] = "上门自提";
                break;
            default:
                $val['distributionMode'] = "上门自提";
        }
        //公司
        $val['comName'] = "沈阳lejaer科技有限公司";
        //是否有图片
        if(empty($val['productPhoto'])){
            $val['productPhoto'] = ROOT_INDEX_REQUEST."/resource/indexData/indexDefault/error.png";
        }
    }
    //供求列表过滤
    private function shopDataWhere(&$result,$where){
        foreach ($result as $key => $val) {
            //类型检索
            if (!empty($where['itemType']) && $where['itemType'] != $val['itemType']) {
                unset($result[$key]);
                continue;
            }
            //牌号检索
            if (!empty($where['brandName']) && ($where['brandName'] != $val['brandName'] || empty($val['brandName']))) {
                unset($result[$key]);
                continue;
            }
            //粒度检索
            if (!empty($where['granularity']) && ($where['granularity'] != $val['granularity'] || empty($val['granularity']))) {
                unset($result[$key]);
                continue;
            }
            //属性检索
            if (!empty($where['attrId']) && !preg_match("/".$where['attrId']."/i", $val['attrAll'])) {
                unset($result[$key]);
                continue;
            }
            //形态检索
            if (!empty($where['styleId']) && $where['styleId']!==$val['attrId']) {
                unset($result[$key]);
                continue;
            }
            //个人喜好===后期数据记录值
        }
    }
    //商城属性数据
    private function shopAttrData(){
        $this->load->model('Index_attr_model');
        //产品系
        $shopFirst = array();
        $shopSon = array();
        $shopAll = Index_attr_model::get_instance()->get_shop_attr();
        foreach ($shopAll as $key=>$value){
            if($value['pid']<1){
                $shopFirst[] = array(
                    "id"=>$value['Id'],
                    "name"=>$value['attrname']
                );
            }else{
                $shopSon[] = array(
                    "id"=>$value['Id'],
                    "pid"=>$value['pid'],
                    "name"=>$value['attrname']
                );
            }
        }
        //产品形态
        $shopStatus = array();
        $status = Index_attr_model::get_instance()->get_status_attr();
        foreach ($status as $key=>$value){
            $shopStatus[] = array(
                "id"=>$value['Id'],
                "name"=>$value['attrname']
            );
        }
        //是否先前有数据
        $result = array(
            "shopFirst"=>$shopFirst,
            "shopSon"=>$shopSon,
            "shopStatus"=>$shopStatus
        );
        return $result;
    }
    //牌号
    public function brandNameInfo(){
        $this->load->model('Attribute_model');
        $brandName = Attribute_model::get_instance()->get_brandname();
        $brandNameList = array();
        if(!empty($brandName)){
            unset($val);
            foreach ($brandName as $key=>&$val){
                $brandNameList[]=array(
                    'id'=>$val['Id'],
                    'name'=>$val['brandName'],
                    'attrId'=>$val['attrId']
                );
            }
        }
        return $brandNameList;
    }
    //粒度
    public function granularityInfo(){
        $this->load->model('Attribute_model');
        $granularity = Attribute_model::get_instance()->get_granularity();
        $granularityList = array();
        if(!empty($granularity)){
            unset($val);
            foreach ($granularity as $key=>&$val){
                $granularityList[]=array(
                    'id'=>$val['Id'],
                    'name'=>$val['granularity'],
                    'attrId'=>$val['attrId']
                );
            }
        }
        return $granularityList;
    }
    //上传临时图片位置
    public function preUpload(){
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)) {
            $post = json_decode($request_body, true);
            $this->load->library('CI_Picture');
            $url = CI_Picture::get_instance()->compressImg($post['data'],"/resource/uploads/shopPre/");
            self::outMessage("200","请求成功！",array("url"=>$url));
        }else{
            self::outMessage("404","发送请求失败！",array("error"=>"检测数据库连接！"));
        }
    }
    //改变文件路径从shopPre到shop
    public function reNamePicture(&$picture){
        $newPath = str_replace("shopPre","shop",$picture);
        if(rename(base_url(str_replace(ROOT_INDEX_REQUEST,"",$picture)),base_url(str_replace(ROOT_INDEX_REQUEST,"",$newPath)))){
            $picture = $newPath;
        }else{
            $picture = '';
        }
    }
    //物流质检企业列表
    public function companyList(){
        $get = $this->input->get();
        if(!empty($get['type'])){
            $this->load->model('Company_model');
            $data = array();
            switch ($get['type']){
                case '1':
                    $data = Company_model::get_instance()->get_cominfos(array("comType"=>1));
                    break;
                case '2':
                    $data = Company_model::get_instance()->get_cominfos(array("comType"=>2));
                    break;
                default:
                    $data = Company_model::get_instance()->get_cominfos(array("comType"=>1));
                    break;
            }
            unset($val);
            foreach ($data as $key=>&$val){
                unset($val['comLicense']);
                unset($val['comContent']);
                unset($val['comLogo']);
                unset($val['createTime']);
                unset($val['createId']);
            }
            $result = array(
                "data"=>json_encode($data),
                "title"=>intval($get['type'])>1?"物流公司":"质检公司"
            );
            $this->load->view('product/companyList', $result);
        }else{
            self::outMessage("404","数据请求失败！",array("error"=>"缺少必要的参数！"));
        }
    }
    /**
     * offer
     * @notice 商城首页中 - 报价
     * @return	void
     * @anthor Json.Wang
     */
    public function offer($params=0){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');

        if(!empty($request_body)){
            $Data = json_decode($request_body,true);
            $Data['endTime'] = strtotime($Data['endTime']);
            //谁创建的供应信息
            $Data["userId"] = $_SESSION["indexUserData"]["Id"];
            //创建时间
            $Data["createTime"] = time();
            //发布商品信息
            if($a=Purchase_model::get_instance()->insertPurchaseOrder($Data)){
                //清除内存中的数据释放内存
                self::outMessage("200","请求成功！",$a);
                exit();
            }else{
                self::outMessage("404","插入数据库失败，请检查网络！",0);
            }
        }else{
            $data = array();
            self::offerParams($data);
            $result = array(
                "typeList"=>json_encode($data['typeList']),
                "payList"=>json_encode($data['payList']),
                "sendList"=>json_encode($data['sendList']),
                "purchaseId"=>$params
            );
            $this->load->view('product/offer', $result);
        }
    }
    /**
     * shopCar
     * @notice 商城首页中 - 购物车页
     * @return	void
     * @anthor Json.Wang
     */
    public function shopCar(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)){
            $Data = json_decode($request_body,true);
            $params = array(
                "title"=>$Data['title'],
                "num"=>$Data['num'],
                "residualNum"=>$Data['residualNum'],
                "minOrderNum"=>$Data['minOrderNum'],
                "unitPrice"=>$Data['unitPrice'],
                "brandName"=>$Data['brandName'],
                "granularity"=>$Data['granularity'],
                "userId"=>$_SESSION["indexUserData"]["Id"],
                "suppleId"=>$Data['Id'],
                "createTime"=>time(),
                "unqidCode"=>$Data['unqidCode']
            );
            //检测购物车内是否有商品
            if($valid = Supple_model::get_instance()->shop_car_where(array("suppleId"=>$Data['Id'],"userId"=>$_SESSION["indexUserData"]["Id"]))){
                self::outMessage("200","Database already exists",0);
            }
            //发布商品信息
            if($a=Supple_model::get_instance()->insert_shop_car($params)){
                //清除内存中的数据释放内存
                self::outMessage("200","Successfully joined the shopping cart",$a);
            }else{
                self::outMessage("404","插入数据库失败，请检查网络！",0);
            }
        }else{
            $result = array();
            $shopCarData = Supple_model::get_instance()->shop_car_where(array("userId"=>$_SESSION["indexUserData"]["Id"]));
            $result = array(
                "shopCarData"=>json_encode($shopCarData)
            );
            $this->load->view('product/shopCar', $result);
        }
    }
    public function offerParams(&$data=array()){
       if(empty($data)){
           //含税
           $data['typeList'][] = array(
               "id"=>1,
               "name"=>'含税'
           );
           $data['typeList'][] = array(
               "id"=>2,
               "name"=>'含税包到'
           );
           $data['typeList'][] = array(
               "id"=>3,
               "name"=>'含税不包到'
           );
           $data['typeList'][] = array(
               "id"=>4,
               "name"=>'不含税'
           );
           $data['typeList'][] = array(
               "id"=>5,
               "name"=>'不含税包到'
           );
           $data['typeList'][] = array(
               "id"=>6,
               "name"=>'不含税不包到'
           );
           //运输方式
           $data['sendList'][] = array(
               "id"=>1,
               "name"=>'送到'
           );
           $data['sendList'][] = array(
               "id"=>2,
               "name"=>'自提'
           );
           //配送方式
           $data['payList'][] = array(
               "id"=>1,
               "name"=>'先款后货'
           );
           $data['payList'][] = array(
               "id"=>2,
               "name"=>'货到付款'
           );
           $data['payList'][] = array(
               "id"=>3,
               "name"=>'分批付款'
           );
       }
    }
    /**
     * delShopCarData
     * @notice 商城首页中 - 购物车页 - 删除购物车数据
     * @return	void
     * @anthor Json.Wang
     */
    public function delShopCarData(){
        $this->load->library('CI_Index');
        CI_Index::get_instance()->valid_to_login();
        $request_body = file_get_contents('php://input');
        if(!empty($request_body)) {
            $Data = json_decode($request_body, true);
            //发布商品信息
            if($a=Supple_model::get_instance()->del_shop_car_data($Data)){
                //清除内存中的数据释放内存
                self::outMessage("200","Item removal cart!",$a);
            }else{
                self::outMessage("404","插入数据库失败，请检查网络！",0);
            }
        }
    }
    /**
     * shopCarPay
     * @notice 商城首页中 - 购物车页 - 付款
     * @return	void
     * @anthor Json.Wang
     */
//    public function shopCarPay(){
//        $this->load->library('CI_Wechat');
//        $data = array(
//            "body"=>"youseyunshang",
//            "setOutTradeNo"=>"youseyunshang".date("YmdHis"),
//            "money"=>1,
//            "url"=>ROOT_INDEX_REQUEST."/IndexProduct/notifyUrl"
//        );
//        $outData = CI_Wechat::get_instance()->jsApiCode($data);
//        $result = array(
//            "jsApiParameters"=>$outData,
//            "money"=>$data['money']/100
//        );
//        self::outMessage("200","success!",$result);
//    }
    public function shopCarPay(){

        $url = ROOT_INDEX_REQUEST."/WeChatPay/example/jsapi.php";
        header("Location:$url");
    }
    /**
     * shopCarPay
     * @notice 商城首页中 - 购物车页 - 回调
     * @return	void
     * @anthor Json.Wang
     */
    public function notifyUrl(){
        $xmlData = file_get_contents('php://input');
        libxml_disable_entity_loader(true);
        $data = json_encode(simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA));
        CI_Redis::get_instance()->redis->setex('createProductFirst123',3600,$data);
        // 向文件追加写入内容
        // 使用 FILE_APPEND 标记，可以在文件末尾追加内容
        //  LOCK_EX 标记可以防止多人同时写入
        // file_put_contents($file, $data, FILE_APPEND | LOCK_EX);
    }
}
