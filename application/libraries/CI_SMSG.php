<?php
require("aliyun-dysms-php-sdk/api_demo/SmsDemo.php");
defined('BASEPATH') OR exit('No direct script access allowed');

class CI_SMSG {
    private static $instance;
    /**
     * 架构方法 设置参数
     * @access public
     * @param  array $config 配置参数
     */
    public function __construct(){
        self::$instance =& $this;
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //发短信
    public function sendCode($data=array()){
        $obj = new SmsDemo();
        return $obj->sendSms($data);
    }
    //随机四位数字
    public function randMum($len=4){
        $chars_array = array(
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
        );
        $charsLen = count($chars_array) - 1;

        $outputstr = "";
        for ($i=0; $i<$len; $i++)
        {
            $outputstr .= $chars_array[mt_rand(0, $charsLen)];
        }
        return $outputstr;
    }
}