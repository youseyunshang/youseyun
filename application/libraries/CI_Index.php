<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CI_Index{
    private static $instance;
    /**
     * 架构方法 设置参数
     * @access public
     * @param  array $config 配置参数
     */
    public function __construct(){
        self::$instance =& $this;
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //验证前台用户是否登录
    public function valid_index_login(){
        if(!empty($_SESSION['indexUserData'])){
            $url = ROOT_INDEX_REQUEST."/indexUsercenter/index";
            header("Location:$url");
        }
    }
    //用户不存在跳转登录页
    public function valid_to_login(){
        if(empty($_SESSION['indexUserData'])){
            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                self::outMessage("404","Please login first",array("valid"=>"noLogin"));
            }
            $request_body = file_get_contents('php://input');
            if(!empty($request_body)){
                self::outMessage("404","Please login first",array("valid"=>"noLogin"));
            }
            $url = ROOT_INDEX_REQUEST."/index/login";
            header("Location:$url");
        }
    }
    //生成用户头像图片
//    public function product_picture_usericon(&$array){
//        $imageName = "2019_".date("His",time())."_".rand(1111,9999).'.png';
//        $path = base_url("/resource/uploads/userlogo");
//        if (!is_dir($path)){ //判断目录是否存在 不存在就创建
//            mkdir($path,0777,true);
//        }
//        $imageSrc= $path."/". $imageName; //图片名字
//        if (strstr($array['avatar'],",")){
//            $image = explode(',',$array['avatar']);
//            $image = $image[1];
//        }
//        if (file_put_contents($imageSrc, base64_decode($image))) {
//            $array['avatar'] = '/resource/uploads/userlogo/'.$imageName;
//        }else{
//            $tmparr=array('data'=>null,"code"=>1,"msg"=>"图片生成失败");
//            echo json_encode($tmparr);
//            die();
//        }
//    }
    //生成用户头像图片
    public function product_picture_upload(&$array){
        $imageName = "2019_".date("His",time())."_".rand(1111,9999).'.png';
        $path = base_url($array['pathUrl']);
        if (!is_dir($path)){ //判断目录是否存在 不存在就创建
            mkdir($path,0777,true);
        }
        $imageSrc= $path."/". $imageName; //图片名字
        if (strstr($array['column'],",")){
            $image = explode(',',$array['column']);
            $image = $image[1];
        }
        if (file_put_contents($imageSrc, base64_decode($image))) {
            $array['column'] = $array['pathUrl'].'/'.$imageName;
        }else{
            $tmparr=array('data'=>null,"code"=>1,"msg"=>"图片生成失败");
            echo json_encode($tmparr);
            die();
        }
    }
    //输出消息
    public function outMessage($code="", $message="", $data=array() ){
        $response = array(
            "code"=>$code,
            "message"=>$message,
            "data"=>$data
        );
        echo json_encode($response);
        die();
    }
}