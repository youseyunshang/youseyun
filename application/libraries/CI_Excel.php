<?php
require("PHPExcel.php");
require("PHPExcel/IOFactory.php");
defined('BASEPATH') OR exit('No direct script access allowed');

class CI_Excel {
    private static $instance;
    /**
     * 架构方法 设置参数
     * @access public
     * @param  array $config 配置参数
     */
    public function __construct(){
        self::$instance =& $this;
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
    //CIexcel导出
    //导出excel
    public function excel_out($head = array(),$outData = array()){
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");
        //$head = array("用户名","真实姓名","电话","性别","身份证","微信","创建日期");
        $col = 0;
        foreach ($head as $field)
        {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }
//// Fetching the table data
        $row = 2;
        //$outData = array(array("用户名1","真实姓名1","电话1","性别1","身份证1","微信1","创建日期1"),array("用户名","真实姓名","电话","性别","身份证","微信","创建日期"),array("用户名","真实姓名","电话","性别","身份证","微信","创建日期"));
        foreach($outData as $data)
        {
            $col = 0;
            foreach ($data as $field)
            {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $field);
                $col++;
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Excel5');
//发送标题强制用户下载文件
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Products_'.date('dMy').'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
    //导入文件数据
    public function excel_in($fileName = "",$sheetPage=1){

        /*读取excel文件，并进行相应处理*/
        if (!file_exists($fileName)) {
            exit("文件" . $fileName . "不存在");
        }
        //$startTime = time(); //返回当前时间的Unix 时间戳
        $objPHPExcel = IOFactory::load($fileName);
        //获取sheet表格数目
        $sheetCount = $objPHPExcel->getSheetCount();
        //默认选中sheet0表
        $sheetSelected = $sheetPage;
        $objPHPExcel->setActiveSheetIndex($sheetSelected);
        //获取表格行数
        $rowCount = $objPHPExcel->getActiveSheet()->getHighestRow();
        //获取表格列数
        $columnCount = $objPHPExcel->getActiveSheet()->getHighestColumn();
        //echo "<div>Sheet Count : " . $sheetCount . "　　行数： " . $rowCount . "　　列数：" . $columnCount . "</div>";
        $allData = array();
        $dataArr = array();
        /* 循环读取每个单元格的数据 */
//行数循环
        for ($row = 1; $row <= $rowCount; $row++) {
//列数循环 , 列数是以A列开始
            for ($column = 'A'; $column <= $columnCount; $column++) {
                $dataArr[] = $objPHPExcel->getActiveSheet()->getCell($column . $row)->getValue();
//                echo $column . $row . ":" . $objPHPExcel->getActiveSheet()->getCell($column . $row)->getValue() . "<br />";
            }
          //  echo "<br/>消耗的内存为：" . (memory_get_peak_usage(true) / 1024 / 1024) . "M";
          //  $endTime = time();

//            echo "<div>解析完后，当前的时间为：" . date("Y-m-d H:i:s") . "　　　
//
//总共消耗的时间为：" . (($endTime - $startTime)) . "秒</div>";
            $allData[] = unserialize(serialize($dataArr));
            $dataArr = NULL;
        }
        return $allData;
    }
}