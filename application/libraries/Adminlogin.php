<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin {
    private $CI = NULL;

    /**
     * 架构方法 设置参数
     * @access public
     * @param  array $config 配置参数
     */
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->library('session');
        //$this->CI->load->library('url_helper');
    }

    /**
     * 使用 $this->name 获取配置
     * @access public
     * @param  string $name 配置名称
     * @return multitype    配置值
     */
    public function validlogin() {
        if(!isset($_SESSION["Id"])){
            $data = array();
            $this->CI->load->view('login/login', $data);
            die();
        }
    }

    /**
     * 设置验证码配置
     * @access public
     * @param  string $name 配置名称
     * @param  string $value 配置值
     * @return void
     */
    public function loginout(){
        if(isset($this->config[$name])) {
            $this->config[$name]    =   $value;
        }
    }

}