<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CI_Redis {
    private static $instance;
    public $redis = null;
    /**
     * 架构方法 设置参数
     * @access public
     * @param  array $config 配置参数
     */
    public function __construct(){
        $this->redis = new Redis();
        $this->redis->connect('127.0.0.1', 6379);//serverip port
        $this->redis->auth('');//my redis password
//        $redis ->set( "test" , "Hello World");
//        echo $redis ->get( "test");
        self::$instance =& $this;
    }
    /**
     * Get the CI singleton
     *
     * @static
     * @return	object
     */
    public static function &get_instance()
    {
        return self::$instance;
    }
}